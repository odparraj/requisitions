<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AdministrativeArea extends Model
{
    use SoftDeletes;
    
    protected $table = 'administrative_areas';
    protected $fillable= ['nombre'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    protected $casts = [];


    public function users()
    {
        return $this->morphToMany('App\User', 'userable');
    }


    public function areasOperativas()
    {
    	return $this->hasMany('App\OperativeArea','administrative_area_id');
    }

    public function requisitions()
    {
    	return $this->hasMany('App\SupplierOrder', 'gerencia_que_autoriza');
    }
}