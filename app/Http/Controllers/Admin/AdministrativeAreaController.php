<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\AdministrativeArea, App\User;

class AdministrativeAreaController extends Controller
{

	/**
     * Instantiate a new controller instance.
     *
     * @return  void
     */
    public function __construct()
    {
        $this->middleware('permission:administrative_areas_create')->only(['store','create']);
        $this->middleware('permission:administrative_areas_read')->only(['index','show']);
        $this->middleware('permission:administrative_areas_update')->only(['update','edit']);
        $this->middleware('permission:administrative_areas_delete')->only(['delete']);
    }

    /**
     * Display a listing of the resource in json format.
     *
     * @return  \Illuminate\Http\Response
     */
    public function toDatatable(Request $request)
    {
        $user= $request->user();
        $permissions=[
            'administrative_areas_read'=> $user->hasPermissionTo('administrative_areas_read'),
            'administrative_areas_update'=> $user->hasPermissionTo('administrative_areas_update'),
            'administrative_areas_delete'=> $user->hasPermissionTo('administrative_areas_delete')
        ];
        $administrative_areas = AdministrativeArea::all()->map(function($item) use($permissions){
            $items= [                
                [   
                    'permission'=>'administrative_areas_read',
                    'name'=>'Show',
                    'action'=>route('administrative_areas.show',['id'=>$item->id]),
                    'icon'=>'fa fa-eye',
                    'target'=>'_self',
                ],
                [
                    'permission'=>'administrative_areas_update',
                    'name'=>'Edit',
                    'action'=>route('administrative_areas.edit',['id'=>$item->id]),
                    'icon'=>'fa fa-edit',
                    'target'=>'_self',
                ],
                [
                    'permission'=>'administrative_areas_delete',
                    'name'=>'Delete', 
                    'data_id'=>$item->id,
                    'class_modal'=>'delete-modal',
                    'icon'=>'fa fa-trash-o',
                ],
            ];

            return[
                
                $item->id,
                $item->nombre,
                
                                view('settings.permissions', [
                    'items'=>$items,
                    'permissions'=>$permissions
                ])->render()
            ];
        })->all();
        return response()->json(['data'=> $administrative_areas ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function index()
    {
        
        $columns = "['Id','Nombre','Actions']";
        $link = 'administrative_areas.dt';
        return view('admin.administrative_areas.index', compact(['columns','link']));
        
    	//$administrative_areas = AdministrativeArea::all();
        //return view('admin.administrative_areas.index', compact(['administrative_areas']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function create()
    {
        $users= User::role('gerente')->get()->mapWithKeys(function($item){
            return [$item->id => $item->name];
        })->toJson();

        return view('admin.administrative_areas.create', compact(['users']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @return  \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([

	        'nombre'=> 'required|string|max:191',
            'users'=> 'required|array',
            'users.*'=> 'required|exists:users,id'
            
	    ]);

	    $administrative_area = AdministrativeArea::create($request->all());

        $administrative_area->users()->sync($request->users);

        $alert=[];
        $alert['status']= 'success';
        $alert['message']= trans('message.successfully_created');
		
		return redirect()->route('administrative_areas.index')->with('alert', $alert);
    }

    /**
     * Display the specified resource.
     *
     * @param    \App\AdministrativeArea  $administrative_area
     * @return  \Illuminate\Http\Response
     */
    public function show(Request $request, AdministrativeArea $administrative_area)
    {
        $users= User::role('gerente')->get()->mapWithKeys(function($item){
            return [$item->id => $item->name];
        })->toJson();

        if (! $request->old()) {
            $input_old= $administrative_area->toArray();
            $input_old['users']= $administrative_area->users()->pluck('id')->toArray();
            $request->replace($input_old);
            $request->flash();
        }

        return view('admin.administrative_areas.show', compact(['administrative_area','users']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param    \App\AdministrativeArea  $administrative_area
     * @return  \Illuminate\Http\Response
     */
    public function edit(Request $request, AdministrativeArea $administrative_area)
    {
        $users= User::role('gerente')->get()->mapWithKeys(function($item){
            return [$item->id => $item->name];
        })->toJson();

    	if (! $request->old()) {
            $input_old= $administrative_area->toArray();
            $input_old['users']= $administrative_area->users()->pluck('id')->toArray();
            $request->replace($input_old);
            $request->flash();
        }

    	return view('admin.administrative_areas.edit', compact(['administrative_area','users']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @param    \App\AdministrativeArea  $administrative_area
     * @return  \Illuminate\Http\Response
     */
    public function update(Request $request, AdministrativeArea $administrative_area)
    {
        $validatedData = $request->validate([

	        'nombre'=> 'required|string|max:191',            
            'users'=> 'required|array',
            'users.*'=> 'required|exists:users,id'
            
	    ]);

    	$administrative_area->fill($request->all())->save();

        $administrative_area->users()->sync($request->users);

    	$alert=[];
        $alert['status']= 'success';
        $alert['message']= trans('message.successfully_updated');
	            
        return redirect()->route('administrative_areas.index')->with('alert', $alert);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param    \App\AdministrativeArea  $administrative_area
     * @return  \Illuminate\Http\Response
     */
    public function destroy(AdministrativeArea $administrative_area)
    {
        $administrative_area->delete();

        $alert=[];
        $alert['status']= 'success';
        $alert['message']= trans('message.successfully_deleted');

        return redirect()->route('administrative_areas.index')->with('alert', $alert);
    }
}