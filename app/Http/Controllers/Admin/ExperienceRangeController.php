<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ExperienceRange;

class ExperienceRangeController extends Controller
{

	/**
     * Instantiate a new controller instance.
     *
     * @return  void
     */
    public function __construct()
    {
        $this->middleware('permission:experience_ranges_create')->only(['store','create']);
        $this->middleware('permission:experience_ranges_read')->only(['index','show']);
        $this->middleware('permission:experience_ranges_update')->only(['update','edit']);
        $this->middleware('permission:experience_ranges_delete')->only(['delete']);
    }

    /**
     * Display a listing of the resource in json format.
     *
     * @return  \Illuminate\Http\Response
     */
    public function toDatatable(Request $request)
    {
        $user= $request->user();
        $permissions=[
            'experience_ranges_read'=> $user->hasPermissionTo('experience_ranges_read'),
            'experience_ranges_update'=> $user->hasPermissionTo('experience_ranges_update'),
            'experience_ranges_delete'=> $user->hasPermissionTo('experience_ranges_delete')
        ];
        $experience_ranges = ExperienceRange::all()->map(function($item) use($permissions){
            $items= [                
                [   
                    'permission'=>'experience_ranges_read',
                    'name'=>'Show',
                    'action'=>route('experience_ranges.show',['id'=>$item->id]),
                    'icon'=>'fa fa-eye',
                    'target'=>'_self',
                ],
                [
                    'permission'=>'experience_ranges_update',
                    'name'=>'Edit',
                    'action'=>route('experience_ranges.edit',['id'=>$item->id]),
                    'icon'=>'fa fa-edit',
                    'target'=>'_self',
                ],
                [
                    'permission'=>'experience_ranges_delete',
                    'name'=>'Delete', 
                    'data_id'=>$item->id,
                    'class_modal'=>'delete-modal',
                    'icon'=>'fa fa-trash-o',
                ],
            ];

            return[
                
                $item->id,
                $item->nombre,
                
                                view('settings.permissions', [
                    'items'=>$items,
                    'permissions'=>$permissions
                ])->render()
            ];
        })->all();
        return response()->json(['data'=> $experience_ranges ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function index()
    {
        
        $columns = "['Id','Nombre','Actions']";
        $link = 'experience_ranges.dt';
        return view('admin.experience_ranges.index', compact(['columns','link']));
        
    	//$experience_ranges = ExperienceRange::all();
        //return view('admin.experience_ranges.index', compact(['experience_ranges']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.experience_ranges.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @return  \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([

	        'nombre'=> 'required|string|max:191',
            
	    ]);

	    $experience_range = ExperienceRange::create($request->all());

        $alert=[];
        $alert['status']= 'success';
        $alert['message']= trans('message.successfully_created');
		
		return redirect()->route('experience_ranges.index')->with('alert', $alert);
    }

    /**
     * Display the specified resource.
     *
     * @param    \App\ExperienceRange  $experience_range
     * @return  \Illuminate\Http\Response
     */
    public function show(Request $request, ExperienceRange $experience_range)
    {
        if (! $request->old()) {
            $request->replace($experience_range->toArray());        
            $request->flash();
        }

        return view('admin.experience_ranges.show', compact(['experience_range']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param    \App\ExperienceRange  $experience_range
     * @return  \Illuminate\Http\Response
     */
    public function edit(Request $request, ExperienceRange $experience_range)
    {
    	if (! $request->old()) {
            $request->replace($experience_range->toArray());        
            $request->flash();
        }

    	return view('admin.experience_ranges.edit', compact(['experience_range']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @param    \App\ExperienceRange  $experience_range
     * @return  \Illuminate\Http\Response
     */
    public function update(Request $request, ExperienceRange $experience_range)
    {
        $validatedData = $request->validate([

	        'nombre'=> 'required|string|max:191',
            
	    ]);

    	$experience_range->fill($request->all())->save();

    	$alert=[];
        $alert['status']= 'success';
        $alert['message']= trans('message.successfully_updated');;
	            
        return redirect()->route('experience_ranges.index')->with('alert', $alert);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param    \App\ExperienceRange  $experience_range
     * @return  \Illuminate\Http\Response
     */
    public function destroy(ExperienceRange $experience_range)
    {
        $experience_range->delete();

        $alert=[];
        $alert['status']= 'success';
        $alert['message']= trans('message.successfully_deleted');

        return redirect()->route('experience_ranges.index')->with('alert', $alert);
    }
}