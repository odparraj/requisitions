<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\MeasurementUnit;

class MeasurementUnitController extends Controller
{

	/**
     * Instantiate a new controller instance.
     *
     * @return  void
     */
    public function __construct()
    {
        $this->middleware('permission:measurement_units_create')->only(['store','create']);
        $this->middleware('permission:measurement_units_read')->only(['index','show']);
        $this->middleware('permission:measurement_units_update')->only(['update','edit']);
        $this->middleware('permission:measurement_units_delete')->only(['delete']);
    }

    /**
     * Display a listing of the resource in json format.
     *
     * @return  \Illuminate\Http\Response
     */
    public function toDatatable(Request $request)
    {
        $user= $request->user();
        $permissions=[
            'measurement_units_read'=> $user->hasPermissionTo('measurement_units_read'),
            'measurement_units_update'=> $user->hasPermissionTo('measurement_units_update'),
            'measurement_units_delete'=> $user->hasPermissionTo('measurement_units_delete')
        ];
        $measurement_units = MeasurementUnit::all()->map(function($item) use($permissions){
            $items= [                
                [   
                    'permission'=>'measurement_units_read',
                    'name'=>'Show',
                    'action'=>route('measurement_units.show',['id'=>$item->id]),
                    'icon'=>'fa fa-eye',
                    'target'=>'_self',
                ],
                [
                    'permission'=>'measurement_units_update',
                    'name'=>'Edit',
                    'action'=>route('measurement_units.edit',['id'=>$item->id]),
                    'icon'=>'fa fa-edit',
                    'target'=>'_self',
                ],
                [
                    'permission'=>'measurement_units_delete',
                    'name'=>'Delete', 
                    'data_id'=>$item->id,
                    'class_modal'=>'delete-modal',
                    'icon'=>'fa fa-trash-o',
                ],
            ];

            return[
                
                $item->id,
                $item->nombre,
                
                                view('settings.permissions', [
                    'items'=>$items,
                    'permissions'=>$permissions
                ])->render()
            ];
        })->all();
        return response()->json(['data'=> $measurement_units ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function index()
    {
        
        $columns = "['Id','Nombre','Actions']";
        $link = 'measurement_units.dt';
        return view('admin.measurement_units.index', compact(['columns','link']));
        
    	//$measurement_units = MeasurementUnit::all();
        //return view('admin.measurement_units.index', compact(['measurement_units']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.measurement_units.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @return  \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([

	        'nombre'=> 'required|string|max:191|unique:measurement_units,nombre',
            
	    ]);

	    $measurement_unit = MeasurementUnit::create($request->all());

        $alert=[];
        $alert['status']= 'success';
        $alert['message']= trans('message.successfully_created');
		
		return redirect()->route('measurement_units.index')->with('alert', $alert);
    }

    /**
     * Display the specified resource.
     *
     * @param    \App\MeasurementUnit  $measurement_unit
     * @return  \Illuminate\Http\Response
     */
    public function show(Request $request, MeasurementUnit $measurement_unit)
    {
        if (! $request->old()) {
            $request->replace($measurement_unit->toArray());        
            $request->flash();
        }

        return view('admin.measurement_units.show', compact(['measurement_unit']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param    \App\MeasurementUnit  $measurement_unit
     * @return  \Illuminate\Http\Response
     */
    public function edit(Request $request, MeasurementUnit $measurement_unit)
    {
    	if (! $request->old()) {
            $request->replace($measurement_unit->toArray());        
            $request->flash();
        }

    	return view('admin.measurement_units.edit', compact(['measurement_unit']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @param    \App\MeasurementUnit  $measurement_unit
     * @return  \Illuminate\Http\Response
     */
    public function update(Request $request, MeasurementUnit $measurement_unit)
    {
        $validatedData = $request->validate([

	        'nombre'=> 'required|string|max:191|unique:measurement_units,nombre,'.$measurement_unit->id,
            
	    ]);

    	$measurement_unit->fill($request->all())->save();

    	$alert=[];
        $alert['status']= 'success';
        $alert['message']= trans('message.successfully_updated');;
	            
        return redirect()->route('measurement_units.index')->with('alert', $alert);

                
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param    \App\MeasurementUnit  $measurement_unit
     * @return  \Illuminate\Http\Response
     */
    public function destroy(MeasurementUnit $measurement_unit)
    {
        $measurement_unit->delete();

        $alert=[];
        $alert['status']= 'success';
        $alert['message']= trans('message.successfully_deleted');

        return redirect()->route('measurement_units.index')->with('alert', $alert);
    }
}