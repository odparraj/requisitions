<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\OperativeArea, App\AdministrativeArea, App\User;

class OperativeAreaController extends Controller
{

	/**
     * Instantiate a new controller instance.
     *
     * @return  void
     */
    public function __construct()
    {
        $this->middleware('permission:operative_areas_create')->only(['store','create']);
        $this->middleware('permission:operative_areas_read')->only(['index','show']);
        $this->middleware('permission:operative_areas_update')->only(['update','edit']);
        $this->middleware('permission:operative_areas_delete')->only(['delete']);
    }

    /**
     * Display a listing of the resource in json format.
     *
     * @return  \Illuminate\Http\Response
     */
    public function toDatatable(Request $request)
    {
        $user= $request->user();
        $permissions=[
            'operative_areas_read'=> $user->hasPermissionTo('operative_areas_read'),
            'operative_areas_update'=> $user->hasPermissionTo('operative_areas_update'),
            'operative_areas_delete'=> $user->hasPermissionTo('operative_areas_delete')
        ];
        $operative_areas = OperativeArea::all()->map(function($item) use($permissions){
            $items= [                
                [   
                    'permission'=>'operative_areas_read',
                    'name'=>'Show',
                    'action'=>route('operative_areas.show',['id'=>$item->id]),
                    'icon'=>'fa fa-eye',
                    'target'=>'_self',
                ],
                [
                    'permission'=>'operative_areas_update',
                    'name'=>'Edit',
                    'action'=>route('operative_areas.edit',['id'=>$item->id]),
                    'icon'=>'fa fa-edit',
                    'target'=>'_self',
                ],
                [
                    'permission'=>'operative_areas_delete',
                    'name'=>'Delete', 
                    'data_id'=>$item->id,
                    'class_modal'=>'delete-modal',
                    'icon'=>'fa fa-trash-o',
                ],
            ];

            return[
                
                $item->id,
                $item->nombre,
                $item->areaAdministrativa->nombre,
                
                view('settings.permissions', [
                    'items'=>$items,
                    'permissions'=>$permissions
                ])->render()
            ];
        })->all();
        return response()->json(['data'=> $operative_areas ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function index()
    {
        
        $columns = "['Id','Nombre', 'Gerencia', 'Actions']";
        $link = 'operative_areas.dt';
        return view('admin.operative_areas.index', compact(['columns','link']));
        
    	//$operative_areas = OperativeArea::all();
        //return view('admin.operative_areas.index', compact(['operative_areas']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function create()
    {
        $areas_administrativas= AdministrativeArea::all()->mapWithKeys(function($item){
            return [ $item->id=> $item->nombre ];
        })->toJson();
        $users= User::role('coordinador')->get()->mapWithKeys(function($item){
            return [$item->id => $item->name];
        })->toJson();
        return view('admin.operative_areas.create', compact(['areas_administrativas','users']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @return  \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([

	        'nombre'=> 'required|string|max:191',
            'administrative_area_id'=> 'required|exists:administrative_areas,id',            
            'users'=> 'required|array',
            'users.*'=> 'required|exists:users,id'
            
	    ]);

	    $operative_area = OperativeArea::create($request->all());

        $alert=[];
        $alert['status']= 'success';
        $alert['message']= trans('message.successfully_created');
		
		return redirect()->route('operative_areas.index')->with('alert', $alert);
    }

    /**
     * Display the specified resource.
     *
     * @param    \App\OperativeArea  $operative_area
     * @return  \Illuminate\Http\Response
     */
    public function show(Request $request, OperativeArea $operative_area)
    {
        $areas_administrativas= AdministrativeArea::all()->mapWithKeys(function($item){
            return [ $item->id=> $item->nombre ];
        })->toJson();
        $users= User::role('coordinador')->get()->mapWithKeys(function($item){
            return [$item->id => $item->name];
        })->toJson();
        if (! $request->old()) {
            $input_old= $operative_area->toArray();
            $input_old['users']= $operative_area->users()->pluck('id')->toArray();
            $request->replace($input_old);
            $request->flash();
        }

        return view('admin.operative_areas.show', compact(['operative_area','areas_administrativas','users']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param    \App\OperativeArea  $operative_area
     * @return  \Illuminate\Http\Response
     */
    public function edit(Request $request, OperativeArea $operative_area)
    {
        $areas_administrativas= AdministrativeArea::all()->mapWithKeys(function($item){
            return [ $item->id=> $item->nombre ];
        })->toJson();
        $users= User::role('coordinador')->get()->mapWithKeys(function($item){
            return [$item->id => $item->name];
        })->toJson();
    	if (! $request->old()) {
            $input_old= $operative_area->toArray();
            $input_old['users']= $operative_area->users()->pluck('id')->toArray();
            $request->replace($input_old);
            $request->flash();
        }

    	return view('admin.operative_areas.edit', compact(['operative_area','areas_administrativas','users']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @param    \App\OperativeArea  $operative_area
     * @return  \Illuminate\Http\Response
     */
    public function update(Request $request, OperativeArea $operative_area)
    {
        $validatedData = $request->validate([

	        'nombre'=> 'required|string|max:191',
            'administrative_area_id'=> 'required|exists:administrative_areas,id',            
            'users'=> 'required|array',
            'users.*'=> 'required|exists:users,id'
            
	    ]);

    	$operative_area->fill($request->all())->save();

    	$alert=[];
        $alert['status']= 'success';
        $alert['message']= trans('message.successfully_updated');;
	            
        return redirect()->route('operative_areas.index')->with('alert', $alert);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param    \App\OperativeArea  $operative_area
     * @return  \Illuminate\Http\Response
     */
    public function destroy(OperativeArea $operative_area)
    {
        $operative_area->delete();

        $alert=[];
        $alert['status']= 'success';
        $alert['message']= trans('message.successfully_deleted');

        return redirect()->route('operative_areas.index')->with('alert', $alert);
    }
}