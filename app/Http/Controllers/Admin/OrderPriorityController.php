<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\OrderPriority;

class OrderPriorityController extends Controller
{

	/**
     * Instantiate a new controller instance.
     *
     * @return  void
     */
    public function __construct()
    {
        $this->middleware('permission:order_priorities_create')->only(['store','create']);
        $this->middleware('permission:order_priorities_read')->only(['index','show']);
        $this->middleware('permission:order_priorities_update')->only(['update','edit']);
        $this->middleware('permission:order_priorities_delete')->only(['delete']);
    }

    /**
     * Display a listing of the resource in json format.
     *
     * @return  \Illuminate\Http\Response
     */
    public function toDatatable(Request $request)
    {
        $user= $request->user();
        $permissions=[
            'order_priorities_read'=> $user->hasPermissionTo('order_priorities_read'),
            'order_priorities_update'=> $user->hasPermissionTo('order_priorities_update'),
            'order_priorities_delete'=> $user->hasPermissionTo('order_priorities_delete')
        ];
        $order_priorities = OrderPriority::all()->map(function($item) use($permissions){
            $items= [                
                [   
                    'permission'=>'order_priorities_read',
                    'name'=>'Show',
                    'action'=>route('order_priorities.show',['id'=>$item->id]),
                    'icon'=>'fa fa-eye',
                    'target'=>'_self',
                ],
                [
                    'permission'=>'order_priorities_update',
                    'name'=>'Edit',
                    'action'=>route('order_priorities.edit',['id'=>$item->id]),
                    'icon'=>'fa fa-edit',
                    'target'=>'_self',
                ],
                [
                    'permission'=>'order_priorities_delete',
                    'name'=>'Delete', 
                    'data_id'=>$item->id,
                    'class_modal'=>'delete-modal',
                    'icon'=>'fa fa-trash-o',
                ],
            ];

            return[
                
                $item->id,
                $item->nombre,
                
                view('settings.permissions', [
                    'items'=>$items,
                    'permissions'=>$permissions
                ])->render()
            ];
        })->all();
        return response()->json(['data'=> $order_priorities ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function index()
    {
        
        $columns = "['Id','Nombre','Actions']";
        $link = 'order_priorities.dt';
        return view('admin.order_priorities.index', compact(['columns','link']));
        
    	//$order_priorities = OrderPriority::all();
        //return view('admin.order_priorities.index', compact(['order_priorities']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.order_priorities.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @return  \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([

	        'nombre'=> 'required|unique:order_priorities',
            
	    ]);

	    $order_priority = OrderPriority::create($request->all());

        $alert=[];
        $alert['status']= 'success';
        $alert['message']= trans('message.successfully_created');
		
		return redirect()->route('order_priorities.index')->with('alert', $alert);
    }

    /**
     * Display the specified resource.
     *
     * @param    \App\OrderPriority  $order_priority
     * @return  \Illuminate\Http\Response
     */
    public function show(Request $request, OrderPriority $order_priority)
    {
        if (! $request->old()) {
            $request->replace($order_priority->toArray());        
            $request->flash();
        }

        return view('admin.order_priorities.show', compact(['order_priority']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param    \App\OrderPriority  $order_priority
     * @return  \Illuminate\Http\Response
     */
    public function edit(Request $request, OrderPriority $order_priority)
    {
    	if (! $request->old()) {
            $request->replace($order_priority->toArray());        
            $request->flash();
        }

    	return view('admin.order_priorities.edit', compact(['order_priority']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @param    \App\OrderPriority  $order_priority
     * @return  \Illuminate\Http\Response
     */
    public function update(Request $request, OrderPriority $order_priority)
    {
        $validatedData = $request->validate([

	        'nombre'=> 'required|unique:order_priorities,nombre,'.$order_priority->id,
            
	    ]);

    	$order_priority->fill($request->all())->save();

    	$alert=[];
        $alert['status']= 'success';
        $alert['message']= trans('message.successfully_updated');;
	            
        return redirect()->route('order_priorities.index')->with('alert', $alert);

                
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param    \App\OrderPriority  $order_priority
     * @return  \Illuminate\Http\Response
     */
    public function destroy(OrderPriority $order_priority)
    {
        $order_priority->delete();

        $alert=[];
        $alert['status']= 'success';
        $alert['message']= trans('message.successfully_deleted');

        return redirect()->route('order_priorities.index')->with('alert', $alert);
    }
}