<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\PaymentMethod;

class PaymentMethodController extends Controller
{

	/**
     * Instantiate a new controller instance.
     *
     * @return  void
     */
    public function __construct()
    {
        $this->middleware('permission:payment_methods_create')->only(['store','create']);
        $this->middleware('permission:payment_methods_read')->only(['index','show']);
        $this->middleware('permission:payment_methods_update')->only(['update','edit']);
        $this->middleware('permission:payment_methods_delete')->only(['delete']);
    }

    /**
     * Display a listing of the resource in json format.
     *
     * @return  \Illuminate\Http\Response
     */
    public function toDatatable(Request $request)
    {
        $user= $request->user();
        $permissions=[
            'payment_methods_read'=> $user->hasPermissionTo('payment_methods_read'),
            'payment_methods_update'=> $user->hasPermissionTo('payment_methods_update'),
            'payment_methods_delete'=> $user->hasPermissionTo('payment_methods_delete')
        ];
        $payment_methods = PaymentMethod::all()->map(function($item) use($permissions){
            $items= [                
                [   
                    'permission'=>'payment_methods_read',
                    'name'=>'Show',
                    'action'=>route('payment_methods.show',['id'=>$item->id]),
                    'icon'=>'fa fa-eye',
                    'target'=>'_self',
                ],
                [
                    'permission'=>'payment_methods_update',
                    'name'=>'Edit',
                    'action'=>route('payment_methods.edit',['id'=>$item->id]),
                    'icon'=>'fa fa-edit',
                    'target'=>'_self',
                ],
                [
                    'permission'=>'payment_methods_delete',
                    'name'=>'Delete', 
                    'data_id'=>$item->id,
                    'class_modal'=>'delete-modal',
                    'icon'=>'fa fa-trash-o',
                ],
            ];

            return[
                
                $item->id,
                $item->nombre,
                
                                view('settings.permissions', [
                    'items'=>$items,
                    'permissions'=>$permissions
                ])->render()
            ];
        })->all();
        return response()->json(['data'=> $payment_methods ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function index()
    {
        
        $columns = "['Id','Nombre','Actions']";
        $link = 'payment_methods.dt';
        return view('admin.payment_methods.index', compact(['columns','link']));
        
    	//$payment_methods = PaymentMethod::all();
        //return view('admin.payment_methods.index', compact(['payment_methods']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.payment_methods.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @return  \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([

	        'nombre'=> 'required|string|max:191',
            
	    ]);

	    $payment_method = PaymentMethod::create($request->all());

        $alert=[];
        $alert['status']= 'success';
        $alert['message']= trans('message.successfully_created');
		
		return redirect()->route('payment_methods.index')->with('alert', $alert);
    }

    /**
     * Display the specified resource.
     *
     * @param    \App\PaymentMethod  $payment_method
     * @return  \Illuminate\Http\Response
     */
    public function show(Request $request, PaymentMethod $payment_method)
    {
        if (! $request->old()) {
            $request->replace($payment_method->toArray());        
            $request->flash();
        }

        return view('admin.payment_methods.show', compact(['payment_method']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param    \App\PaymentMethod  $payment_method
     * @return  \Illuminate\Http\Response
     */
    public function edit(Request $request, PaymentMethod $payment_method)
    {
    	if (! $request->old()) {
            $request->replace($payment_method->toArray());        
            $request->flash();
        }

    	return view('admin.payment_methods.edit', compact(['payment_method']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @param    \App\PaymentMethod  $payment_method
     * @return  \Illuminate\Http\Response
     */
    public function update(Request $request, PaymentMethod $payment_method)
    {
        $validatedData = $request->validate([

	        'nombre'=> 'required|string|max:191',
            
	    ]);

    	$payment_method->fill($request->all())->save();

    	$alert=[];
        $alert['status']= 'success';
        $alert['message']= trans('message.successfully_updated');;
	            
        return redirect()->route('payment_methods.index')->with('alert', $alert);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param    \App\PaymentMethod  $payment_method
     * @return  \Illuminate\Http\Response
     */
    public function destroy(PaymentMethod $payment_method)
    {
        $payment_method->delete();

        $alert=[];
        $alert['status']= 'success';
        $alert['message']= trans('message.successfully_deleted');

        return redirect()->route('payment_methods.index')->with('alert', $alert);
    }
}