<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\PurchaseOrder;

class PurchaseOrderController extends Controller
{

	/**
     * Instantiate a new controller instance.
     *
     * @return  void
     */
    public function __construct()
    {
        /* $this->middleware('permission:purchase_orders_create')->only(['store','create']);
        $this->middleware('permission:purchase_orders_read')->only(['index','show']);
        $this->middleware('permission:purchase_orders_update')->only(['update','edit']);
        $this->middleware('permission:purchase_orders_delete')->only(['delete']); */
    }

    /**
     * Display a listing of the resource in json format.
     *
     * @return  \Illuminate\Http\Response
     */
    public function toDatatable(Request $request)
    {
        $user= $request->user();
        $permissions=[
            'purchase_orders_read'=> true/* $user->hasPermissionTo('purchase_orders_read') */,
            'purchase_orders_update'=> true/* $user->hasPermissionTo('purchase_orders_update') */,
            'purchase_orders_delete'=> true/* $user->hasPermissionTo('purchase_orders_delete' )*/
        ];
        $purchase_orders = PurchaseOrder::all()->map(function($item) use($permissions){
            $items= [                
                [   
                    'permission'=>'purchase_orders_read',
                    'name'=>'Show',
                    'action'=>route('purchase_orders.show',['id'=>$item->id]),
                    'icon'=>'fa fa-eye',
                    'target'=>'_self',
                ],
                [
                    'permission'=>'purchase_orders_update',
                    'name'=>'Edit',
                    'action'=>route('purchase_orders.edit',['id'=>$item->id]),
                    'icon'=>'fa fa-edit',
                    'target'=>'_self',
                ],
                [
                    'permission'=>'purchase_orders_delete',
                    'name'=>'Delete', 
                    'data_id'=>$item->id,
                    'class_modal'=>'delete-modal',
                    'icon'=>'fa fa-trash-o',
                ],
            ];

            return[
                
                $item->id,
                $item->cotizacion,
                
                $item->fecha_orden,
                
                $item->fecha_pedido,
                
                $item->fecha_entrega,
                
                                view('settings.permissions', [
                    'items'=>$items,
                    'permissions'=>$permissions
                ])->render()
            ];
        })->all();
        return response()->json(['data'=> $purchase_orders ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function index()
    {
        
        $columns = "['Id','Cotizacion', 'Fecha Orden', 'Fecha Pedido', 'Fecha Entrega','Actions']";
        $link = 'purchase_orders.dt';
        return view('admin.purchase_orders.index', compact(['columns','link']));
        
    	//$purchase_orders = PurchaseOrder::all();
        //return view('admin.purchase_orders.index', compact(['purchase_orders']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.purchase_orders.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @return  \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([

	        'cotizacion'=> 'required|exists:quotations,id',
            'fecha_orden'=> 'required|date_format:"Y-m-d"',
            'fecha_pedido'=> 'required|date_format:"Y-m-d"',
            'fecha_entrega'=> 'required|date_format:"Y-m-d"',
            'observaciones'=> 'nullable|string',
            
	    ]);

	    $purchase_order = PurchaseOrder::create($request->all());

        $alert=[];
        $alert['status']= 'success';
        $alert['message']= trans('message.successfully_created');
		
		return redirect()->route('purchase_orders.index')->with('alert', $alert);
    }

    /**
     * Display the specified resource.
     *
     * @param    \App\PurchaseOrder  $purchase_order
     * @return  \Illuminate\Http\Response
     */
    public function show(Request $request, PurchaseOrder $purchase_order)
    {
        if (! $request->old()) {
            $request->replace($purchase_order->toArray());        
            $request->flash();
        }

        return view('admin.purchase_orders.show', compact(['purchase_order']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param    \App\PurchaseOrder  $purchase_order
     * @return  \Illuminate\Http\Response
     */
    public function edit(Request $request, PurchaseOrder $purchase_order)
    {
    	if (! $request->old()) {
            $request->replace($purchase_order->toArray());        
            $request->flash();
        }

    	return view('admin.purchase_orders.edit', compact(['purchase_order']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @param    \App\PurchaseOrder  $purchase_order
     * @return  \Illuminate\Http\Response
     */
    public function update(Request $request, PurchaseOrder $purchase_order)
    {
        $validatedData = $request->validate([

	        /* 'cotizacion'=> 'required|exists:quotations,id', */
            'fecha_orden'=> 'required|date_format:"Y-m-d"',
            'fecha_entrega'=> 'required|date_format:"Y-m-d"',
            'observaciones'=> 'nullable|string',
            
	    ]);

    	$purchase_order->fill($request->all())->save();

    	$alert=[];
        $alert['status']= 'success';
        $alert['message']= trans('message.successfully_updated');
	            
        return redirect()->route('supplier_orders.orders_index',['id'=>$purchase_order->quotation->supplierOrder->id])->with('alert', $alert);

                
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param    \App\PurchaseOrder  $purchase_order
     * @return  \Illuminate\Http\Response
     */
    public function destroy(PurchaseOrder $purchase_order)
    {
        $purchase_order->delete();

        $alert=[];
        $alert['status']= 'success';
        $alert['message']= trans('message.successfully_deleted');

        return redirect()->route('purchase_orders.index')->with('alert', $alert);
    }
}