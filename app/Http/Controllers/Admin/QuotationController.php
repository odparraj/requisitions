<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Quotation;

class QuotationController extends Controller
{

	/**
     * Instantiate a new controller instance.
     *
     * @return  void
     */
    public function __construct()
    {
        $this->middleware('permission:quotations_create')->only(['store','create']);
        $this->middleware('permission:quotations_read')->only(['index','show']);
        $this->middleware('permission:quotations_update')->only(['update','edit']);
        $this->middleware('permission:quotations_delete')->only(['delete']);
    }

    /**
     * Display a listing of the resource in json format.
     *
     * @return  \Illuminate\Http\Response
     */
    public function toDatatable(Request $request)
    {
        $user= $request->user();
        $permissions=[
            'quotations_read'=> $user->hasPermissionTo('quotations_read'),
            'quotations_update'=> $user->hasPermissionTo('quotations_update'),
            'quotations_delete'=> $user->hasPermissionTo('quotations_delete')
        ];
        $quotations = Quotation::all()->map(function($item) use($permissions){
            $items= [                
                [   
                    'permission'=>'quotations_read',
                    'name'=>'Show',
                    'action'=>route('quotations.show',['id'=>$item->id]),
                    'icon'=>'fa fa-eye',
                    'target'=>'_self',
                ],
                [
                    'permission'=>'quotations_update',
                    'name'=>'Edit',
                    'action'=>route('quotations.edit',['id'=>$item->id]),
                    'icon'=>'fa fa-edit',
                    'target'=>'_self',
                ],
                [
                    'permission'=>'quotations_delete',
                    'name'=>'Delete', 
                    'data_id'=>$item->id,
                    'class_modal'=>'delete-modal',
                    'icon'=>'fa fa-trash-o',
                ],
            ];

            return[
                
                $item->id,
                $item->orden,
                
                $item->proveedor,
                
                $item->contenido,
                
                                view('settings.permissions', [
                    'items'=>$items,
                    'permissions'=>$permissions
                ])->render()
            ];
        })->all();
        return response()->json(['data'=> $quotations ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function index()
    {
        
        $columns = "['Id','Orden', 'Proveedor', 'Contenido','Actions']";
        $link = 'quotations.dt';
        return view('admin.quotations.index', compact(['columns','link']));
        
    	//$quotations = Quotation::all();
        //return view('admin.quotations.index', compact(['quotations']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.quotations.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @return  \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([

	        'orden'=> 'required|exists:supplier_orders,id',
            'proveedor'=> 'required|exists:suppliers,id',
            'contenido'=> 'required',
            
	    ]);

	    $quotation = Quotation::create($request->all());

        $alert=[];
        $alert['status']= 'success';
        $alert['message']= trans('message.successfully_created');
		
		return redirect()->route('quotations.index')->with('alert', $alert);
    }

    /**
     * Display the specified resource.
     *
     * @param    \App\Quotation  $quotation
     * @return  \Illuminate\Http\Response
     */
    public function show(Request $request, Quotation $quotation)
    {
        if (! $request->old()) {
            $request->replace($quotation->toArray());        
            $request->flash();
        }

        return view('admin.quotations.show', compact(['quotation']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param    \App\Quotation  $quotation
     * @return  \Illuminate\Http\Response
     */
    public function edit(Request $request, Quotation $quotation)
    {
    	if (! $request->old()) {
            $request->replace($quotation->toArray());        
            $request->flash();
        }

    	return view('admin.quotations.edit', compact(['quotation']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @param    \App\Quotation  $quotation
     * @return  \Illuminate\Http\Response
     */
    public function update(Request $request, Quotation $quotation)
    {
        $validatedData = $request->validate([

	        'orden'=> 'required|exists:supplier_orders,id',
            'proveedor'=> 'required|exists:suppliers,id',
            'contenido'=> 'required',
            
	    ]);

    	$quotation->fill($request->all())->save();

    	$alert=[];
        $alert['status']= 'success';
        $alert['message']= trans('message.successfully_updated');;
	            
        return redirect()->route('quotations.index')->with('alert', $alert);

                
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param    \App\Quotation  $quotation
     * @return  \Illuminate\Http\Response
     */
    public function destroy(Quotation $quotation)
    {
        $quotation->delete();

        $alert=[];
        $alert['status']= 'success';
        $alert['message']= trans('message.successfully_deleted');

        return redirect()->route('quotations.index')->with('alert', $alert);
    }
}