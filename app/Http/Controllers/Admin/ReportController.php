<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\SupplierOrder;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource in json format.
     *
     * @return  \Illuminate\Http\Response
     */
    public function toDatatable(Request $request)
    {
        //$user= $request->user();
        $respuestas= [];
        $suplierOrders = SupplierOrder::with('purchaseOrders.quotation.supplier', 'purchaseOrders.respuestas')->get();
        
        foreach ($suplierOrders as $suplierOrder) {
            $purchaseOrders = $suplierOrder->purchaseOrders;

            if (count($purchaseOrders) == 0 ) {
                $respuestas[] = [                
                    $suplierOrder->id,
                    $suplierOrder->created_at->format('Y-m-d'),
                    $suplierOrder->fecha_de_autorizacion,
                    $suplierOrder->areaOperativa->nombre,
                    $suplierOrder->itemsToString(),
                    $suplierOrder->estado,
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    ''
                ];
            } else {
                foreach ($purchaseOrders as $purchaseOrder) {
                    $respuestas[] = [                
                        $suplierOrder->id,
                        $suplierOrder->created_at->format('Y-m-d'),
                        $suplierOrder->fecha_de_autorizacion,
                        $suplierOrder->areaOperativa->nombre,
                        $suplierOrder->itemsToString(),
                        $suplierOrder->estado,
                        $purchaseOrder->id,
                        $purchaseOrder->itemsToString(),                        
                        array_get($purchaseOrder, 'fecha_orden', 'NA'),
                        $purchaseOrder->calcularTotal(),                        
                        array_get($purchaseOrder->quotation->supplier, 'informacion_general.razon_social', 'NA'),
                        $purchaseOrder->fecha_entrega,
                        $purchaseOrder->gestionDeCompras(),
                        $purchaseOrder->calidad(),
                        ''
                    ]; 
                }
            }
        }
        return response()->json(['data'=> $respuestas ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function index()
    {        
        $columns = "['Requisición No.', 'Fecha Requisición interna', 'Fecha de Autorización', 'Área que solicita', 'Producto y/o Servicio', 'Estado', 'No. Orden de compra', 'Productos Asignados a Orden', 'Fecha Orden de Compra', 'Valor Orden de Compra', 'Nombre del proveedor', 'Fecha Entrega', 'Resultado Encuesta - Desempeño Gestión de Compras', 'Resultado Encuesta - Calidad del producto y/o servicio', '']";
        $link = 'reports.dt';
        return view('admin.reports.index', compact(['columns','link']));
    } 
}
