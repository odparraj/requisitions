<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\SatisfactionQuestion;

class SatisfactionQuestionController extends Controller
{

	/**
     * Instantiate a new controller instance.
     *
     * @return  void
     */
    public function __construct()
    {
        $this->middleware('permission:satisfaction_questions_create')->only(['store','create']);
        $this->middleware('permission:satisfaction_questions_read')->only(['index','show']);
        $this->middleware('permission:satisfaction_questions_update')->only(['update','edit']);
        $this->middleware('permission:satisfaction_questions_delete')->only(['delete']);
    }

    /**
     * Display a listing of the resource in json format.
     *
     * @return  \Illuminate\Http\Response
     */
    public function toDatatable(Request $request)
    {
        $user= $request->user();
        $permissions=[
            'satisfaction_questions_read'=> $user->hasPermissionTo('satisfaction_questions_read'),
            'satisfaction_questions_update'=> $user->hasPermissionTo('satisfaction_questions_update'),
            'satisfaction_questions_delete'=> $user->hasPermissionTo('satisfaction_questions_delete')
        ];
        $satisfaction_questions = SatisfactionQuestion::all()->map(function($item) use($permissions){
            $items= [                
                [   
                    'permission'=>'satisfaction_questions_read',
                    'name'=>'Show',
                    'action'=>route('satisfaction_questions.show',['id'=>$item->id]),
                    'icon'=>'fa fa-eye',
                    'target'=>'_self',
                ],
                [
                    'permission'=>'satisfaction_questions_update',
                    'name'=>'Edit',
                    'action'=>route('satisfaction_questions.edit',['id'=>$item->id]),
                    'icon'=>'fa fa-edit',
                    'target'=>'_self',
                ],
                [
                    'permission'=>'satisfaction_questions_delete',
                    'name'=>'Delete', 
                    'data_id'=>$item->id,
                    'class_modal'=>'delete-modal',
                    'icon'=>'fa fa-trash-o',
                ],
            ];

            return[
                
                $item->id,
                $item->enunciado,
                
                                view('settings.permissions', [
                    'items'=>$items,
                    'permissions'=>$permissions
                ])->render()
            ];
        })->all();
        return response()->json(['data'=> $satisfaction_questions ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function index()
    {
        
        $columns = "['Id','Enunciado','Actions']";
        $link = 'satisfaction_questions.dt';
        return view('admin.satisfaction_questions.index', compact(['columns','link']));
        
    	//$satisfaction_questions = SatisfactionQuestion::all();
        //return view('admin.satisfaction_questions.index', compact(['satisfaction_questions']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.satisfaction_questions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @return  \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([

	        'enunciado'=> 'required|string',
            
	    ]);

	    $satisfaction_question = SatisfactionQuestion::create($request->all());

        $alert=[];
        $alert['status']= 'success';
        $alert['message']= trans('message.successfully_created');
		
		return redirect()->route('satisfaction_questions.index')->with('alert', $alert);
    }

    /**
     * Display the specified resource.
     *
     * @param    \App\SatisfactionQuestion  $satisfaction_question
     * @return  \Illuminate\Http\Response
     */
    public function show(Request $request, SatisfactionQuestion $satisfaction_question)
    {
        if (! $request->old()) {
            $request->replace($satisfaction_question->toArray());        
            $request->flash();
        }

        return view('admin.satisfaction_questions.show', compact(['satisfaction_question']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param    \App\SatisfactionQuestion  $satisfaction_question
     * @return  \Illuminate\Http\Response
     */
    public function edit(Request $request, SatisfactionQuestion $satisfaction_question)
    {
    	if (! $request->old()) {
            $request->replace($satisfaction_question->toArray());        
            $request->flash();
        }

    	return view('admin.satisfaction_questions.edit', compact(['satisfaction_question']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @param    \App\SatisfactionQuestion  $satisfaction_question
     * @return  \Illuminate\Http\Response
     */
    public function update(Request $request, SatisfactionQuestion $satisfaction_question)
    {
        $validatedData = $request->validate([

	        'enunciado'=> 'required|string',
            
	    ]);

    	$satisfaction_question->fill($request->all())->save();

    	$alert=[];
        $alert['status']= 'success';
        $alert['message']= trans('message.successfully_updated');;
	            
        return redirect()->route('satisfaction_questions.index')->with('alert', $alert);

                
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param    \App\SatisfactionQuestion  $satisfaction_question
     * @return  \Illuminate\Http\Response
     */
    public function destroy(SatisfactionQuestion $satisfaction_question)
    {
        $satisfaction_question->delete();

        $alert=[];
        $alert['status']= 'success';
        $alert['message']= trans('message.successfully_deleted');

        return redirect()->route('satisfaction_questions.index')->with('alert', $alert);
    }
}