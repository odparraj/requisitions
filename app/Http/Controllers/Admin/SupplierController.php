<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Supplier;
use App\Mail\SupplierInvitation;
use App\Notifications\SystemNotification;
use App\OperativeArea;
use App\Product;
use Mail;

class SupplierController extends Controller
{

    /**
     * Instantiate a new controller instance.
     *
     * @return  void
     */
    public function __construct()
    {
        $this->middleware('permission:suppliers_create')->only(['store', 'create']);
        $this->middleware('permission:suppliers_read')->only(['index', 'show']);
        $this->middleware('permission:suppliers_update')->only(['update', 'edit']);
        $this->middleware('permission:suppliers_delete')->only(['delete']);
    }

    /**
     * Display a listing of the resource in json format.
     *
     * @return  \Illuminate\Http\Response
     */
    public function toDatatable(Request $request)
    {
        $user = $request->user();
        $permissions = [
            'suppliers_read' => $user->hasPermissionTo('suppliers_read'),
            'suppliers_update' => $user->hasPermissionTo('suppliers_update'),
            'suppliers_delete' => $user->hasPermissionTo('suppliers_delete')
        ];
        $suppliers = Supplier::where('active', true)->get()->map(function ($item) use ($permissions) {
            $items = [
                [
                    'permission' => 'suppliers_read',
                    'name' => 'Show',
                    'action' => route('suppliers.show', ['id' => $item->id]),
                    'icon' => 'fa fa-eye',
                    'target' => '_self',
                ],
                [
                    'permission' => 'suppliers_update',
                    'name' => 'Edit',
                    'action' => route('suppliers.edit', ['id' => $item->id]),
                    'icon' => 'fa fa-edit',
                    'target' => '_self',
                ],
                [
                    'permission' => 'suppliers_delete',
                    'name' => 'Delete',
                    'data_id' => $item->id,
                    'class_modal' => 'delete-modal',
                    'icon' => 'fa fa-trash-o',
                ],
                [
                    'permission' => 'suppliers_read',
                    'name' => 'Asignar Productos y Areas',
                    'action' => route('supplier.edit_products', ['id' => $item->id]),
                    'icon' => 'fa fa-plus-square',
                    'target' => '_self',
                    'btn_class' => 'btn-success'
                ],
            ];

            return [

                $item->id,

                array_get($item->informacion_general, 'razon_social', 'No tiene'),
                array_get($item->informacion_general, 'nit', 'No tiene'),
                array_get($item->informacion_general, 'direccion', 'No tiene'),
                array_get($item->informacion_general, 'telefono', 'No tiene'),
                array_get($item->informacion_general, 'email', 'No tiene'),
                $item->areasToString(),
                $item->productsToString(),

                view('settings.permissions', [
                    'items' => $items,
                    'permissions' => $permissions
                ])->render()
            ];
        })->all();
        return response()->json(['data' => $suppliers]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function index()
    {

        $columns = "['Id', 'Razon Social', 'Nit', 'Dirección', 'Telefono', 'Email', 'Areas', 'Productos', 'Actions']";
        $link = 'suppliers.dt';
        return view('admin.suppliers.index', compact(['columns', 'link']));

        //$suppliers = Supplier::all();
        //return view('admin.suppliers.index', compact(['suppliers']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.suppliers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @return  \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([

            'informacion_general' => 'required|array',
            'informacion_de_la_empresa' => 'required|array',
            'clase_de_proveedor' => 'required|array',
            'sector_economico' => 'required|array',
            'informacion_tributaria' => 'required|array',
            'informacion_financiera' => 'required|array',
            'documentos_requeridos' => 'required|array',

            'informacion_general.razon_social' => 'required|string|max:191',
            'informacion_general.nit' => 'required|string|max:191',
            'informacion_general.ciudad' => 'required|string|max:191',
            'informacion_general.direccion' => 'required|string|max:191',
            'informacion_general.telefono' => 'required|string|max:191',
            'informacion_general.email' => 'required|email|max:191',
            'informacion_general.persona_de_contacto.nombre' => 'required|string|max:191',
            'informacion_general.persona_de_contacto.cargo' => 'required|string|max:191',
            'informacion_general.persona_de_contacto.telefono' => 'required|string|max:191',
            'informacion_general.persona_de_contacto.email' => 'required|email|max:191',
            'informacion_general.certificacion_de_calidad.check' => 'required|in:si,no',
            'informacion_general.certificacion_de_calidad.cual' => 'required_if:informacion_general.certificacion_de_calidad.check,si|string|max:191',

            'informacion_de_la_empresa.actividad_principal.codigo_ciiu' => 'required|string|max:191',
            'informacion_de_la_empresa.actividad_principal.descripcion' => 'required|string|max:191',
            'informacion_de_la_empresa.actividad_secundaria.codigo_ciiu' => 'required|string|max:191',
            'informacion_de_la_empresa.actividad_secundaria.descripcion' => 'required|string|max:191',

            'clase_de_proveedor.fabricante' => 'required|in:si,no',
            'clase_de_proveedor.distribuidor' => 'required|in:si,no',
            'clase_de_proveedor.importador' => 'required|in:si,no',
            'clase_de_proveedor.comercializador' => 'required|in:si,no',
            'clase_de_proveedor.contratista' => 'required|in:si,no',
            'clase_de_proveedor.prestador_de_servicios' => 'required|in:si,no',
            'clase_de_proveedor.otro.check' => 'required|in:si,no',
            'clase_de_proveedor.otro.cual' => 'required_if:clase_de_proveedor.otro.check,si|string|max:191',

            'sector_economico.metalmecanica' => 'required|in:si,no',
            'sector_economico.financiero' => 'required|in:si,no',
            'sector_economico.software_hardware' => 'required|in:si,no',
            'sector_economico.ferreteria' => 'required|in:si,no',
            'sector_economico.construccion' => 'required|in:si,no',
            'sector_economico.salud' => 'required|in:si,no',
            'sector_economico.transporte' => 'required|in:si,no',
            'sector_economico.alimentos' => 'required|in:si,no',
            'sector_economico.consultoria' => 'required|in:si,no',
            'sector_economico.otro.check' => 'required|in:si,no',
            'sector_economico.otro.cual' => 'required_if:sector_economico.otro.check,si|string|max:191',

            'informacion_tributaria.declarante' => 'required|in:si,no',
            'informacion_tributaria.regimen_comun' => 'required|in:si,no',
            'informacion_tributaria.regimen_simplificado' => 'required|in:si,no',
            'informacion_tributaria.autorretenedor' => 'required|in:si,no',
            'informacion_tributaria.gran_contribuyente' => 'required|in:si,no',
            'informacion_tributaria.entidad_sin_animo_de_lucro' => 'required|in:si,no',
            'informacion_tributaria.industria_y_comercio.actividad_principal' => 'required|string|max:191',
            'informacion_tributaria.industria_y_comercio.tarifa' => 'required|numeric',
            'informacion_tributaria.industria_y_comercio.ciudad' => 'required|string|max:191',

            'informacion_financiera.fecha_de_corte' => 'required|integer|between:1,30',
            'informacion_financiera.total_activos' => 'required|numeric',
            'informacion_financiera.total_pasivos' => 'required|numeric',
            'informacion_financiera.total_patrimonio' => 'required|numeric',
            'informacion_financiera.total_ingresos' => 'required|numeric',
            'informacion_financiera.total_egresos' => 'required|numeric',
            'informacion_financiera.total_utilidad' => 'required|numeric',

            'documentos_requeridos.rut' => 'required|in:si,no',
            'documentos_requeridos.camara_de_comercio' => 'required|in:si,no',
            'documentos_requeridos.referencias_comerciales' => 'required|in:si,no',
            'documentos_requeridos.certificacion_bancaria' => 'required|in:si,no',
            'documentos_requeridos.certificacion_de_calidad' => 'required|in:si,no',

        ]);

        $supplier = Supplier::create($request->all());

        $alert = [];
        $alert['status'] = 'success';
        $alert['message'] = trans('message.successfully_created');

        return redirect()->route('suppliers.index')->with('alert', $alert);
    }

    /**
     * Display the specified resource.
     *
     * @param    \App\Supplier  $supplier
     * @return  \Illuminate\Http\Response
     */
    public function show(Request $request, Supplier $supplier)
    {
        if (!$request->old()) {
            $request->replace($supplier->toArray());
            $request->flash();
        }

        return view('admin.suppliers.show', compact(['supplier']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param    \App\Supplier  $supplier
     * @return  \Illuminate\Http\Response
     */
    public function edit(Request $request, Supplier $supplier)
    {
        if (!$request->old()) {
            $request->replace($supplier->toArray());
            $request->flash();
        }

        return view('admin.suppliers.edit', compact(['supplier']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @param    \App\Supplier  $supplier
     * @return  \Illuminate\Http\Response
     */
    public function update(Request $request, Supplier $supplier)
    {
        $validatedData = $request->validate([

            'informacion_general' => 'required|array',
            'informacion_de_la_empresa' => 'required|array',
            'clase_de_proveedor' => 'required|array',
            'sector_economico' => 'required|array',
            'informacion_tributaria' => 'required|array',
            'informacion_financiera' => 'required|array',
            'documentos_requeridos' => 'nullable|array',

            'informacion_general.razon_social' => 'required|string|max:191',
            'informacion_general.nit' => 'required|string|max:191',
            'informacion_general.ciudad' => 'required|string|max:191',
            'informacion_general.direccion' => 'required|string|max:191',
            'informacion_general.telefono' => 'required|string|max:191',
            'informacion_general.email' => 'required|email|max:191',
            'informacion_general.persona_de_contacto.nombre' => 'required|string|max:191',
            'informacion_general.persona_de_contacto.cargo' => 'required|string|max:191',
            'informacion_general.persona_de_contacto.telefono' => 'required|string|max:191',
            'informacion_general.persona_de_contacto.email' => 'required|email|max:191',
            'informacion_general.certificacion_de_calidad.check' => 'required|in:si,no',
            'informacion_general.certificacion_de_calidad.cual' => 'required_if:informacion_general.certificacion_de_calidad.check,si|string|max:191',

            'informacion_de_la_empresa.actividad_principal.codigo_ciiu' => 'required|string|max:191',
            'informacion_de_la_empresa.actividad_principal.descripcion' => 'required|string|max:191',
            'informacion_de_la_empresa.actividad_secundaria.codigo_ciiu' => 'nullable|string|max:191',
            'informacion_de_la_empresa.actividad_secundaria.descripcion' => 'nullable|string|max:191',

            'clase_de_proveedor.fabricante' => 'required|in:si,no',
            'clase_de_proveedor.distribuidor' => 'required|in:si,no',
            'clase_de_proveedor.importador' => 'required|in:si,no',
            'clase_de_proveedor.comercializador' => 'required|in:si,no',
            'clase_de_proveedor.contratista' => 'required|in:si,no',
            'clase_de_proveedor.prestador_de_servicios' => 'required|in:si,no',
            'clase_de_proveedor.otro.check' => 'required|in:si,no',
            'clase_de_proveedor.otro.cual' => 'required_if:clase_de_proveedor.otro.check,si|string|max:191',

            'sector_economico.metalmecanica' => 'required|in:si,no',
            'sector_economico.financiero' => 'required|in:si,no',
            'sector_economico.software_hardware' => 'required|in:si,no',
            'sector_economico.ferreteria' => 'required|in:si,no',
            'sector_economico.construccion' => 'required|in:si,no',
            'sector_economico.salud' => 'required|in:si,no',
            'sector_economico.transporte' => 'required|in:si,no',
            'sector_economico.alimentos' => 'required|in:si,no',
            'sector_economico.consultoria' => 'required|in:si,no',
            'sector_economico.otro.check' => 'required|in:si,no',
            'sector_economico.otro.cual' => 'required_if:sector_economico.otro.check,si|string|max:191',

            'informacion_tributaria.declarante' => 'required|in:si,no',
            'informacion_tributaria.regimen_comun' => 'required|in:si,no',
            'informacion_tributaria.regimen_simplificado' => 'required|in:si,no',
            'informacion_tributaria.autorretenedor' => 'required|in:si,no',
            'informacion_tributaria.gran_contribuyente' => 'required|in:si,no',
            'informacion_tributaria.entidad_sin_animo_de_lucro' => 'required|in:si,no',
            'informacion_tributaria.industria_y_comercio.actividad_principal' => 'required|string|max:191',
            'informacion_tributaria.industria_y_comercio.tarifa' => 'required|numeric',
            'informacion_tributaria.industria_y_comercio.ciudad' => 'required|string|max:191',

            'informacion_financiera.fecha_de_corte' => 'nullable|date_format:"Y-m-d"',
            'informacion_financiera.total_activos' => 'nullable|numeric',
            'informacion_financiera.total_pasivos' => 'nullable|numeric',
            'informacion_financiera.total_patrimonio' => 'nullable|numeric',
            'informacion_financiera.total_ingresos' => 'nullable|numeric',
            'informacion_financiera.total_egresos' => 'nullable|numeric',
            'informacion_financiera.total_utilidad' => 'nullable|numeric',

            'documentos_requeridos.rut' => 'nullable',
            'documentos_requeridos.camara_de_comercio' => 'nullable',
            'documentos_requeridos.referencias_comerciales_1' => 'nullable',
            'documentos_requeridos.referencias_comerciales_2' => 'nullable',
            'documentos_requeridos.certificacion_bancaria' => 'nullable',
            'documentos_requeridos.certificacion_de_calidad' => 'nullable',

        ]);
        $input = $request->all();
        $docs = [
            'rut',
            'camara_de_comercio',
            'referencias_comerciales_1',
            'referencias_comerciales_2',
            'certificacion_bancaria',
            'certificacion_de_calidad'
        ];
        foreach ($docs as $doc) {
            if ($request->hasFile('documentos_requeridos.' . $doc) && $request->file('documentos_requeridos.' . $doc)->isValid()) {
                $input['documentos_requeridos'][$doc] = $request->file('documentos_requeridos.' . $doc)->store('files');
            }
        }

        $supplier->fill($input)->save();

        $user = $supplier->users()->first();

        if($user){
            $user->email = array_get($input, 'informacion_general.email');
            $user->save();
        }

        $alert = [];
        $alert['status'] = 'success';
        $alert['message'] = trans('message.successfully_updated');;

        return redirect()->route('suppliers.index')->with('alert', $alert);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param    \App\Supplier  $supplier
     * @return  \Illuminate\Http\Response
     */
    public function destroy(Supplier $supplier)
    {
        $supplier->active = false;

        $supplier->save();

        $alert = [];
        $alert['status'] = 'success';
        $alert['message'] = trans('message.successfully_deleted');

        return redirect()->route('suppliers.index')->with('alert', $alert);
    }


    public function supplierRegisterView()
    {
        return view('web.supplier_register');
    }
    public function supplierRegisterSuccess()
    {
        return view('web.supplier_register_success');
    }

    public function supplierRegisterRequest(Request $request)
    {
        $validatedData = $request->validate([

            'informacion_general' => 'required|array',
            'informacion_de_la_empresa' => 'required|array',
            'clase_de_proveedor' => 'required|array',
            'sector_economico' => 'required|array',
            'informacion_tributaria' => 'required|array',
            'informacion_financiera' => 'required|array',
            'documentos_requeridos' => 'nullable|array',

            'informacion_general.razon_social' => 'required|string|max:191',
            'informacion_general.nit' => 'required|string|max:191',
            'informacion_general.ciudad' => 'required|string|max:191',
            'informacion_general.direccion' => 'required|string|max:191',
            'informacion_general.telefono' => 'required|string|max:191',
            'informacion_general.email' => 'required|email|unique:users,email|max:191',
            'informacion_general.persona_de_contacto.nombre' => 'required|string|max:191',
            'informacion_general.persona_de_contacto.cargo' => 'required|string|max:191',
            'informacion_general.persona_de_contacto.telefono' => 'required|string|max:191',
            'informacion_general.persona_de_contacto.email' => 'required|email|max:191',
            'informacion_general.certificacion_de_calidad.check' => 'required|in:si,no',
            'informacion_general.certificacion_de_calidad.cual' => 'required_if:informacion_general.certificacion_de_calidad.check,si|string|max:191',

            'informacion_de_la_empresa.actividad_principal.codigo_ciiu' => 'required|string|max:191',
            'informacion_de_la_empresa.actividad_principal.descripcion' => 'required|string|max:191',
            'informacion_de_la_empresa.actividad_secundaria.codigo_ciiu' => 'nullable|string|max:191',
            'informacion_de_la_empresa.actividad_secundaria.descripcion' => 'nullable|string|max:191',

            'clase_de_proveedor.fabricante' => 'required|in:si,no',
            'clase_de_proveedor.distribuidor' => 'required|in:si,no',
            'clase_de_proveedor.importador' => 'required|in:si,no',
            'clase_de_proveedor.comercializador' => 'required|in:si,no',
            'clase_de_proveedor.contratista' => 'required|in:si,no',
            'clase_de_proveedor.prestador_de_servicios' => 'required|in:si,no',
            'clase_de_proveedor.otro.check' => 'required|in:si,no',
            'clase_de_proveedor.otro.cual' => 'required_if:clase_de_proveedor.otro.check,si|string|max:191',

            'sector_economico.metalmecanica' => 'required|in:si,no',
            'sector_economico.financiero' => 'required|in:si,no',
            'sector_economico.software_hardware' => 'required|in:si,no',
            'sector_economico.ferreteria' => 'required|in:si,no',
            'sector_economico.construccion' => 'required|in:si,no',
            'sector_economico.salud' => 'required|in:si,no',
            'sector_economico.transporte' => 'required|in:si,no',
            'sector_economico.alimentos' => 'required|in:si,no',
            'sector_economico.consultoria' => 'required|in:si,no',
            'sector_economico.otro.check' => 'required|in:si,no',
            'sector_economico.otro.cual' => 'required_if:sector_economico.otro.check,si|string|max:191',

            'informacion_tributaria.declarante' => 'required|in:si,no',
            'informacion_tributaria.regimen_comun' => 'required|in:si,no',
            'informacion_tributaria.regimen_simplificado' => 'required|in:si,no',
            'informacion_tributaria.autorretenedor' => 'required|in:si,no',
            'informacion_tributaria.gran_contribuyente' => 'required|in:si,no',
            'informacion_tributaria.entidad_sin_animo_de_lucro' => 'required|in:si,no',
            'informacion_tributaria.industria_y_comercio.actividad_principal' => 'required|string|max:191',
            'informacion_tributaria.industria_y_comercio.tarifa' => 'required|numeric',
            'informacion_tributaria.industria_y_comercio.ciudad' => 'required|string|max:191',

            'informacion_financiera.fecha_de_corte' => 'nullable|date_format:"Y-m-d"',
            'informacion_financiera.total_activos' => 'nullable|numeric',
            'informacion_financiera.total_pasivos' => 'nullable|numeric',
            'informacion_financiera.total_patrimonio' => 'nullable|numeric',
            'informacion_financiera.total_ingresos' => 'nullable|numeric',
            'informacion_financiera.total_egresos' => 'nullable|numeric',
            'informacion_financiera.total_utilidad' => 'nullable|numeric',

            'documentos_requeridos.rut' => 'nullable|file',
            'documentos_requeridos.camara_de_comercio' => 'nullable|file',
            'documentos_requeridos.referencias_comerciales_1' => 'nullable|file',
            'documentos_requeridos.referencias_comerciales_2' => 'nullable|file',
            'documentos_requeridos.certificacion_bancaria' => 'nullable|file',
            'documentos_requeridos.certificacion_de_calidad' => 'nullable|file',

        ]);

        $input = $request->all();
        $docs = [
            'rut',
            'camara_de_comercio',
            'referencias_comerciales_1',
            'referencias_comerciales_2',
            'certificacion_bancaria',
            'certificacion_de_calidad'
        ];

        foreach ($docs as $doc) {
            if ($request->hasFile('documentos_requeridos.' . $doc) && $request->file('documentos_requeridos.' . $doc)->isValid()) {
                $input['documentos_requeridos'][$doc] = $request->file('documentos_requeridos.' . $doc)->store('files');
            } else {
                $input['documentos_requeridos'][$doc] = '';
            }
        }

        $supplier = Supplier::create($input);

        $password = str_random(6);

        $user = [];
        $user['password'] = bcrypt($password);
        $user['name'] = $request->input('informacion_general.razon_social');
        $user['email'] = $request->input('informacion_general.email');

        $user = $supplier->users()->create($user);

        $user->assignRole('proveedor');

        $notification = new SystemNotification([]);
        $notification->args = [
            'subject' => "Creación de cuenta en la plataforma Compras LCB",
            'greeting' => 'Hola, ' . $user['name'],
            'introduction' => 'Tu usuario es ' . $user['email'] . ' y tu contraseña es ' . $password
        ];
        $user->notify($notification);

        $alert = [];
        $alert['status'] = 'success';
        $alert['message'] = 'El Proveedor ha sido registrado Satisfactoriamente';

        return redirect()->route('supplier_external.register.success')->with('alert', $alert);
    }

    public function formInvitation()
    {
        return view('admin.suppliers.invitation');
    }
    public function sendInvitation(Request $request)
    {
        $request->validate([
            'emails' => 'required|array',
            'emails.*' => 'email'
        ]);

        $emails = $request->emails;
        Mail::to($emails[0])
            ->cc(array_slice($emails, 1))
            ->send(new SupplierInvitation());

        $alert = [];
        $alert['status'] = 'success';
        $alert['message'] = 'Invitaciones enviadas de manera correcta';

        return redirect()->route('suppliers.index')->with('alert', $alert);
    }

    public function formProducts(Request $request, Supplier $supplier)
    {
        $products_options = Product::get()->map(function ($item) {
            return ['id' => $item->id, 'nombre' => $item->nombre];
        })->toJson();

        $areas_options = OperativeArea::get()->map(function ($item) {
            return ['id' => $item->id, 'nombre' => $item->nombre];
        })->toJson();

        if (!$request->old()) {
            $input_old = $supplier->toArray();
            $products = $supplier->products()->get()->map(function ($item) {
                return ['id' => $item->id, 'nombre' => $item->nombre];
            })->toJson();

            $areas = $supplier->areas()->get()->map(function ($item) {
                return ['id' => $item->id, 'nombre' => $item->nombre];
            })->toJson();

            $input_old['products'] = $products;
            $input_old['areas'] = $areas;

            $request->replace($input_old);
            $request->flash();
        }

        return view('admin.suppliers.products', compact(['supplier', 'products_options', 'areas_options']));
    }
    public function updateProducts(Request $request, Supplier $supplier)
    {
        $request->validate([
            'products' => 'required|array',
            'products.*' => 'exists:products,id',
            'areas' => 'required|array',
            'areas.*' => 'exists:operative_areas,id'
        ]);

        $supplier->products()->sync($request->products);

        $supplier->areas()->sync($request->areas);

        $alert = [];
        $alert['status'] = 'success';
        $alert['message'] = 'Productos Actualizados';

        return redirect()->route('suppliers.index')->with('alert', $alert);
    }
}
