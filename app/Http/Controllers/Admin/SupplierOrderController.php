<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\{Quotation, SupplierOrder, OperativeArea, OrderStatus, OrderPriority, MeasurementUnit, PurchaseOrder, PaymentMethod, ExperienceRange};
use PDF;

use App\Http\Controllers\Admin\SupplierOrderTraits\{AutorizationTrait, PreselectionTrait, RequestQuotationTrait, RequisitionTrait, ResponseSupplierTrait, ScoreTrait, SendTrait};
use Exception;

class SupplierOrderController extends Controller
{
    use AutorizationTrait, PreselectionTrait, RequestQuotationTrait, RequisitionTrait, ResponseSupplierTrait, ScoreTrait, SendTrait;
	/**
     * Instantiate a new controller instance.
     *
     * @return  void
     */
    public function __construct()
    {
        $this->middleware('permission:supplier_orders_create')->only(['store','create']);
        $this->middleware('permission:supplier_orders_read')->only(['index','show']);
        $this->middleware('permission:supplier_orders_update')->only(['update','edit']);
        $this->middleware('permission:supplier_orders_delete')->only(['delete']);
        $this->middleware('permission:Autorizar Compra')->only([
            'toDatatableAutorization',
            'indexAutorization',
            'editAutorization',
            'updateAutorization',
            'refuseAutorization'
        ]);
        $this->middleware('permission:Crear Requisicion')->only([
            'toDatatableRequisition',
            'indexRequisition',
            'createRequisition',
            'storeRequisition',
        ]);
    }

    /**
     * Display a listing of the resource in json format.
     *
     * @return  \Illuminate\Http\Response
     */
    public function toDatatable(Request $request)
    {
        $user= $request->user();
        $permissions=[
            'supplier_orders_read'=> $user->hasPermissionTo('supplier_orders_read'),
            'supplier_orders_update'=> $user->hasPermissionTo('supplier_orders_update'),
            'supplier_orders_delete'=> $user->hasPermissionTo('supplier_orders_delete')
        ];
        $supplier_orders = SupplierOrder::all()->map(function($item) use($permissions){
            $items= [                
                [   
                    'permission'=>'supplier_orders_read',
                    'name'=>'Show',
                    'action'=>route('supplier_orders.show',['id'=>$item->id]),
                    'icon'=>'fa fa-eye',
                    'target'=>'_self',
                ],
                [
                    'permission'=>'supplier_orders_update',
                    'name'=>'Edit',
                    'action'=>route('supplier_orders.edit',['id'=>$item->id]),
                    'icon'=>'fa fa-edit',
                    'target'=>'_self',
                ],
                [
                    'permission'=>'supplier_orders_delete',
                    'name'=>'Delete', 
                    'data_id'=>$item->id,
                    'class_modal'=>'delete-modal',
                    'icon'=>'fa fa-trash-o',
                ],
            ];

            return[                
                $item->id,
                $item->areaOperativa->nombre,                
                $item->tramitante->name,                
                $item->solicitante->name,                
                $item->fecha_de_autorizacion,
                '<pre>'.json_encode($item->requisicion_de_compra).'</pre>',                
                $item->estado,                
                $item->prioridad,                
                '<pre>'.json_encode($item->observaciones).'</pre>',  
                view('settings.permissions', [
                    'items'=>$items,
                    'permissions'=>$permissions
                ])->render()
            ];
        })->all();
        return response()->json(['data'=> $supplier_orders ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function index()
    {
        
        $columns = "['Id','Area Que Solicita', 'Persona Que Realiza', 'Persona Que Solicita', 'Fecha De Autorizacion', 'Requisicion De Compra', 'Estado', 'Prioridad', 'Observaciones','Actions']";
        $link = 'supplier_orders.dt';
        return view('admin.supplier_orders.index', compact(['columns','link']));
        
    	//$supplier_orders = SupplierOrder::all();
        //return view('admin.supplier_orders.index', compact(['supplier_orders']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function create()
    {
        $areas_operativas= OperativeArea::get()->mapWithKeys(function($item){
            return [ $item->id => $item->nombre ];
        })->toJson();

        $estados= OrderStatus::get()->mapWithKeys(function($item){
            return [ $item->id => $item->nombre ];
        })->toJson();

        $prioridades= OrderPriority::get()->mapWithKeys(function($item){
            return [ $item->id => $item->nombre ];
        })->toJson();

        $unidades= MeasurementUnit::orderBy('nombre','asc')->get()->mapWithKeys(function($item){
            return [ $item->id => $item->nombre ];
        })->toJson();

        return view('admin.supplier_orders.create', compact(['areas_operativas','estados','prioridades','unidades']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @return  \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
	        'area_que_solicita'=> 'required|exists:operative_areas,id',
            'persona_que_realiza'=> 'required|exists:users,id',
            'persona_que_solicita'=> 'required|exists:users,id',
            'fecha_de_entrega'=> 'required|date_format:"Y-m-d"',
            'consecutivo'=> 'required',
            'requisicion_de_compra'=> 'required|array',
            'estado'=> 'required|exists:order_statuses,id',
            'prioridad'=> 'required|exists:order_priorities,id',
            'observaciones'=> 'required|array',            
	    ]);

	    $supplier_order = SupplierOrder::create($request->all());

        $alert=[];
        $alert['status']= 'success';
        $alert['message']= trans('message.successfully_created');
		
		return redirect()->route('supplier_orders.index')->with('alert', $alert);
    }

    /**
     * Display the specified resource.
     *
     * @param    \App\SupplierOrder  $supplier_order
     * @return  \Illuminate\Http\Response
     */
    public function show(Request $request, SupplierOrder $supplier_order)
    {
        if (! $request->old()) {
            $request->replace($supplier_order->toArray());        
            $request->flash();
        }

        return view('admin.supplier_orders.show', compact(['supplier_order']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param    \App\SupplierOrder  $supplier_order
     * @return  \Illuminate\Http\Response
     */
    public function edit(Request $request, SupplierOrder $supplier_order)
    {
    	if (! $request->old()) {
            $request->replace($supplier_order->toArray());        
            $request->flash();
        }

    	return view('admin.supplier_orders.edit', compact(['supplier_order']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @param    \App\SupplierOrder  $supplier_order
     * @return  \Illuminate\Http\Response
     */
    public function update(Request $request, SupplierOrder $supplier_order)
    {
        $validatedData = $request->validate([
	        'area_que_solicita'=> 'required|exists:operative_areas,id',
            'persona_que_realiza'=> 'required|exists:users,id',
            'persona_que_solicita'=> 'required|exists:users,id',
            'fecha_de_entrega'=> 'required|date_format:"Y-m-d"',
            'consecutivo'=> 'required',
            'requisicion_de_compra'=> 'required|array',
            'estado'=> 'required|exists:order_statuses,id',
            'prioridad'=> 'required|exists:order_priorities,id',
            'observaciones'=> 'required|array',            
	    ]);

    	$supplier_order->fill($request->all())->save();

    	$alert=[];
        $alert['status']= 'success';
        $alert['message']= trans('message.successfully_updated');;
	            
        return redirect()->route('supplier_orders.index')->with('alert', $alert);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param    \App\SupplierOrder  $supplier_order
     * @return  \Illuminate\Http\Response
     */
    public function destroy(Request $request, SupplierOrder $supplier_order)
    {
        try {
            $supplier_order->apply('anular');

            $obs = $request->input('observaciones');

            if($obs){
                $observaciones= $supplier_order->observaciones;
                array_set($observaciones, 'Anulada', $obs);
                $supplier_order->observaciones= $observaciones;
            }

            $supplier_order->save();
        } catch(Exception $e) {
            $alert=[];
            $alert['status']= 'danger';
            $alert['message']= 'La orden ya fue anulada';
            
            return redirect()->back()->with('alert', $alert);
        }

        $alert=[];
        $alert['status']= 'success';
        $alert['message']= trans('message.successfully_deleted');

        return redirect()->back()->with('alert', $alert);
    }


    public function pdfRequisition(SupplierOrder $supplier_order){
        $unidades= MeasurementUnit::orderBy('nombre','asc')->get()->mapWithKeys(function($item){
            return [ $item->id => $item->nombre ];
        })->toArray();

        $pdf = PDF::loadView('pdf.requisicion_de_compra',compact(['supplier_order','unidades']))->setPaper('a4', 'landscape');
        
        return $pdf->stream('requisicion_de_compra.pdf');
    }

    public function pdfPurchaseOrder(PurchaseOrder $purchase_order){
        $unidades= MeasurementUnit::orderBy('nombre','asc')->get()->mapWithKeys(function($item){
            return [ $item->id => $item->nombre ];
        })->toArray();

        $pdf = PDF::loadView('pdf.orden_de_compra',compact(['purchase_order','unidades']))->setPaper('a4');
        
        return $pdf->stream('orden_de_compra.pdf');
    }

    public function observaciones(SupplierOrder $supplier_order)
    {
        $map= [
            'requisition'=> 'Radicada',
            'autorization'=> 'Autorizada',
            'reject' => 'Rechazada',
            'pause' => 'En Espera',
            'preseleccion'=> 'Preselección',
            'regenerar' => 'Regenerar Ordenes De Compra',
            'Anulada' => 'Anulada',
        ];
        return view('observaciones', compact(['supplier_order','map']));
    }

    public function cotizaciones(SupplierOrder $supplier_order)
    {
        $cotizaciones= $supplier_order->cotizaciones;
        return view('cotizaciones', compact(['cotizaciones']));
    }

    public function selectionMatrix(SupplierOrder $supplier_order)
    {
        $formas_de_pago= PaymentMethod::all()->mapWithKeys(function($item){
            return [$item->id => $item->nombre];
        })->toArray();

        $tiempos_de_experiencia= ExperienceRange::all()->mapWithKeys(function($item){
            return [$item->id => $item->nombre];
        })->toArray();

        $unidades= MeasurementUnit::orderBy('nombre','asc')->get()->mapWithKeys(function($item){
            return [ $item->id => $item->nombre ];
        })->toArray();

        $cotizaciones= $supplier_order->cotizaciones()->whereNotNull('contenido')->get();
        return view('selection_matrix', compact([
            'supplier_order', 
            'formas_de_pago', 
            'tiempos_de_experiencia',
            'unidades',
            'cotizaciones',
            //'seleccionados'
        ]));
    }

    public function formCreateOrder(Request $request, SupplierOrder $supplier_order)
    {
        $formas_de_pago= PaymentMethod::all()->mapWithKeys(function($item){
            return [$item->id => $item->nombre];
        })->toArray();

        $tiempos_de_experiencia= ExperienceRange::all()->mapWithKeys(function($item){
            return [$item->id => $item->nombre];
        })->toArray();

        $unidades= MeasurementUnit::orderBy('nombre','asc')->get()->mapWithKeys(function($item){
            return [ $item->id => $item->nombre ];
        })->toArray();

        $cotizaciones= $supplier_order->cotizaciones()->whereNotNull('contenido')->get();
        
        if (! $request->old()) {

            $input_old= $supplier_order->toArray();            
            $seleccionados= [];

            foreach ($cotizaciones as $key => $cotizacion) {
                $data= $cotizacion->purchaseOrders()->first();
                $seleccionados[$cotizacion->id]= $data?$data->items:[];
            }

            $input_old['seleccionados'] = $seleccionados;

            $request->replace($input_old);
            $request->flash();
        }

        return view('admin.supplier_orders.addPurchaseOrder', compact([
            'supplier_order', 
            'formas_de_pago', 
            'tiempos_de_experiencia',
            'unidades',
            'cotizaciones',
            'seleccionados'
        ]));
    }

    public function storeOrder(Request $request, SupplierOrder $supplier_order)
    {
        $validatedData = $request->validate([
            'seleccionados'=>'required|array',
            'observaciones.preseleccion'=> 'nullable|string',
            'documentos_anexos'=>'nullable|array',
            'documentos_anexos.*.nombre'=>'required|string',
            'documentos_anexos.*.file'=> 'nullable|file',
        ]);

        // $input= $request->all();        

        // if ($request->documentos_anexos) {            
        //     foreach ($request->documentos_anexos?:[] as $key => $document) {
        //         $input['documentos_anexos'][$key]['file']= $document['file']->store('files');
        //     }
        // }else{
        //     $input['documentos_anexos']=[];
        // }

        //return response()->json($input);
        

        $observaciones= $supplier_order->observaciones;
        array_set($observaciones, 'preseleccion', $request->input('observaciones.preseleccion'));

        $supplier_order->observaciones= $observaciones;
        // if($request->user()->hasRole('gerente')){
        //     $supplier_order->estado=5;
        // }
        /* try {
            $supplier_order->apply('preseleccionar');
        } catch (\Throwable $th) {
            
            $alert=[];
            $alert['status']= 'danger';
            $alert['message']= 'Ya existe una preselección vigente.';

            return response()->json($alert, 403);
            //throw $th;
        } */

        $ids= [];
        foreach ($request->seleccionados as $quotation_id => $itemsDisordered) {
            $ids[]= $quotation_id;
            
            $quotation= Quotation::find($quotation_id);

            $purchaseOrder= $quotation->purchaseOrders()->first();

            $items= [];

            for($i=0; $i < count($itemsDisordered) ; $i++){
                $items[$i]= $itemsDisordered[''.$i];
            }

            if($purchaseOrder){
                $data=['items'=>$items];
                $purchaseOrder->fill($data)->save();
            }else{
                $now= date('Y-m-d');
                $data=['fecha_orden'=>$now, 'fecha_pedido'=>$now, 'fecha_entrega'=>$now, 'items'=>$items];
                $quotation->purchaseOrders()->create($data);
            }

        }

        // Elimino las ordenes de compra que no corresponen a ninguna seleccion
        $supplier_order->purchaseOrders()->whereNotIn('quotations.id',$ids)->forceDelete();
        
        //$supplier_order->apply('preseleccionar');

        if ($supplier_order->estado == 'Regenerando Ordenes') {
            $supplier_order->apply('regenerar_ordenes');
        }

        if ($supplier_order->estado == 'Generando Ordenes') {
            $supplier_order->apply('seleccionar');
        }

        $supplier_order->save();

        $supplier_order->notifyUsers();

        $alert=[];
        $alert['status']= 'success';
        $alert['message']= trans('message.successfully_updated');
        $alert['data']= $request->all();

        $request->session()->flash('alert', $alert);
        
        return response()->json($alert);
                
        //return redirect()->route('supplier_orders.index_preselection')->with('alert', $alert);
    }

    public function orderJson(Request $request, SupplierOrder $supplier_order)
    {
        $user= $request->user();
        $permissions=[
            'supplier_orders_read'=> true,
        ];
        $purchaseOrders = $supplier_order->purchaseOrders->map(function($item) use($permissions,$supplier_order){
            $items= [                
                [   
                    'permission'=>'supplier_orders_read',
                    'name'=>'Show',
                    'action'=>route('purchase_orders_pdf',['id'=>$item->id]),
                    'icon'=>'fa fa-eye',
                    'target'=>'_blank',
                ],
                [
                    'permission'=>'supplier_orders_read',
                    'name'=>'Edit',
                    'action'=>route('purchase_orders.edit',['id'=>$item->id]),
                    'icon'=>'fa fa-edit',
                    'target'=>'_self',
                ]
            ];

            return[                
                $item->id,
                $item->quotation->supplier->informacion_general['razon_social'],
                $item->fecha_orden,                
                $item->fecha_entrega,
                $supplier_order->fecha_de_autorizacion,               
                view('settings.permissions', [
                    'items'=>$items,
                    'permissions'=>$permissions
                ])->render()
            ];
        })->all();
        return response()->json(['data'=> $purchaseOrders ]);
    }

    public function listOrder(SupplierOrder $supplier_order)
    {
        $columns = "['Id','Proveedor', 'Fecha de Orden', 'Fecha de Entrega', 'Fecha De Autorizacion', 'Actions']";
        $link = 'supplier_orders.orders_json';
        $params= ['id'=>$supplier_order->id];
        return view('admin.supplier_orders.index', compact(['columns','link','params']));
    }

    public function sendToCalification(Request $request, SupplierOrder $supplier_order)
    {
        $alert=[];
        $alert['status']= 'success';
        $alert['message']= 'Registro actualizado satisfactoriamente';
        
        try {
            $supplier_order->apply('solicitar_calificacion');
            $supplier_order->save();
        } catch (\Throwable $th) {

            $alert['status']= 'danger';
            $alert['message']= 'Error cambiando el estado de la solicitud';
        }

        return redirect()->route('supplier_orders.index_quotation')->with('alert', $alert);

    }
}