<?php

namespace App\Http\Controllers\Admin\SupplierOrderTraits;

use Illuminate\Http\Request;
use App\SupplierOrder, App\OrderPriority;

trait AutorizationTrait
{
    public function toDatatableAutorization(Request $request)
    {
        $user= $request->user();
        $permissions=[
            'Autorizar Compra'=> $user->hasPermissionTo('Autorizar Compra')
        ];

        $supplier_orders = $user->areasAdministrativas->first()->requisitions->map(function($item) use($permissions){

            $items=[];
            if($item->estado=='Radicada'){
                $items= [
                    [
                        'permission'=>'Autorizar Compra',
                        'name'=>'Poner en espera',
                        'action'=>route('supplier_orders.pause_autorization',['id'=>$item->id]),
                        'icon'=>'fa fa-pause',
                        'target'=>'_self',
                        'btn_class'=>'btn-info'
                    ],
                    [
                        'permission'=>'Autorizar Compra',
                        'name'=>'Autorizar',
                        'action'=>route('supplier_orders.edit_autorization',['id'=>$item->id]),
                        'icon'=>'fa fa-edit',
                        'target'=>'_self',
                        'btn_class'=>'btn-success'
                    ],
                    [
                        'permission'=>'Autorizar Compra',
                        'name'=>'Rechazar',
                        'action'=>route('supplier_orders.reject_autorization',['id'=>$item->id]),
                        'icon'=>'fa fa-ban',
                        'target'=>'_self',
                        'btn_class'=>'btn-danger'
                    ],
                    [
                        'permission'=>'Autorizar Compra',
                        'name'=>'Ver Observaciones', 
                        'data_id'=>$item->id,
                        'class_modal'=>'observaciones-modal',
                        'icon'=>'fa fa-eye',
                        'btn_class'=>'btn-primary'
                    ],
                ];
            }else if($item->estado=='En Espera'){
                $items= [
                    [
                        'permission'=>'Autorizar Compra',
                        'name'=>'Autorizar',
                        'action'=>route('supplier_orders.edit_autorization',['id'=>$item->id]),
                        'icon'=>'fa fa-edit',
                        'target'=>'_self',
                        'btn_class'=>'btn-success'
                    ],
                    [
                        'permission'=>'Autorizar Compra',
                        'name'=>'Rechazar',
                        'action'=>route('supplier_orders.reject_autorization',['id'=>$item->id]),
                        'icon'=>'fa fa-ban',
                        'target'=>'_self',
                        'btn_class'=>'btn-danger'
                    ],
                    [
                        'permission'=>'Autorizar Compra',
                        'name'=>'Ver Observaciones', 
                        'data_id'=>$item->id,
                        'class_modal'=>'observaciones-modal',
                        'icon'=>'fa fa-eye',
                        'btn_class'=>'btn-primary'
                    ],
                ];
            }else{
                $items= [
                    [
                        'permission'=>'Autorizar Compra',
                        'name'=>'Ver Observaciones', 
                        'data_id'=>$item->id,
                        'class_modal'=>'observaciones-modal',
                        'icon'=>'fa fa-eye',
                        'btn_class'=>'btn-primary'
                    ],
                ];
            }

            return[                
                $item->id,
                $item->areaOperativa->nombre,
                $item->fecha_de_autorizacion,                
                $item->estado,                
                $item->priority->nombre,
                '<a href="'.route('supplier_orders.pdf_requisition',$item->id).'" target="_blank">Ver</a>',                
                view('includes.documentos_anexos', [
                    'items'=>$item->documentos_anexos?:[]
                ])->render(),
                view('settings.permissions', [
                    'items'=>$items,
                    'permissions'=>$permissions
                ])->render()
            ];
        })->all();
        return response()->json(['data'=> $supplier_orders ]);
    }

    public function indexAutorization()
    {
        $columns = "['Id', 'Area Que Solicita', 'Fecha De Autorizacion', 'Estado', 'Prioridad', 'Ver', 'Documentos Anexos', 'Actions']";
        $link = 'supplier_orders.dt_autorization';
        $title= 'Autorizar Compras';
        return view('admin.supplier_orders.index', compact(['columns','link', 'title']));
    }

    public function editAutorization(Request $request, SupplierOrder $supplier_order)
    {
        $prioridades= OrderPriority::get()->mapWithKeys(function($item){
            return [ $item->id => $item->nombre ];
        })->toJson();

        if (! $request->old()) {
            $request->replace($supplier_order->toArray());        
            $request->flash();
        }

        return view('admin.supplier_orders.autorization', compact(['supplier_order', 'prioridades']));
    }

    public function updateAutorization(Request $request, SupplierOrder $supplier_order)
    {
        $validatedData = $request->validate([
            'prioridad'=> 'required|exists:order_priorities,id',
            'observaciones'=> 'required|array',
            'observaciones.autorization'=> 'nullable|string',
        ]);

        $supplier_order->prioridad= $request->prioridad;

        $observaciones= $supplier_order->observaciones;
        array_set($observaciones, 'autorization', $request->input('observaciones.autorization'));

        $supplier_order->observaciones= $observaciones;

        $supplier_order->apply('autorizar');
        $supplier_order->fecha_de_autorizacion = date('Y-m-d');
        $supplier_order->save();

        $supplier_order->notifyUsers('accepted');

        $alert=[];
        $alert['status']= 'success';
        $alert['message']= trans('message.successfully_updated');
                
        return redirect()->route('supplier_orders.index_autorization')->with('alert', $alert);

    }

    public function rejectAutorization(Request $request, SupplierOrder $supplier_order)
    {

        if (! $request->old()) {
            $request->replace($supplier_order->toArray());        
            $request->flash();
        }

        return view('admin.supplier_orders.reject', compact(['supplier_order']));
    }

    public function refuseAutorization(Request $request, SupplierOrder $supplier_order)
    {

        $validatedData = $request->validate([
            'observaciones'=> 'required|array',
            'observaciones.reject'=> 'nullable|string',
        ]);

        if ($supplier_order->estado=='Radicada') {
            
            $observaciones= $supplier_order->observaciones;
            array_set($observaciones, 'reject', $request->input('observaciones.reject'));

            $supplier_order->observaciones= $observaciones;

            $supplier_order->apply('negar');
            $supplier_order->fecha_de_autorizacion = date('Y-m-d');
            $supplier_order->save();
            $supplier_order->notifyUsers('refuse');
        }

        $alert=[];
        $alert['status']= 'success';
        $alert['message']= 'La solicitud fue rechazada de manera correcta';

        return redirect()->route('supplier_orders.index_autorization')->with('alert', $alert);
    }

    public function pauseAutorization(Request $request, SupplierOrder $supplier_order)
    {

        if (! $request->old()) {
            $request->replace($supplier_order->toArray());        
            $request->flash();
        }

        return view('admin.supplier_orders.pause', compact(['supplier_order']));
    }

    public function stopAutorization(Request $request, SupplierOrder $supplier_order)
    {

        $validatedData = $request->validate([
            'observaciones'=> 'required|array',
            'observaciones.pause'=> 'nullable|string',
        ]);

        if ($supplier_order->estado=='Radicada') {
            
            $observaciones= $supplier_order->observaciones;
            array_set($observaciones, 'pause', $request->input('observaciones.pause'));

            $supplier_order->observaciones= $observaciones;

            $supplier_order->apply('pausar');
            $supplier_order->fecha_de_autorizacion = date('Y-m-d');
            $supplier_order->save();
            $supplier_order->notifyUsers('pause');
        }

        $alert=[];
        $alert['status']= 'success';
        $alert['message']= 'La solicitud fue puesta en espera';

        return redirect()->route('supplier_orders.index_autorization')->with('alert', $alert);
    }
}