<?php

namespace App\Http\Controllers\Admin\SupplierOrderTraits;

use Illuminate\Http\Request;
use App\SupplierOrder, App\PaymentMethod, App\ExperienceRange, App\MeasurementUnit, App\Quotation;

trait PreselectionTrait
{
    public function toDatatablePreselection(Request $request)
    {
        $user= $request->user();

        $permissions=[
            'Preselecionar Proveedor'=> $user->hasPermissionTo('Preselecionar Proveedor')
        ];

        $supplier_orders = SupplierOrder::whereIn('estado',['En Proceso','Generando Ordenes'])->get()->map(function($item) use($permissions, $user){

            $items=[];
            /*if($item->estado == 4){*/
            if (count($item->purchaseOrders)==0) {
                $items= [
                    [
                        'permission'=>'Preselecionar Proveedor',
                        'name'=>'Hacer Selección',
                        'action'=>route('supplier_orders.edit_preselection', $item->id),
                        'icon'=>'fa fa-edit',
                        'target'=>'_self',
                        'btn_class'=>'btn-success'
                    ],
                    [
                        'permission'=>'Preselecionar Proveedor',
                        'name'=>'Ver Observaciones', 
                        'data_id'=>$item->id,
                        'class_modal'=>'observaciones-modal',
                        'icon'=>'fa fa-eye',
                        'btn_class'=>'btn-primary'
                    ],
                ];
            } else {
                $items= [
                    [
                        'permission'=>'Preselecionar Proveedor',
                        'name'=>'Ver Observaciones', 
                        'data_id'=>$item->id,
                        'class_modal'=>'observaciones-modal',
                        'icon'=>'fa fa-eye',
                        'btn_class'=>'btn-primary'
                    ],
                ];
            }
            /*}*/

            return[
                
                $item->id,
                $item->areaOperativa->nombre,
                $item->fecha_de_autorizacion,                
                $item->estado,                
                $item->priority->nombre,
                /*'<a href="'.route('supplier_orders.pdf_requisition',$item->id).'" target="_blank">Ver</a>',*/                
                view('settings.permissions', [
                    'items'=>$items,
                    'permissions'=>$permissions
                ])->render()
            ];
        })->all();
        return response()->json(['data'=> $supplier_orders ]);
    }

    public function indexPreselection()
    {
        $columns = "['Id', 'Area Que Solicita', 'Fecha De Autorizacion', 'Estado', 'Prioridad', 'Actions']";
        $link = 'supplier_orders.dt_preselection';
        $title= 'Preseleccionar Proveedores';
        return view('admin.supplier_orders.index', compact(['columns','link','title']));
    }

    public function editPreselection(Request $request, SupplierOrder $supplier_order)
    {
        $formas_de_pago= PaymentMethod::all()->mapWithKeys(function($item){
            return [$item->id => $item->nombre];
        })->toArray();

        $tiempos_de_experiencia= ExperienceRange::all()->mapWithKeys(function($item){
            return [$item->id => $item->nombre];
        })->toArray();

        $unidades= MeasurementUnit::orderBy('nombre','asc')->get()->mapWithKeys(function($item){
            return [ $item->id => $item->nombre ];
        })->toArray();

        $cotizaciones= $supplier_order->cotizaciones()->whereNotNull('contenido')->get();
        
        if (! $request->old()) {

            $input_old= $supplier_order->toArray();            
            $seleccionados= [];

            foreach ($cotizaciones as $key => $cotizacion) {
                $data= $cotizacion->purchaseOrders()->first();
                $seleccionados[$cotizacion->id]= $data?$data->items:[];
            }

            $input_old['seleccionados'] = $seleccionados;

            $request->replace($input_old);
            $request->flash();
        }

        return view('admin.supplier_orders.preselection', compact([
            'supplier_order', 
            'formas_de_pago', 
            'tiempos_de_experiencia',
            'unidades',
            'cotizaciones',
            'seleccionados'
        ]));
    }

    public function updatePreselection(Request $request, SupplierOrder $supplier_order)
    {
        $validatedData = $request->validate([
            'seleccionados'=>'required|array',
            'observaciones.preseleccion'=> 'nullable|string',
            'documentos_anexos'=>'nullable|array',
            'documentos_anexos.*.nombre'=>'required|string',
            'documentos_anexos.*.file'=> 'nullable|file',
        ]);

        // $input= $request->all();        

        // if ($request->documentos_anexos) {            
        //     foreach ($request->documentos_anexos?:[] as $key => $document) {
        //         $input['documentos_anexos'][$key]['file']= $document['file']->store('files');
        //     }
        // }else{
        //     $input['documentos_anexos']=[];
        // }

        //return response()->json($input);
        

        $observaciones= $supplier_order->observaciones;
        array_set($observaciones, 'preseleccion', $request->input('observaciones.preseleccion'));

        $supplier_order->observaciones= $observaciones;

        try {
            $user= $request->user();
            if($user->hasRole('gerente')){
                $supplier_order->apply('seleccionar');

            }elseif ($user->hasRole('coordinador financiero')){

                $supplier_order->apply('preseleccionar');
            }
        } catch (\Throwable $th) {
            
            $alert=[];
            $alert['status']= 'danger';
            $alert['message']= 'Error cambiando el estado de la solicitud';

            return response()->json($alert, 403);
            //throw $th;
        }

        $ids= [];
        foreach ($request->seleccionados as $quotation_id => $itemsDisordered) {
            $ids[]= $quotation_id;

            $quotation= Quotation::find($quotation_id);

            $purchaseOrder= $quotation->purchaseOrders()->first();

            $items= [];

            for($i=0; $i < count($itemsDisordered) ; $i++){
                $items[$i]= $itemsDisordered[''.$i];
            }

            if($purchaseOrder){
                $data=['items'=>$items];
                $purchaseOrder->fill($data)->save();
            }else{
                $now= date('Y-m-d');
                $data=['fecha_orden'=>$now, 'fecha_pedido'=>$now, 'fecha_entrega'=>$now, 'items'=>$items];
                $quotation->purchaseOrders()->create($data);
            }

        }

        // Elimino las ordenes de compra que no corresponen a ninguna seleccion
        $supplier_order->purchaseOrders()->whereNotIn('quotations.id',$ids)->forceDelete();

        //$supplier_order->apply('preseleccionar');
        $supplier_order->save();

        $supplier_order->notifyUsers();

        $alert=[];
        $alert['status']= 'success';
        $alert['message']= trans('message.successfully_updated');
        $alert['data']= $request->all();

        $request->session()->flash('alert', $alert);

        return response()->json($alert);
    }
}