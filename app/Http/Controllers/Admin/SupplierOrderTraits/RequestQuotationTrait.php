<?php

namespace App\Http\Controllers\Admin\SupplierOrderTraits;

use Illuminate\Http\Request;
use App\SupplierOrder, App\Supplier;

trait RequestQuotationTrait
{
    public function toDatatableQuotation(Request $request)
    {
        $user= $request->user();        
        $permissions=[
            'Cotizar Requisicion'=> $user->hasPermissionTo('Cotizar Requisicion'),
            'supplier_orders_delete'=> $user->hasPermissionTo('supplier_orders_delete')
        ];

        $supplier_orders = SupplierOrder::whereIn('estado',[
            'Negada',
            'Terminada',
            'En Espera',
            'Autorizada',
            'En Proceso',
            'Generando Ordenes',
            'Regenerando Ordenes',
            'Pendiente Entrega',
            'Pendiente Evaluacion',
            'Anulada'
        ])->get()->map(function($item) use($permissions){
            
            $items= [
                [
                    'permission'=>'supplier_orders_delete',
                    'name'=>'Anular Orden', 
                    'data_id'=> $item->id,
                    'class_modal'=>'delete-modal',
                    'btn_class' => 'btn-danger',
                    'icon'=>'fa fa-close',
                ],
                [
                    'permission'=>'Cotizar Requisicion',
                    'name'=>'Solicitar Cotizacion',
                    'action'=>route('supplier_orders.edit_quotation',['id'=>$item->id]),
                    'icon'=>'fa fa-plus-square',
                    'target'=>'_self',
                    'btn_class'=>'btn-success'
                ],
                [
                    'permission'=>'Cotizar Requisicion',
                    'name'=>'Ver Cotizaciones', 
                    'data_id'=>$item->id,
                    'class_modal'=>'cotizaciones-modal',
                    'icon'=>'fa fa-list',
                    'btn_class'=>'btn-info'
                ],
                [
                    'permission'=>'Cotizar Requisicion',
                    'name'=>'Matriz de selección',
                    'action'=>route('supplier_orders.selection_matrix',['id'=>$item->id]),
                    'icon'=>'fa fa-th',
                    'target'=>'_blank',
                    'btn_class'=>'bg-yellow'
                ],
                [
                    'permission'=>'Cotizar Requisicion',
                    'name'=>'Ver Observaciones', 
                    'data_id'=>$item->id,
                    'class_modal'=>'observaciones-modal',
                    'icon'=>'fa fa-eye',
                    'btn_class'=>'btn-primary'
                ],
            ];
            if(in_array( $item->estado, ['Terminada'])){
                $items[]= [
                    'permission'=>'Cotizar Requisicion',
                    'name'=>'Ver Encuesta',
                    'action'=>route('supplier_orders.survey',['id'=>$item->id]),
                    'icon'=>'fa fa-file-text-o',
                    'target'=>'_self',
                    'btn_class'=>'btn-danger'
                ];
            }
            if(in_array( $item->estado, ['Pendiente Entrega'])){
                $items[]= [
                    'permission'=>'Cotizar Requisicion',
                    'name'=>'ReGenerar Ordenes',
                    'action'=>route('supplier_orders.regenerate_get',['id'=>$item->id]),
                    'icon'=>'fa fa-undo',
                    'target'=>'_self',
                    'btn_class'=>'bg-purple'
                ];
                $items[]= [
                    'permission'=> 'Cotizar Requisicion',
                    'name'=>'Enviar a Pendiente Evaluacion',
                    'data_id'=>$item->id,
                    'class_modal'=>'calification-modal',
                    'icon'=>'fa fa-send',
                    'target'=>'_self',
                    'btn_class'=>'btn-success'
                ];
            }
            if(in_array( $item->estado, ['Generando Ordenes', 'Regenerando Ordenes'])){
                $items[]= [
                    'permission'=>'Cotizar Requisicion',
                    'name'=>'Asignar Ordenes de Compra',
                    'action'=>route('supplier_orders.form_order',['id'=>$item->id]),
                    'icon'=>'fa fa-plus-circle',
                    'target'=>'_blank',
                    'btn_class'=>'bg-purple'
                ];
            }
            if(in_array( $item->estado, ['Anulada'])){
                $items= [
                    [
                        'permission'=>'Cotizar Requisicion',
                        'name'=>'Ver Cotizaciones', 
                        'data_id'=>$item->id,
                        'class_modal'=>'cotizaciones-modal',
                        'icon'=>'fa fa-list',
                        'btn_class'=>'btn-info'
                    ],
                    [
                        'permission'=>'Cotizar Requisicion',
                        'name'=>'Ver Observaciones', 
                        'data_id'=>$item->id,
                        'class_modal'=>'observaciones-modal',
                        'icon'=>'fa fa-eye',
                        'btn_class'=>'btn-primary'
                    ],
                ];
            }

            return [                
                $item->id,
                $item->gerencia->nombre,
                $item->areaOperativa->nombre,
                $item->fecha_de_autorizacion,                
                $item->estado,                
                $item->priority->nombre,
                '<a href="'.route('supplier_orders.pdf_requisition',$item->id).'" target="_blank">Ver</a>',                
                view('includes.documentos_anexos', [
                    'items'=>$item->documentos_anexos?:[]
                ])->render(),
                view('includes.documentos_anexos', [
                    'items'=>$item->purchaseOrdersFiles()?:[]
                ])->render(),
                view('settings.permissions', [
                    'items'=>$items,
                    'permissions'=>$permissions
                ])->render()
            ];
        })->all();
        return response()->json(['data'=> $supplier_orders ]);
    }

    public function indexQuotation()
    {
        $columns = "['Id','Gerencia Que Autoriza','Area Que Solicita', 'Fecha De Autorizacion', 'Estado', 'Prioridad', 'Ver', 'Documentos Anexos', 'Ordenes De Compra' ,'Actions']";
        $link = 'supplier_orders.dt_quotation';
        $title= 'Solicitar cotizaciones';
        return view('admin.supplier_orders.index', compact(['columns','link','title']));
    }

    public function editQuotation(Request $request, SupplierOrder $supplier_order)
    {
        $proveedores= Supplier::get()->map(function($item){
            return [ 'id'=>$item->id, 'razon_social' => array_get($item,'informacion_general.razon_social','Proveedor sin nombre') ];
        })->toJson();

        if (! $request->old()) {
            $request->replace($supplier_order->toArray());        
            $request->flash();
        }

        return view('admin.supplier_orders.quotation', compact(['supplier_order', 'proveedores']));
    }

    public function updateQuotation(Request $request, SupplierOrder $supplier_order)
    {
        $validatedData = $request->validate([
            'suppliers'=> 'required|exists:suppliers,id',
        ]);

        $supplier_order->apply('solicitar_cotizaciones');
        $supplier_order->save();

        $data=[];
        foreach ($request->suppliers?:[] as $proveedor) {
            $data[]=['proveedor'=>$proveedor];
        }

        //$supplier_order->cotizaciones()->delete();
        $quotations= $supplier_order->cotizaciones()->createMany($data);

        $emails=[];

        foreach ($quotations as $quotation){

            if($email= $quotation->supplier->getEmail()){
                $emails[] = $email;
            }
        }
        //notificar proveedores
        $supplier_order->notifyUsers('request-quotation', $emails);

        $alert=[];
        $alert['status']= 'success';
        $alert['message']= trans('message.successfully_updated');
                
        return redirect()->route('supplier_orders.index_quotation')->with('alert', $alert);
    }

    public function regenerateGet(Request $request, SupplierOrder $supplier_order)
    {

        if (! $request->old()) {
            $request->replace($supplier_order->toArray());        
            $request->flash();
        }

        return view('admin.supplier_orders.regenerate', compact(['supplier_order']));
    }

    public function regeneratePut(Request $request, SupplierOrder $supplier_order)
    {
        $validatedData = $request->validate([
            'observaciones'=> 'required|array',
            'observaciones.regenerar'=> 'nullable|string',
        ]);

        if ($supplier_order->estado=='Pendiente Entrega') {
            $observaciones= $supplier_order->observaciones;
            array_set($observaciones, 'regenerar', $request->input('observaciones.regenerar'));

            $supplier_order->observaciones= $observaciones;

            $supplier_order->apply('corregir_ordenes');
            $supplier_order->save();
            //$supplier_order->notifyUsers('regenerar');
        }
        $alert=[];
        $alert['status']= 'success';
        $alert['message']= 'Se habilitó la generación de ordenes nuevamente';

        return redirect()->route('supplier_orders.index_quotation')->with('alert', $alert);
    }

}