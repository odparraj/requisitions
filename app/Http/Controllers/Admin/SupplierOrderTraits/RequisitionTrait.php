<?php

namespace App\Http\Controllers\Admin\SupplierOrderTraits;

use App\AdministrativeArea;
use App\OperativeArea;
use App\OrderPriority;
use Illuminate\Http\Request;
use App\SupplierOrder, App\PaymentMethod, App\MeasurementUnit, App\User, App\PurchaseOrder;

trait RequisitionTrait
{
    /*Solicitud de requisicion*/

    public function toDatatableRequisition(Request $request)
    {
        $user = $request->user();
        $permissions = [
            'Crear Requisicion' => $user->hasPermissionTo('Crear Requisicion')
        ];

        $supplier_orders = $user->areasOperativas->first()->requisitions->map(function ($item) use ($permissions) {

            $items = [
                [
                    'permission' => 'Crear Requisicion',
                    'name' => 'Ver Observaciones',
                    'data_id' => $item->id,
                    'class_modal' => 'observaciones-modal',
                    'icon' => 'fa fa-eye',
                    'btn_class' => 'btn-primary'
                ],
            ];

            return [
                $item->id,
                $item->created_at->format('Y-m-d'),
                $item->fecha_de_autorizacion,
                $item->areaOperativa->nombre,
                $item->estado,
                '<a href="' . route('supplier_orders.pdf_requisition', $item->id) . '" target="_blank">Ver</a>',
                view('includes.documentos_anexos', [
                    'items' => $item->documentos_anexos ?: []
                ])->render(),
                view('includes.documentos_anexos', [
                    'items' => $item->purchaseOrdersFiles() ?: []
                ])->render(),
                view('settings.permissions', [
                    'items' => $items,
                    'permissions' => $permissions
                ])->render()
            ];
        })->all();
        return response()->json(['data' => $supplier_orders]);
    }

    public function indexRequisition()
    {
        $columns = "['Id', 'Fecha De Solicitud', 'Fecha De Autorizacion', 'Area Que Solicita', 'Estado', 'Ver', 'Documentos Anexos', 'Ordenes De Compra', 'Actions']";
        $link = 'supplier_orders.dt_requisition';
        $title = 'Solicitudes de requisición';
        return view('admin.supplier_orders.index', compact(['columns', 'link', 'title']));
    }

    public function createRequisition(Request $request)
    {

        $unidades = MeasurementUnit::orderBy('nombre', 'asc')->get()->mapWithKeys(function ($item) {
            return [$item->id => $item->nombre];
        })->toJson();

        $user = $request->user();
        $areaOperativa = $user->areasOperativas->first()->id;

        $empleados = User::role('empleado')->whereHas('areasOperativas', function ($query) use ($areaOperativa) {
            $query->where('id', $areaOperativa);
        })->get()->mapWithKeys(function ($item) {
            return [$item->id => $item->name];
        })->toJson();

        $prioridades = OrderPriority::get()->mapWithKeys(function ($item) {
            return [$item->id => $item->nombre];
        })->toJson();

        $gerencias = AdministrativeArea::get()->mapWithKeys(function ($item) {
            return [$item->id => $item->nombre];
        })->toJson();

        return view('admin.supplier_orders.requisition', compact(['unidades', 'empleados', 'prioridades', 'gerencias']));
    }

    public function storeRequisition(Request $request)
    {
        $validatedData = $request->validate([
            'persona_que_solicita' => 'required|exists:users,id',
            'gerencia_que_autoriza' => 'required|exists:administrative_areas,id',
            'prioridad' => 'required|exists:order_priorities,id',
            'fecha_de_entrega' => 'required|date_format:"Y-m-d"',
            'proveedor_recomendado' => 'required|array',
            'proveedor_recomendado.nombre' => 'nullable|string',
            'proveedor_recomendado.contacto.telefono' => 'nullable|string',
            'proveedor_recomendado.contacto.nombre' => 'nullable|string',
            'requisicion_de_compra' => 'required|array',
            'requisicion_de_compra.*.unidad' => 'required|exists:measurement_units,id',
            'requisicion_de_compra.*.cantidad' => 'required|numeric',
            'requisicion_de_compra.*.descripcion' => 'required|string',
            'requisicion_de_compra.*.especificaciones_tecnicas' => 'required|string',
            'observaciones' => 'required|array',
            'observaciones.requisition' => 'nullable|string',
            'documentos_anexos' => 'nullable|array',
            'documentos_anexos.*.nombre' => 'required|string',
            'documentos_anexos.*.file' => 'nullable|file',
        ]);

        $user = $request->user();
        $areaOperativa = $user->areasOperativas->first()->id;

        $input = $request->all();
        $input['consecutivo'] = str_random(6);
        $input['area_que_solicita'] = $areaOperativa;
        $input['persona_que_realiza'] = $user->id;

        if ($request->documentos_anexos) {
            foreach ($request->documentos_anexos ?: [] as $key => $document) {
                $input['documentos_anexos'][$key]['file'] = $document['file']->store('files');
            }
        } else {
            $input['documentos_anexos'] = [];
        }

        foreach ($request->requisicion_de_compra ?: [] as $idx => $detalle) {
            $input['requisicion_de_compra'][$idx]['id'] = $idx + 1;
        }

        $supplier_order = SupplierOrder::create($input);
        $supplier_order->estado = 'Nueva';
        $supplier_order->apply('crear');
        $supplier_order->save();

        $supplier_order->notifyUsers('create');

        $alert = [];
        $alert['status'] = 'success';
        $alert['message'] = trans('message.successfully_created');

        return redirect()->route('supplier_orders.index_requisition')->with('alert', $alert);
    }
}
