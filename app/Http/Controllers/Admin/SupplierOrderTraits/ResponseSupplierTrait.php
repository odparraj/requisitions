<?php

namespace App\Http\Controllers\Admin\SupplierOrderTraits;

use Illuminate\Http\Request;
use App\PaymentMethod, App\ExperienceRange, App\MeasurementUnit, App\Quotation;

trait ResponseSupplierTrait
{
    /*Solicitud de requisicion*/

    public function toDatatableSupplierQuote(Request $request)
    {
        $user= $request->user();

        $permissions=[
            'Crear Cotizacion'=> $user->hasPermissionTo('Crear Cotizacion')
        ];

        $supplier_orders = $user->suppliers->first()->quotations()
            ->whereHas('supplierOrder', function($query){
                $query->whereIn('estado',['En Proceso']);
            })
            ->get()
            ->map(function($item) use($permissions){

            $items=[];
            if($item->supplierOrder->estado=='En Proceso' && (! $item->contenido) ){
                $items= [
                    [
                        'permission'=>'Crear Cotizacion',
                        'name'=>'Cotizar',
                        'action'=>route('supplier_orders.edit_supplier_quote', $item->id),
                        'icon'=>'fa fa-edit',
                        'target'=>'_self',
                        'btn_class'=>'btn-success'
                    ]
                ];
            }

            return [
                
                $item->id,
                $item->supplierOrder->areaOperativa->nombre,
                $item->supplierOrder->fecha_de_autorizacion,                
                $item->supplierOrder->estado,                
                $item->supplierOrder->priority->nombre,
                '<a href="'.route('supplier_orders.pdf_requisition',$item->supplierOrder->id).'" target="_blank">Ver</a>',
                view('includes.documentos_anexos', [
                    'items'=>$item->supplierOrder->documentos_anexos?:[]
                ])->render(),                
                view('settings.permissions', [
                    'items'=>$items,
                    'permissions'=>$permissions
                ])->render()
            ];
        })->all();
        return response()->json(['data'=> $supplier_orders ]);
    }

    public function indexSupplierQuote()
    {
        $columns = "['Id', 'Area Que Solicita', 'Fecha De Autorizacion', 'Estado', 'Prioridad', 'Ver', 'Documentos Anexos', 'Actions']";
        $link = 'supplier_orders.dt_supplier_quote';
        $title= 'Mis Cotizaciones';
        return view('admin.supplier_orders.index', compact(['columns','link','title']));
    }

    public function editSupplierQuote(Request $request, Quotation $quotation)
    {
        $formas_de_pago= PaymentMethod::all()->mapWithKeys(function($item){
            return [$item->id => $item->nombre];
        })->toJson();

        $tiempos_de_experiencia= ExperienceRange::all()->mapWithKeys(function($item){
            return [$item->id => $item->nombre];
        })->toJson();

        if (! $request->old()) {
            
            $input_old['cotizacion']= $quotation->contenido;
            $data= array_get($quotation->supplierOrder, 'requisicion_de_compra', []);

            foreach ($data as $key => $item) {                    
                $input_old['cotizacion']['contenido'][$key]['unidad']= MeasurementUnit::find($item['unidad'])->nombre;
                // $input_old['cotizacion']['contenido'][$key]['cantidad']= $item['cantidad'];
                $input_old['cotizacion']['contenido'][$key]['nombre']= $item['descripcion'];

                if (! $quotation->contenido) {
                    $input_old['cotizacion']['contenido'][$key]['cantidad']= $item['cantidad'];
                    $input_old['cotizacion']['contenido'][$key]['status']= true;
                    $input_old['cotizacion']['contenido'][$key]['descripcion']= '';
                    $input_old['cotizacion']['contenido'][$key]['valor_unitario']= 0;
                    $input_old['cotizacion']['contenido'][$key]['iva_tarifa']= 0;
                    $input_old['cotizacion']['contenido'][$key]['descuento_tarifa']= 0;
                }
            }
            $request->replace($input_old);
            $request->flash();
        }

        return view('admin.supplier_orders.quote', compact(['quotation', 'formas_de_pago', 'tiempos_de_experiencia']));
    }

    public function updateSupplierQuote(Request $request, Quotation $quotation)
    {
        $validatedData = $request->validate([
            'cotizacion'=> 'required|array',
            'cotizacion.descuento_general'=> 'required|numeric',
            'cotizacion.contenido'=> 'required|array',
            'cotizacion.contenido.*.status'=> 'required|in:true,false',
            'cotizacion.contenido.*.descripcion'=> 'nullable|string',
            'cotizacion.contenido.*.valor_unitario'=> 'nullable|numeric',
            'cotizacion.contenido.*.descuento_tarifa'=> 'nullable|numeric|min:0|max:100',
            'cotizacion.contenido.*.iva_tarifa'=> 'nullable|numeric|min:0|max:100',
            'cotizacion.criterios'=> 'required|array',
            'cotizacion.criterios.calidad'=> 'required|string',
            'cotizacion.criterios.forma_de_pago'=> 'required|exists:payment_methods,id',
            'cotizacion.criterios.descuento'=> 'required|string',
            'cotizacion.criterios.tiempo_de_entrega'=> 'required|string',
            'cotizacion.criterios.validez_de_la_oferta'=> 'required|string',
            'cotizacion.criterios.garantia'=> 'required|string',
            'cotizacion.criterios.servicio_postventa'=> 'required|string',
            'cotizacion.criterios.experiencia'=> 'required|exists:experience_ranges,id',
            'cotizacion.criterios.numero_de_clientes'=> 'required|string',
            'cotizacion.criterios.certificado_de_gestion'=> 'required|string',
            'cotizacion.observaciones'=> 'nullable|string',
            'cotizacion.documento_anexo'=> 'nullable',
        ]);

        //dd($request->all());
        $input = $request->all();

        if ($request->hasFile('cotizacion.documento_anexo')) {
            $file = ($input['cotizacion']['documento_anexo'])->store('files');
            $input['cotizacion']['documento_anexo'] = $file;
        }

        $quotation->contenido= $input['cotizacion'];

        $quotation->save();

        $requisicion_de_compra= array_get($quotation->supplierOrder,'requisicion_de_compra',[]);
        foreach ($requisicion_de_compra as $key => $item) {
            $requisicion_de_compra[$key]['cotizaciones'][$quotation->id] = array_get($input, 'cotizacion.contenido.'.$key);
        }

        $supplier_order = $quotation->supplierOrder;
        $supplier_order->requisicion_de_compra = $requisicion_de_compra;
        
        $supplier_order->save();
        $supplier_order->notifyUsers('response-quotation', [$quotation->supplier->getEmail()]);

        $alert=[];
        $alert['status']= 'success';
        $alert['message']= trans('message.successfully_updated');
                
        return redirect()->route('supplier_orders.index_supplier_quote')->with('alert', $alert);

    }

}