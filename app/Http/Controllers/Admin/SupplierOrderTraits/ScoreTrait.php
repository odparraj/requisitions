<?php

namespace App\Http\Controllers\Admin\SupplierOrderTraits;

use Illuminate\Http\Request;
use App\SupplierOrder, App\PurchaseOrder, App\SatisfactionQuestion;

trait ScoreTrait
{
    // Calificacion del proveedor
    public function toDatatableScore(Request $request)
    {
        $user= $request->user();        
        $permissions=[
            'Calificar Proveedor'=> $user->hasPermissionTo('Calificar Proveedor')
        ];

        $supplier_orders = $user->areasOperativas->first()->requisitions->map(function($item) use($permissions){

            $items=[];
            if($item->estado=='Pendiente Evaluacion'){
                $items= [
                    [
                        'permission'=>'Calificar Proveedor',
                        'name'=>'Calificar Proveedores',
                        'action'=>route('supplier_orders.edit_score',['id'=>$item->id]),
                        'icon'=>'fa fa-edit',
                        'target'=>'_self',
                        'btn_class'=>'btn-success'
                    ]
                ];
            }

            return[
                $item->id,
                $item->areaOperativa->nombre,
                $item->fecha_de_autorizacion,                
                $item->estado,                
                $item->priority->nombre,
                '<a href="'.route('supplier_orders.pdf_requisition',$item->id).'" target="_blank">Ver</a>',                
                view('settings.permissions', [
                    'items'=>$items,
                    'permissions'=>$permissions
                ])->render()
            ];
        })->all();
        return response()->json(['data'=> $supplier_orders ]);
    }

    public function indexScore()
    {
        $columns = "['Id', 'Area Que Solicita', 'Fecha De Autorizacion', 'Estado', 'Prioridad', 'Ver', 'Actions']";
        $link = 'supplier_orders.dt_score';
        $title= 'Calificar Proveedores';
        return view('admin.supplier_orders.index', compact(['columns','link','title']));
    }

    public function editScore(Request $request, SupplierOrder $supplier_order)
    {
        $preguntas= SatisfactionQuestion::all()->toJson();
        $supplier_order= SupplierOrder::with(['purchaseOrders.quotation.supplier'])->find($supplier_order->id);
        $items = [];

        foreach ($supplier_order->purchaseOrders ?? [] as $purchase_order ) {
            $items[$purchase_order->id] = $purchase_order->itemsToString();
        }
        
        $items = json_encode($items);

        if (! $request->old()) {
            $request->replace($supplier_order->toArray());
            $request->flash();
        }
        return view('admin.supplier_orders.score', compact(['supplier_order', 'preguntas', 'items']));
    }

    public function updateScore(Request $request, SupplierOrder $supplier_order)
    {
        foreach ($request->respuestas as $purchase_order => $encuesta) {
            $purchase_order= PurchaseOrder::find($purchase_order);
            $respuestas=[];
            foreach ($encuesta as $pregunta => $respuesta) {
                $respuestas[$pregunta]= ['score'=> $respuesta];
            }
            $purchase_order->respuestas()->sync($respuestas);
            $purchase_order->observaciones_encuesta = array_get($request->observaciones?:[], $purchase_order->id, null);
            $purchase_order->save();
        }

        $supplier_order->apply('calificar');
        $supplier_order->save();

        $supplier_order->notifyUsers();

        $alert=[];
        $alert['status']= 'success';
        $alert['message']= trans('message.successfully_updated');
                
        return redirect()->route('supplier_orders.index_score')->with('alert', $alert);

    }

    public function showSurvey(SupplierOrder $supplier_order)
    {
        $supplier_order->loadMissing('purchaseOrders.quotation.supplier');
        $supplier_order->loadMissing('purchaseOrders.respuestas');

        $purchaseOrders = $supplier_order->purchaseOrders;
        
        return view('survey', compact(['purchaseOrders']));
    }
}