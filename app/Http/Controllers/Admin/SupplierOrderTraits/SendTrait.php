<?php

namespace App\Http\Controllers\Admin\SupplierOrderTraits;

use Illuminate\Http\Request;
use App\{SupplierOrder, PurchaseOrder, PaymentMethod, ExperienceRange, MeasurementUnit, Quotation};

trait SendTrait
{
    //enviar solicitud
    public function toDatatableSend(Request $request)
    {
        $user= $request->user();        
        $permissions=[
            'Enviar Ordenes'=> $user->hasPermissionTo('Enviar Ordenes')
        ];

        if($user->hasRole('gestor de compras')){
            $supplier_orders = SupplierOrder::where('estado','Ordenes Generadas')->get()->map(function($item) use($permissions){
    
                //$items=[];
                //if($item->estado=='Generando Ordenes'){
                    $items= [
                        [
                            'permission'=>'Enviar Ordenes',
                            'name'=>'Enviar a Pendiente de Entrega',
                            'data_id'=>$item->id,
                            'class_modal'=>'confirm-modal',
                            'icon'=>'fa fa-send',
                            'target'=>'_self',
                            'btn_class'=>'btn-success'
                        ],
                        [
                            'permission'=>'Enviar Ordenes',
                            'name'=>'Modificar Ordenes de Compra',
                            'action'=>route('supplier_orders.orders_index',['id'=>$item->id]),
                            'icon'=>'fa fa-edit',
                            'target'=>'_blank',
                            'btn_class'=>'bg-navy'
                        ],
                        [
                            'permission'=>'Enviar Ordenes',
                            'name'=>'Ver Observaciones',
                            'data_id'=>$item->id,
                            'class_modal'=>'observaciones-modal',
                            'icon'=>'fa fa-eye',
                            'btn_class'=>'btn-primary'
                        ],
                    ];
                //}
    
                return[
                    
                    $item->id,
                    $item->areaOperativa->nombre,
                    $item->fecha_de_autorizacion,                
                    $item->estado,                
                    $item->priority->nombre,
                    '<a href="'.route('supplier_orders.pdf_requisition',$item->id).'" target="_blank">Ver</a>',
                    view('includes.documentos_anexos', [
                        'items'=>$item->purchaseOrdersFiles()?:[]
                    ])->render(),                
                    view('settings.permissions', [
                        'items'=>$items,
                        'permissions'=>$permissions
                    ])->render()
                ];
            })->all();
            return response()->json(['data'=> $supplier_orders ]);
        }else{

            $supplier_orders = SupplierOrder::where('estado','Generando Ordenes')->get()->map(function($item) use($permissions){
    
                $items=[];
                if($item->estado=='Generando Ordenes'){
                    $items= [
                        [
                            'permission'=>'Enviar Ordenes',
                            'name'=>'Enviar Ordenes',
                            'action'=>route('supplier_orders.edit_send',['id'=>$item->id]),
                            'icon'=>'fa fa-send',
                            'target'=>'_self',
                            'btn_class'=>'btn-success'
                        ]
                    ];
                }
    
                return[
                    
                    $item->id,
                    $item->areaOperativa->nombre,
                    $item->fecha_de_autorizacion,                
                    $item->estado,                
                    $item->priority->nombre,
                    '<a href="'.route('supplier_orders.pdf_requisition',$item->id).'" target="_blank">Ver</a>',
                    view('includes.documentos_anexos', [
                        'items'=>$item->purchaseOrdersFiles()?:[]
                    ])->render(),                
                    view('settings.permissions', [
                        'items'=>$items,
                        'permissions'=>$permissions
                    ])->render()
                ];
            })->all();
            return response()->json(['data'=> $supplier_orders ]);
        }

    }

    public function indexSend()
    {
        $columns = "['Id', 'Area Que Solicita', 'Fecha De Autorizacion', 'Estado', 'Prioridad', 'Ver', 'Ordenes de Compra', 'Actions']";
        $link = 'supplier_orders.dt_send';
        $title= 'Generar Ordenes De Compra';
        return view('admin.supplier_orders.index', compact(['columns','link','title']));
    }

    public function updateSend(Request $request, SupplierOrder $supplier_order)
    {
        $alert=[];
        $alert['status']= 'success';
        $alert['message']= 'Registro actualizado satisfactoriamente';

        try {
            $supplier_order->apply('consolidar_ordenes');
            $supplier_order->save();
        } catch (\Throwable $th) {

            $alert['status']= 'danger';
            $alert['message']= 'Error cambiando el estado de la solicitud';
        }

        return redirect()->route('supplier_orders.index_send')->with('alert', $alert);

    }
}