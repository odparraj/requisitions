<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDF, Storage;
use App\PurchaseOrder;

class WebController extends Controller
{
    public function getPdf(PurchaseOrder $purchase_order){
    	$pdf = PDF::loadView('pdf.orden_de_compra_version_'. $purchase_order->document_version, compact(['purchase_order']))->setPaper('a4');
		return $pdf->stream('orden_de_compra.pdf');
    }

    public function file($file)
    {
        if (Storage::exists('files/'.$file)) {
            return response()->file(storage_path('app/files/'.$file));
        }
        return 'none';
    }
}
