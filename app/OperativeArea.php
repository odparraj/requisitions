<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OperativeArea extends Model
{
    use SoftDeletes;
    
    protected $table = 'operative_areas';
    protected $fillable= ['nombre', 'administrative_area_id'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    protected $casts = [];

    public function areaAdministrativa(){
    	return $this->belongsTo('App\AdministrativeArea','administrative_area_id');
    }

    public function users()
    {
        return $this->morphToMany('App\User', 'userable');
    }

    public function requisitions()
    {
        return $this->hasMany('App\SupplierOrder','area_que_solicita');
    }
}