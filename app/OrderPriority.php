<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderPriority extends Model
{
    use SoftDeletes;
    
    protected $table = 'order_priorities';
    protected $fillable= ['nombre'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    protected $casts = [];
         
}