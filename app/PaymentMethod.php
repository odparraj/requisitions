<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PaymentMethod extends Model
{
    use SoftDeletes;
    
    protected $table = 'payment_methods';
    protected $fillable= ['nombre'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    protected $casts = [];        
}