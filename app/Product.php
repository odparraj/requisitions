<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;
    
    protected $table = 'products';
    protected $fillable= ['nombre'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    protected $casts = [];
}