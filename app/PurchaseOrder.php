<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PurchaseOrder extends Model
{
    use SoftDeletes;
    
    protected $table = 'purchase_orders';
    protected $fillable= ['cotizacion', 'items', 'fecha_orden', 'fecha_pedido', 'fecha_entrega', 'observaciones'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    protected $casts = [
        'items'=>'array'
    ];
    
    public function quotation()
    {
        return $this->belongsTo('App\Quotation', 'cotizacion');
    }

    public function respuestas()
    {
        return $this->belongsToMany(
            'App\SatisfactionQuestion',
            'purchase_order_satisfaction_question',
            'purchase_order_id',
            'satisfaction_question_id'
        )->withPivot('score');
    }

    public function calcularTotalItem($item){
        $valor = floatval($item['valor_unitario']);
        $descuento= $valor * (floatval($item['descuento_tarifa'])/100);
        $base= $valor - $descuento;
        $iva = $base * (floatval($item['iva_tarifa'])/100);
        $cantidad= floatval($item['cantidad']);
        $result= ($base + $iva)*$cantidad;
        return $result;
    }

    public function calcularNeto($item){
        $valor = floatval($item['valor_unitario']);
        $descuento= $valor * (floatval($item['descuento_tarifa'])/100);
        $base= $valor - $descuento;
        $iva = $base * (floatval($item['iva_tarifa'])/100);
        $result= ($base + $iva);
        return $result;
    }

    public function calcularIva(){
        $result=0;
        foreach ($this->items?:[] as $key => $item) {
            $valor = floatval($item['valor_unitario']);
            $descuento= $valor * (floatval($item['descuento_tarifa'])/100);
            $base= $valor - $descuento;
            $iva = $base * (floatval($item['iva_tarifa'])/100);
            $cantidad= floatval($item['cantidad']);
            $result += $iva*$cantidad;
        }

        return $result;
    }

    public function calcularSubtotal(){
        $result=0;
        foreach ($this->items?:[] as $key => $item) {
            $valor = floatval($item['valor_unitario']);
            $descuento= $valor * (floatval($item['descuento_tarifa'])/100);
            $base= $valor - $descuento;
            $cantidad= floatval($item['cantidad']);
            //$iva = $base * (floatval($item['iva_tarifa'])/100);
            $result += $base*$cantidad;
        }
        return $result;
    }

    public function calcularTotal(){
        return $this->calcularIva() + $this->calcularSubtotal();
    }

    public function gestionDeCompras()
    {        
        $data = $this->respuestas->where(
            'pivot.satisfaction_question_id',
            config('encuesta.gestion_de_compras', 4)
        )->first();
        return array_get($data, 'pivot.score', 1);
    }

    public function calidad()
    {   
        $data = $this->respuestas->where(
            'pivot.satisfaction_question_id',
            config('encuesta.calidad', 1)
        )->first();
        return array_get($data, 'pivot.score', 1);
    }

    public function itemsToString()
    {
        $items = array_pluck($this->items?:[], 'descripcion');

        return  implode($items, ', ');
    }

    protected static function boot(): void
    {
        parent::boot();
        static::creating(static function ($model) {
            $model->document_version = config('liceo.order_pdf_version', 5);
        });
    }
}