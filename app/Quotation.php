<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Quotation extends Model
{
    use SoftDeletes;
    
    protected $table = 'quotations';
    protected $fillable= ['orden', 'proveedor', 'contenido'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    protected $casts = [
        'contenido'=> 'array'
    ];

    public function supplierOrder()
    {
        return $this->belongsTo('App\SupplierOrder', 'orden');
    }
    
    public function supplier()
    {
        return $this->belongsTo('App\Supplier', 'proveedor');
    }

    public function purchaseOrders()
    {
        return $this->hasMany('App\PurchaseOrder','cotizacion');
    }
    
    public function calcularValor($key)
    {        
        if(isset($this->contenido['contenido']) && $this->contenido['contenido'][$key]['status']=='true'){
            $valor = floatval($this->contenido['contenido'][$key]['valor_unitario']);
            $descuento= $valor * (floatval($this->contenido['contenido'][$key]['descuento_tarifa'])/100);
            $base= $valor - $descuento;
            $iva = $base * (floatval($this->contenido['contenido'][$key]['iva_tarifa'])/100);
            return $base + $iva;
        }else{
            return 'No Cotizado';
        }
    }

    public function calcularValorCantidad($key)
    {
        if(isset($this->contenido['contenido']) && $this->contenido['contenido'][$key]['status']=='true'){
            $valor = floatval($this->contenido['contenido'][$key]['valor_unitario']);
            $descuento= $valor * (floatval($this->contenido['contenido'][$key]['descuento_tarifa'])/100);
            $base= $valor - $descuento;
            $iva = $base * (floatval($this->contenido['contenido'][$key]['iva_tarifa'])/100);
            $cantidad= floatval($this->contenido['contenido'][$key]['cantidad']??0);
            return ($base + $iva)*$cantidad;
        }else{
            return 'No Cotizado';
        }
    }

    public function calcularTotal()
    {
        $order= $this->supplierOrder;
        $total= 0;
        foreach(array_get($this->contenido,'contenido',[]) as $key => $item){
            if($item['status']=='true'){
                $valor = floatval($this->contenido['contenido'][$key]['valor_unitario']);
                $descuento= $valor * (floatval($this->contenido['contenido'][$key]['descuento_tarifa'])/100);
                $base= $valor - $descuento;
                $iva = $base * (floatval($this->contenido['contenido'][$key]['iva_tarifa'])/100);
                $cantidad= floatval($this->contenido['contenido'][$key]['cantidad']??0);
                $total += ($base + $iva) * $cantidad;
            }
        }
        return $total - floatval($this->contenido['descuento_general']);
    }

    public function comprarItem($key)
    {
        $order= $this->supplierOrder;
        if(isset($this->contenido['contenido']) && $this->contenido['contenido'][$key]['status']=='true'){
            $valor = floatval($this->contenido['contenido'][$key]['valor_unitario']);
            $descuento= $valor * (floatval($this->contenido['contenido'][$key]['descuento_tarifa'])/100);
            $base= $valor - $descuento;
            $iva = $base * (floatval($this->contenido['contenido'][$key]['iva_tarifa'])/100);
            $cantidad= floatval($this->contenido['contenido'][$key]['cantidad']??0);
            $valor_unitario= ($base + $iva);
            $valor_total= $valor_unitario * $cantidad;
            $respuesta= [
                'item'=> $key + 1,
                'detalle'=> array_get($order->requisicion_de_compra, $key.'.descripcion',''),
                'cantidad'=> $cantidad,
                'valor_unitario'=> $valor_unitario,
                'valor_total'=> $valor_total
            ];
            return $respuesta;
        }else{
            return 'No Cotizado';
        }
    }
}