<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SatisfactionQuestion extends Model
{
    use SoftDeletes;
    
    protected $table = 'satisfaction_questions';
    protected $fillable= ['enunciado'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    protected $casts = [];
        
}