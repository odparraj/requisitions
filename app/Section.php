<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Section extends Model
{
	use SoftDeletes;
    protected $table = 'sections';
    protected $fillable = ['display_name', 'icon', 'route', 'permission'];
    protected $hidden = ['id', 'created_at', 'updated_at', 'deleted_at'];
    protected $dates= ['created_at', 'updated_at', 'deleted_at'];
}
