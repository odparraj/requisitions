<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Supplier extends Model
{
    use SoftDeletes;

    protected $table = 'suppliers';
    protected $fillable = ['informacion_general', 'informacion_de_la_empresa', 'clase_de_proveedor', 'sector_economico', 'informacion_tributaria', 'informacion_financiera', 'documentos_requeridos'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    protected $casts = [
        'informacion_general' => 'array',
        'informacion_de_la_empresa' => 'array',
        'clase_de_proveedor' => 'array',
        'sector_economico' => 'array',
        'informacion_tributaria' => 'array',
        'informacion_financiera' => 'array',
        'documentos_requeridos' => 'array',
    ];

    public function users()
    {
        return $this->morphToMany('App\User', 'userable');
    }


    public function quotations()
    {
        return $this->hasMany('App\Quotation', 'proveedor');
    }

    public function products()
    {
        return $this->belongsToMany('App\Product', 'product_supplier', 'supplier_id', 'product_id');
    }

    public function areas()
    {
        return $this->belongsToMany('App\OperativeArea', 'operative_area_supplier', 'supplier_id', 'operative_area_id');
    }

    public function getEmail()
    {
        return array_get($this->informacion_general, 'email', '');
    }

    public function productsToString()
    {
        $items = array_pluck($this->products ?: [], 'nombre');

        return  implode(', ', $items);
    }

    public function areasToString()
    {
        $items = array_pluck($this->areas ?: [], 'nombre');

        return  implode(', ', $items);
    }
}
