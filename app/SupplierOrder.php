<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Notifications\SystemNotification;
use Exception;
use Iben\Statable\Statable;
use Illuminate\Support\Facades\DB;
use Notification;

class SupplierOrder extends Model
{
    use SoftDeletes, Statable;
    
    protected $table = 'supplier_orders';
    protected $fillable= [
        'area_que_solicita',
        'gerencia_que_autoriza',
        'persona_que_realiza',
        'persona_que_solicita',
        'proveedor_recomendado',
        'fecha_de_entrega',
        'fecha_de_autorizacion',
        'consecutivo',
        'requisicion_de_compra',
        'estado',
        'prioridad',
        'observaciones',
        'documentos_anexos'
    ];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    protected $casts = [
        'proveedor_recomendado' => 'array',
    	'requisicion_de_compra'=> 'array',
    	'observaciones'=> 'array',
        'documentos_anexos'=> 'array'
    ];

    protected function getGraph()
    {
    	return 'supplier_order'; // the SM config to use
    }

    protected function saveBeforeTransition()
    {
        return true;
    }

    public function areaOperativa()
    {
    	return $this->belongsTo('App\OperativeArea','area_que_solicita');
    }

    public function gerencia()
    {
    	return $this->belongsTo('App\AdministrativeArea','gerencia_que_autoriza');
    }

    public function status()
    {
    	return $this->belongsTo('App\OrderStatus','estado');
    }

    public function priority()
    {
    	return $this->belongsTo('App\OrderPriority','prioridad');
    }

    public function solicitante()
    {
    	return $this->belongsTo('App\User','persona_que_solicita');
    }

    public function tramitante()
    {
    	return $this->belongsTo('App\User','persona_que_realiza');
    }

    public function autorizante()
    {
        $areaAdministrativa = $this->gerencia ? : $this->areaOperativa->areaAdministrativa;

        $user = User::whereHas('areasAdministrativas', function ($query) use($areaAdministrativa) {
            $query->where('id', $areaAdministrativa->id);
        })->first();

    	return $user;
    }

    public function cotizaciones(){
        return $this->hasMany('App\Quotation','orden');
    }

    public function purchaseOrders()
    {
        return $this->hasManyThrough('App\PurchaseOrder', 'App\Quotation', 'orden', 'cotizacion');
    }

    public function purchaseOrdersFiles()
    {
        $files=[];

        foreach ($this->purchaseOrders?:[] as $key => $value) {
            $files[]=[
                'file' => url('orden/'.$value->id),
                'nombre' => 'Orden de compra para '. array_get($value->quotation->supplier,'informacion_general.razon_social','NONE')
            ];
        }
        return $files;
    }

    public function itemsToString()
    {
        $items = array_pluck($this->requisicion_de_compra?:[], 'descripcion');

        return  implode($items, ', ');
    }

    public function gestionDeCompras()
    {   
        $purchaseOrders = $this->purchaseOrders;
        $respuestas = $purchaseOrders->map(function($order){
            $data = $order->respuestas->where(
                'pivot.satisfaction_question_id',
                config('encuesta.gestion_de_compras',1)
            )->first();
            return array_get($data, 'pivot.score', 1);
        });
        return $respuestas->avg();
    }

    public function calidad()
    {   
        $purchaseOrders = $this->purchaseOrders;
        $respuestas = $purchaseOrders->map(function($order){
            $data = $order->respuestas->where(
                'pivot.satisfaction_question_id',
                config('encuesta.calidad',2)
            )->first();
            return array_get($data, 'pivot.score', 1);
        });
        return $respuestas->avg();
    }

    public function notifyUsers($action='default', $emails = [])
    {
        $users=[];
        $notification= new SystemNotification([]);

        switch ($action){
            case 'create':
                $notification->args = [
                    'subject'=> "Solicitud #{$this->id}",
                    'greeting'=> 'Nueva Solicitud Radicada',
                    'introduction'=> 'Su solicitud ha sido tramitada exitosamente, podrá ver el estado de su orden ingresando a la plataforma'
                ];
                $this->tramitante->notify($notification);
                $notification->args = [
                    'subject'=> "Solicitud #{$this->id}",
                    'greeting'=> 'Nueva Solicitud Radicada',
                    'introduction'=> 'Han tramitado una nueva solicitud por favor ingrese para autorizar o rechazar'
                ];
                Notification::send($this->areaOperativa->areaAdministrativa->users, $notification);

                break;
            case 'refuse':
                $notification->args = [
                    'subject'=> "Solicitud #{$this->id}",
                    'greeting'=> "La solicitud #{$this->id} fue Rechazada",
                    'introduction'=> "Observaciones: ".array_get($this->observaciones,'reject','Ninguna')
                ];
                $this->tramitante->notify($notification);
                break;

            case 'pause':
                $notification->args = [
                    'subject'=> "Solicitud #{$this->id}",
                    'greeting'=> "La solicitud #{$this->id} fue puesta en espera",
                    'introduction'=> "Observaciones: ".array_get($this->observaciones,'pause','Ninguna')
                ];
                $this->tramitante->notify($notification);
                break;

            case 'accepted':
                $notification->args = [
                    'subject'=> "Solicitud #{$this->id}",
                    'greeting'=> "La solicitud #{$this->id} fue Autorizada",
                    'introduction'=> "Observaciones: ".array_get($this->observaciones,'autorization','Ninguna')
                ];
                $this->tramitante->notify($notification);
                $notification->args = [
                    'subject'=> "Solicitud #{$this->id}",
                    'greeting'=> 'Nueva Solicitud Autorizada',
                    'introduction'=> 'Se ha autorizado una nueva solicitud, por favor ingrese a la plataforma para iniciar el proceso de cotización'
                ];
                Notification::send(User::role('gestor de compras')->get(), $notification);
                break;

            case 'request-quotation':
                $notification->args = [
                    'subject'=> "Solicitud #{$this->id}",
                    'greeting'=> 'Nueva Solicitud De Cotización',
                    'introduction'=> 'El liceo de colombia ha solicitado una cotización, por favor ingrese a la plataforma para diligenciar, gracias por su colaboración'
                ];
                Notification::send(User::role('proveedor')->whereIn('email',$emails)->get(), $notification);
                break;
            case 'response-quotation':

                $notification->args = [
                    'subject'=> "Solicitud #{$this->id}",
                    'greeting'=> 'Registro de cotización exitoso',
                    'introduction'=> 'Gracias por diligenciar la solicitud de cotización'
                ];
                Notification::send(User::role('proveedor')->whereIn('email',$emails)->get(), $notification);


                $cotizaciones= $this->cotizaciones()->whereNotNull('contenido')->get();
                $count= count($cotizaciones);

                $notification->args = [
                    'subject'=> "Solicitud #{$this->id}",
                    'greeting'=> "Nueva cotización registrada para la solicitud #{$this->id}",
                    'introduction'=> "Existen {$count}/{$this->cotizaciones()->count()} cotizaciones, puede ingresar a la plataforma para iniciar la selección de proveedores, si así lo considera."
                ];
                Notification::send(User::role('coordinador financiero')->get(), $notification);


            default:
                break;
        }
    }
}