<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use Notifiable, HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'cedula', 'cargo'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    

    public function areasOperativas()
    {
        return $this->morphedByMany('App\OperativeArea', 'userable');
    }

    public function areasAdministrativas()
    {
        return $this->morphedByMany('App\AdministrativeArea', 'userable');
    }

    public function requisitions()
    {
        return $this->hasMany('App\SupplierOrder','persona_que_solicita');
    }

    public function suppliers()
    {
        return $this->morphedByMany('App\Supplier','userable');
    }
}
