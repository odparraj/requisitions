<?php

return [
    'supplier_order' => [
        // class of your domain object
        'class' => App\SupplierOrder::class,

        // name of the graph (default is "default")
        'graph' => 'supplier_order',

        // property of your object holding the actual state (default is "state")
        'property_path' => 'estado',

        // list of all possible states
        'states' => [
            'Nueva',
            'Radicada',
            'En Espera',
            'Autorizada',
            'Negada',
            'En Proceso',
            'Generando Ordenes',
            'Regenerando Ordenes',
            'Ordenes Generadas',
            'Pendiente Entrega',
            'Pendiente Evaluacion',
            'Terminada',
            'Anulada'
        ],
        // list of all possible transitions
        'transitions' => [
            'crear' => [
                'from' => ['Nueva'],
                'to' => 'Radicada',
            ],
            'pausar' => [
                'from' =>  ['Radicada'],
                'to' => 'En Espera',
            ],
            'autorizar' => [
                'from' =>  ['Radicada','En Espera'],
                'to' => 'Autorizada',
            ],
            'negar' => [
                'from' => ['Radicada','En Espera'],
                'to' => 'Negada',
            ],
            'solicitar_cotizaciones' => [
                'from' => ['Autorizada', 'En Proceso'],
                'to' =>  'En Proceso',
            ],
            'preseleccionar' => [
                'from' => ['En Proceso'],
                'to' =>  'Generando Ordenes',
            ],
            'seleccionar' => [
                'from' => ['Generando Ordenes'],
                'to' =>  'Ordenes Generadas',
            ],
            'consolidar_ordenes' => [
                'from' => ['Ordenes Generadas'],
                'to' =>  'Pendiente Entrega',
            ],
            'corregir_ordenes' => [
                'from' => ['Pendiente Entrega'],
                'to' =>  'Regenerando Ordenes',
            ],
            'regenerar_ordenes' => [
                'from' => ['Regenerando Ordenes'],
                'to' =>  'Pendiente Entrega',
            ],
            'solicitar_calificacion' => [
                'from' => ['Pendiente Entrega'],
                'to' =>  'Pendiente Evaluacion',
            ],
            'calificar' => [
                'from' => ['Pendiente Evaluacion'],
                'to' =>  'Terminada',
            ],
            'anular' => [
                'from' => [
                    'Nueva',
                    'Radicada',
                    'Autorizada',
                    'Negada',
                    'En Proceso',
                    'Generando Ordenes',
                    'Pendiente Entrega',
                    'Pendiente Evaluacion',
                    'Terminada'
                ],
                'to' =>  'Anulada',
            ],
        ],

        // list of all callbacks
        'callbacks' => [
            // will be called when testing a transition
            'guard' => [
                'guard_on_submitting' => [
                    // call the callback on a specific transition
                    'on' => 'submit_changes',
                    // will call the method of this class
                    'do' => ['MyClass', 'handle'],
                    // arguments for the callback
                    'args' => ['object'],
                ],
                'guard_on_approving' => [
                    // call the callback on a specific transition
                    'on' => 'approve',
                    // will check the ability on the gate or the class policy
                    'can' => 'approve',
                ],
            ],

            // will be called before applying a transition
            'before' => [],

            // will be called after applying a transition
            'after' => [
                'history' => [
                    'do' => 'StateHistoryManager@storeHistory'
                ]
            ],
        ],
    ],
];
