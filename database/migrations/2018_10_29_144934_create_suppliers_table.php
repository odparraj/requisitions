<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuppliersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        Schema::create('suppliers', function (Blueprint $table) {
            $table->increments('id');

			$table->text('informacion_general');
            $table->text('informacion_de_la_empresa');
            $table->text('clase_de_proveedor');
            $table->text('sector_economico');
            $table->text('informacion_tributaria');
            $table->text('informacion_financiera');
            $table->text('documentos_requeridos');
            
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        Schema::dropIfExists('suppliers');
    }
}
