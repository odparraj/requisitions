<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSupplierOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        Schema::create('supplier_orders', function (Blueprint $table) {
            $table->increments('id');

			$table->unsignedInteger('area_que_solicita');
            $table->unsignedInteger('persona_que_realiza');
            $table->unsignedInteger('persona_que_solicita');
            $table->text('proveedor_recomendado');
            $table->date('fecha_de_entrega');
            $table->string('consecutivo');
            $table->text('requisicion_de_compra');
            $table->text('documentos_anexos');
            $table->string('estado')->default('Nueva');
            $table->unsignedInteger('prioridad');
            $table->text('observaciones');
            
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        Schema::dropIfExists('supplier_orders');
    }
}
