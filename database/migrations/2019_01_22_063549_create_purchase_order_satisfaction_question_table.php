<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseOrderSatisfactionQuestionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_order_satisfaction_question', function (Blueprint $table) {
            $table->unsignedInteger('purchase_order_id');
            $table->unsignedInteger('satisfaction_question_id');
            $table->unsignedTinyInteger('score');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_order_satisfaction_question');
    }
}
