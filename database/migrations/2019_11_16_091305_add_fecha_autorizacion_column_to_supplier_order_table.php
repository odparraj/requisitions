<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFechaAutorizacionColumnToSupplierOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        Schema::table('supplier_orders', function (Blueprint $table) {
            $table->date('fecha_de_autorizacion')->nullable()->after('fecha_de_entrega');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        Schema::table('supplier_orders', function (Blueprint $table) {
            $table->dropColumn('fecha_de_autorizacion');
        });
    }
}
