<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddObservacionesColumnToPurchaseOrderTable extends Migration
{
     /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        Schema::table('purchase_orders', function (Blueprint $table) {
            $table->text('observaciones')->nullable()->after('fecha_entrega');
            $table->text('observaciones_encuesta')->nullable()->after('fecha_entrega');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        Schema::table('purchase_orders', function (Blueprint $table) {
            $table->dropColumn('observaciones');
            $table->dropColumn('observaciones_encuesta');
        });
    }
}
