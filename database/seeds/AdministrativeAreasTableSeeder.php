<?php

use Illuminate\Database\Seeder;
use App\AdministrativeArea;

class AdministrativeAreasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('administrative_areas')->insert([
			['nombre' => 'Rectoría','created_at' => '2018-10-17 17:36:16','updated_at' => '2018-10-17 17:36:16','deleted_at' => NULL],
			['nombre' => 'Gerencia Operativa','created_at' => '2018-10-17 17:36:31','updated_at' => '2018-10-17 17:36:31','deleted_at' => NULL],
			['nombre' => 'Gerencia Administrativa y Financiera','created_at' => '2018-10-17 17:36:42','updated_at' => '2018-10-17 17:36:42','deleted_at' => NULL]
        ]);
    }
}
