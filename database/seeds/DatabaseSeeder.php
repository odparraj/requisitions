<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\User;
use App\Notifications\SystemNotification;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        app()['cache']->forget('spatie.permission.cache');

        /* $this->call([
            SectionsTableSeeder::class,
            RolesTableSeeder::class,
            PermissionsTableSeeder::class,
            AdministrativeAreasTableSeeder::class,
            ExperienceRangesTableSeeder::class,
            MeasurementUnitsTableSeeder::class,
            OperativeAreasTableSeeder::class,
            OrderPrioritiesTableSeeder::class,
            // OrderStatusesTableSeeder::class,
            PaymentMethodsTableSeeder::class,
            SatisfactionQuestionsTableSeeder::class,
            SupplierOrdersTableSeeder::class,
            SuppliersTableSeeder::class
        ]);

        $role = Role::findByName('super-admin')->givePermissionTo(Permission::all());
        $role = Role::findByName('coordinador')->givePermissionTo(['Crear Requisicion', 'Calificar Proveedor']);
        $role = Role::findByName('gerente')->givePermissionTo(['Autorizar Compra', 'Preselecionar Proveedor']);
        $role = Role::findByName('coordinador financiero')->givePermissionTo(['Preselecionar Proveedor']);
        $role = Role::findByName('proveedor')->givePermissionTo(['Crear Cotizacion']);
        $role = Role::findByName('gestor de compras')->givePermissionTo(['Cotizar Requisicion', 'Enviar Ordenes']);

        $notification = new SystemNotification([]);

        $user = User::create(
            ['name' => 'Super Admin', 'email' => 'oscar.parra@mixedmedia-ad.com', 'password' => bcrypt('m1x3dm3d14')]
        );
        $user->assignRole('super-admin'); */


        // Creacion de los gerentes
        $usersByArea = [
            [
                [
                    'CEDULA' => '1032374249',
                    'NOMBRES' => 'ADRIANA MARIA',
                    'APELLIDOS' => 'GONZALEZ PULIDO',
                    'CARGO' => 'PSICOLOGA',
                    'CORREO' => 'adriana.gonzalez@lcb.edu.co',
                    'ROLE' => 'coordinador',
                ],
                [
                    'CEDULA' => '52409085',
                    'NOMBRES' => 'ALEXANDRA ',
                    'APELLIDOS' => 'ARIAS VIRACACHA',
                    'CARGO' => 'PSICOLOGA',
                    'CORREO' => 'alexandra.arias@lcb.edu.co',
                    'ROLE' => 'coordinador',
                ],
                [
                    'CEDULA' => '52218428',
                    'NOMBRES' => 'ANDREA CECILIA',
                    'APELLIDOS' => 'LUCERO ZARATE',
                    'CARGO' => 'PSICOLOGA',
                    'CORREO' => 'andrea.lucero@lcb.edu.co',
                    'ROLE' => 'coordinador',
                ],
                [
                    'CEDULA' => '52848552',
                    'NOMBRES' => 'ANGELA PIEDAD',
                    'APELLIDOS' => 'ARIAS GONZALEZ',
                    'CARGO' => 'JEFE DE AREA',
                    'CORREO' => 'angela.arias@lcb.edu.co',
                    'ROLE' => 'coordinador',
                ],
                [
                    'CEDULA' => '51866205',
                    'NOMBRES' => 'CAROLINA            ',
                    'APELLIDOS' => 'CORREA BARBOSA                           ',
                    'CARGO' => 'RECTORA             ',
                    'CORREO' => 'rectoria@lcb.edu.co',
                    'ROLE' => 'coordinador',
                ],
                [
                    'CEDULA' => '1013593344',
                    'NOMBRES' => 'DANNY ALEXANDER ',
                    'APELLIDOS' => 'HERNANDEZ CORTES                  ',
                    'CARGO' => 'JEFE DE AREA',
                    'CORREO' => 'danny.hernandez@lcb.edu.co',
                    'ROLE' => 'coordinador',
                ],
                [
                    'CEDULA' => '39548284',
                    'NOMBRES' => 'ELISA',
                    'APELLIDOS' => 'SIERRA LESMES',
                    'CARGO' => 'DIRECTOR DE CICLO',
                    'CORREO' => 'director.primaria@lcb.edu.co',
                    'ROLE' => 'coordinador',
                ],
                [
                    'CEDULA' => '52312057',
                    'NOMBRES' => 'FANNY YANETH ',
                    'APELLIDOS' => 'CANGREJO GOMEZ',
                    'CARGO' => 'PSICOLOGA',
                    'CORREO' => 'fanny.cangrejo@lcb.edu.co',
                    'ROLE' => 'coordinador',
                ],
                [
                    'CEDULA' => '80008814',
                    'NOMBRES' => 'JAVIER FRANCISCO',
                    'APELLIDOS' => 'NOSSA MARQUEZ',
                    'CARGO' => 'JEFE DE AREA',
                    'CORREO' => 'ict@lcb.edu.co',
                    'ROLE' => 'coordinador',
                ],
                [
                    'CEDULA' => '3110053',
                    'NOMBRES' => 'JHON JAIRO',
                    'APELLIDOS' => 'ARCE BERRIO',
                    'CARGO' => 'INTEGRADOR TIC',
                    'CORREO' => 'integrador@lcb.edu.co',
                    'ROLE' => 'coordinador',
                ],
                [
                    'CEDULA' => '79685829',
                    'NOMBRES' => 'JOSE ALEJANDRO ',
                    'APELLIDOS' => 'RODRIGUEZ GOYES                    ',
                    'CARGO' => 'JEFE DE AREA',
                    'CORREO' => 'alejandro.rodriguez@lcb.edu.co',
                    'ROLE' => 'coordinador',
                ],
                [
                    'CEDULA' => '52992290',
                    'NOMBRES' => 'JOYCE VIVIANE',
                    'APELLIDOS' => 'CUELLAR REYES ',
                    'CARGO' => 'PSICOLOGA',
                    'CORREO' => 'joyce.cuellar@lcb.edu.co',
                    'ROLE' => 'coordinador',
                ],
                [
                    'CEDULA' => '51949393',
                    'NOMBRES' => 'LUZ HELENA ',
                    'APELLIDOS' => 'SILVA CALDERON',
                    'CARGO' => 'JEFE DE AREA',
                    'CORREO' => 'luz.silva@lcb.edu.co',
                    'ROLE' => 'coordinador',
                ],
                [
                    'CEDULA' => '75073641',
                    'NOMBRES' => 'MARCO HUMBERTO',
                    'APELLIDOS' => 'ARIAS LOPEZ',
                    'CARGO' => 'COORDINADOR DE BIENESTAR',
                    'CORREO' => 'coor.bie.bachillerato@lcb.edu.co',
                    'ROLE' => 'coordinador',
                ],
                [
                    'CEDULA' => '20678447',
                    'NOMBRES' => 'MARIA DEL PILAR ',
                    'APELLIDOS' => 'VARGAS ALMECIGA',
                    'CARGO' => 'COORDINADOR DE BIENE',
                    'CORREO' => 'coor.bie.preescolar@lcb.edu.co',
                    'ROLE' => 'coordinador',
                ],
                [
                    'CEDULA' => '39698829',
                    'NOMBRES' => 'MARIA GEORGINA',
                    'APELLIDOS' => 'SANCHEZ CASTELLANOS                ',
                    'CARGO' => 'DIRECTOR DE CICLO',
                    'CORREO' => 'director.preescolar@lcb.edu.co',
                    'ROLE' => 'coordinador',
                ],
                [
                    'CEDULA' => '79985351',
                    'NOMBRES' => 'MILTON ANDRES',
                    'APELLIDOS' => 'FIGUEREDO VERGARA',
                    'CARGO' => 'JEFE DE AREA',
                    'CORREO' => 'milton.figueredo@lcb.edu.co',
                    'ROLE' => 'coordinador',
                ],
                [
                    'CEDULA' => '52258789',
                    'NOMBRES' => 'NATALIA',
                    'APELLIDOS' => 'SANCHEZ FORERO',
                    'CARGO' => 'COORDINADOR  DE DEPARTAMENTO DE APOYO AL APRENDIZAJE',
                    'CORREO' => 'natalia.sanchez@lcb.edu.co',
                    'ROLE' => 'coordinador',
                ],
                [
                    'CEDULA' => '80797203',
                    'NOMBRES' => 'NELSON ANDRES ',
                    'APELLIDOS' => 'ROJAS CALDERON                      ',
                    'CARGO' => 'DIRECTOR DE CICLO',
                    'CORREO' => 'coordinadorcie@lcb.edu.co',
                    'ROLE' => 'coordinador',
                ],
                [
                    'CEDULA' => '79524634',
                    'NOMBRES' => 'OSCAR FERNANDO ',
                    'APELLIDOS' => 'CORREA BARBOSA                     ',
                    'CARGO' => 'VICERECTOR',
                    'CORREO' => 'vicerrectoria@lcb.edu.co',
                    'ROLE' => 'coordinador',
                ],
                [
                    'CEDULA' => '75080000',
                    'NOMBRES' => 'RUBEN DARIO ',
                    'APELLIDOS' => 'NEIRA HURTADO',
                    'CARGO' => 'JEFE DE AREA',
                    'CORREO' => 'ruben.neira@lcb.edu',
                    'ROLE' => 'coordinador',
                ],
                [
                    'CEDULA' => '1019037962',
                    'NOMBRES' => 'SUSEIH ',
                    'APELLIDOS' => 'CAJAMARCA BLANCO ',
                    'CARGO' => 'JEFE DE AREA',
                    'CORREO' => 'suseih.cajamarca@lcb.edu.co',
                    'ROLE' => 'coordinador',
                ],
                [
                    'CEDULA' => '52704700',
                    'NOMBRES' => 'ADRIANA DEL PILAR',
                    'APELLIDOS' => 'NIÑO SUESCA                     ',
                    'CARGO' => 'DOCENTE',
                    'CORREO' => 'adriana.suesca@lcb.edu.co',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '1070005524',
                    'NOMBRES' => 'ADRIANA LUCIA ',
                    'APELLIDOS' => 'ZIPAQUIRA COLORADO                  ',
                    'CARGO' => 'DOCENTE',
                    'CORREO' => 'adriana.zipaquira@lcb.edu.co',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '491739',
                    'NOMBRES' => 'ALAN JAMES',
                    'APELLIDOS' => 'LEGGETT                                ',
                    'CARGO' => 'DOCENTE',
                    'CORREO' => 'alan.leggett@lcb.edu.co',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '52046592',
                    'NOMBRES' => 'ALCIRA              ',
                    'APELLIDOS' => 'RODRIGUEZ GALINDO                          ',
                    'CARGO' => 'ASISTENTE DE DE CICLO',
                    'CORREO' => 'asistentebachillerato@lcb.edu.co',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '79860182',
                    'NOMBRES' => 'ALEXANDER',
                    'APELLIDOS' => 'ORTIZ MARTINEZ',
                    'CARGO' => 'DOCENTE',
                    'CORREO' => 'alexander.ortiz@lcb.edu.co',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '1056806',
                    'NOMBRES' => 'ALEXIS',
                    'APELLIDOS' => 'SMAY',
                    'CARGO' => 'DOCENTE',
                    'CORREO' => 'alexis.smay@lcb.edu.co',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '52825485',
                    'NOMBRES' => 'ANDREA',
                    'APELLIDOS' => 'DEVIA BRICEÑO',
                    'CARGO' => 'DOCENTE',
                    'CORREO' => 'andrea.briceno@lcb.edu.co',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '1072669491',
                    'NOMBRES' => 'ANDREA ISABEL',
                    'APELLIDOS' => 'OCHOA CORTES',
                    'CARGO' => 'DOCENTE',
                    'CORREO' => 'andrea.ochoa@lcb.edu.co',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '1012465099',
                    'NOMBRES' => 'ANDRES FELIPE',
                    'APELLIDOS' => 'CABEZAS BERNAL',
                    'CARGO' => 'ANALISTA DE SISTEMAS',
                    'CORREO' => 'analista.sistema@lcb.edu.co',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '1007639687',
                    'NOMBRES' => 'ANGIE EULALIA',
                    'APELLIDOS' => 'SANJUAN RAMBAO',
                    'CARGO' => 'AUXILIAR DE AULA',
                    'CORREO' => '',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '1233906149',
                    'NOMBRES' => 'ANGIE LORENA',
                    'APELLIDOS' => 'VARGAS GARCIA',
                    'CARGO' => 'AUXILIAR DE AULA PRE',
                    'CORREO' => '',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '1013607913',
                    'NOMBRES' => 'ARIEL YESIDT ',
                    'APELLIDOS' => 'RAMIREZ NAVARRO                      ',
                    'CARGO' => 'DOCENTE',
                    'CORREO' => 'ariel.ramirez@lcb.edu.co',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '26671725',
                    'NOMBRES' => 'ASTRID IBON ',
                    'APELLIDOS' => 'CARRASQUILLA ALTAMAR',
                    'CARGO' => 'ASISTENTE DE CICLO',
                    'CORREO' => 'astrid.carrasquilla@lcb.edu.co',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '1032416110',
                    'NOMBRES' => 'CAMILA ANDREA',
                    'APELLIDOS' => 'GAITAN SOLORZANO',
                    'CARGO' => 'DOCENTE',
                    'CORREO' => 'camila.gaitan@lcb.edu.co',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '1072667474',
                    'NOMBRES' => 'CAROL STEFANY',
                    'APELLIDOS' => 'RODRIGUEZ JIMENEZ',
                    'CARGO' => 'DOCENTE',
                    'CORREO' => 'stefany.rodriguez@lcb.edu.co',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '52708134',
                    'NOMBRES' => 'CAROL XIMENA',
                    'APELLIDOS' => 'BOLAÑOS PRIETO                       ',
                    'CARGO' => 'DOCENTE',
                    'CORREO' => 'carol.bolanos@lcb.edu.co',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => 'PAS 7958125',
                    'NOMBRES' => 'CHRISTOPHER WILLIAM',
                    'APELLIDOS' => 'DOWLING',
                    'CARGO' => 'DOCENTE',
                    'CORREO' => 'christopher.dowling@lcb.edu.co',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '35197415',
                    'NOMBRES' => 'CLAUDIA PATRICIA ',
                    'APELLIDOS' => 'LARA CONTRERAS                   ',
                    'CARGO' => 'DOCENTE',
                    'CORREO' => 'claudia.lara@lcb.edu.co',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '1136881692',
                    'NOMBRES' => 'CRISTIAN SNEIDER',
                    'APELLIDOS' => 'GRANADOS CARDENAS                ',
                    'CARGO' => 'DOCENTE',
                    'CORREO' => 'cristian.granados@lcb.edu.co',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '1018443225',
                    'NOMBRES' => 'DANIEL FELIPE',
                    'APELLIDOS' => 'PEÑA CUELLAR',
                    'CARGO' => 'DOCENTE',
                    'CORREO' => 'daniel.pena@lcb.edu.co',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '1019030637',
                    'NOMBRES' => 'DANIELA',
                    'APELLIDOS' => 'GUERRA LOSADA',
                    'CARGO' => 'DOCENTE',
                    'CORREO' => 'daniela.guerra@lcb.edu.co',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '1032460866',
                    'NOMBRES' => 'DAVID ALBERTO ',
                    'APELLIDOS' => 'GUTIERREZ CIFUENTES',
                    'CARGO' => 'DOCENTE',
                    'CORREO' => 'david.gutierrez@lcb.edu.co',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '80171944',
                    'NOMBRES' => 'DAVID RICARDO ',
                    'APELLIDOS' => 'BONILLA CABALLERO                   ',
                    'CARGO' => 'DOCENTE',
                    'CORREO' => 'david.bonilla@lcb.edu.co',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '52754483',
                    'NOMBRES' => 'DERLY ',
                    'APELLIDOS' => 'AGUIRRE RESTREPO',
                    'CARGO' => 'APRENDIZ DEL SENA   ',
                    'CORREO' => '',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '53006107',
                    'NOMBRES' => 'DIANA MARCELA ',
                    'APELLIDOS' => 'RODRIGUEZ CORTES                    ',
                    'CARGO' => 'DOCENTE',
                    'CORREO' => 'diana.rodriguez@lcb.edu.co',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '1030663788',
                    'NOMBRES' => 'DIEGO ALEJANDRO ',
                    'APELLIDOS' => 'OCAMPO CABAS',
                    'CARGO' => 'AUXILIAR DE RECURSOS FISICOS',
                    'CORREO' => 'asistenterecursos@lcb.edu.co',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '1019028551',
                    'NOMBRES' => 'EDGAR LEONARDO ',
                    'APELLIDOS' => 'VILLABON ARANGO                    ',
                    'CARGO' => 'DOCENTE',
                    'CORREO' => 'edgar.villabon@lcb.edu.co',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '52992491',
                    'NOMBRES' => 'ERIKA MARCELA ',
                    'APELLIDOS' => 'HERRERA CASTILLO',
                    'CARGO' => 'DOCENTE',
                    'CORREO' => 'erika.herrera@lcb.edu.co',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '1019110882',
                    'NOMBRES' => 'EVELIN JOHANA',
                    'APELLIDOS' => 'BUITRAGO CORTES',
                    'CARGO' => 'AUXILIAR DE AULA',
                    'CORREO' => '',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '1093305',
                    'NOMBRES' => 'FLORANGEL CAROLINA',
                    'APELLIDOS' => 'ALVAREZ MUJICA',
                    'CARGO' => 'DOCENTE',
                    'CORREO' => 'carolina.alvarez@lcb.edu.co',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '961450',
                    'NOMBRES' => 'FRANCIS MARK ',
                    'APELLIDOS' => 'MAWO',
                    'CARGO' => 'DOCENTE',
                    'CORREO' => 'francis.mawo@lcb.edu.co',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '1019031794',
                    'NOMBRES' => 'GERALDINE           ',
                    'APELLIDOS' => 'CUERVO MARTINEZ                         ',
                    'CARGO' => 'DOCENTE',
                    'CORREO' => 'geraldine.cuervo@lcb.edu.co',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '53178309',
                    'NOMBRES' => 'INGRID ROCIO ',
                    'APELLIDOS' => 'CORTAZAR TORRES',
                    'CARGO' => 'DOCENTE',
                    'CORREO' => 'ingrid.cortazar@lcb.edu.co',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '1105611488',
                    'NOMBRES' => 'JANEIDDY PAOLA ',
                    'APELLIDOS' => 'VARGAS ALFONSO',
                    'CARGO' => 'APRENDIZ DEL SENA   ',
                    'CORREO' => '',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '1233890902',
                    'NOMBRES' => 'JEIMMY PAOLA',
                    'APELLIDOS' => 'FORERO MIRANDA',
                    'CARGO' => 'AUXILIAR DE AULA',
                    'CORREO' => '',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '35198427',
                    'NOMBRES' => 'JENNY ALEXANDRA              ',
                    'APELLIDOS' => 'FRANCO NIETO                      ',
                    'CARGO' => 'DOCENTE',
                    'CORREO' => 'jenny.franco@lcb.edu.co',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '1033688006',
                    'NOMBRES' => 'JENNY ROCIO',
                    'APELLIDOS' => 'ROMERO RUBIO ',
                    'CARGO' => 'APRENDIZ DEL SENA   ',
                    'CORREO' => '',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '1019086548',
                    'NOMBRES' => 'JESSICA ALEJANDRA',
                    'APELLIDOS' => 'SANTIAGO PULIDO',
                    'CARGO' => 'DOCENTE',
                    'CORREO' => 'jessica.santiago@lcb.edu.co',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '80852273',
                    'NOMBRES' => 'JHON FREDT',
                    'APELLIDOS' => 'POSADA POSADA                          ',
                    'CARGO' => 'DOCENTE',
                    'CORREO' => 'jhon.posada@lcb.edu.co',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '1014277208',
                    'NOMBRES' => 'JHON SEBASTIAN',
                    'APELLIDOS' => 'HERNANDEZ AGUILERA',
                    'CARGO' => 'DOCENTE',
                    'CORREO' => 'jhon.hernandez@lcb.edu.co',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '80244417',
                    'NOMBRES' => 'JOSE ARGEMIRO ',
                    'APELLIDOS' => 'DUARTE RINCON',
                    'CARGO' => 'DOCENTE',
                    'CORREO' => 'jose.duarte@lcb.edu.co',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '494977',
                    'NOMBRES' => 'JOSIMAR CAROLINA',
                    'APELLIDOS' => 'GONZALEZ DEL MAR ',
                    'CARGO' => 'DOCENTE',
                    'CORREO' => 'josimar.gonzalez@lcb.edu.co',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '79944394',
                    'NOMBRES' => 'JUAN MAURICIO ',
                    'APELLIDOS' => 'RODRIGUEZ LEON                      ',
                    'CARGO' => 'DOCENTE',
                    'CORREO' => 'juanmauricio.rodriguez@lcb.edu.co',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '51687370',
                    'NOMBRES' => 'JULIA ASTRID',
                    'APELLIDOS' => 'DUQUE MARTINEZ',
                    'CARGO' => 'DOCENTE',
                    'CORREO' => 'julia.duque@lcb.edu.co',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => 'CE 1095924',
                    'NOMBRES' => 'JULIAN LOUIS',
                    'APELLIDOS' => 'BUTTIGIEG',
                    'CARGO' => 'DOCENTE',
                    'CORREO' => '',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '1071165268',
                    'NOMBRES' => 'JULIETH ALEXANDRA             ',
                    'APELLIDOS' => 'AYALA VENEGAS                             ',
                    'CARGO' => 'DOCENTE',
                    'CORREO' => 'julieth.ayala@lcb.edu.co',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '1010164225',
                    'NOMBRES' => 'JULIO CESAR ',
                    'APELLIDOS' => 'PARRAGA BELTRAN',
                    'CARGO' => 'DOCENTE',
                    'CORREO' => 'julio.parraga@lcb.edu.co',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '52932323',
                    'NOMBRES' => 'KAREN MARIA ',
                    'APELLIDOS' => 'NEWBALL BENITEZ',
                    'CARGO' => 'DOCENTE',
                    'CORREO' => 'karen.newball@lcb.edu.co',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '53133028',
                    'NOMBRES' => 'KAREN PAOLA ',
                    'APELLIDOS' => 'LEGUIZAMO PAEZ                        ',
                    'CARGO' => 'DOCENTE',
                    'CORREO' => 'karen.leguizamo@lcb.edu.co',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '53178899',
                    'NOMBRES' => 'KATERINE',
                    'APELLIDOS' => 'DIAZ RODRIGUEZ',
                    'CARGO' => 'DOCENTE',
                    'CORREO' => 'katerine.diaz@lcb.edu.co',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => 'PAS 640483598',
                    'NOMBRES' => 'KATHERINE MARIE',
                    'APELLIDOS' => 'BOCHNIA',
                    'CARGO' => 'DOCENTE',
                    'CORREO' => 'katherine.bochnia@lcb.edu.co',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '1014296430',
                    'NOMBRES' => 'KATTERINE',
                    'APELLIDOS' => 'CONTRERAS CORTES',
                    'CARGO' => 'APRENDIZ DEL SENA   ',
                    'CORREO' => '',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => 'PAS 561606425',
                    'NOMBRES' => 'KIRAN KUMAR',
                    'APELLIDOS' => 'SAHONTA',
                    'CARGO' => 'DOCENTE',
                    'CORREO' => 'kiran.sahonta@lcb.edu.co',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '1022381222',
                    'NOMBRES' => 'LADY VIVIANA',
                    'APELLIDOS' => 'CHICUAZUQUE ÁVILA',
                    'CARGO' => 'DOCENTE',
                    'CORREO' => 'lady.chicuazuque@lcb.edu.co',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '1022332343',
                    'NOMBRES' => 'LAURA               ',
                    'APELLIDOS' => 'SARMIENTO AHUMADA                           ',
                    'CARGO' => 'DOCENTE',
                    'CORREO' => 'laura.sarmiento@lcb.edu.co',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '1070919813',
                    'NOMBRES' => 'LAURA CATALINA',
                    'APELLIDOS' => 'CRUZ MARTINEZ',
                    'CARGO' => 'DOCENTE',
                    'CORREO' => 'laura.cruz@lcb.edu.co',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '1030565504',
                    'NOMBRES' => 'LAURA CATALINA ',
                    'APELLIDOS' => 'ESCOBAR GIRALDO',
                    'CARGO' => 'DOCENTE',
                    'CORREO' => 'laura.escobar@lcb.edu.co',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '1030632635',
                    'NOMBRES' => 'LAURA KATHERIN ',
                    'APELLIDOS' => 'SIERRA MORENO                      ',
                    'CARGO' => 'DOCENTE',
                    'CORREO' => 'laura.sierra@lcb.edu.co',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '1057575973',
                    'NOMBRES' => 'LAURA MARCELA ',
                    'APELLIDOS' => 'GUERRERO AMADO                      ',
                    'CARGO' => 'DOCENTE',
                    'CORREO' => 'laura.guerrero@lcb.edu.co',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '1022379577',
                    'NOMBRES' => 'LAURA MARIA',
                    'APELLIDOS' => 'VILLAGRAN GUTIERREZ',
                    'CARGO' => 'DOCENTE',
                    'CORREO' => 'laura.villagran@lcb.edu.co',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '1233891608',
                    'NOMBRES' => 'LAURA TATIANA',
                    'APELLIDOS' => 'SOTELO TORRES                       ',
                    'CARGO' => 'AUXILIAR DE AULA',
                    'CORREO' => '',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '1070972886',
                    'NOMBRES' => 'LAURA VANESSA ',
                    'APELLIDOS' => 'MORENO CANO                         ',
                    'CARGO' => 'DOCENTE',
                    'CORREO' => 'laura.moreno@lcb.edu.co',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '1015437423',
                    'NOMBRES' => 'LAURA YESENIA',
                    'APELLIDOS' => 'SOLANO VINCHIRA',
                    'CARGO' => 'DOCENTE',
                    'CORREO' => 'laura.solano@lcb.edu.co',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '1075674806',
                    'NOMBRES' => 'LEIDY LORENA ',
                    'APELLIDOS' => 'GARCIA FORERO                        ',
                    'CARGO' => 'AUXILIAR DE AULA ',
                    'CORREO' => '',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '1058431079',
                    'NOMBRES' => 'LEIDY PAOLA ',
                    'APELLIDOS' => 'PAVA AVENDAÑO ',
                    'CARGO' => 'AUXILIAR DE CICLO',
                    'CORREO' => 'auxiliarprimaria@lcb.edu.co',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '1032458115',
                    'NOMBRES' => 'LHASSA              ',
                    'APELLIDOS' => 'QUIJANO LIZARRALDE                         ',
                    'CARGO' => 'DOCENTE',
                    'CORREO' => 'lhassa.quijano@lcb.edu.co',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '1140415315',
                    'NOMBRES' => 'LILYBETH',
                    'APELLIDOS' => 'ALVARADO ANCINEZ',
                    'CARGO' => 'DOCENTE',
                    'CORREO' => 'lilybeth.alvarado@lcb.edu.co',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '1020778295',
                    'NOMBRES' => 'LINA MARIA',
                    'APELLIDOS' => 'PERDOMO GUZMAN',
                    'CARGO' => 'DOCENTE',
                    'CORREO' => 'lina.perdomo@lcb.edu.co',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '1020741571',
                    'NOMBRES' => 'LINA MARIA ',
                    'APELLIDOS' => 'BOSHELL VILLAMIZAR',
                    'CARGO' => 'DOCENTE',
                    'CORREO' => '',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '1032475531',
                    'NOMBRES' => 'LINDA ESTEFANY ',
                    'APELLIDOS' => 'RODRIGUEZ VARGAS',
                    'CARGO' => 'DOCENTE',
                    'CORREO' => 'linda.rodriguez@lcb.edu.co',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '22641199',
                    'NOMBRES' => 'LISBETH MARGARITA            ',
                    'APELLIDOS' => 'PULIDO MATOS                    ',
                    'CARGO' => 'DOCENTE',
                    'CORREO' => 'lisbeth.pulido@lcb.edu.co',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '1018441826',
                    'NOMBRES' => 'LORENA              ',
                    'APELLIDOS' => 'RODRIGUEZ MORA                             ',
                    'CARGO' => 'AUXILIAR DE CICLO PR',
                    'CORREO' => 'asistentepreescolar@lcb.edu.co',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '80183831',
                    'NOMBRES' => 'LUIS ALEJANDRO ',
                    'APELLIDOS' => 'CASTRO URREGO',
                    'CARGO' => 'DOCENTE',
                    'CORREO' => 'luis.castro@lcb.edu.co',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '1072193235',
                    'NOMBRES' => 'LUIS GABRIEL',
                    'APELLIDOS' => 'GONZALEZ CANTOR',
                    'CARGO' => 'DOCENTE',
                    'CORREO' => 'gabriel.gonzalez@lcb.edu.co',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '1013621456',
                    'NOMBRES' => 'LUISA FERNANDA ',
                    'APELLIDOS' => 'ORTEGA GUTIERREZ',
                    'CARGO' => 'DOCENTE',
                    'CORREO' => 'luisa.ortega@lcb.edu.co',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '1056483749',
                    'NOMBRES' => 'LUZ DARY',
                    'APELLIDOS' => 'JEREZ JEREZ',
                    'CARGO' => 'APRENDIZ DEL SENA   ',
                    'CORREO' => '',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => 'PAS 736847631',
                    'NOMBRES' => 'MARIA',
                    'APELLIDOS' => 'PRONINA',
                    'CARGO' => 'DOCENTE',
                    'CORREO' => '',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '39785381',
                    'NOMBRES' => 'MARIA AZUCENA',
                    'APELLIDOS' => 'LANCHEROS BELLO',
                    'CARGO' => 'DOCENTE',
                    'CORREO' => '',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '51769020',
                    'NOMBRES' => 'MARIA CLAUDIA ',
                    'APELLIDOS' => 'ROMERO POTTES                       ',
                    'CARGO' => 'DOCENTE',
                    'CORREO' => 'maria.romero@lcb.edu.co',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '1019071247',
                    'NOMBRES' => 'MARTHA LILIANA',
                    'APELLIDOS' => 'VEGA AVILA',
                    'CARGO' => 'AUXILIAR DE AULA',
                    'CORREO' => '',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '1030630315',
                    'NOMBRES' => 'MAYRA SORAYA ',
                    'APELLIDOS' => 'GARZON MUTIS                         ',
                    'CARGO' => 'DOCENTE',
                    'CORREO' => '',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => 'PAS 563754629',
                    'NOMBRES' => 'MEGAN LAUREL',
                    'APELLIDOS' => 'ATKIN',
                    'CARGO' => 'DOCENTE',
                    'CORREO' => '',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '1010227565',
                    'NOMBRES' => 'MELANEE',
                    'APELLIDOS' => 'ROJAS MORENO',
                    'CARGO' => 'AUXILIAR DE AULA',
                    'CORREO' => '',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '1072650187',
                    'NOMBRES' => 'MIGUEL RICARDO',
                    'APELLIDOS' => 'ALARCON ALFONSO',
                    'CARGO' => 'DOCENTE',
                    'CORREO' => '',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '1032436011',
                    'NOMBRES' => 'MONICA              ',
                    'APELLIDOS' => 'GUTIERREZ DE PIÑERES',
                    'CARGO' => 'DOCENTE',
                    'CORREO' => 'monica.gutierrez@lcb.edu.co',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '53051506',
                    'NOMBRES' => 'NATALIA             ',
                    'APELLIDOS' => 'ZUÑIGA PIÑEROS                            ',
                    'CARGO' => 'DOCENTE',
                    'CORREO' => 'natalia.zuniga@lcb.edu.co',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '52541942',
                    'NOMBRES' => 'NATALIA ANDREA             ',
                    'APELLIDOS' => 'CASTILLO RODRIGUEZ                 ',
                    'CARGO' => 'DOCENTE',
                    'CORREO' => 'natalia.castillo@lcb.edu.co',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '1030656574',
                    'NOMBRES' => 'NATALY ',
                    'APELLIDOS' => 'REINA ALFONSO',
                    'CARGO' => 'AUXILIAR DE AULA',
                    'CORREO' => '',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '35424837',
                    'NOMBRES' => 'NELLY YOHANNA ',
                    'APELLIDOS' => 'VANEGAS MARTINEZ',
                    'CARGO' => 'AUXILIAR DE AULA PRE',
                    'CORREO' => '',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '7702757',
                    'NOMBRES' => 'NESTOR MAURICIO',
                    'APELLIDOS' => 'DIAZ PEREZ',
                    'CARGO' => 'DOCENTE',
                    'CORREO' => 'mauricio.diaz@lcb.edu.co',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '1019091822',
                    'NOMBRES' => 'NICOL STHEFANIA',
                    'APELLIDOS' => 'ALVARADO  AGUDELO',
                    'CARGO' => 'AUXILIAR DE AULA',
                    'CORREO' => '',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '52343201',
                    'NOMBRES' => 'OLGA LUCIA ',
                    'APELLIDOS' => 'FRANCO SUAREZ                          ',
                    'CARGO' => 'DOCENTE',
                    'CORREO' => 'olga.franco@lcb.edu.co',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '81754694',
                    'NOMBRES' => 'OSCAR FABIÁN',
                    'APELLIDOS' => 'LÓPEZ JIMENEZ',
                    'CARGO' => 'DOCENTE',
                    'CORREO' => '',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '79954263',
                    'NOMBRES' => 'PABLO ALDEMAR',
                    'APELLIDOS' => 'TIQUE GUEVARA',
                    'CARGO' => 'DOCENTE',
                    'CORREO' => 'pablo.tique@lcb.edu.co',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '1014187673',
                    'NOMBRES' => 'PAULA ALEJANDRA',
                    'APELLIDOS' => 'ROCHA SEGURA',
                    'CARGO' => 'DOCENTE',
                    'CORREO' => 'paula.rocha@lcb.edu.co',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '1015443911',
                    'NOMBRES' => 'PAULA ALEJANDRA ',
                    'APELLIDOS' => 'SANDOVAL VARGAS ',
                    'CARGO' => 'AUXILIAR DE AULA',
                    'CORREO' => '',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '1010203188',
                    'NOMBRES' => 'PAULA CAMILA ',
                    'APELLIDOS' => 'PAEZ OVIEDO ',
                    'CARGO' => 'DOCENTE',
                    'CORREO' => 'paula.paez@lcb.edu.co',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '1018408278',
                    'NOMBRES' => 'PAULA KARINA',
                    'APELLIDOS' => 'ROBLES SUAREZ',
                    'CARGO' => 'DOCENTE',
                    'CORREO' => '',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '496428',
                    'NOMBRES' => 'PHILIP DANIEL',
                    'APELLIDOS' => 'BRUCHMAN',
                    'CARGO' => 'DOCENTE',
                    'CORREO' => '',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '1076768',
                    'NOMBRES' => 'RAVINDER KUMAR',
                    'APELLIDOS' => 'KINNERA',
                    'CARGO' => 'DOCENTE',
                    'CORREO' => '',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '1010222241',
                    'NOMBRES' => 'SUSAN CRISTINA',
                    'APELLIDOS' => 'AMAYA CORTES',
                    'CARGO' => 'DOCENTE',
                    'CORREO' => 'susan.amaya@lcb.edu.co',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '52823427',
                    'NOMBRES' => 'TATIANA',
                    'APELLIDOS' => 'ACUÑA HERNANDEZ',
                    'CARGO' => 'DOCENTE',
                    'CORREO' => '',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '1016049071',
                    'NOMBRES' => 'VALERIA',
                    'APELLIDOS' => 'DIAZ CORREA',
                    'CARGO' => 'DOCENTE',
                    'CORREO' => 'valeria.diaz@lcb.edu.co',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '1030553434',
                    'NOMBRES' => 'VICTORIA            ',
                    'APELLIDOS' => 'GARCIA FRANCO                            ',
                    'CARGO' => 'DOCENTE',
                    'CORREO' => 'victoria.garcia@lcb.edu.co',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '1019101498',
                    'NOMBRES' => 'WENDY PAOLA',
                    'APELLIDOS' => 'SANCHEZ IBAGUE',
                    'CARGO' => 'AUXILIAR DE CICLO',
                    'CORREO' => 'asistenteprimaria@lcb.edu.co',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '1069753988',
                    'NOMBRES' => 'WENDY YURANY',
                    'APELLIDOS' => 'GARCIA PEREZ',
                    'CARGO' => 'DOCENTE',
                    'CORREO' => '',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '1072703751',
                    'NOMBRES' => 'WILDER FABIAN ',
                    'APELLIDOS' => 'GOMEZ MORA                          ',
                    'CARGO' => 'DOCENTE',
                    'CORREO' => 'wilder.gomez@lcb.edu.co',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => 'PAS 545830222',
                    'NOMBRES' => 'WILLIAM EDWARD',
                    'APELLIDOS' => 'DICKEY',
                    'CARGO' => 'DOCENTE',
                    'CORREO' => '',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '1013645350',
                    'NOMBRES' => 'YEINNY ZARIK ',
                    'APELLIDOS' => 'VEGA PORRAS',
                    'CARGO' => 'DOCENTE',
                    'CORREO' => '',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '1016104588',
                    'NOMBRES' => 'YENIFER YULIET',
                    'APELLIDOS' => 'QUINTERO ARDILA',
                    'CARGO' => 'AUXILIAR DE AULA',
                    'CORREO' => '',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '1030672563',
                    'NOMBRES' => 'YESSICA DANIELA',
                    'APELLIDOS' => 'BERMUDEZ VARGAS',
                    'CARGO' => 'DOCENTE',
                    'CORREO' => 'yessica.bermudez@lcb.edu.co',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '1069729182',
                    'NOMBRES' => 'YULY ANDREA               ',
                    'APELLIDOS' => 'MORENO SOSA                           ',
                    'CARGO' => 'DOCENTE',
                    'CORREO' => 'yuli.moreno@lcb.edu.co',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => '52780640',
                    'NOMBRES' => 'ZULMA YOLANDA ',
                    'APELLIDOS' => 'LIZARAZO SALAMANCA',
                    'CARGO' => 'DOCENTE',
                    'CORREO' => '',
                    'ROLE' => 'empleado',
                ],
            ],
            [
                [
                    'CEDULA' => 1022354611,
                    'NOMBRES' => 'DIANA PAOLA',
                    'APELLIDOS' => 'PUERTO GARCES',
                    'CARGO' => 'JEFE DE ENFERMERIA ',
                    'CORREO' => 'enfermeria@lcb.edu.co',
                    'ROLE' => 'coordinador',
                ],
                [
                    'CEDULA' => 1073510056,
                    'NOMBRES' => 'LADY NATALIA ',
                    'APELLIDOS' => 'CASTILLO CHACON ',
                    'CARGO' => 'AUXILIAR DE ENFERMERIA',
                    'CORREO' => '',
                    'ROLE' => 'empleado',
                ],
            ],
            [],
            [
                [
                    'CEDULA' => 51896313,
                    'NOMBRES' => 'MARIA ROSALBA ',
                    'APELLIDOS' => 'WILCHES CASTIBLANCO                 ',
                    'CARGO' => 'ASISTENTE DE GERENCIA OPERATIVA',
                    'CORREO' => 'asistentego@lcb.edu.co',
                    'ROLE' => 'coordinador',
                ],
                [
                    'CEDULA' => 80021137,
                    'NOMBRES' => 'CARLOS ALBERTO ',
                    'APELLIDOS' => 'MOLANO AVENDAÑO                    ',
                    'CARGO' => 'AUXILIAR DE MANTENIMIENTO',
                    'CORREO' => '',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => 7173292,
                    'NOMBRES' => 'FERNANDO            ',
                    'APELLIDOS' => 'CALLEJAS                                 ',
                    'CARGO' => 'ENCARGADO DE MANTENIMIENTO',
                    'CORREO' => '',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => 91015481,
                    'NOMBRES' => 'LEONEL ',
                    'APELLIDOS' => 'NIEVES AREVALO                             ',
                    'CARGO' => 'AUXILIAR DE MANTENIMIENTO',
                    'CORREO' => '',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => 80258507,
                    'NOMBRES' => 'ROGELIO',
                    'APELLIDOS' => 'YACUMA TAO',
                    'CARGO' => 'AUXILIAR DE MANTENIMIENTO',
                    'CORREO' => '',
                    'ROLE' => 'empleado',
                ],
            ],
            [
                [
                    'CEDULA' => 91112056,
                    'NOMBRES' => 'JOSUE ',
                    'APELLIDOS' => 'OTERO LARROTTA',
                    'CARGO' => 'ADMINISTRADOR DE RESTAURANTE ESCOLAR',
                    'CORREO' => 'adminrestaurante@lcb.edu.co',
                    'ROLE' => 'coordinador',
                ],
                [
                    'CEDULA' => 1066721539,
                    'NOMBRES' => 'AGRIPINA DEL CARMEN ',
                    'APELLIDOS' => 'MARTINEZ SUAREZ',
                    'CARGO' => 'AUXILIAR DE SERVICIOS GENERALES',
                    'CORREO' => '',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => 52644547,
                    'NOMBRES' => 'ALBA ROCIO',
                    'APELLIDOS' => 'PEREZ TABAREZ',
                    'CARGO' => 'AUXILIAR DE COCINA  ',
                    'CORREO' => '',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => 52444393,
                    'NOMBRES' => 'ALIRIS',
                    'APELLIDOS' => 'RANGEL GIL',
                    'CARGO' => 'AUXILIAR DE SERVICIOS GENERALES',
                    'CORREO' => '',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => 1056074927,
                    'NOMBRES' => 'ANA HERMENCIA ',
                    'APELLIDOS' => 'HUERTAS CARO',
                    'CARGO' => 'AUXILIAR DE SERVICIOS GENERALES',
                    'CORREO' => '',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => 1068576128,
                    'NOMBRES' => 'ANA VICTORIA ',
                    'APELLIDOS' => 'IZQUIERDO ESTRADA',
                    'CARGO' => 'AUXILIAR DE SERVICIOS GENERALES',
                    'CORREO' => '',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => 35475679,
                    'NOMBRES' => 'BETZABE ',
                    'APELLIDOS' => 'MORA ORTIZ                                ',
                    'CARGO' => 'ENCARGADO DE COMEDOR',
                    'CORREO' => '',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => 1032485921,
                    'NOMBRES' => 'CARLOS ANDRES ',
                    'APELLIDOS' => 'GAMEZ JIMENEZ',
                    'CARGO' => 'COCINERO ENCARGADO',
                    'CORREO' => '',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => 55065637,
                    'NOMBRES' => 'CECILIA',
                    'APELLIDOS' => 'SANDOVAL VALDERRAMA',
                    'CARGO' => 'AUXILIAR DE SERVICIOS GENERALES',
                    'CORREO' => '',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => 1013618222,
                    'NOMBRES' => 'DERLI YURANI',
                    'APELLIDOS' => 'VARGAS ROMERO',
                    'CARGO' => 'AUXILIAR DE SERVICIOS GENERALES',
                    'CORREO' => '',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => 1080184113,
                    'NOMBRES' => 'DIANA KATHERINE ',
                    'APELLIDOS' => 'VARGAS ROCHA                      ',
                    'CARGO' => 'SUPERVISORA DE SERVICIOS GENERALES',
                    'CORREO' => '',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => 52692918,
                    'NOMBRES' => 'DIANA LICETH ',
                    'APELLIDOS' => 'LANCHEROS MORALES',
                    'CARGO' => 'AUXILIAR DE SERVICIOS GENERALES',
                    'CORREO' => '',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => 1020730225,
                    'NOMBRES' => 'DIEGO ARMANDO',
                    'APELLIDOS' => 'BARON HERNANDEZ',
                    'CARGO' => 'CHEF',
                    'CORREO' => 'chef@lcb.edu.co',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => 24495580,
                    'NOMBRES' => 'FABIOLA             ',
                    'APELLIDOS' => 'ARANGO ALVAREZ                            ',
                    'CARGO' => 'AUXILIAR DE SERVICIOS GENERALES',
                    'CORREO' => '',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => 50916972,
                    'NOMBRES' => 'GLORIA INES ',
                    'APELLIDOS' => 'PEREZ PESTANA                      ',
                    'CARGO' => 'AUXILIAR DE SERVICIOS GENERALES',
                    'CORREO' => '',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => 52863757,
                    'NOMBRES' => 'GLORIA PATRICIA ',
                    'APELLIDOS' => 'RIVERA GIL',
                    'CARGO' => 'AUXILIAR DE COCINA  ',
                    'CORREO' => '',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => 1020772398,
                    'NOMBRES' => 'JENNY CAROLINA ',
                    'APELLIDOS' => 'CARDENAS',
                    'CARGO' => 'AUXILIAR DE SERVICIOS GENERALES',
                    'CORREO' => '',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => 1049899539,
                    'NOMBRES' => 'KATHIA',
                    'APELLIDOS' => 'CASTRO RANGEL',
                    'CARGO' => 'AUXILIAR DE SERVICIOS GENERALES',
                    'CORREO' => '',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => 34946543,
                    'NOMBRES' => 'KATIA PATRICIA',
                    'APELLIDOS' => 'CANCHILA ARIAS',
                    'CARGO' => 'AUXILIAR DE COCINA  ',
                    'CORREO' => '',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => 1042348384,
                    'NOMBRES' => 'KELLY DEL CARMEN ',
                    'APELLIDOS' => 'ARIZA BRIEVA',
                    'CARGO' => 'AUXILIAR DE SERVICIOS GENERALES',
                    'CORREO' => '',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => 53068713,
                    'NOMBRES' => 'LINA TATIANA ',
                    'APELLIDOS' => 'MONTENEGRO RIVERA',
                    'CARGO' => 'AUXILIAR DE SERVICIOS GENERALES',
                    'CORREO' => '',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => 1020812706,
                    'NOMBRES' => 'LUISA FERNANDA ',
                    'APELLIDOS' => 'REYES LOPEZ                         ',
                    'CARGO' => 'AUXILIAR DE SERVICIOS GENERALES',
                    'CORREO' => '',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => 52863160,
                    'NOMBRES' => 'LUZ ANGELA ',
                    'APELLIDOS' => 'LAMPREA NOVA',
                    'CARGO' => 'AUXILIAR DE SERVICIOS GENERALES',
                    'CORREO' => '',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => 51963181,
                    'NOMBRES' => 'MARIA CLARA ',
                    'APELLIDOS' => 'VARGAS OBANDO                         ',
                    'CARGO' => 'AUXILIAR  DE SERVICIOS GENERALES',
                    'CORREO' => '',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => 51626599,
                    'NOMBRES' => 'MARIA ELISA               ',
                    'APELLIDOS' => 'PINEDA PINEDA                         ',
                    'CARGO' => 'AUXILIAR DE COCINA  ',
                    'CORREO' => '',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => 1018432576,
                    'NOMBRES' => 'MARY JULIETH ',
                    'APELLIDOS' => 'CRUZ URBANO',
                    'CARGO' => 'AUXILIAR DE SERVICIOS GENERALES',
                    'CORREO' => '',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => 52202618,
                    'NOMBRES' => 'MARY PATRICIA ',
                    'APELLIDOS' => 'CASTRO VARGAS',
                    'CARGO' => 'AUXILIAR DE SERVICIOS GENERALES',
                    'CORREO' => '',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => 52508472,
                    'NOMBRES' => 'MERCI',
                    'APELLIDOS' => 'LOAIZA DUCUARA',
                    'CARGO' => 'AUXILIAR DE SERVICIOS GENERALES',
                    'CORREO' => '',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => 1020734329,
                    'NOMBRES' => 'OLGA LUCIA ',
                    'APELLIDOS' => 'BELTRAN RODRIGUEZ',
                    'CARGO' => 'AUXILIAR DE SERVICIOS GENERALES',
                    'CORREO' => '',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => 39804411,
                    'NOMBRES' => 'RUBIELA             ',
                    'APELLIDOS' => 'CRUZ TIBAVIZCO                            ',
                    'CARGO' => 'AUXILIAR DE SERVICIOS GENERALES',
                    'CORREO' => '',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => 51790644,
                    'NOMBRES' => 'SANDRA MARIA              ',
                    'APELLIDOS' => 'CORREA BARBOSA                       ',
                    'CARGO' => 'GERENTE OPERATIVA   ',
                    'CORREO' => 'gerenteoperativa@lcb.edu.co',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => 30349267,
                    'NOMBRES' => 'YOLANDA',
                    'APELLIDOS' => 'MARTINEZ BEJARANO',
                    'CARGO' => 'AUXILIAR DE COCINA  ',
                    'CORREO' => '',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => 52987965,
                    'NOMBRES' => 'YUDY MARCELA ',
                    'APELLIDOS' => 'CAMELO GOMEZ',
                    'CARGO' => 'AUXILIAR DE SERVICIOS GENERALES',
                    'CORREO' => '',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => 53125119,
                    'NOMBRES' => 'ZULAY LORENA ',
                    'APELLIDOS' => 'LUQUE VELASQUEZ',
                    'CARGO' => 'AUXILIAR DE SERVICIOS GENERALES',
                    'CORREO' => '',
                    'ROLE' => 'empleado',
                ],
            ],
            [
                [
                    'CEDULA' => 1014198530,
                    'NOMBRES' => 'EDWIN YEZID ',
                    'APELLIDOS' => 'GUTIERREZ RIAÑO                       ',
                    'CARGO' => 'ANALISTA DE SISTEMAS',
                    'CORREO' => 'sistemas@lcb.edu.co',
                    'ROLE' => 'coordinador',
                ],
                [
                    'CEDULA' => 3110053,
                    'NOMBRES' => 'JHON JAIRO',
                    'APELLIDOS' => 'ARCE BERRIO',
                    'CARGO' => 'INTEGRADOR TIC',
                    'CORREO' => 'integrador@lcb.edu.co',
                    'ROLE' => 'coordinador',
                ],
                [
                    'CEDULA' => 1012465099,
                    'NOMBRES' => 'ANDRES FELIPE',
                    'APELLIDOS' => 'CABEZAS BERNAL',
                    'CARGO' => 'ANALISTA DE SISTEMAS',
                    'CORREO' => 'soporte@lcb.edu.co',
                    'ROLE' => 'coordinador',
                ],
            ],
            [
                [
                    'CEDULA' => 52800678,
                    'NOMBRES' => 'NANCY               ',
                    'APELLIDOS' => 'AMAYA MEDINA                                ',
                    'CARGO' => 'LIDER DE TALENTO HUMANO ',
                    'CORREO' => 'talentohumano@lcb.edu.co',
                    'ROLE' => 'coordinador',
                ],
                [
                    'CEDULA' => 53028435,
                    'NOMBRES' => 'JOHANNA CAROLINA',
                    'APELLIDOS' => 'SAY SONSA',
                    'CARGO' => 'ANALISTA DE TALENTO HUMANO',
                    'CORREO' => 'asistentetalento@lcb.edu.co',
                    'ROLE' => 'empleado',
                ],
            ],
            [
                [
                    'CEDULA' => 1026261018,
                    'NOMBRES' => 'MARIA FERNANDA',
                    'APELLIDOS' => 'BETANCOURT ORTIZ ',
                    'CARGO' => 'COORDINADORA DE COMUNICACIONES',
                    'CORREO' => 'comunicaciones@lcb.edu.co',
                    'ROLE' => 'coordinador',
                ],
                [
                    'CEDULA' => 1072667844,
                    'NOMBRES' => 'OSCAR ALEXANDER',
                    'APELLIDOS' => 'MORENO OLAYA',
                    'CARGO' => 'DISEÑADOR DE COMUNICACIONES',
                    'CORREO' => 'disenografico@lcb.edu.co',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => 1121923873,
                    'NOMBRES' => 'SANTIAGO ',
                    'APELLIDOS' => 'GAONA RAMIREZ',
                    'CARGO' => 'COMUMUNITY MANAGER',
                    'CORREO' => '',
                    'ROLE' => 'empleado',
                ],
            ],
            [
                [
                    'CEDULA' => 19302941,
                    'NOMBRES' => 'RICARDO             ',
                    'APELLIDOS' => 'PINTO ROJAS                               ',
                    'CARGO' => 'LIDER DE ADMISIONES',
                    'CORREO' => 'admisiones@lcb.edu.co',
                    'ROLE' => 'coordinador',
                ],
                [
                    'CEDULA' => 52997008,
                    'NOMBRES' => 'ANA MARIA ',
                    'APELLIDOS' => 'RICCI SEPULVEDA',
                    'CARGO' => 'PSICOLOGA DE ADMISIONES',
                    'CORREO' => 'psicologa.admisiones@lcb.edu.co',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => 53008628,
                    'NOMBRES' => 'ANDREA BIBIANA ',
                    'APELLIDOS' => 'CASTIBLANCO AGUILERA               ',
                    'CARGO' => 'ASISTENTE DE ADMISIONES',
                    'CORREO' => 'asistenteadmisiones@lcb.edu.co',
                    'ROLE' => 'empleado',
                ],
            ],
            [
                [
                    'CEDULA' => 52355841,
                    'NOMBRES' => 'LUZ MERY ',
                    'APELLIDOS' => 'PEREZ HERRERA                            ',
                    'CARGO' => 'COORDINADORA FINANCIERA',
                    'CORREO' => 'financiera@lcb.edu.co',
                    'ROLE' => 'coordinador',
                ],
                [
                    'CEDULA' => 52928875,
                    'NOMBRES' => 'MARIA ANGELICA ',
                    'APELLIDOS' => 'MARTINEZ PINEDA                    ',
                    'CARGO' => 'CONTADOR            ',
                    'CORREO' => 'contabilidad@lcb.edu.co',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => 65716226,
                    'NOMBRES' => 'ROSALBA             ',
                    'APELLIDOS' => 'CARRILLO CASTELLANOS                      ',
                    'CARGO' => 'TESORERA            ',
                    'CORREO' => 'tesoreria@lcb.edu.co',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => 1233890018,
                    'NOMBRES' => 'DANIEL ENRIQUE',
                    'APELLIDOS' => 'LAMPREA URICOCHEA',
                    'CARGO' => 'ANALISTA CONTABLE',
                    'CORREO' => 'analistacontable@lcb.edu.co',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => 1014197228,
                    'NOMBRES' => 'JENNY ASTRID ',
                    'APELLIDOS' => 'BECERRA FUENTES                      ',
                    'CARGO' => 'AUXILIAR DE COMPRAS',
                    'CORREO' => 'compras@lcb.edu.co',
                    'ROLE' => 'empleado',
                ],
                [
                    'CEDULA' => 1031173902,
                    'NOMBRES' => 'FABIO ANDRES',
                    'APELLIDOS' => 'ALVAREZ RUIZ',
                    'CARGO' => 'ANALISTA DE TESORERIA',
                    'CORREO' => 'tesoreria1@lcb.edu.co',
                    'ROLE' => 'empleado',
                ],
            ],
            [
                [
                    'CEDULA' => 1032399098,
                    'NOMBRES' => 'CARLOS ALEJANDRO ',
                    'APELLIDOS' => 'MORENO HERNANDEZ',
                    'CARGO' => 'COORDINADOR DE TRANSPORTE',
                    'CORREO' => 'auxiliartransporte@lcb.edu.co',
                    'ROLE' => 'coordinador',
                ],
                [
                    'CEDULA' => 51737333,
                    'NOMBRES' => 'CARMEN ROSA ',
                    'APELLIDOS' => 'HERNANDEZ CAMACHO                     ',
                    'CARGO' => 'ASISTENTE DE TRANSPORTE',
                    'CORREO' => 'asistentetransporte@lcb.edu.co',
                    'ROLE' => 'empleado',
                ],
            ],
            [
                [
                    "CEDULA" => 80193261,
                    "NOMBRES" => "YOLIMA",
                    "APELLIDOS" => "RUIZ PACHECO",
                    "CARGO" => "COORDINADOR DE BIENESTAR Y CULTURA",
                    "CORREO" => "bienestar@lcb.edu.co",
                    "ROLE" => "coordinador",
                ]
            ],
            [
                [
                    'CEDULA' => 52645951,
                    'NOMBRES' => 'MARIA CRISTINA               ',
                    'APELLIDOS' => 'CARDENAS TORRES                    ',
                    'CARGO' => 'SECRETARIA ACADEMICA',
                    'CORREO' => 'secretaria.academica@lcb.edu.co',
                    'ROLE' => 'coordinador',
                ],
            ],
            [
                [
                    'CEDULA' => 52582882,
                    'NOMBRES' => 'LUZ OBEIDA               ',
                    'APELLIDOS' => 'JARAMILLO GONZALEZ                     ',
                    'CARGO' => 'RECEPCIONISTA',
                    'CORREO' => 'asistenteadministrativa@lcb.edu.co',
                    'ROLE' => 'coordinador',
                ],
            ],
            [
                [
                    'CEDULA' => 1101176243,
                    'NOMBRES' => 'LINA MARIA',
                    'APELLIDOS' => 'MARTINEZ MOSQUERA',
                    'CARGO' => 'COORDINADOR ADMINISTRATIVO',
                    'CORREO' => 'coord.admin@lcb.edu.co',
                    'ROLE' => 'coordinador',
                ],
            ],
            [
                [
                    'CEDULA' => 52080697,
                    'NOMBRES' => 'MARIA DEL PILAR',
                    'APELLIDOS' => 'BARRIGA LOSADA',
                    'CARGO' => 'COORDINADORA SISTEMAS DE GESTIÓN',
                    'CORREO' => 'coord.sistemasdegestion@lcb.edu.co',
                    'ROLE' => 'coordinador',
                ],
            ]
        ];
        //$notification = new SystemNotification([]);
        $update = 0;
        $create = 0;
        foreach ($usersByArea as $key => $area) {
            foreach ($area as $user) {
                $data = $user;
                $password = str_random(6);
                $encriptedPassword = bcrypt($password);
                $slugName = str_slug($data['NOMBRES'] . ' ' . $data['APELLIDOS']);
                $correo = str_replace_array(' ', [''], $data['CORREO']);
                $correo =  $correo ? $correo : $slugName."@lcb.edu.co";
                $insertUser = User::where('email', $correo)->first();

                if ($insertUser) {
                    // 'name', 'email', 'password', 'cedula', 'cargo'
                    $insertUser->name = title_case(str_replace('-', ' ', $slugName));
                    //$insertUser->password = $encriptedPassword;
                    $insertUser->cedula = str_slug($data['CEDULA']);
                    $insertUser->cargo = trim($data['CARGO']);
                    $insertUser->save();
                    $insertUser->assignRole($data['ROLE']);
                    $insertUser->areasOperativas()->attach($key + 1);
                    echo "{$insertUser->email} {$password} update\n";
                    $update ++;
                } else {
                    $insertUser = new User();
                    $insertUser->name = title_case(str_replace('-', ' ', $slugName));
                    $insertUser->email = $correo;
                    $insertUser->password = $encriptedPassword;
                    $insertUser->cedula = str_slug($data['CEDULA']);
                    $insertUser->cargo = trim($data['CARGO']);
                    $insertUser->save();
                    $insertUser->assignRole($data['ROLE']);
                    $insertUser->areasOperativas()->attach($key + 1);
                    echo "{$insertUser->email} {$password} create\n";
                    $create ++;
                }
                
                /* if($user['role'] == 'coordinador') { */
                    
                /* } */
            }
        }
        echo "Created {$create} | Updated {$update}\n";

        /* $user= User::create(
            ['name'=>'coordinador', 'email'=>'coordinador@liceo.com', 'password'=> bcrypt('123456')]
        );
        $user->assignRole('coordinador');
        $user->areasOperativas()->attach(1); */
        /* $gerentes = [
            ['name' => 'CAROLINA CORREA BARBOSA', 'email' => 'rectoria@lcb.edu.co', 'cedula' => '51866205', 'cargo' => 'Rectora', 'role' => 'gerente'],
            ['name' => 'SANDRA MARIA CORREA BARBOSA', 'email' => 'gerenteoperativa@lcb.edu.co', 'cedula' => '51790644', 'cargo' => 'Gerente Operativa', 'role' => 'gerente'],
            ['name' => 'AMPARO CORREA BARBOSA', 'email' => 'admin@lcb.edu.co', 'cedula' => '51992770', 'cargo' => 'Gerente Administrativa y Financiera', 'role' => 'gerente'],
        ];

        foreach ($gerentes as $gerente) {
            $data = $gerente;
            unset($data['role']);
            $password = str_random(6);
            $data['password'] = bcrypt($password);
            $insertUser = User::create($data);
            $insertUser->assignRole($gerente['role']);
            $insertUser->areasAdministrativas()->attach($key + 1);

            $notification->args = [
                'subject' => "Creación de cuenta en la plataforma Compras LCB",
                'greeting' => 'Hola, ' . $gerente['name'],
                'introduction' => 'su usuario es ' . $gerente['email'] . ' y su contraseña es ' . $password
            ];
            $insertUser->notify($notification);
        } */

        /* $user= User::create(
            ['name'=>'gerente', 'email'=>'gerente@liceo.com', 'password'=> bcrypt('123456')]
        );
        $user->assignRole('gerente');
        $user->areasAdministrativas()->attach(1); */

        /* $user = User::create(
            ['name' => 'coordinador financiero', 'email' => 'coordinadorfinanciero@liceo.com', 'password' => bcrypt('123456')]
        );
        $user->assignRole('coordinador financiero');

        $user = User::create(
            ['name' => 'gestor de compras', 'email' => 'gestordecompras@liceo.com', 'password' => bcrypt('123456')]
        );
        $user->assignRole('gestor de compras'); */

        /* $user= User::create(
            ['name'=>'proveedor 1', 'email'=>'proveedor1@liceo.com', 'password'=> bcrypt('123456')]
        );
        $user->assignRole('proveedor');
        $user->suppliers()->attach(1);

        $user= User::create(
            ['name'=>'proveedor 2', 'email'=>'proveedor2@liceo.com', 'password'=> bcrypt('123456')]
        );
        $user->assignRole('proveedor');
        $user->suppliers()->attach(2);

        $user= User::create(
            ['name'=>'proveedor 3', 'email'=>'proveedor3@liceo.com', 'password'=> bcrypt('123456')]
        );
        $user->assignRole('proveedor');
        $user->suppliers()->attach(3);

        $user= User::create(
            ['name'=>'empleado 1', 'email'=>'empleado1@liceo.com', 'password'=> bcrypt('123456')]
        );
        $user->assignRole('empleado');
        $user->areasOperativas()->attach(1);

        $user= User::create(
            ['name'=>'empleado 2', 'email'=>'empleado2@liceo.com', 'password'=> bcrypt('123456')]
        );
        $user->assignRole('empleado');
        $user->areasOperativas()->attach(1);

        $user= User::create(
            ['name'=>'empleado 3', 'email'=>'empleado3@liceo.com', 'password'=> bcrypt('123456')]
        );
        $user->assignRole('empleado');
        $user->areasOperativas()->attach(1); */
    }
}
