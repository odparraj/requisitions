<?php
use Illuminate\Database\Seeder;
use App\ExperienceRange;

class ExperienceRangesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return  void
     */
    public function run()
    {
        DB::table('experience_ranges')->insert([
        	['nombre' => 'Sin Experiencia','created_at' => '2018-10-18 16:01:15','updated_at' => '2018-10-18 16:01:15','deleted_at' => NULL],
			['nombre' => 'Menos de 1 Año','created_at' => '2018-10-18 16:01:34','updated_at' => '2018-10-18 16:01:34','deleted_at' => NULL],
			['nombre' => 'Entre 1 y 3 Años','created_at' => '2018-10-18 16:01:52','updated_at' => '2018-10-18 16:01:52','deleted_at' => NULL],
			['nombre' => 'Entre 3 y 5 Años','created_at' => '2018-10-18 16:02:06','updated_at' => '2018-10-18 16:02:06','deleted_at' => NULL],
			['nombre' => 'Entre 5 y 10 Años','created_at' => '2018-10-18 16:02:18','updated_at' => '2018-10-18 16:02:18','deleted_at' => NULL],
			['nombre' => 'Más de 10 Años','created_at' => '2018-10-18 16:02:50','updated_at' => '2018-10-18 16:02:50','deleted_at' => NULL],
        ]);
    }
}