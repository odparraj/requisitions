<?php
use Illuminate\Database\Seeder;
use App\MeasurementUnit;

class MeasurementUnitsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return  void
     */
    public function run()
    {
        DB::table('measurement_units')->insert([
        	['nombre' => 'Kg','created_at' => '2018-11-14 15:51:35','updated_at' => '2018-11-14 15:51:35','deleted_at' => NULL],
			['nombre' => 'Lb','created_at' => '2018-11-14 15:51:43','updated_at' => '2018-11-14 15:51:43','deleted_at' => NULL],
			['nombre' => 'Lt','created_at' => '2018-11-14 15:51:49','updated_at' => '2018-11-14 15:51:49','deleted_at' => NULL],
			['nombre' => 'Gl','created_at' => '2018-11-14 16:01:05','updated_at' => '2018-11-14 16:01:12','deleted_at' => NULL],
        ]);
    }
}