<?php

use Illuminate\Database\Seeder;
use App\OperativeArea;

class OperativeAreasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('operative_areas')->insert([
			['nombre' => 'Académica', 'administrative_area_id'=>1,'created_at' => '2018-10-17 17:33:02','updated_at' => '2018-10-17 17:33:02','deleted_at' => NULL],
			['nombre' => 'Enfermería', 'administrative_area_id'=>1, 'created_at' => '2018-10-17 17:35:15','updated_at' => '2018-10-17 17:35:15','deleted_at' => NULL],
			['nombre' => 'Biblioteca', 'administrative_area_id'=>1, 'created_at' => '2018-10-17 17:35:37','updated_at' => '2018-10-17 17:35:37','deleted_at' => NULL],
			['nombre' => 'Recursos Físicos', 'administrative_area_id'=>2, 'created_at' => '2018-10-17 17:33:37','updated_at' => '2018-10-17 17:33:37','deleted_at' => NULL],
			['nombre' => 'Restaurante', 'administrative_area_id'=>2, 'created_at' => '2018-10-17 17:35:03','updated_at' => '2018-10-17 17:35:03','deleted_at' => NULL],			
			['nombre' => 'Sistemas', 'administrative_area_id'=>3, 'created_at' => '2018-10-17 17:33:16','updated_at' => '2018-10-17 17:33:16','deleted_at' => NULL],
			['nombre' => 'Talento Humano', 'administrative_area_id'=>3, 'created_at' => '2018-10-17 17:33:48','updated_at' => '2018-10-17 17:33:48','deleted_at' => NULL],
			['nombre' => 'Comunicación', 'administrative_area_id'=>3, 'created_at' => '2018-10-17 17:34:15','updated_at' => '2018-10-17 17:34:15','deleted_at' => NULL],
			['nombre' => 'Admisiones', 'administrative_area_id'=>3, 'created_at' => '2018-10-17 17:34:27','updated_at' => '2018-10-17 17:34:27','deleted_at' => NULL],
			['nombre' => 'Financiera', 'administrative_area_id'=>3, 'created_at' => '2018-10-17 17:34:43','updated_at' => '2018-10-17 17:34:43','deleted_at' => NULL],
			['nombre' => 'Transporte', 'administrative_area_id'=>3, 'created_at' => '2018-10-17 17:34:53','updated_at' => '2018-10-17 17:34:53','deleted_at' => NULL],
			['nombre' => 'Bienestar', 'administrative_area_id'=>3, 'created_at' => '2018-10-17 17:35:48','updated_at' => '2018-10-17 17:35:48','deleted_at' => NULL],
			['nombre' => 'Asistente Administrativa Académica', 'administrative_area_id'=>1, 'created_at' => '2018-10-17 17:35:48','updated_at' => '2018-10-17 17:35:48','deleted_at' => NULL],
			['nombre' => 'Asistente Administrativa', 'administrative_area_id'=>3, 'created_at' => '2018-10-17 17:35:48','updated_at' => '2018-10-17 17:35:48','deleted_at' => NULL],
			['nombre' => 'Calidad', 'administrative_area_id'=>3, 'created_at' => '2018-10-17 17:35:48','updated_at' => '2018-10-17 17:35:48','deleted_at' => NULL],
		]);	
    }
}
