<?php
use Illuminate\Database\Seeder;
use App\OrderPriority;

class OrderPrioritiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return  void
     */
    public function run()
    {
        DB::table('order_priorities')->insert([
			[ 'nombre' => 'Alta','created_at' => '2018-11-09 21:03:33','updated_at' => '2018-11-09 21:03:33','deleted_at' => NULL ],
			[ 'nombre' => 'Media','created_at' => '2018-11-09 21:03:40','updated_at' => '2018-11-09 21:03:40','deleted_at' => NULL ],
			[ 'nombre' => 'Baja','created_at' => '2018-11-09 21:03:46','updated_at' => '2018-11-09 21:03:46','deleted_at' => NULL ]
        ]);
    }
}