<?php
use Illuminate\Database\Seeder;
use App\PaymentMethod;

class PaymentMethodsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return  void
     */
    public function run()
    {
        DB::table('payment_methods')->insert([
        	['nombre' => 'Contado','created_at' => '2018-10-18 15:57:45','updated_at' => '2018-10-18 15:57:45','deleted_at' => NULL],
			['nombre' => 'Crédito 15 días','created_at' => '2018-10-18 15:58:03','updated_at' => '2018-10-18 15:58:03','deleted_at' => NULL],
			['nombre' => 'Crédito 30 días','created_at' => '2018-10-18 15:58:11','updated_at' => '2018-10-18 15:58:11','deleted_at' => NULL],
			['nombre' => 'Crédito 45 días','created_at' => '2018-10-18 15:58:19','updated_at' => '2018-10-18 15:58:19','deleted_at' => NULL],
			['nombre' => 'Crédito 60 días','created_at' => '2018-10-18 15:58:28','updated_at' => '2018-10-18 15:58:28','deleted_at' => NULL],
			['nombre' => 'Crédito 75 días','created_at' => '2018-10-18 15:58:38','updated_at' => '2018-10-18 15:58:38','deleted_at' => NULL],
			['nombre' => 'Crédito 90 días','created_at' => '2018-10-18 15:59:01','updated_at' => '2018-10-18 15:59:01','deleted_at' => NULL],
        ]);
    }
}