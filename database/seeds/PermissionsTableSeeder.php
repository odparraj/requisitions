<?php

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = array(
		  array('name' => 'sections_create','guard_name' => 'web','created_at' => '2018-11-30 17:20:15','updated_at' => '2018-11-30 17:20:15'),
		  array('name' => 'sections_read','guard_name' => 'web','created_at' => '2018-11-30 17:20:15','updated_at' => '2018-11-30 17:20:15'),
		  array('name' => 'sections_update','guard_name' => 'web','created_at' => '2018-11-30 17:20:15','updated_at' => '2018-11-30 17:20:15'),
		  array('name' => 'sections_delete','guard_name' => 'web','created_at' => '2018-11-30 17:20:15','updated_at' => '2018-11-30 17:20:15'),
		  array('name' => 'users_create','guard_name' => 'web','created_at' => '2018-11-30 17:20:15','updated_at' => '2018-11-30 17:20:15'),
		  array('name' => 'users_read','guard_name' => 'web','created_at' => '2018-11-30 17:20:15','updated_at' => '2018-11-30 17:20:15'),
		  array('name' => 'users_update','guard_name' => 'web','created_at' => '2018-11-30 17:20:15','updated_at' => '2018-11-30 17:20:15'),
		  array('name' => 'users_delete','guard_name' => 'web','created_at' => '2018-11-30 17:20:15','updated_at' => '2018-11-30 17:20:15'),
		  array('name' => 'roles_create','guard_name' => 'web','created_at' => '2018-11-30 17:20:15','updated_at' => '2018-11-30 17:20:15'),
		  array('name' => 'roles_read','guard_name' => 'web','created_at' => '2018-11-30 17:20:15','updated_at' => '2018-11-30 17:20:15'),
		  array('name' => 'roles_update','guard_name' => 'web','created_at' => '2018-11-30 17:20:15','updated_at' => '2018-11-30 17:20:15'),
		  array('name' => 'roles_delete','guard_name' => 'web','created_at' => '2018-11-30 17:20:15','updated_at' => '2018-11-30 17:20:15'),
		  array('name' => 'permissions_create','guard_name' => 'web','created_at' => '2018-11-30 17:20:15','updated_at' => '2018-11-30 17:20:15'),
		  array('name' => 'permissions_read','guard_name' => 'web','created_at' => '2018-11-30 17:20:15','updated_at' => '2018-11-30 17:20:15'),
		  array('name' => 'permissions_update','guard_name' => 'web','created_at' => '2018-11-30 17:20:15','updated_at' => '2018-11-30 17:20:15'),
		  array('name' => 'permissions_delete','guard_name' => 'web','created_at' => '2018-11-30 17:20:15','updated_at' => '2018-11-30 17:20:15'),
		  array('name' => 'administrative_areas_create','guard_name' => 'web','created_at' => '2018-11-30 17:20:15','updated_at' => '2018-11-30 17:20:15'),
		  array('name' => 'administrative_areas_read','guard_name' => 'web','created_at' => '2018-11-30 17:20:16','updated_at' => '2018-11-30 17:20:16'),
		  array('name' => 'administrative_areas_update','guard_name' => 'web','created_at' => '2018-11-30 17:20:16','updated_at' => '2018-11-30 17:20:16'),
		  array('name' => 'administrative_areas_delete','guard_name' => 'web','created_at' => '2018-11-30 17:20:16','updated_at' => '2018-11-30 17:20:16'),
		  array('name' => 'experience_ranges_create','guard_name' => 'web','created_at' => '2018-11-30 17:20:16','updated_at' => '2018-11-30 17:20:16'),
		  array('name' => 'experience_ranges_read','guard_name' => 'web','created_at' => '2018-11-30 17:20:16','updated_at' => '2018-11-30 17:20:16'),
		  array('name' => 'experience_ranges_update','guard_name' => 'web','created_at' => '2018-11-30 17:20:16','updated_at' => '2018-11-30 17:20:16'),
		  array('name' => 'experience_ranges_delete','guard_name' => 'web','created_at' => '2018-11-30 17:20:16','updated_at' => '2018-11-30 17:20:16'),
		  array('name' => 'measurement_units_create','guard_name' => 'web','created_at' => '2018-11-30 17:20:16','updated_at' => '2018-11-30 17:20:16'),
		  array('name' => 'measurement_units_read','guard_name' => 'web','created_at' => '2018-11-30 17:20:16','updated_at' => '2018-11-30 17:20:16'),
		  array('name' => 'measurement_units_update','guard_name' => 'web','created_at' => '2018-11-30 17:20:16','updated_at' => '2018-11-30 17:20:16'),
		  array('name' => 'measurement_units_delete','guard_name' => 'web','created_at' => '2018-11-30 17:20:16','updated_at' => '2018-11-30 17:20:16'),
		  array('name' => 'operative_areas_create','guard_name' => 'web','created_at' => '2018-11-30 17:20:16','updated_at' => '2018-11-30 17:20:16'),
		  array('name' => 'operative_areas_read','guard_name' => 'web','created_at' => '2018-11-30 17:20:16','updated_at' => '2018-11-30 17:20:16'),
		  array('name' => 'operative_areas_update','guard_name' => 'web','created_at' => '2018-11-30 17:20:16','updated_at' => '2018-11-30 17:20:16'),
		  array('name' => 'operative_areas_delete','guard_name' => 'web','created_at' => '2018-11-30 17:20:16','updated_at' => '2018-11-30 17:20:16'),
		  array('name' => 'order_priorities_create','guard_name' => 'web','created_at' => '2018-11-30 17:20:16','updated_at' => '2018-11-30 17:20:16'),
		  array('name' => 'order_priorities_read','guard_name' => 'web','created_at' => '2018-11-30 17:20:16','updated_at' => '2018-11-30 17:20:16'),
		  array('name' => 'order_priorities_update','guard_name' => 'web','created_at' => '2018-11-30 17:20:16','updated_at' => '2018-11-30 17:20:16'),
		  array('name' => 'order_priorities_delete','guard_name' => 'web','created_at' => '2018-11-30 17:20:16','updated_at' => '2018-11-30 17:20:16'),
		  array('name' => 'order_statuses_create','guard_name' => 'web','created_at' => '2018-11-30 17:20:16','updated_at' => '2018-11-30 17:20:16'),
		  array('name' => 'order_statuses_read','guard_name' => 'web','created_at' => '2018-11-30 17:20:16','updated_at' => '2018-11-30 17:20:16'),
		  array('name' => 'order_statuses_update','guard_name' => 'web','created_at' => '2018-11-30 17:20:16','updated_at' => '2018-11-30 17:20:16'),
		  array('name' => 'order_statuses_delete','guard_name' => 'web','created_at' => '2018-11-30 17:20:16','updated_at' => '2018-11-30 17:20:16'),
		  array('name' => 'payment_methods_create','guard_name' => 'web','created_at' => '2018-11-30 17:20:16','updated_at' => '2018-11-30 17:20:16'),
		  array('name' => 'payment_methods_read','guard_name' => 'web','created_at' => '2018-11-30 17:20:16','updated_at' => '2018-11-30 17:20:16'),
		  array('name' => 'payment_methods_update','guard_name' => 'web','created_at' => '2018-11-30 17:20:16','updated_at' => '2018-11-30 17:20:16'),
		  array('name' => 'payment_methods_delete','guard_name' => 'web','created_at' => '2018-11-30 17:20:16','updated_at' => '2018-11-30 17:20:16'),
		  array('name' => 'supplier_orders_create','guard_name' => 'web','created_at' => '2018-11-30 17:20:16','updated_at' => '2018-11-30 17:20:16'),
		  array('name' => 'supplier_orders_read','guard_name' => 'web','created_at' => '2018-11-30 17:20:16','updated_at' => '2018-11-30 17:20:16'),
		  array('name' => 'supplier_orders_update','guard_name' => 'web','created_at' => '2018-11-30 17:20:16','updated_at' => '2018-11-30 17:20:16'),
		  array('name' => 'supplier_orders_delete','guard_name' => 'web','created_at' => '2018-11-30 17:20:16','updated_at' => '2018-11-30 17:20:16'),
		  array('name' => 'suppliers_create','guard_name' => 'web','created_at' => '2018-11-30 17:20:16','updated_at' => '2018-11-30 17:20:16'),
		  array('name' => 'suppliers_read','guard_name' => 'web','created_at' => '2018-11-30 17:20:16','updated_at' => '2018-11-30 17:20:16'),
		  array('name' => 'suppliers_update','guard_name' => 'web','created_at' => '2018-11-30 17:20:16','updated_at' => '2018-11-30 17:20:16'),
		  array('name' => 'suppliers_delete','guard_name' => 'web','created_at' => '2018-11-30 17:20:16','updated_at' => '2018-11-30 17:20:16'),
		  array('name' => 'quotations_create','guard_name' => 'web','created_at' => '2018-11-30 17:20:16','updated_at' => '2018-11-30 17:20:16'),
		  array('name' => 'quotations_read','guard_name' => 'web','created_at' => '2018-11-30 17:20:16','updated_at' => '2018-11-30 17:20:16'),
		  array('name' => 'quotations_update','guard_name' => 'web','created_at' => '2018-11-30 17:20:16','updated_at' => '2018-11-30 17:20:16'),
		  array('name' => 'quotations_delete','guard_name' => 'web','created_at' => '2018-11-30 17:20:16','updated_at' => '2018-11-30 17:20:16'),
		  array('name' => 'satisfaction_questions_create','guard_name' => 'web','created_at' => '2018-11-30 21:58:34','updated_at' => '2018-11-30 21:58:34'),
		  array('name' => 'satisfaction_questions_read','guard_name' => 'web','created_at' => '2018-11-30 21:58:34','updated_at' => '2018-11-30 21:58:34'),
		  array('name' => 'satisfaction_questions_update','guard_name' => 'web','created_at' => '2018-11-30 21:58:34','updated_at' => '2018-11-30 21:58:34'),
		  array('name' => 'satisfaction_questions_delete','guard_name' => 'web','created_at' => '2018-11-30 21:58:34','updated_at' => '2018-11-30 21:58:34'),
		  array('name' => 'Crear Requisicion','guard_name' => 'web','created_at' => '2018-11-30 17:20:16','updated_at' => '2018-11-30 17:20:16'),
		  array('name' => 'Autorizar Compra','guard_name' => 'web','created_at' => '2018-11-30 17:20:16','updated_at' => '2018-11-30 17:20:16'),
		  array('name' => 'Cotizar Requisicion','guard_name' => 'web','created_at' => '2018-11-30 17:20:16','updated_at' => '2018-11-30 17:20:16'),
		  array('name' => 'Calificar Proveedor','guard_name' => 'web','created_at' => '2018-12-04 17:18:03','updated_at' => '2018-12-04 17:18:03'),
		  array('name' => 'Crear Cotizacion','guard_name' => 'web','created_at' => '2018-12-05 15:02:11','updated_at' => '2018-12-05 15:02:11'),
		  array('name' => 'Preselecionar Proveedor','guard_name' => 'web','created_at' => '2018-12-07 19:44:22','updated_at' => '2018-12-07 19:44:22'),
		  array('name' => 'Enviar Ordenes','guard_name' => 'web','created_at' => '2018-12-07 19:44:22','updated_at' => '2018-12-07 19:44:22')
		);

		DB::table('permissions')->insert($permissions);
    }
}
