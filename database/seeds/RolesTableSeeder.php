<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      	$roles = array(
		  array('id' => '1','name' => 'super-admin','guard_name' => 'web','created_at' => '2018-11-30 17:20:15','updated_at' => '2018-11-30 17:20:15'),
		  array('id' => '2','name' => 'admin','guard_name' => 'web','created_at' => '2018-11-30 17:20:15','updated_at' => '2018-11-30 17:20:15'),
		  array('id' => '3','name' => 'gerente','guard_name' => 'web','created_at' => '2018-11-30 17:20:15','updated_at' => '2018-11-30 17:20:15'),
		  array('id' => '4','name' => 'coordinador','guard_name' => 'web','created_at' => '2018-11-30 17:20:15','updated_at' => '2018-11-30 17:20:15'),
		  array('id' => '5','name' => 'empleado','guard_name' => 'web','created_at' => '2018-11-30 17:20:15','updated_at' => '2018-11-30 17:20:15'),
		  array('id' => '6','name' => 'proveedor','guard_name' => 'web','created_at' => '2018-12-05 15:11:30','updated_at' => '2018-12-07 19:45:30'),
          array('id' => '7','name' => 'coordinador financiero','guard_name' => 'web','created_at' => '2018-12-07 19:45:18','updated_at' => '2018-12-07 19:45:18'),
          array('id' => '8','name' => 'gestor de compras','guard_name' => 'web','created_at' => '2018-12-07 19:45:18','updated_at' => '2018-12-07 19:45:18')
		);		
		
        DB::table('roles')->insert($roles);
    }
}
