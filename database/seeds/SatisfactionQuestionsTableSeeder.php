<?php
use Illuminate\Database\Seeder;
use App\SatisfactionQuestion;

class SatisfactionQuestionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return  void
     */
    public function run()
    {
        $satisfaction_questions = array(
            array('enunciado' => 'Calidad del producto o servicio'),
            array('enunciado' => 'Puntualidad en la entrega del producto o servicio'),
            array('enunciado' => 'Cumplimiento con lo requerido'),
            array('enunciado' => 'Volvería usted a tomar los servicios del Proveedor'),
            array('enunciado' => 'Desempeño Gestión de Compras')
        );

        DB::table('satisfaction_questions')->insert($satisfaction_questions);
    }
}