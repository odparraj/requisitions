<?php

use Illuminate\Database\Seeder;

class SectionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sections = array(
		  array('display_name' => 'Sections','icon' => 'glyphicon glyphicon-link','route' => 'sections.index','permission' => 'sections_read','created_at' => NULL,'updated_at' => NULL,'deleted_at' => NULL),
		  array('display_name' => 'Users','icon' => 'glyphicon glyphicon-link','route' => 'users.index','permission' => 'users_read','created_at' => NULL,'updated_at' => NULL,'deleted_at' => NULL),
		  array('display_name' => 'Roles','icon' => 'glyphicon glyphicon-link','route' => 'roles.index','permission' => 'roles_read','created_at' => NULL,'updated_at' => NULL,'deleted_at' => NULL),
		  array('display_name' => 'Permissions','icon' => 'glyphicon glyphicon-link','route' => 'permissions.index','permission' => 'permissions_read','created_at' => NULL,'updated_at' => NULL,'deleted_at' => NULL),
		  array('display_name' => 'Areas Administrativas','icon' => 'glyphicon glyphicon-link','route' => 'administrative_areas.index','permission' => 'administrative_areas_read','created_at' => NULL,'updated_at' => NULL,'deleted_at' => NULL),
		  array('display_name' => 'Rangos De Experiencia','icon' => 'glyphicon glyphicon-link','route' => 'experience_ranges.index','permission' => 'experience_ranges_read','created_at' => NULL,'updated_at' => NULL,'deleted_at' => NULL),
		  array('display_name' => 'Unidades De Medida','icon' => 'glyphicon glyphicon-link','route' => 'measurement_units.index','permission' => 'measurement_units_read','created_at' => NULL,'updated_at' => NULL,'deleted_at' => NULL),
		  array('display_name' => 'Areas Operativas','icon' => 'glyphicon glyphicon-link','route' => 'operative_areas.index','permission' => 'operative_areas_read','created_at' => NULL,'updated_at' => NULL,'deleted_at' => NULL),
		  array('display_name' => 'Prioridades De Orden','icon' => 'glyphicon glyphicon-link','route' => 'order_priorities.index','permission' => 'order_priorities_read','created_at' => NULL,'updated_at' => NULL,'deleted_at' => NULL),
		  array('display_name' => 'Formas De Pago','icon' => 'glyphicon glyphicon-link','route' => 'payment_methods.index','permission' => 'payment_methods_read','created_at' => NULL,'updated_at' => NULL,'deleted_at' => NULL),
		  array('display_name' => 'Ordenes de Proveedores','icon' => 'glyphicon glyphicon-link','route' => 'supplier_orders.index','permission' => 'supplier_orders_read','created_at' => NULL,'updated_at' => NULL,'deleted_at' => NULL),
		  array('display_name' => 'Proveedores','icon' => 'glyphicon glyphicon-link','route' => 'suppliers.index','permission' => 'suppliers_read','created_at' => NULL,'updated_at' => NULL,'deleted_at' => NULL),
		  array('display_name' => 'Cotizaciones','icon' => 'glyphicon glyphicon-link','route' => 'quotations.index','permission' => 'quotations_read','created_at' => NULL,'updated_at' => NULL,'deleted_at' => NULL),
		  array('display_name' => 'Solicitudes De Requisición','icon' => 'glyphicon glyphicon-plus','route' => 'supplier_orders.index_requisition','permission' => 'Crear Requisicion','created_at' => NULL,'updated_at' => '2018-11-30 20:51:46','deleted_at' => NULL),
		  array('display_name' => 'Autorizar Compras','icon' => 'fa fa-check-square-o','route' => 'supplier_orders.index_autorization','permission' => 'Autorizar Compra','created_at' => NULL,'updated_at' => NULL,'deleted_at' => NULL),
		  array('display_name' => 'Solicitudes De Cotización','icon' => 'fa fa-money','route' => 'supplier_orders.index_quotation','permission' => 'Cotizar Requisicion','created_at' => NULL,'updated_at' => '2018-12-07 19:20:21','deleted_at' => NULL),
		  array('display_name' => 'Encuesta De Satisfacción','icon' => 'glyphicon glyphicon-link','route' => 'satisfaction_questions.index','permission' => 'satisfaction_questions_read','created_at' => '2018-11-30 21:58:35','updated_at' => '2018-11-30 21:58:35','deleted_at' => NULL),
		  array('display_name' => 'Calificar Proveedor','icon' => 'fa fa-check-square-o','route' => 'supplier_orders.index_score','permission' => 'Calificar Proveedor','created_at' => '2018-12-04 17:24:34','updated_at' => '2018-12-04 17:24:34','deleted_at' => NULL),
		  array('display_name' => 'Cotización del proveedor','icon' => 'fa fa-money','route' => 'supplier_orders.index_supplier_quote','permission' => 'Crear Cotizacion','created_at' => '2018-12-05 15:26:19','updated_at' => '2018-12-05 15:26:19','deleted_at' => NULL),
		  array('display_name' => 'Preselección de proveedores','icon' => 'fa fa-edit','route' => 'supplier_orders.index_preselection','permission' => 'Preselecionar Proveedor','created_at' => '2018-12-07 19:47:01','updated_at' => '2018-12-07 19:47:01','deleted_at' => NULL),
		  array('display_name' => 'Ordenes de compra','icon' => 'fa fa-send','route' => 'supplier_orders.index_send','permission' => 'Enviar Ordenes','created_at' => '2018-12-07 19:47:01','updated_at' => '2018-12-07 19:47:01','deleted_at' => NULL)
		);
		
        DB::table('sections')->insert($sections);
    }
}
