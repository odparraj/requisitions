<?php
use Illuminate\Database\Seeder;
use App\SupplierOrder;

class SupplierOrdersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return  void
     */
    public function run()
    {
        $supplierOrders = SupplierOrder::all();

        foreach ($supplierOrders as $supplierOrder) {
            $gerencia = $supplierOrder->areaOperativa->areaAdministrativa;
            $supplierOrder->gerencia_que_autoriza = $supplierOrder->gerencia_que_autoriza ? : $gerencia->id;
            $supplierOrder->save();
        }
    }
}