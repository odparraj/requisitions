<div class="form-group" :class="{'has-error': errors.has({!! $name !!}) }">
    @if(isset($label))
    <label class="control-label">{!!$label!!}</label>
    @endif
    <div v-if="{!!$model!!} &&  (typeof {!!$model!!} == 'string')">
        <a class="btn btn-danger btn-xs btn-inline" target="_blank" :href="{!!$model!!}">Ver Archivo Actual</a>
        <input v-if="{!!$model!!} &&  (typeof {!!$model!!} == 'string')" type="hidden" :name="{!!$name!!}" :value="{!!$model!!}">
        <button type="button" v-if="{!!$model!!} &&  (typeof {!!$model!!} == 'string')" v-on:click="{!!$model!!}=''" class="btn btn-primary btn-xs"><i class="fa fa-edit"></i></button>
    </div>
    <input v-if="! {!!$model!!}" class="form-control input-sm"
    	   :name="{!!$name!!}"
    	   type="file"
    	   v-validate="{!!$validate!!}"
    	   placeholder="{!!isset($placeholder)?$placeholder:''!!}"
    	   autocomplete="off">
   	<span class="help-block" v-if="errors.has({!!$name!!})">{!! "{{errors.first(".$name.")".'}'.'}' !!}</span>
</div>