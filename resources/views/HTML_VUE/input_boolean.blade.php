<div class="form-group" :class="{'has-error': errors.has({!! $name !!}) }">            
  	<label class="checkbox-inline">
    	<input type="checkbox" :checked="{!! $model !!}=='si'" @change="$event.target.checked?{!! $model !!}='si':{!! $model !!}='no'">
        {!!$label!!}
  	</label>
  	<input v-if="{!! $model !!}=='si'" type="hidden" :name="{!! $name !!}" value="si">
  	<input v-if="{!! $model !!}!='si'" type="hidden" :name="{!! $name !!}" value="no">
    @if(isset($help))
    <span style="display: inline-block;" class="glyphicon glyphicon-question-sign" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="{{$help}}"></span>
    @endif
    <span class="help-block" v-if="errors.has({!! $name !!})">{!! "{{errors.first(".$name.")".'}'.'}' !!}</span>
</div>