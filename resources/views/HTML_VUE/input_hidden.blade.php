<div class="form-group" :class="{'has-error': errors.has({!! $name !!}) }">
    <input class="form-control input-sm"
           v-model="{!!$model!!}"
    	   :name="{!!$name!!}"
    	   type="hidden"
    	   v-validate="{!!$validate!!}"
    	   autocomplete="off">
   	<span class="help-block" v-if="errors.has({!!$name!!})">{!! "{{errors.first(".$name.")".'}'.'}' !!}</span>
</div>