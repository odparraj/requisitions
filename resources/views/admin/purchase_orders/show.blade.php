@extends('adminlte::layouts.app')

@section('htmlheader_title')
	Ordenes De Compra
@endsection

@section('contentheader_title')
	Ordenes De Compra
@endsection

@section('contentheader_description')
	Show
@endsection

@section('main-content')
	<div class="row" id="sectionApp" v-cloak>
		<div class="col-md-12">
			@include('includes.session-alert')
			<form id="form-section">
                <div class="row">
    				<div class="col-md-12">@include('HTML_VUE.input_text',[ 'label'=>'Cotizacion', 'placeholder'=>'Cotizacion', 'model'=>'cotizacion', 'name'=>"'cotizacion'", 'validate'=>"'required'"])</div>
    				<div class="col-md-12">@include('HTML_VUE.input_date',[ 'label'=>'Fecha Orden', 'placeholder'=>'Fecha Orden', 'model'=>'fecha_orden', 'name'=>"'fecha_orden'", 'validate'=>"'required'"])</div>
    				<div class="col-md-12">@include('HTML_VUE.input_date',[ 'label'=>'Fecha Pedido', 'placeholder'=>'Fecha Pedido', 'model'=>'fecha_pedido', 'name'=>"'fecha_pedido'", 'validate'=>"'required'"])</div>
    				<div class="col-md-12">@include('HTML_VUE.input_date',[ 'label'=>'Fecha Entrega', 'placeholder'=>'Fecha Entrega', 'model'=>'fecha_entrega', 'name'=>"'fecha_entrega'", 'validate'=>"'required'"])</div>
    				                    
                </div>

			</form>
		</div>
	</div>
@endsection

@section('local-css')
	<style type="text/css">
		[v-cloak] {
			display: none;
		}
		.grid-images-item{
            width: 100%;
            padding-top: 100%;
            border:solid 1px;
            background-size: contain;
            background-position: 50% 50%;
            background-repeat: no-repeat;
        }
        .container-sortable{
            background: transparent;
            border: 1px solid #3c8dbc;
            border-top: 3px solid #3c8dbc;
        }
	</style>
@endsection

@section('local-script')

    <!--<script src="http://requisitions.test/js/dist/locale/es.js" type="text/javascript"></script>
    <script src="http://requisitions.test/js/moment.min.js"></script>-->

    <script type="text/javascript">
        
        //Vue.use(VeeValidate, {locale:'es'});

        var section = new Vue({
            el: '#sectionApp',
            data: {

            	cotizacion:`{!!old('cotizacion')!!}` ,
                fecha_orden:`{!!old('fecha_orden')!!}` ,
                fecha_pedido:`{!!old('fecha_pedido')!!}` ,
                fecha_entrega:`{!!old('fecha_entrega')!!}` 
                
            }
        });

        //section.$validator.installDateTimeValidators(moment);
        
        @foreach($errors->toArray()?:[] as $key=>$value)
            section.errors.add('{{dot_to_html($key)}}', '{{$errors->first($key)}}');
        @endforeach
        
    </script>
    <script type="text/javascript">
        $(function(){
            $("#sectionApp :input").prop("disabled", true);
        });
    </script>
    {{ session()->forget('_old_input') }}
@endsection