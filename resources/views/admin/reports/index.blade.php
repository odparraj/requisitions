@extends('adminlte::layouts.app')

@section('htmlheader_title')
	Reporte General
@endsection

@section('contentheader_title')
    Reporte General
@endsection

@section('contentheader_description')
    @lang('message.index')
@endsection

@section('main-content')	
	<div class="row" id="sectionApp">
		<div class="col-md-12">
			@include('includes.session-alert')

            <datatable :columns="{!!$columns!!}" 
            			:url="'{!! route($link) !!}'"
            			:is-unique="false">
            </datatable>
		</div>
	</div>
@endsection
@section('local-script')
    <script type="text/javascript">        
        //Vue.use(VeeValidate, {locale:'es'});

        var section = new Vue({
            el: '#sectionApp',
            data:{
                base_url: '{{route('reports.index')}}',
            },
            methods:{
            },
            mounted(){
            }
        });        
    </script>
    {{ session()->forget('_old_input') }}
@endsection