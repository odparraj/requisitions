@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Matriz Comparativa y Selección Proveedores
@endsection

@section('contentheader_title')
    Matriz Comparativa y Selección Proveedores
@endsection

@section('contentheader_description')
    
@endsection

@section('main-content')
    <div class="row" id="sectionApp" v-cloak>
        <div class="col-md-12">
            @include('includes.session-alert')
            <form id="form-section" action="{{ route('supplier_orders.store_order',$supplier_order->id) }}" method="POST" enctype="multipart/form-data" @submit.prevent="validateBeforeSubmit">
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                
                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-primary container-sortable">
                            <div class="box-header with-border">
                                <h3 class="box-title">Matriz Comparativa Evaluación y Selección de Proveedores</h3>
                                <div class="box-tools pull-right">
                                    <!-- Collapse Button -->
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                        <i class="fa fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="box-body">
                                <table class="table table-condensed table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Items a evaluar</th>
                                            @foreach($cotizaciones?:[] as $cotizacion)
                                            <th width="{!!70/(count($cotizaciones))!!}%">Proveedor {!!$loop->iteration!!}</th>
                                            @endforeach
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Razon social del proveedor</td>
                                            @foreach($cotizaciones?:[] as $cotizacion)
                                            <td>{!!array_get($cotizacion->supplier,'informacion_general.razon_social','Proveedor sin nombre')!!}</td>
                                            @endforeach                                            
                                        </tr>
                                        <tr>
                                            <td>Cotización N°</td>
                                            @foreach($cotizaciones?:[] as $cotizacion)
                                            <td>{!!$cotizacion->id!!}</td>
                                            @endforeach                                            
                                        </tr>
                                        @foreach($supplier_order->requisicion_de_compra?:[] as $key=> $item)
                                        <tr>
                                            <td><b>{{'Destalle '.$item['id']}} :</b> {{$item['descripcion']}}</td>                                            
                                            @foreach($cotizaciones?:[] as $cotizacion)
                                            <td class="text-left">{!!array_get($cotizacion->contenido,'contenido.'.$key.'.descripcion','')!!}</td>
                                            @endforeach
                                        </tr>
                                        <tr>
                                            <td>Valor unitario</td>
                                            @foreach($cotizaciones?:[] as $cotizacion)
                                            <td class="text-right">{!!array_get($cotizacion->contenido,'contenido.'.$key.'.status',null)=='true'?number_format(array_get($cotizacion->contenido,'contenido.'.$key.'.valor_unitario','0'),2):'No Cotizado'!!}</td>
                                            @endforeach                                            
                                        </tr>
                                        <tr>
                                            <td>% Iva</td>
                                            @foreach($cotizaciones?:[] as $cotizacion)
                                            <td class="text-right">{!!array_get($cotizacion->contenido,'contenido.'.$key.'.status',null)=='true'?array_get($cotizacion->contenido,'contenido.'.$key.'.iva_tarifa',''):'No Cotizado'!!}</td>
                                            @endforeach                                            
                                        </tr>
                                        <tr>
                                            <td>% Dto</td>
                                            @foreach($cotizaciones?:[] as $cotizacion)
                                            <td class="text-right">{!!array_get($cotizacion->contenido,'contenido.'.$key.'.status',null)=='true'?array_get($cotizacion->contenido,'contenido.'.$key.'.descuento_tarifa',''):'No Cotizado'!!}</td>
                                            @endforeach                                            
                                        </tr>
                                        <tr>
                                            <td>Valor Real Unitario</td>
                                            @foreach($cotizaciones?:[] as $cotizacion)
                                            <td class="text-right">{!!array_get($cotizacion->contenido,'contenido.'.$key.'.status',null)=='true'?number_format($cotizacion->calcularValor($key),2):'No Cotizado'!!}</td>
                                            @endforeach                                            
                                        </tr>
                                        <tr>
                                            <td>Cantidad Cotizada</td>
                                            @foreach($cotizaciones?:[] as $cotizacion)
                                            <td class="text-right">{!!array_get($cotizacion->contenido,'contenido.'.$key.'.status',null)=='true'?array_get($cotizacion->contenido,'contenido.'.$key.'.cantidad',0):'0'!!}/[{!!array_get($item, 'cantidad','')!!}]</td>
                                            @endforeach                                            
                                        </tr>
                                        <tr>
                                            <td>Total Valor Real * Cantidad Cotizada</td>
                                            @foreach($cotizaciones?:[] as $cotizacion)
                                            <td class="text-right">{!!array_get($cotizacion->contenido,'contenido.'.$key.'.status',null)=='true'?number_format($cotizacion->calcularValorCantidad($key),2):'No Cotizado'!!}</td>
                                            @endforeach                                            
                                        </tr>
                                            
                                        @endforeach
                                        <tr>
                                            <td><b>Total General</b></td>
                                            @foreach($cotizaciones?:[] as $cotizacion)
                                            <td class="text-right"><b>{!!number_format($cotizacion->calcularTotal(),2)!!}</b></td>
                                            @endforeach                                            
                                        </tr>
                                        <tr>
                                            <td><b>Dto General</b></td>
                                            @foreach($cotizaciones?:[] as $cotizacion)
                                            <td class="text-right"><b>{!!array_get($cotizacion->contenido,'descuento_general','0')!!}</b></td>
                                            @endforeach                                            
                                        </tr>
                                        <tr>
                                            <td>Descuentos Otorgados</td>
                                            @foreach($cotizaciones?:[] as $cotizacion)
                                            <td>{!!array_get($cotizacion->contenido,'criterios.descuento','')!!}</td>
                                            @endforeach                                            
                                        </tr>
                                        <tr>
                                            <td>Forma de pago</td>
                                            @foreach($cotizaciones?:[] as $cotizacion)
                                            @php
                                            $metodo_de_pago= array_get($cotizacion->contenido,'criterios.forma_de_pago','');
                                            @endphp
                                            <td>{!!array_get($formas_de_pago,$metodo_de_pago,'')!!}</td>
                                            @endforeach                                            
                                        </tr>
                                        <tr>
                                            <td>Garantías</td>
                                            @foreach($cotizaciones?:[] as $cotizacion)
                                            <td>{!!array_get($cotizacion->contenido,'criterios.garantia','')!!}</td>
                                            @endforeach                                            
                                        </tr>
                                        <tr>
                                            <td>Disponibilidad del producto o servicio</td>
                                            @foreach($cotizaciones?:[] as $cotizacion)
                                            <td>{!!array_get($cotizacion->contenido,'criterios.tiempo_de_entrega','')!!}</td>
                                            @endforeach                                            
                                        </tr>
                                        <tr>
                                            <td>Tiempo de Experiencia</td>
                                            @foreach($cotizaciones?:[] as $cotizacion)
                                            @php
                                            $experiencia= array_get($cotizacion->contenido,'criterios.experiencia','');
                                            @endphp
                                            <td>{!!array_get($tiempos_de_experiencia,$experiencia,'')!!}</td>
                                            @endforeach                                            
                                        </tr>
                                        <tr>
                                            <td>Sistema de Gestión de Calidad</td>
                                            @foreach($cotizaciones?:[] as $cotizacion)
                                            <td>{!!array_get($cotizacion->contenido,'criterios.certificado_de_gestion','')!!}</td>
                                            @endforeach                                            
                                        </tr>
                                        {{-- <tr>
                                            <td>Anexó documentos iniciales requeridos</td>
                                            @foreach($cotizaciones?:[] as $cotizacion)
                                            <td>{!!array_get($cotizacion->contenido,'criterios.descuento','')!!}</td>
                                            @endforeach                                            
                                        </tr> --}}
                                        <tr>
                                            <td>Observaciones del proveedor</td>
                                            @foreach($cotizaciones?:[] as $cotizacion)
                                            <td>{!!array_get($cotizacion->contenido,'observaciones','')!!}</td>
                                            @endforeach                                            
                                        </tr>
                                        <tr>
                                            <td>Documento Anexo</td>
                                            @foreach($cotizaciones?:[] as $cotizacion)
                                                @php
                                                    $file = array_get($cotizacion->contenido,'documento_anexo','')
                                                @endphp
                                                @if($file)
                                                <td><a role="button" class="btn btn-xs btn-primary" target="_blank" href="{!!url($file)!!}">ver</a></td>
                                                @else
                                                <td></td>
                                                @endif
                                            @endforeach                                            
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>                
                <div class="row hidden-print">
                    <div class="col-md-2">
                        <div class="box box-primary container-sortable">
                            <div class="box-header with-border">
                                <h3 class="box-title">Requisición</h3>
                            </div>
                            <div class="box-body">
                                <draggable class="draggableClass" v-model="requisicion_de_compra" :clone="jsonCopy" :options="{handle:'.handle', group:{name:'detalles',pull:'clone',put:'false'}}">
                                    <div v-for="(item, index) in requisicion_de_compra" class="box" :key="item.id">                            
                                        <div class="box-header with-border">
                                            <h3 class="box-title">
                                                <span class="handle" style="cursor:pointer;">
                                                    <i class="fa fa-ellipsis-v"></i>
                                                    <i class="fa fa-ellipsis-v"></i>
                                                </span>
                                                Detalle @{{item.id}}
                                            </h3>
                                        </div>
                                    </div>
                                </draggable>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-10">
                    @foreach( $cotizaciones?:[] as $key => $cotizacion )
                        <div class="box box-primary container-sortable">
                            <div class="box-header with-border">
                                <h3 class="box-title">{{ array_get($cotizacion->supplier, 'informacion_general.razon_social', 'Proveedor sin nombre') }}</h3>
                                <div class="box-tools pull-right">
                                    <!-- Collapse Button -->
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                        <i class="fa fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="box-body">
                                <draggable class="draggableClass" v-model="seleccionados[{{$cotizacion->id}}]" :options="{handle:'.handle1',group:'detalles'}" @change="clone($event,'seleccionados[{{$cotizacion->id}}]',{{$cotizacion->id}})">
                                    <div v-for="(item, index) in seleccionados[{{$cotizacion->id}}]" class="box">
                                        <div class="box-header with-border">
                                            <h3 class="box-title">
                                                <span class="handle handle1" style="cursor:pointer;">
                                                    <i class="fa fa-ellipsis-v"></i>
                                                    <i class="fa fa-ellipsis-v"></i>
                                                </span>
                                                Detalle @{{item.id}}
                                            </h3>
                                            <div class="box-tools pull-right">
                                                <button type="button" class="btn btn-box-tool" title="Remove" v-on:click="removeDatailFromProvider(index,'seleccionados[{{$cotizacion->id}}]')"><i class="fa fa-times"></i></button>
                                            </div>
                                        </div>
                                        <div class="box-body">
                                            <div class="row">
                                                <div class="col-md-12"><b>Descripción:</b> @{{item.descripcion}}</div>
                                                <div class="col-md-12">@include('HTML_VUE.textarea',['model'=>"seleccionados[$cotizacion->id][index]['descripcion_provedor']",'name'=>"'seleccionados[$cotizacion->id]['+index+'][descripcion_provedor]'",'label'=>'Nombre del producto o servicio [Proveedor]','validate'=>"'required'"])</div>
                                                <div class="col-md-2">@include('HTML_VUE.select_simple',['model'=>"seleccionados[$cotizacion->id][index]['unidad']", 'name'=>"'seleccionados[$cotizacion->id]['+index+'][unidad]'", 'placeholder'=>'U. Medida', 'label'=>'U. Medida', 'validate'=>"'required'",'options'=>'unidades'])</div>
                                                <div class="col-md-2">@include('HTML_VUE.input_number',['model'=>"seleccionados[$cotizacion->id][index]['cantidad']", 'name'=>"'seleccionados[$cotizacion->id]['+index+'][cantidad]'", 'placeholder'=>'Cantidad', 'label'=>'Cantidad', 'validate'=>"'required|decimal:2'"])</div>
                                                <div class="col-md-2">@include('HTML_VUE.input_number',['model'=>"seleccionados[$cotizacion->id][index]['valor_unitario']",'name'=>"'seleccionados[$cotizacion->id]['+index+'][valor_unitario]'",'label'=>'V/r Unitario','validate'=>"'required|decimal:2'",'step'=>'any'])</div>
                                                <div class="col-md-2">@include('HTML_VUE.input_text',['model'=>"seleccionados[$cotizacion->id][index]['descuento_tarifa']",'name'=>"'seleccionados[$cotizacion->id]['+index+'][descuento_tarifa]'",'label'=>'% Dto','validate'=>"'required|decimal:2'"])</div>
                                                <div class="col-md-2">@include('HTML_VUE.input_text',['model'=>"seleccionados[$cotizacion->id][index]['iva_tarifa']",'name'=>"'seleccionados[$cotizacion->id]['+index+'][iva_tarifa]'",'label'=>'% Iva','validate'=>"'required|decimal:2'"])</div>
                                                <div class="col-md-2"><div><b>Valor Real:</b> @{{calcularValor(item) | currency}}</div><div><b>Valor Total:</b> @{{calcularValor(item)*item.cantidad | currency}}</div></div>
                                            </div>
                                        </div>
                                    </div>
                                </draggable>
                            </div>
                        </div>
                    @endforeach
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">@include('HTML_VUE.textarea',[ 'label'=>'Observaciones', 'placeholder'=>'Observaciones', 'model'=>'observaciones.preseleccion', 'name'=>"'observaciones[preseleccion]'", 'validate'=>"''"])</div>
                </div>
                {{--<div class="row hidden-print">
                    <div class="col-md-12">
                        <div class="box box-primary container-sortable">
                            <div class="box-header with-border">
                                <h3 class="box-title">Documentos Anexos</h3>
                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-primary btn-xs" v-on:click="addDocumentosAnexos()">Archivo</button>
                                    
                                </div>
                            </div>
                            <div class="box-body">                                
                                <div v-for="(item, index) in documentos_anexos" class="box">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">
                                            <span class="handle" style="cursor: pointer;">
                                                <i class="fa fa-ellipsis-v"></i>
                                                <i class="fa fa-ellipsis-v"></i>
                                            </span>
                                            @{{item['nombre']}}
                                            
                                        </h3>
                                        <div class="box-tools pull-right">
                                            <button type="button" class="btn btn-box-tool sortable-button" :data-target="'#documentos_anexos_'+index" data-toggle="collapse" title="Collapse"><i class="fa fa-window-maximize"></i></button>
                                            <button type="button" class="btn btn-box-tool" title="Remove" v-on:click="removeDocumentosAnexos(index)"><i class="fa fa-times"></i></button>
                                        </div>
                                    </div>
                                    <div class="box-body collapse" :id="'documentos_anexos_'+index">
                                        <div class="row">
                                            <div class="col-md-8">@include('HTML_VUE.input_text',['model'=>"documentos_anexos[index]['nombre']", 'name'=>"'documentos_anexos['+index+'][nombre]'", 'placeholder'=>'Nombre', 'label'=>'Nombre', 'validate'=>"'required'"])</div>
                                            <div class="col-md-4">@include('HTML_VUE.input_file',['model'=>"documentos_anexos[index]['file']", 'name'=>"'documentos_anexos['+index+'][file]'", 'placeholder'=>'Documento pdf | excel', 'label'=>'Documento pdf | excel', 'validate'=>"'required'"])</div>
                                        </div>
                                    </div>
                                </div>                                
                            </div>
                        </div>
                    </div>
                </div>--}}
                <div class="row">
                    <div class="col-md-12">
                        <div class="pull-right">FT-FIN-042-V1</div>
                    </div>
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-primary hidden-print">Guardar Selección</button>
                    </div>
                </div>
            </form>
        </div>
        
                
    </div>
@endsection

@section('local-css')
    <style type="text/css">
        [v-cloak] {
            display: none;
        }

        .container-sortable{
            background: transparent;
            border: 1px solid #3c8dbc;
            border-top: 3px solid #3c8dbc;
        }

        .table-bordered {
            border: 1px solid #337ab7;
        }

        .table-bordered>tbody>tr>td, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>td, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>thead>tr>th {
            border: 1px solid #337ab7;
        }

        .checkbox label, .radio label {
            min-height: 20px;
            padding-left: 0;
            margin-bottom: 0;
            font-weight: 400;
            cursor: pointer;
        }

        .checkbox input[type=checkbox], .checkbox-inline input[type=checkbox], .radio input[type=radio], .radio-inline input[type=radio] {
            position: static; 
            margin-left: 0; 
        }
        .table-hover>tbody>tr:hover {
            background-color: white;
        }

        @media print {
            table td.highlighted {
                background-color: rgba(247, 202, 24, 0.3) !important;
            }
        }

        .draggableClass{
            min-height: 20px;
        }

    </style>
@endsection

@section('local-script')

    <!--<script src="http://liceo_compras.test/js/dist/locale/es.js" type="text/javascript"></script>
    <script src="http://liceo_compras.test/js/moment.min.js"></script>-->
    <script src="{{url('/js/objectToFormData.js')}}"></script>

    <script type="text/javascript">
        
        //Vue.use(VeeValidate, {locale:'es'});
        Vue.filter('currency', function (value) {
            return '$' + value.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
        })

        var section = new Vue({
            el: '#sectionApp',
            data: {
                seleccionados: {!!json_encode(old('seleccionados')?:[])!!},
                observaciones:{
                    preseleccion: `{!!old('observaciones.preseleccion')!!}`
                },                
                requisicion_de_compra: {!!json_encode($supplier_order->requisicion_de_compra?:[])!!},
                unidades: {!!json_encode($unidades?:[])!!},
                documentos_anexos: {!!json_encode(old('documentos_anexos')?:[])!!},
            },
            methods: {
                validateBeforeSubmit() {
                    this.$validator.validateAll().then((result) => {
                        if (result) {
                            //document.querySelector('#form-section').submit();
                            var that= this;
                            var data= JSONToFormData({
                                observaciones: that.observaciones, 
                                seleccionados: that.seleccionados,
                                _method:"PUT",
                                _token:"{{ csrf_token() }}"
                            });

                            var inputs = document.querySelectorAll("#form-section input[name^='documentos_anexos']");
                            for (i = 0; i < inputs.length; i++) {
                                if(inputs[i].files && inputs[i].files[0]){
                                    data.append(inputs[i].name, inputs[i].files[0]);
                                }else{
                                    data.append(inputs[i].name, inputs[i].value);
                                    console.log(inputs[i].value);
                                }
                            }
                            $.ajax({
                                method: "POST",
                                url: "{{ route('supplier_orders.store_order',$supplier_order->id) }}",
                                data: data,
                                cache: false,
                                contentType: false,
                                processData: false,
                            })
                            .done((data) => {
                                console.log(data);
                                //alert("Data Saved:");
                                window.location.replace("{{ route('supplier_orders.index_quotation') }}")
                            })
                            .fail((data)=>{
                                console.log(data.responseJSON.message);
                                alert( `Error al procesar la solicitud: ${data.responseJSON.message}`);
                            });
                          
                        }else{
                            alert('Existen errores en el formulario!!!');
                        }
                    });
                },
                removeDatailFromProvider(index,provider){
                    eval('this.'+provider+'.splice('+index+',1)');
                },
                jsonCopy(src) {
                    console.log('Clone event');
                    let data= JSON.parse(JSON.stringify(src));
                    data['cantidad']= '';
                    data['descripcion_provedor']= '';
                    data['valor_unitario']= '';
                    data['descuento_tarifa']= '';
                    data['iva_tarifa']= '';
                    return data;
                },
                clone(e, lista, cotizacion){
                    if(e.added){
                        let data= JSON.parse(JSON.stringify(e.added.element));
                        let idx= e.added.newIndex

                        if(data.cotizaciones[cotizacion]['status']=="true"){
                            data['cantidad']= data.cotizaciones[cotizacion]['cantidad'];
                            data['descripcion_provedor']= data.cotizaciones[cotizacion]['descripcion'];
                            data['valor_unitario']= data.cotizaciones[cotizacion]['valor_unitario'];
                            data['descuento_tarifa']= data.cotizaciones[cotizacion]['descuento_tarifa'];
                            data['iva_tarifa']= data.cotizaciones[cotizacion]['iva_tarifa'];
                            console.log(data,idx);
                            eval('this.'+lista+'.splice('+idx+',1)');
                            eval('this.'+lista+'.splice('+idx+',0,data)');
                        }else{
                            eval('this.'+lista+'.splice('+idx+',1)');
                            alert('El proveedor no cotizó este detalle');    
                        }
                    }
                },
                calcularValor(item){
                    return ( item.valor_unitario * (1 -( item.descuento_tarifa/100 ) ) ) * (1+(item.iva_tarifa/100));
                },
                addDocumentosAnexos(){
                    this.documentos_anexos.push({nombre:'', file:''});
                },
                removeDocumentosAnexos(index){
                    this.documentos_anexos.splice(index,1);
                }
            },
            computed:{
            },
            mounted(){
                $(document).on('click', '.box-header .box-tools button.sortable-button', function(){
                    $('i', this).toggleClass('fa-window-minimize');
                    $('i', this).toggleClass('fa-window-maximize');
                });
            }
        });

        //section.$validator.installDateTimeValidators(moment);
        
        @foreach($errors->toArray()?:[] as $key=>$value)
            section.errors.add('{{dot_to_html($key)}}', '{{$errors->first($key)}}');
        @endforeach
            
    </script>
    {{ session()->forget('_old_input') }}
@endsection