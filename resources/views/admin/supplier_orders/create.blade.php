@extends('adminlte::layouts.app')

@section('htmlheader_title')
	Ordenes De Proveedor
@endsection

@section('contentheader_title')
	Ordenes De Proveedor
@endsection

@section('contentheader_description')
	@lang('message.create')
@endsection

@section('main-content')
	<div class="row" id="sectionApp" v-cloak>
		<div class="col-md-12">
			@include('includes.session-alert')
			<form id="form-section" action="{{ route('supplier_orders.store') }}" method="POST" enctype="multipart/form-data" @submit.prevent="validateBeforeSubmit">
				<input type="hidden" name="_method" value="POST">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="row">
    				<div class="col-md-4">@include('HTML_VUE.select_simple',[ 'label'=>'Area Que Solicita', 'placeholder'=>'Area Que Solicita', 'model'=>'area_que_solicita', 'name'=>"'area_que_solicita'", 'validate'=>"'required'", 'options'=>'areas_operativas'])</div>
    				<div class="col-md-4">@include('HTML_VUE.select_simple',[ 'label'=>'Persona Que Realiza', 'placeholder'=>'Persona Que Realiza', 'model'=>'persona_que_realiza', 'name'=>"'persona_que_realiza'", 'validate'=>"'required'",'options'=>'areas_operativas'])</div>
    				<div class="col-md-4">@include('HTML_VUE.select_simple',[ 'label'=>'Persona Que Solicita', 'placeholder'=>'Persona Que Solicita', 'model'=>'persona_que_solicita', 'name'=>"'persona_que_solicita'", 'validate'=>"'required'",'options'=>'areas_operativas'])</div>
                </div>
                <div class="row">
    				<div class="col-md-3">@include('HTML_VUE.input_date',[ 'label'=>'Fecha De Entrega', 'placeholder'=>'Fecha De Entrega', 'model'=>'fecha_de_entrega', 'name'=>"'fecha_de_entrega'", 'validate'=>"'required|date_format:YYYY-MM-DD'"])</div>
    				<div class="col-md-3">@include('HTML_VUE.input_text',[ 'label'=>'Consecutivo', 'placeholder'=>'Consecutivo', 'model'=>'consecutivo', 'name'=>"'consecutivo'", 'validate'=>"'required'"])</div>
                    <div class="col-md-3">@include('HTML_VUE.select_simple',[ 'label'=>'Estado', 'placeholder'=>'Estado', 'model'=>'estado', 'name'=>"'estado'", 'validate'=>"'required'",'options'=>'estados'])</div>
                    <div class="col-md-3">@include('HTML_VUE.select_simple',[ 'label'=>'Prioridad', 'placeholder'=>'Prioridad', 'model'=>'prioridad', 'name'=>"'prioridad'", 'validate'=>"'required'",'options'=>'prioridades'])</div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-primary container-sortable">
                            <div class="box-header with-border">
                                <h3 class="box-title">Requisición De Compra</h3>
                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-primary btn-xs" v-on:click="addRequisicionDeCompra('Agregar Detalle')">Agregar Detalle</button>                              
                                    
                                </div>
                            </div>
                            <div class="box-body">
                                <draggable v-model="requisicion_de_compra" :options="{draggable:'.box'}">
                                    <div v-for="(item, index) in requisicion_de_compra" class="box">
                                        <div class="box-header with-border">
                                            <h3 class="box-title">
                                                <span class="handle">
                                                    <i class="fa fa-ellipsis-v"></i>
                                                    <i class="fa fa-ellipsis-v"></i>
                                                </span>
                                                Detalle @{{index+1}}
                                                
                                            </h3>
                                            <div class="box-tools pull-right">
                                                <button type="button" class="btn btn-box-tool sortable-button" :data-target="'#requisicion_de_compra_'+index" data-toggle="collapse" title="Collapse"><i class="fa fa-window-maximize"></i></button>
                                                <button type="button" class="btn btn-box-tool" title="Remove" v-on:click="removeRequisicionDeCompra(index)"><i class="fa fa-times"></i></button>
                                            </div>
                                        </div>
                                        <div class="box-body collapse" :id="'requisicion_de_compra_'+index">
                                            <div class="row">
                                                <div v-if="item['type'] =='Agregar Detalle'">
                                                    <input type="hidden" :name="'requisicion_de_compra['+index+'][type]'" value="Agregar Detalle">                                                    
                                                    <div class="col-md-2">@include('HTML_VUE.select_simple',['model'=>"requisicion_de_compra[index]['data']['unidad']", 'name'=>"'requisicion_de_compra['+index+'][data][unidad]'", 'placeholder'=>'Unidad De Medida', 'label'=>'Unidad De Medida', 'validate'=>"'required'",'options'=>'unidades'])</div>
                                                    <div class="col-md-2">@include('HTML_VUE.input_text',['model'=>"requisicion_de_compra[index]['data']['cantidad']", 'name'=>"'requisicion_de_compra['+index+'][data][cantidad]'", 'placeholder'=>'Cantidad', 'label'=>'Cantidad', 'validate'=>"'required'"])</div>
                                                    <div class="col-md-5">@include('HTML_VUE.textarea',['model'=>"requisicion_de_compra[index]['data']['descripcion']", 'name'=>"'requisicion_de_compra['+index+'][data][descripcion]'", 'placeholder'=>'Descripción Detallada Del Producto o Servicio', 'label'=>'Descripción Detallada Del Producto o Servicio', 'validate'=>"'required'"])</div>
                                                    <div class="col-md-3">@include('HTML_VUE.textarea',['model'=>"requisicion_de_compra[index]['data']['especificaciones_tecnicas']", 'name'=>"'requisicion_de_compra['+index+'][data][especificaciones_tecnicas]'", 'placeholder'=>'Especificaciones Técnicas', 'label'=>'Especificaciones Técnicas', 'validate'=>"'required'"])</div>                                                                                                    
                                                </div>                                   
                                            </div>
                                        </div>
                                    </div>
                                </draggable>
                            </div>
                        </div>
                    </div>                                      
                </div>
                <div class="row">
                    <div class="col-md-12">@include('HTML_VUE.input_text',[ 'label'=>'Observaciones', 'placeholder'=>'Observaciones', 'model'=>'observaciones', 'name'=>"'observaciones'", 'validate'=>"''"])</div>
                </div>
                <div class="row">
    				<div class="col-md-12">
    					<button type="submit" class="btn btn-primary">{{trans('message.create')}}</button>
    				</div>
                </div>
			</form>
		</div>
	</div>
@endsection

@section('local-css')
	<style type="text/css">
		[v-cloak] {
			display: none;
		}
		.grid-images-item{
            width: 100%;
            padding-top: 100%;
            border:solid 1px;
            background-size: contain;
            background-position: 50% 50%;
            background-repeat: no-repeat;
        }
        .container-sortable{
            background: transparent;
            border: 1px solid #3c8dbc;
            border-top: 3px solid #3c8dbc;
        }
	</style>
@endsection

@section('local-script')

    <!--<script src="http://liceo_compras.test/js/dist/locale/es.js" type="text/javascript"></script>
    <script src="http://liceo_compras.test/js/moment.min.js"></script>-->

    <script type="text/javascript">
        
        //Vue.use(VeeValidate, {locale:'es'});

        var section = new Vue({
            el: '#sectionApp',
            data: {

            	area_que_solicita:`{!!old('area_que_solicita')!!}` ,
                persona_que_realiza:`{!!old('persona_que_realiza')!!}` ,
                persona_que_solicita:`{!!old('persona_que_solicita')!!}` ,
                fecha_de_entrega:`{!!old('fecha_de_entrega')!!}` ,
                consecutivo:`{!!old('consecutivo')!!}` ,
                requisicion_de_compra: {!!json_encode(old('requisicion_de_compra')?:[])!!},
                estado:`{!!old('estado')!!}` ,
                prioridad:`{!!old('prioridad')!!}` ,
                observaciones:`{!!old('observaciones')!!}`,

                areas_operativas: {!!$areas_operativas!!},
                estados: {!!$estados!!},
                prioridades: {!!$prioridades!!},
                unidades: {!!$unidades!!}
            },
            methods: {
                addRequisicionDeCompra(type){
                    elements= {
                        'Agregar Detalle': {
                            type:'Agregar Detalle',
                            data: { 
                                unidad:'',   cantidad:'',   descripcion:'',   especificaciones_tecnicas:''                                 
                            }
                        },                            
                        
                    };
                    this.requisicion_de_compra.push(elements[type]);
                },
                removeRequisicionDeCompra(index){
                    this.requisicion_de_compra.splice(index,1);
                },
                validateBeforeSubmit() {
                    this.$validator.validateAll().then((result) => {
				        if (result) {
				          document.querySelector('#form-section').submit();
				          
				        }else{
				        	alert('Existen errores en el formulario!!!');
				        }
				    });
                },
                addGridImage(e){

                    var model= $(e.currentTarget).data('model');
                    var idx= $(e.currentTarget).data('idx');
                    var info= $(e.currentTarget).data('info');
                    if(idx!=undefined){
                        model= model.replace('index',idx);
                    }
                    console.log('model '+model);
                    console.log('idx '+idx);
                    var files = e.target.files || e.dataTransfer.files;
                    var vm = this;

                    for (var i = 0; i < files.length; i++) {
                        var reader = new FileReader();
                        reader.onload = function(e) {
                            var is_array;
                            eval('is_array= Array.isArray(vm.'+model+');');

                            if(is_array){
                                if(info!=undefined){
                                    console.log('info '+info);
                                    let data={
                                        image: e.target.result,
                                        text1:'',
                                        text2:'',
                                        text3:'',
                                    }
                                    eval('vm.'+model+".push(data);");
                                }else{
                                    eval('vm.'+model+".push(e.target.result);");
                                }
                            }else{
                                eval('vm.'+model+"= e.target.result;");
                            }
                        }
                        reader.readAsDataURL(files[i]);
                    }
                },
                removeGridImage(key,model,index=undefined){
                    console.log('key: '+key+' model: '+model+' index:'+index);
                    if(index != undefined){
                        model= model.replace('index',index);
                    }
                    eval('this.'+model+".splice(key,1);");
                    //this[model].splice(key,1);
                }
            },
            mounted(){
                $(document).on('change', '.input-image', this.addGridImage);
                $(document).on('click', '.box-header .box-tools button.sortable-button', function(){
                    $('i', this).toggleClass('fa-window-minimize');
                    $('i', this).toggleClass('fa-window-maximize');
                });
            }
        });

        //section.$validator.installDateTimeValidators(moment);
        
        @foreach($errors->toArray()?:[] as $key=>$value)
            section.errors.add('{{dot_to_html($key)}}', '{{$errors->first($key)}}');
        @endforeach
        
    </script>
    {{ session()->forget('_old_input') }}
@endsection