@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{!! isset($title)?$title:'Ordenes De Proveedor'!!}
@endsection

@section('contentheader_title')
    {!! isset($title)?$title:'Ordenes De Proveedor'!!}
@endsection

@section('contentheader_description')
    @lang('message.index')
@endsection

@section('main-content')	
	<div class="row" id="sectionApp">
		<div class="col-md-12">
			@include('includes.session-alert')
            @if (isset($params))
            <datatable :columns="{!!$columns!!}" 
            			:url="'{!! route($link, $params) !!}'"
            			:is-unique="false">
            </datatable>    
            @else                
            <datatable :columns="{!!$columns!!}" 
            			:url="'{!! route($link) !!}'"
            			:is-unique="false">
            </datatable>
            @endif
		</div>
        
        <div class="col-md-12">
            @include('includes.button-create',['permission'=>'supplier_orders_create', 'url'=>route('supplier_orders.create')])
            @include('includes.button-action',['text'=>'Crear Requisición','permission'=>'Crear Requisicion', 'url'=>route('supplier_orders.create_requisition')])
        </div>

        @include('includes.delete-modal',['permission'=>'supplier_orders_delete', 'showInput'=> true])
        @include('includes.confirm-modal')
        @include('includes.calification-modal')

        @include('includes.info-modal', [
            'action'=>'observaciones',
            'title'=> 'Observaciones',
        ])

        @include('includes.info-modal', [
            'action'=>'cotizaciones',
            'title'=> 'Cotizaciones',
        ])
	</div>
@endsection
@section('local-script')

    <!--<script src="http://liceo_compras.test/js/dist/locale/es.js" type="text/javascript"></script>
    <script src="http://liceo_compras.test/js/moment.min.js"></script>-->

    <script type="text/javascript">
        
        //Vue.use(VeeValidate, {locale:'es'});

        var section = new Vue({
            el: '#sectionApp',
            data:{
                base_url: '{{route('supplier_orders.index')}}',
                base_url_confirm: '{{route('supplier_orders.index_send')}}',
                base_url_calification: '{{route('supplier_orders.index_send_to_calification')}}',
                to_delete: null,
                to_action: null,
                to_confirm: null,
                to_calification: null,
                info_html: null
            },
            methods:{
                confirmar(e){
                    var id= $(e.currentTarget).data('id');
                    this.to_confirm= id;
                    console.log(this.to_confirm);
                    $('#confirmModal').modal('show');
                },
                calificar(e){
                    var id= $(e.currentTarget).data('id');
                    this.to_calification= id;
                    console.log(this.to_confirm);
                    $('#calificationModal').modal('show');
                },
                delete(e){
                    var id= $(e.currentTarget).data('id');
                    this.to_delete= id;
                    console.log(this.to_delete);
                    $('#deleteModal').modal('show');
                },
                /* rechazar(e){
                    var id= $(e.currentTarget).data('id');
                    this.to_action= id;
                    console.log(this.to_delete);
                    $('#rechazarModal').modal('show');
                }, */
                observaciones(e){
                    var id= $(e.currentTarget).data('id');
                    var that= this;
                    $.get( `{!!url('admin/supplier_orders/observaciones')!!}/${id}`, function( data ) {
                        that.info_html= data;
                        $('#observacionesModal').modal('show');
                    });
                },
                cotizaciones(e){
                    var id= $(e.currentTarget).data('id');
                    var that= this;
                    $.get( `{!!url('admin/supplier_orders/cotizaciones')!!}/${id}`, function( data ) {
                        that.info_html= data;
                        $('#cotizacionesModal').modal('show');
                    });
                },
            },
            mounted(){
                $(document).on("click",".delete-modal", this.delete);
                $(document).on("click",".confirm-modal", this.confirmar);
                $(document).on("click",".calification-modal", this.calificar);
                $(document).on("click",".observaciones-modal", this.observaciones);
                $(document).on("click",".cotizaciones-modal", this.cotizaciones);
            }
        });

        //section.$validator.installDateTimeValidators(moment);
        
    </script>
    {{ session()->forget('_old_input') }}
@endsection