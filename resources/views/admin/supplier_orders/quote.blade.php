@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Plantilla Económica
@endsection

@section('contentheader_title')
    Plantilla Económica
@endsection

@section('contentheader_description')
    
@endsection

@section('main-content')
    <div class="row" id="sectionApp" v-cloak>
        <div class="col-md-12">
            @include('includes.session-alert')
            <h4>Por favor cotizar los items que correspondan a su actividad económica o que desee incluir en su cotización, de lo contrario desmarcar la casilla de cotizar.</h4>
            <form id="form-section" action="{{ route('supplier_orders.update_supplier_quote',$quotation->id) }}" method="POST" enctype="multipart/form-data" @submit.prevent="validateBeforeSubmit">
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-condensed table-bordered">
                            <thead>
                                <tr>
                                    <th>Cotizar</th>
                                    <th>Unidad</th>
                                    <th>Cantidad</th>
                                    <th>Producto o Servicio Solicitado</th>
                                    <th>Descripción Detallada Del Proveedor</th>
                                    <th>V/r Unitario</th>
                                    <th>Dto Unitario (%)</th>
                                    <th>V/r Unitario con descuento</th>
                                    <th>Iva Tarifa(%)</th>
                                    <th>Iva Valor</th>
                                    <th>V/r Total</th>
                                </tr>
                            </thead>
                            <tbody>

                                <tr v-for="(item,index) in cotizacion['contenido']">
                                    <td>
                                        <input type="checkbox" v-model="cotizacion['contenido'][index]['status']" :name="'cotizacion[contenido]['+index+'][status]'" value="true">
                                        <input v-if="! cotizacion['contenido'][index]['status']" type="hidden" model="cotizacion['contenido'][index]['status']" :name="'cotizacion[contenido]['+index+'][status]'" value="false">
                                    </td>
                                    <td>@{{item['unidad']}}</td>
                                    <td v-if="cotizacion['contenido'][index]['status']">@include('HTML_VUE.input_number',['model'=>"cotizacion['contenido'][index]['cantidad']",'name'=>"'cotizacion[contenido]['+index+'][cantidad]'",'placeholder'=>'cantidad','validate'=>"'required'",'step'=>'1'])</td>
                                    <td v-else>@include('HTML_VUE.input_number',['model'=>"cotizacion['contenido'][index]['cantidad']",'name'=>"'cotizacion[contenido]['+index+'][cantidad]'",'placeholder'=>'cantidad','validate'=>"''",'step'=>'1'])</td>
                                    <td>@{{item['nombre']}}</td>

                                    <td v-if="cotizacion['contenido'][index]['status']">@include('HTML_VUE.textarea',['model'=>"cotizacion['contenido'][index]['descripcion']",'name'=>"'cotizacion[contenido]['+index+'][descripcion]'",'placeholder'=>'Nombre del producto o servicio','validate'=>"'required'"])</td>
                                    <td v-else>@include('HTML_VUE.textarea',['model'=>"cotizacion['contenido'][index]['descripcion']",'name'=>"'cotizacion[contenido]['+index+'][descripcion]'",'placeholder'=>'Nombre del producto o servicio','validate'=>"''"])</td>

                                    <td v-if="cotizacion['contenido'][index]['status']">@include('HTML_VUE.input_number',['model'=>"cotizacion['contenido'][index]['valor_unitario']",'name'=>"'cotizacion[contenido]['+index+'][valor_unitario]'",'placeholder'=>'V/r Unitario','validate'=>"'required|decimal:2'",'step'=>'any'])</td>
                                    <td v-else>@include('HTML_VUE.input_number',['model'=>"cotizacion['contenido'][index]['valor_unitario']",'name'=>"'cotizacion[contenido]['+index+'][valor_unitario]'",'placeholder'=>'V/r Unitario','validate'=>"'decimal:2'",'step'=>'any'])</td>

                                    <td v-if="cotizacion['contenido'][index]['status']">@include('HTML_VUE.input_number',['model'=>"cotizacion['contenido'][index]['descuento_tarifa']",'name'=>"'cotizacion[contenido]['+index+'][descuento_tarifa]'",'placeholder'=>'Dto tarifa','validate'=>"'required|decimal:2'", 'step'=>'any'])</td>
                                    <td v-else>@include('HTML_VUE.input_number',['model'=>"cotizacion['contenido'][index]['descuento_tarifa']",'name'=>"'cotizacion[contenido]['+index+'][descuento_tarifa]'",'placeholder'=>'Dto tarifa','validate'=>"'decimal:2'", 'step'=>'any'])</td>

                                    <td class="text-right">@{{ (item.valor_unitario * (1-(item.descuento_tarifa/100))) | currency }}</td>

                                    <td v-if="cotizacion['contenido'][index]['status']">@include('HTML_VUE.input_number',['model'=>"cotizacion['contenido'][index]['iva_tarifa']",'name'=>"'cotizacion[contenido]['+index+'][iva_tarifa]'",'placeholder'=>'iva tarifa','validate'=>"'required|decimal:2'", 'step'=>'any'])</td>
                                    <td v-else>@include('HTML_VUE.input_number',['model'=>"cotizacion['contenido'][index]['iva_tarifa']",'name'=>"'cotizacion[contenido]['+index+'][iva_tarifa]'",'placeholder'=>'iva tarifa','validate'=>"'decimal:2'",'step'=>'any'])</td>

                                    <td class="text-right">@{{ item.cantidad * ( item.valor_unitario * (1-(item.descuento_tarifa/100)) * (item.iva_tarifa/100) ) | currency }}</td>
                                    <td class="text-right">@{{ item.cantidad * ( item.valor_unitario * (1-(item.descuento_tarifa/100)) * (1+(item.iva_tarifa/100)) ) | currency }}</td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="8"></td>
                                    <td colspan="2">Subtotal:</td>
                                    <td class="text-right"><h4>@{{calcularSubtotal | currency}}</h4></td>
                                </tr>
                                <tr>
                                    <td colspan="8"></td>
                                    <td colspan="2">IVA:</td>
                                    <td class="text-right"><h4>@{{calcularIva | currency}}</h4></td>
                                </tr>
                                <tr>
                                    <td colspan="8">
                                        <div class="row">
                                            <div class="col-md-4">
                                                @include('HTML_VUE.input_number',['model'=>"cotizacion['descuento_general']",'name'=>"'cotizacion[descuento_general]'",'label'=>'Descuento General','placeholder'=>'Descuento General','validate'=>"'required|decimal:2'",'step'=>'any'])
                                                <h4>@{{  descuentoGeneral | currency }}</h4>
                                            </div>
                                        </div>
                                    </td>
                                    <td colspan="2">Total:</td>
                                    <td class="text-right"><h4>@{{calcularTotal | currency}}</h4></td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-primary container-sortable">
                            <div class="box-header with-border">
                                <h3 class="box-title">Criterios de Seleccion</h3>
                            </div>
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-12">@include('HTML_VUE.textarea',['model'=>"cotizacion['criterios']['calidad']", 'name'=>"'cotizacion[criterios][calidad]'", 'placeholder'=>'Calidad del producto o servicio? por qué?', 'label'=>'1. Calidad del producto o servicio? por qué?', 'validate'=>"'required'"])</div>
                                    <div class="col-md-12">@include('HTML_VUE.select_simple',['model'=>"cotizacion['criterios']['forma_de_pago']", 'name'=>"'cotizacion[criterios][forma_de_pago]'", 'placeholder'=>'Forma de pago?', 'label'=>'2. Forma de pago?', 'validate'=>"'required'",'options'=>'formas_de_pago'])</div>
                                    <div class="col-md-12">@include('HTML_VUE.textarea',['model'=>"cotizacion['criterios']['descuento']", 'name'=>"'cotizacion[criterios][descuento]'", 'placeholder'=>'Tiene descuento por pago inmediato? Qué porcentaje?', 'label'=>'3. Tiene descuento por pago inmediato? Qué porcentaje?', 'validate'=>"'required'"])</div>
                                    <div class="col-md-12">@include('HTML_VUE.textarea',['model'=>"cotizacion['criterios']['tiempo_de_entrega']", 'name'=>"'cotizacion[criterios][tiempo_de_entrega]'", 'placeholder'=>'Tiempo de entrega del producto o servicio?', 'label'=>'4. Tiempo de entrega del producto o servicio?', 'validate'=>"'required'"])</div>
                                    <div class="col-md-12">@include('HTML_VUE.textarea',['model'=>"cotizacion['criterios']['validez_de_la_oferta']", 'name'=>"'cotizacion[criterios][validez_de_la_oferta]'", 'placeholder'=>'Validez de la oferta?', 'label'=>'5. Validez de la oferta?', 'validate'=>"'required'"])</div>
                                    <div class="col-md-12">@include('HTML_VUE.textarea',['model'=>"cotizacion['criterios']['garantia']", 'name'=>"'cotizacion[criterios][garantia]'", 'placeholder'=>'El producto o servicio tiene garantía? Cuál?', 'label'=>'6. El producto o servicio tiene garantía? Cuál?', 'validate'=>"'required'"])</div>
                                    <div class="col-md-12">@include('HTML_VUE.textarea',['model'=>"cotizacion['criterios']['servicio_postventa']", 'name'=>"'cotizacion[criterios][servicio_postventa]'", 'placeholder'=>'Servicio postventa del producto o servicio?', 'label'=>'7. Servicio postventa del producto o servicio?', 'validate'=>"'required'"])</div>
                                    <div class="col-md-12">@include('HTML_VUE.select_simple',['model'=>"cotizacion['criterios']['experiencia']", 'name'=>"'cotizacion[criterios][experiencia]'", 'placeholder'=>'Tiempo de experiencia certificada?', 'label'=>'8. Tiempo de experiencia certificada?', 'validate'=>"'required'",'options'=>'tiempos_de_experiencia'])</div>
                                    <div class="col-md-12">@include('HTML_VUE.input_text',['model'=>"cotizacion['criterios']['numero_de_clientes']", 'name'=>"'cotizacion[criterios][numero_de_clientes]'", 'placeholder'=>'Número de clientes', 'label'=>'10. Número de clientes', 'validate'=>"'required'"])</div>
                                    <div class="col-md-12">@include('HTML_VUE.textarea',['model'=>"cotizacion['criterios']['certificado_de_gestion']", 'name'=>"'cotizacion[criterios][certificado_de_gestion]'", 'placeholder'=>'La empresa se encuentra certificada en sistemas de gestion de calidad? Cuál?', 'label'=>'11. La empresa se encuentra certificada en sistemas de gestion de calidad? Cuál?', 'validate'=>"'required'"])</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">@include('HTML_VUE.textarea',[ 'label'=>'Observaciones', 'placeholder'=>'Observaciones', 'model'=>'cotizacion.observaciones', 'name'=>"'cotizacion[observaciones]'", 'validate'=>"''"])</div>
                </div>
                <div class="row">
                    <div class="col-md-12">@include('HTML_VUE.input_file',[ 'label'=>'Documento Anexo', 'placeholder'=>'Documento Anexo', 'model'=>'cotizacion.documento_anexo', 'name'=>"'cotizacion[documento_anexo]'", 'validate'=>"''"])</div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-primary">Enviar Cotización</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('local-css')
    <style type="text/css">
        [v-cloak] {
            display: none;
        }

        .container-sortable{
            background: transparent;
            border: 1px solid #3c8dbc;
            border-top: 3px solid #3c8dbc;
        }

        .table-bordered {
            border: 1px solid #337ab7;
        }

        .table-bordered>tbody>tr>td, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>td, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>thead>tr>th {
            border: 1px solid #337ab7;
        }

        .table-hover>tbody>tr:hover {
            background-color: white;
        }
    </style>
@endsection

@section('local-script')

    <!--<script src="http://liceo_compras.test/js/dist/locale/es.js" type="text/javascript"></script>
    <script src="http://liceo_compras.test/js/moment.min.js"></script>-->

    <script type="text/javascript">
        
        //Vue.use(VeeValidate, {locale:'es'});
        Vue.filter('currency', function (value) {
            value = Number(value);
            return '$' + value.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
        })

        var section = new Vue({
            el: '#sectionApp',
            data: {
                requisicion_de_compra: {!!json_encode(old('requisicion_de_compra')?:[])!!},
                formas_de_pago: {!!$formas_de_pago!!},
                tiempos_de_experiencia: {!!$tiempos_de_experiencia!!},                
                
                cotizacion:{
                    descuento_general: {!!old('cotizacion.descuento_general')?:0!!},
                    contenido: {!!json_encode(old('cotizacion.contenido')?:[])!!},
                    criterios: {
                        calidad: `{!!old('cotizacion.criterios.calidad')!!}`,
                        forma_de_pago: `{!!old('cotizacion.criterios.forma_de_pago')!!}`,
                        descuento: `{!!old('cotizacion.criterios.descuento')!!}`,
                        tiempo_de_entrega: `{!!old('cotizacion.criterios.tiempo_de_entrega')!!}`,
                        validez_de_la_oferta: `{!!old('cotizacion.criterios.validez_de_la_oferta')!!}`,
                        garantia: `{!!old('cotizacion.criterios.garantia')!!}`,
                        servicio_postventa: `{!!old('cotizacion.criterios.servicio_postventa')!!}`,
                        experiencia: `{!!old('cotizacion.criterios.experiencia')!!}`,
                        numero_de_clientes: `{!!old('cotizacion.criterios.numero_de_clientes')!!}`,
                        certificado_de_gestion: `{!!old('cotizacion.criterios.certificado_de_gestion')!!}`,
                        documentos_iniciales: `{!!old('cotizacion.criterios.documentos_iniciales')!!}`,
                    },
                    observaciones: `{!!old('cotizacion.observaciones')!!}`,
                    documento_anexo: `{!!old('cotizacion.documento_anexo')!!}`,
                }                
                
            },
            methods: {
                validateBeforeSubmit() {
                    this.$validator.validateAll().then((result) => {
                        if (result) {
                          document.querySelector('#form-section').submit();
                          
                        }else{
                            alert('Existen errores en el formulario!!!');
                        }
                    });
                },
                calcularValor(item){
                    return ( item.valor_unitario * (1 -( item.descuento_tarifa/100 ) ) ) * (1+(item.iva_tarifa/100));
                }
            },
            computed:{
                calcularSubtotal: function(){
                    var sum = 0;
                    for (let i=0; i < this.cotizacion.contenido.length; i++) {
                        if(this.cotizacion.contenido[i]['status']){                            
                            sum += Number(this.cotizacion.contenido[i]['cantidad']) * Number(this.cotizacion.contenido[i]['valor_unitario']) * (1 - (Number(this.cotizacion.contenido[i]['descuento_tarifa']/100)));                       
                        }
                    }
                    return sum;
                },
                calcularIva: function(){
                    var sum = 0;
                    for (let i=0; i < this.cotizacion.contenido.length; i++) {
                        if(this.cotizacion.contenido[i]['status']){
                            sum += Number(this.cotizacion.contenido[i]['cantidad']) * (Number(this.cotizacion.contenido[i]['valor_unitario'])* (1 - (Number(this.cotizacion.contenido[i]['descuento_tarifa']/100)))) * Number(this.cotizacion.contenido[i]['iva_tarifa'])/100;
                        }
                    }
                    return sum;
                },
                calcularTotal: function(){                    
                    return (this.calcularSubtotal + this.calcularIva) /* - Number(this.cotizacion.descuento_general) */;
                },
                descuentoGeneral: function(){
                    return this.cotizacion['descuento_general'];
                }
            },
            mounted(){
                $(document).on('click', '.box-header .box-tools button.sortable-button', function(){
                    $('i', this).toggleClass('fa-window-minimize');
                    $('i', this).toggleClass('fa-window-maximize');
                });
            }
        });

        //section.$validator.installDateTimeValidators(moment);
        
        @foreach($errors->toArray()?:[] as $key=>$value)
            section.errors.add('{{dot_to_html($key)}}', '{{$errors->first($key)}}');
        @endforeach
        
    </script>
    {{ session()->forget('_old_input') }}
@endsection