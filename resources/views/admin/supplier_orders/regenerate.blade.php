@extends('adminlte::layouts.app')

@section('htmlheader_title')
	Habilitar la generación de ordenes nuevamente
@endsection

@section('contentheader_title')
	Habilitar la generación de ordenes nuevamente
@endsection

@section('contentheader_description')
    consecutivo {{$supplier_order->id}}
@endsection

@section('main-content')
	<div class="row" id="sectionApp" v-cloak>
		<div class="col-md-12">
			@include('includes.session-alert')
			<form id="form-section" action="{{ route('supplier_orders.regenerate_put', $supplier_order->id) }}" method="POST" enctype="multipart/form-data" @submit.prevent="validateBeforeSubmit">
				<input type="hidden" name="_method" value="PUT">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="row">
                    <div class="col-md-12">@include('HTML_VUE.textarea',[ 'label'=>'Observaciones', 'placeholder'=>'Observaciones', 'model'=>'observaciones.regenerar', 'name'=>"'observaciones[regenerar]'", 'validate'=>"''"])</div>
                </div>
                <div class="row">
    				<div class="col-md-12">
    					<button type="submit" class="btn btn-primary">Habilitar Generación De Ordenes</button>
    				</div>
                </div>
			</form>
		</div>
	</div>
@endsection

@section('local-css')
	<style type="text/css">
		[v-cloak] {
			display: none;
		}
		.grid-images-item{
            width: 100%;
            padding-top: 100%;
            border:solid 1px;
            background-size: contain;
            background-position: 50% 50%;
            background-repeat: no-repeat;
        }
        .container-sortable{
            background: transparent;
            border: 1px solid #3c8dbc;
            border-top: 3px solid #3c8dbc;
        }
	</style>
@endsection

@section('local-script')

    <!--<script src="http://liceo_compras.test/js/dist/locale/es.js" type="text/javascript"></script>
    <script src="http://liceo_compras.test/js/moment.min.js"></script>-->

    <script type="text/javascript">
        
        //Vue.use(VeeValidate, {locale:'es'});

        var section = new Vue({
            el: '#sectionApp',
            data: {
                observaciones:{
                    regenerar: `{!!old('observaciones.regenerar')!!}`
                }
                
            },
            methods: {	            
                validateBeforeSubmit() {
                    this.$validator.validateAll().then((result) => {
				        if (result) {
				          document.querySelector('#form-section').submit();
				          
				        }else{
				        	alert('Existen errores en el formulario!!!');
				        }
				    });
                }
            },
            mounted(){
            }
        });

        //section.$validator.installDateTimeValidators(moment);
        
        @foreach($errors->toArray()?:[] as $key=>$value)
            section.errors.add('{{dot_to_html($key)}}', '{{$errors->first($key)}}');
        @endforeach
        
    </script>
    {{ session()->forget('_old_input') }}
@endsection