@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Solicitud de compra
@endsection

@section('contentheader_title')
    Solicitud de compra
@endsection

@section('contentheader_description')
    Agregar nueva
@endsection

@section('main-content')
    <div class="row" id="sectionApp" v-cloak>
        <div class="col-md-12">
            @include('includes.session-alert')
            <form id="form-section" action="{{ route('supplier_orders.store_requisition') }}" method="POST" enctype="multipart/form-data" @submit.prevent="validateBeforeSubmit">
                <input type="hidden" name="_method" value="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="row">                    
                    <div class="col-md-4">@include('HTML_VUE.input_text',[ 'label'=>'Proveedor Recomendado', 'placeholder'=>'Proveedor Recomendado', 'model'=>"proveedor_recomendado['nombre']", 'name'=>"'proveedor_recomendado[nombre]'", 'validate'=>"''"])</div>
                    <div class="col-md-4">@include('HTML_VUE.input_text',[ 'label'=>'Persona De Contacto', 'placeholder'=>'Persona De Contacto', 'model'=>"proveedor_recomendado['contacto']['nombre']", 'name'=>"'proveedor_recomendado[contacto][nombre]'", 'validate'=>"''"])</div>
                    <div class="col-md-4">@include('HTML_VUE.input_text',[ 'label'=>'Teléfono De Contacto', 'placeholder'=>'Teléfono De Contacto', 'model'=>"proveedor_recomendado['contacto']['telefono']", 'name'=>"'proveedor_recomendado[contacto][telefono]'", 'validate'=>"''"])</div>
                </div>

                <div class="row">
                    <div class="col-md-4">@include('HTML_VUE.select_simple',[ 'label'=>'Persona Que Solicita', 'placeholder'=>'Persona Que Solicita', 'model'=>'persona_que_solicita', 'name'=>"'persona_que_solicita'", 'validate'=>"'required'",'options'=>'empleados'])</div>
                    <div class="col-md-4">@include('HTML_VUE.input_date',[ 'label'=>'Fecha De Entrega', 'placeholder'=>'Fecha De Entrega', 'model'=>'fecha_de_entrega', 'name'=>"'fecha_de_entrega'", 'validate'=>"'required|date_format:YYYY-MM-DD'"])</div>
                    <div class="col-md-4">@include('HTML_VUE.select_simple',[ 'label'=>'Prioridad', 'placeholder'=>'Prioridad', 'model'=>'prioridad', 'name'=>"'prioridad'", 'validate'=>"'required'",'options'=>'prioridades'])</div>
                </div>

                <div class="row">
                    <div class="col-md-4">@include('HTML_VUE.select_simple',[ 'label'=>'Gerencia que autoriza', 'placeholder'=>'Gerencia que autoriza', 'model'=>'gerencia_que_autoriza', 'name'=>"'gerencia_que_autoriza'", 'validate'=>"'required'",'options'=>'gerencias'])</div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-primary container-sortable">
                            <div class="box-header with-border">
                                <h3 class="box-title">Requisición De Compra</h3>
                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-primary btn-xs" v-on:click="addRequisicionDeCompra()">Agregar Detalle</button>                              
                                    
                                </div>
                            </div>
                            <div class="box-body">
                                <draggable v-model="requisicion_de_compra" :options="{handle:'.handle'}">
                                    <div v-for="(item, index) in requisicion_de_compra" class="box">
                                        <div class="box-header with-border">
                                            <h3 class="box-title">
                                                <span class="handle" style="cursor:pointer;">
                                                    <i class="fa fa-ellipsis-v"></i>
                                                    <i class="fa fa-ellipsis-v"></i>
                                                </span>
                                                Detalle @{{index+1}}
                                                
                                            </h3>
                                            <div class="box-tools pull-right">
                                                <button type="button" class="btn btn-box-tool sortable-button" :data-target="'#requisicion_de_compra_'+index" data-toggle="collapse" title="Collapse"><i class="fa fa-window-maximize"></i></button>
                                                <button type="button" class="btn btn-box-tool" title="Remove" v-on:click="removeRequisicionDeCompra(index)"><i class="fa fa-times"></i></button>
                                            </div>
                                        </div>
                                        <div class="box-body collapse" :id="'requisicion_de_compra_'+index">
                                            <div class="row">
                                                <div class="col-md-2">@include('HTML_VUE.select_simple',['model'=>"requisicion_de_compra[index]['unidad']", 'name'=>"'requisicion_de_compra['+index+'][unidad]'", 'placeholder'=>'Unidad De Medida', 'label'=>'Unidad De Medida', 'validate'=>"'required'",'options'=>'unidades'])</div>
                                                <div class="col-md-2">@include('HTML_VUE.input_number',['model'=>"requisicion_de_compra[index]['cantidad']", 'name'=>"'requisicion_de_compra['+index+'][cantidad]'", 'placeholder'=>'Cantidad', 'label'=>'Cantidad', 'validate'=>"'required'"])</div>
                                                <div class="col-md-5">@include('HTML_VUE.textarea',['model'=>"requisicion_de_compra[index]['descripcion']", 'name'=>"'requisicion_de_compra['+index+'][descripcion]'", 'placeholder'=>'Descripción Detallada Del Producto o Servicio', 'label'=>'Descripción Detallada Del Producto o Servicio', 'validate'=>"'required'"])</div>
                                                <div class="col-md-3">@include('HTML_VUE.textarea',['model'=>"requisicion_de_compra[index]['especificaciones_tecnicas']", 'name'=>"'requisicion_de_compra['+index+'][especificaciones_tecnicas]'", 'placeholder'=>'Especificaciones Técnicas', 'label'=>'Especificaciones Técnicas', 'validate'=>"'required'"])</div>
                                            </div>
                                        </div>
                                    </div>
                                </draggable>
                                <div v-if="requisicion_de_compra.length==0" class="form-group" :class="{'has-error': errors.has('requisicion_de_compra') }">
                                    <input class="form-control input-sm" :name="'requisicion_de_compra'" type="hidden" v-validate="'required'">
                                    <span class="help-block" v-if="errors.has('requisicion_de_compra')">@{{ errors.first('requisicion_de_compra') }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-primary container-sortable">
                            <div class="box-header with-border">
                                <h3 class="box-title">Documentos Anexos</h3>
                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-primary btn-xs" v-on:click="addDocumentosAnexos()">Archivo</button>
                                    
                                </div>
                            </div>
                            <div class="box-body">
                                {{-- <draggable v-model="documentos_anexos" :options="{handle:'.handle'}"> --}}
                                    <div v-for="(item, index) in documentos_anexos" class="box">
                                        <div class="box-header with-border">
                                            <h3 class="box-title">
                                                <span class="handle" style="cursor: pointer;">
                                                    <i class="fa fa-ellipsis-v"></i>
                                                    <i class="fa fa-ellipsis-v"></i>
                                                </span>
                                                @{{item['nombre']}}
                                                
                                            </h3>
                                            <div class="box-tools pull-right">
                                                <button type="button" class="btn btn-box-tool sortable-button" :data-target="'#documentos_anexos_'+index" data-toggle="collapse" title="Collapse"><i class="fa fa-window-maximize"></i></button>
                                                <button type="button" class="btn btn-box-tool" title="Remove" v-on:click="removeDocumentosAnexos(index)"><i class="fa fa-times"></i></button>
                                            </div>
                                        </div>
                                        <div class="box-body collapse" :id="'documentos_anexos_'+index">
                                            <div class="row">
                                                <div class="col-md-8">@include('HTML_VUE.input_text',['model'=>"documentos_anexos[index]['nombre']", 'name'=>"'documentos_anexos['+index+'][nombre]'", 'placeholder'=>'Nombre', 'label'=>'Nombre', 'validate'=>"'required'"])</div>
                                                <div class="col-md-4">@include('HTML_VUE.input_file',['model'=>"documentos_anexos[index]['file']", 'name'=>"'documentos_anexos['+index+'][file]'", 'placeholder'=>'Documento', 'label'=>'Documento', 'validate'=>"'required'"])</div>
                                            </div>
                                        </div>
                                    </div>
                                {{-- </draggable> --}}
                            </div>
                        </div>
                    </div>                                      
                </div>
                <div class="row">
                    <div class="col-md-12">@include('HTML_VUE.textarea',[ 'label'=>'Observaciones', 'placeholder'=>'Observaciones', 'model'=>'observaciones.requisition', 'name'=>"'observaciones[requisition]'", 'validate'=>"''"])</div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-primary">{{trans('message.create')}}</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('local-css')
    <style type="text/css">
        [v-cloak] {
            display: none;
        }
        .grid-images-item{
            width: 100%;
            padding-top: 100%;
            border:solid 1px;
            background-size: contain;
            background-position: 50% 50%;
            background-repeat: no-repeat;
        }
        .container-sortable{
            background: transparent;
            border: 1px solid #3c8dbc;
            border-top: 3px solid #3c8dbc;
        }
    </style>
@endsection

@section('local-script')

    <!--<script src="http://liceo_compras.test/js/dist/locale/es.js" type="text/javascript"></script>
    <script src="http://liceo_compras.test/js/moment.min.js"></script>-->

    <script type="text/javascript">
        
        //Vue.use(VeeValidate, {locale:'es'});

        var section = new Vue({
            el: '#sectionApp',
            data: {

                proveedor_recomendado: {
                    nombre: `{!!old('proveedor_recomendado.nombre')!!}`, 
                    contacto:{
                        nombre: `{!!old('proveedor_recomendado.contacto.nombre')!!}`,
                        telefono: `{!!old('proveedor_recomendado.contacto.telefono')!!}` 
                    }
                },
                persona_que_solicita:`{!!old('persona_que_solicita')!!}` ,
                gerencia_que_autoriza:`{!!old('gerencia_que_autoriza')!!}` ,
                fecha_de_entrega:`{!!old('fecha_de_entrega')!!}` ,
                prioridad:`{!!old('prioridad')!!}` ,
                requisicion_de_compra: {!!json_encode(old('requisicion_de_compra')?:[])!!},
                documentos_anexos: {!!json_encode(old('documentos_anexos')?:[])!!},
                observaciones:{
                    requisition: `{!!old('observaciones.requisition')!!}`
                },
                unidades: {!!$unidades!!},
                empleados: {!!$empleados!!},
                prioridades: {!!$prioridades!!},
                gerencias:{!!$gerencias!!}

            },
            methods: {
                addRequisicionDeCompra(){
                    this.requisicion_de_compra.push({ unidad:'',   cantidad:'',   descripcion:'',   especificaciones_tecnicas:'' });
                },
                removeRequisicionDeCompra(index){
                    this.requisicion_de_compra.splice(index,1);
                },
                addDocumentosAnexos(){
                    this.documentos_anexos.push({nombre:'', file:''});
                },
                removeDocumentosAnexos(index){
                    this.documentos_anexos.splice(index,1);
                },
                validateBeforeSubmit() {
                    this.$validator.validateAll().then((result) => {
                        if (result) {
                          document.querySelector('#form-section').submit();
                          
                        }else{
                            alert('Existen errores en el formulario!!!');
                        }
                    });
                }
            },
            mounted(){
                $(document).on('click', '.box-header .box-tools button.sortable-button', function(){
                    $('i', this).toggleClass('fa-window-minimize');
                    $('i', this).toggleClass('fa-window-maximize');
                });
            }
        });

        //section.$validator.installDateTimeValidators(moment);
        
        @foreach($errors->toArray()?:[] as $key=>$value)
            section.errors.add('{{dot_to_html($key)}}', '{{$errors->first($key)}}');
        @endforeach
        
    </script>
    {{ session()->forget('_old_input') }}
@endsection