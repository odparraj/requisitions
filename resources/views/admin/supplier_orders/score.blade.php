@extends('adminlte::layouts.app')

@section('htmlheader_title')
	Encuesta de Satisfacción
@endsection

@section('contentheader_title')
	Encuesta de Satisfacción
@endsection

@section('contentheader_description')
	
@endsection

@section('main-content')
	<div class="row" id="sectionApp" v-cloak>
		<div class="col-md-12">
            @include('includes.session-alert')
            
            <p><b>Malo:</b> No cumple con el criterio evaluado. Se debe cambiar al Proveedor.</p>
            <p><b>Bueno:</b> Cumple con el criterio establecido dentro de las especificaciones contratadas.</p>
            <p><b>Excelente:</b> El resultado supera las expectativas del criterio evaluado. Son los mejores dentro del criterio evaluado.<p>

			<form id="form-section" action="{{ route('supplier_orders.update_score', $supplier_order->id) }}" method="POST" enctype="multipart/form-data" @submit.prevent="validateBeforeSubmit">
				<input type="hidden" name="_method" value="PUT">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
                
                <div class="row" v-for="purchase_order in supplier_order.purchase_orders">
                    
                    <div class="col-md-12">
                        <h1>Calificación del proveedor: @{{purchase_order.quotation.supplier.informacion_general.razon_social}}</h1>
                        <p><b>@{{items[purchase_order.id]}}</b></p>
                        <table class="table table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th class="text-left" width="70%">Pregunta</th>
                                    <th class="text-center" width="10%">Malo</th>
                                    <th class="text-center" width="10%">Bueno</th>
                                    <th class="text-center" width="10%">Excelente</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="pregunta in preguntas">
                                    <td>
                                        <h4>@{{pregunta.enunciado}}</h4>
                                        <div class="form-group" :class="{'has-error': errors.has('respuestas['+purchase_order.id+']['+pregunta.id+']') }">
                                            <span class="help-block" v-if="errors.has('respuestas['+purchase_order.id+']['+pregunta.id+']')">@{{errors.first('respuestas['+purchase_order.id+']['+pregunta.id+']')}}</span>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="radio">
                                            <label>
                                                <img class="img-responsive" src="{{ url('images/malo.png') }}">
                                                <input type="radio" v-validate="'required'" :name="'respuestas['+purchase_order.id+']['+pregunta.id+']'" value="1">
                                            </label>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="radio">
                                            <label>
                                                <img class="img-responsive" src="{{ url('images/bueno.png') }}">
                                                <input type="radio" v-validate="'required'" :name="'respuestas['+purchase_order.id+']['+pregunta.id+']'" value="2">
                                            </label>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="radio">
                                            <label>
                                                <img class="img-responsive" src="{{ url('images/excelente.png') }}">
                                                <input type="radio" v-validate="'required'" :name="'respuestas['+purchase_order.id+']['+pregunta.id+']'" value="3">
                                            </label>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">                            
                            <label class="control-label">Observaciones</label>                           
                            <textarea class="form-control"
                                    v-model="observaciones[purchase_order.id]"
                                    :name="'observaciones['+purchase_order.id+']'"
                                    placeholder="observaciones"
                                    rows="2" style="resize: vertical;"></textarea>
                        </div>
                    </div>
                </div>
                
                <div class="row">
    				<div class="col-md-12">
    					<button type="submit" class="btn btn-primary">Enviar Calificación</button>
    				</div>
                </div>
			</form>
		</div>
	</div>
@endsection

@section('local-css')
	<style type="text/css">
		[v-cloak] {
			display: none;
		}
		.grid-images-item{
            width: 100%;
            padding-top: 100%;
            border:solid 1px;
            background-size: contain;
            background-position: 50% 50%;
            background-repeat: no-repeat;
        }
        .container-sortable{
            background: transparent;
            border: 1px solid #3c8dbc;
            border-top: 3px solid #3c8dbc;
        }

        .table-bordered {
            border: 1px solid #337ab7;
        }

        .table-bordered>tbody>tr>td, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>td, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>thead>tr>th {
            border: 1px solid #337ab7;
        }

        .checkbox label, .radio label {
            min-height: 20px;
            padding-left: 0;
            margin-bottom: 0;
            font-weight: 400;
            cursor: pointer;
        }

        .checkbox input[type=checkbox], .checkbox-inline input[type=checkbox], .radio input[type=radio], .radio-inline input[type=radio] {
            position: static; 
            margin-left: 0; 
        }
        .table-hover>tbody>tr:hover {
            background-color: white;
        }

	</style>
@endsection

@section('local-script')

    <!--<script src="http://liceo_compras.test/js/dist/locale/es.js" type="text/javascript"></script>
    <script src="http://liceo_compras.test/js/moment.min.js"></script>-->

    <script type="text/javascript">
        
        //Vue.use(VeeValidate, {locale:'es'});

        var section = new Vue({
            el: '#sectionApp',
            data: {
                supplier_order:{!!$supplier_order!!},
                preguntas:{!!$preguntas!!},
                items: {!!$items!!},
                observaciones:{}
            },
            methods: {	            
                validateBeforeSubmit() {
                    this.$validator.validateAll().then((result) => {
				        if (result) {
				          document.querySelector('#form-section').submit();
				          
				        }else{
				        	alert('Existen errores en el formulario!!!');
				        }
				    });
                }
            },
            mounted(){
            }
        });

        //section.$validator.installDateTimeValidators(moment);
        
        @foreach($errors->toArray()?:[] as $key=>$value)
            section.errors.add('{{dot_to_html($key)}}', '{{$errors->first($key)}}');
        @endforeach
        
    </script>
    {{ session()->forget('_old_input') }}
@endsection