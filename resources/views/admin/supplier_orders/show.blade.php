@extends('adminlte::layouts.app')

@section('htmlheader_title')
	Ordenes De Proveedor
@endsection

@section('contentheader_title')
	Ordenes De Proveedor
@endsection

@section('contentheader_description')
	Show
@endsection

@section('main-content')
	<div class="row" id="sectionApp" v-cloak>
		<div class="col-md-12">
			@include('includes.session-alert')
			<form id="form-section">
                <div class="row">
    				<div class="col-md-12">@include('HTML_VUE.select_simple',[ 'label'=>'Area Que Solicita', 'placeholder'=>'Area Que Solicita', 'model'=>'area_que_solicita', 'name'=>"'area_que_solicita'", 'validate'=>"'required'"])</div>
    				<div class="col-md-12">@include('HTML_VUE.input_text',[ 'label'=>'Persona Que Realiza', 'placeholder'=>'Persona Que Realiza', 'model'=>'persona_que_realiza', 'name'=>"'persona_que_realiza'", 'validate'=>"'required'"])</div>
    				<div class="col-md-12">@include('HTML_VUE.select_simple',[ 'label'=>'Persona Que Solicita', 'placeholder'=>'Persona Que Solicita', 'model'=>'persona_que_solicita', 'name'=>"'persona_que_solicita'", 'validate'=>"'required'"])</div>
    				<div class="col-md-12">@include('HTML_VUE.input_date',[ 'label'=>'Fecha De Entrega', 'placeholder'=>'Fecha De Entrega', 'model'=>'fecha_de_entrega', 'name'=>"'fecha_de_entrega'", 'validate'=>"'required'"])</div>
    				<div class="col-md-12">@include('HTML_VUE.input_text',[ 'label'=>'Consecutivo', 'placeholder'=>'Consecutivo', 'model'=>'consecutivo', 'name'=>"'consecutivo'", 'validate'=>"'required'"])</div>
    				<div class="col-md-12">@include('HTML_VUE.input_text',[ 'label'=>'Requisicion De Compra', 'placeholder'=>'Requisicion De Compra', 'model'=>'requisicion_de_compra', 'name'=>"'requisicion_de_compra'", 'validate'=>"'required'"])</div>
    				<div class="col-md-12">@include('HTML_VUE.select_simple',[ 'label'=>'Estado', 'placeholder'=>'Estado', 'model'=>'estado', 'name'=>"'estado'", 'validate'=>"'required'"])</div>
    				<div class="col-md-12">@include('HTML_VUE.select_simple',[ 'label'=>'Prioridad', 'placeholder'=>'Prioridad', 'model'=>'prioridad', 'name'=>"'prioridad'", 'validate'=>"'required'"])</div>
    				<div class="col-md-12">@include('HTML_VUE.input_text',[ 'label'=>'Observaciones', 'placeholder'=>'Observaciones', 'model'=>'observaciones', 'name'=>"'observaciones'", 'validate'=>"'required'"])</div>
    				                    
                </div>

			</form>
		</div>
	</div>
@endsection

@section('local-css')
	<style type="text/css">
		[v-cloak] {
			display: none;
		}
		.grid-images-item{
            width: 100%;
            padding-top: 100%;
            border:solid 1px;
            background-size: contain;
            background-position: 50% 50%;
            background-repeat: no-repeat;
        }
        .container-sortable{
            background: transparent;
            border: 1px solid #3c8dbc;
            border-top: 3px solid #3c8dbc;
        }
	</style>
@endsection

@section('local-script')

    <!--<script src="http://liceo_compras.test/js/dist/locale/es.js" type="text/javascript"></script>
    <script src="http://liceo_compras.test/js/moment.min.js"></script>-->

    <script type="text/javascript">
        
        //Vue.use(VeeValidate, {locale:'es'});

        var section = new Vue({
            el: '#sectionApp',
            data: {

            	area_que_solicita:`{!!old('area_que_solicita')!!}` ,
                persona_que_realiza:`{!!old('persona_que_realiza')!!}` ,
                persona_que_solicita:`{!!old('persona_que_solicita')!!}` ,
                fecha_de_entrega:`{!!old('fecha_de_entrega')!!}` ,
                consecutivo:`{!!old('consecutivo')!!}` ,
                requisicion_de_compra:`{!!old('requisicion_de_compra')!!}` ,
                estado:`{!!old('estado')!!}` ,
                prioridad:`{!!old('prioridad')!!}` ,
                observaciones:`{!!old('observaciones')!!}` 
                
            }
        });

        //section.$validator.installDateTimeValidators(moment);
        
        @foreach($errors->toArray()?:[] as $key=>$value)
            section.errors.add('{{dot_to_html($key)}}', '{{$errors->first($key)}}');
        @endforeach
        
    </script>
    <script type="text/javascript">
        $(function(){
            $("#sectionApp :input").prop("disabled", true);
        });
    </script>
    {{ session()->forget('_old_input') }}
@endsection