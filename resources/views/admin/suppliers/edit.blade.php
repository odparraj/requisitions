@extends('adminlte::layouts.app')

@section('htmlheader_title')
Proveedores
@endsection

@section('contentheader_title')
Proveedores
@endsection

@section('contentheader_description')
@lang('message.edit')
@endsection

@section('main-content')
<div class="row" id="sectionApp" v-cloak>
    <div class="col-md-12">
        @include('includes.session-alert')
        <form id="form-section" action="{{ route('suppliers.update', $supplier->id) }}" method="POST" enctype="multipart/form-data" @submit.prevent="validateBeforeSubmit">
            <input type="hidden" name="_method" value="PUT">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Información General</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-4">@include('HTML_VUE.input_text',[ 'label'=>'Nombre o razón social como aparece en el RUT', 'placeholder'=>'Nombre o razón social como aparece en el RUT', 'model'=>'informacion_general.razon_social', 'name'=>"'informacion_general[razon_social]'", 'validate'=>"'required'"])</div>
                        <div class="col-md-4">@include('HTML_VUE.input_text',[ 'label'=>'NIT o C.C.', 'placeholder'=>'NIT o C.C.', 'model'=>'informacion_general.nit', 'name'=>"'informacion_general[nit]'", 'validate'=>"'required'"])</div>
                        <div class="col-md-4">@include('HTML_VUE.input_text',[ 'label'=>'Ciudad', 'placeholder'=>'Ciudad', 'model'=>'informacion_general.ciudad', 'name'=>"'informacion_general[ciudad]'", 'validate'=>"'required'"])</div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">@include('HTML_VUE.input_text',[ 'label'=>'Dirección Completa', 'placeholder'=>'Dirección Completa', 'model'=>'informacion_general.direccion', 'name'=>"'informacion_general[direccion]'", 'validate'=>"'required'"])</div>
                        <div class="col-md-4">@include('HTML_VUE.input_text',[ 'label'=>'Teléfono', 'placeholder'=>'Teléfono', 'model'=>'informacion_general.telefono', 'name'=>"'informacion_general[telefono]'", 'validate'=>"'required'"])</div>
                        <div class="col-md-4">@include('HTML_VUE.input_text',[ 'label'=>'Email', 'placeholder'=>'Email', 'model'=>'informacion_general.email', 'name'=>"'informacion_general[email]'", 'validate'=>"'required|email'"])</div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <h5>Persona de contacto:</h5>
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-3">@include('HTML_VUE.input_text',[ 'label'=>'Nombre', 'placeholder'=>'Nombre', 'model'=>'informacion_general.persona_de_contacto.nombre', 'name'=>"'informacion_general[persona_de_contacto][nombre]'", 'validate'=>"'required'"])</div>
                                    <div class="col-md-3">@include('HTML_VUE.input_text',[ 'label'=>'Cargo', 'placeholder'=>'Cargo', 'model'=>'informacion_general.persona_de_contacto.cargo', 'name'=>"'informacion_general[persona_de_contacto][cargo]'", 'validate'=>"'required'"])</div>
                                    <div class="col-md-3">@include('HTML_VUE.input_text',[ 'label'=>'Teléfono', 'placeholder'=>'Teléfono', 'model'=>'informacion_general.persona_de_contacto.telefono', 'name'=>"'informacion_general[persona_de_contacto][telefono]'", 'validate'=>"'required'"])</div>
                                    <div class="col-md-3">@include('HTML_VUE.input_text',[ 'label'=>'Email', 'placeholder'=>'Email', 'model'=>'informacion_general.persona_de_contacto.email', 'name'=>"'informacion_general[persona_de_contacto][email]'", 'validate'=>"'required|email'"])</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <h5>Tiene certificación de calidad:</h5>
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-3">@include('HTML_VUE.select_simple',[ 'label'=>'Respuesta', 'placeholder'=>'Respuesta', 'model'=>'informacion_general.certificacion_de_calidad.check', 'name'=>"'informacion_general[certificacion_de_calidad][check]'", 'validate'=>"'required'", 'options'=>'options_si_no'])</div>
                                    <div v-if="informacion_general.certificacion_de_calidad.check == 'si' " class="col-md-9">@include('HTML_VUE.input_text',[ 'label'=>'Cuál?', 'placeholder'=>'Cuál?', 'model'=>'informacion_general.certificacion_de_calidad.cual', 'name'=>"'informacion_general[certificacion_de_calidad][cual]'", 'validate'=>"'required'"])</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Información de la empresa</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-3">@include('HTML_VUE.input_text',[ 'label'=>'Actividad Principal Cod. CIIU', 'placeholder'=>'Actividad Principal Cod. CIIU', 'model'=>'informacion_de_la_empresa.actividad_principal.codigo_ciiu', 'name'=>"'informacion_de_la_empresa[actividad_principal][codigo_ciiu]'", 'validate'=>"'required'"])</div>
                        <div class="col-md-9">@include('HTML_VUE.input_text',[ 'label'=>'Descripción de la actividad económica', 'placeholder'=>'Descripción de la actividad económica', 'model'=>'informacion_de_la_empresa.actividad_principal.descripcion', 'name'=>"'informacion_de_la_empresa[actividad_principal][descripcion]'", 'validate'=>"'required'"])</div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">@include('HTML_VUE.input_text',[ 'label'=>'Actividad Secundaria Cod. CIIU', 'placeholder'=>'Actividad Secundaria Cod. CIIU', 'model'=>'informacion_de_la_empresa.actividad_secundaria.codigo_ciiu', 'name'=>"'informacion_de_la_empresa[actividad_secundaria][codigo_ciiu]'", 'validate'=>"''"])</div>
                        <div class="col-md-9">@include('HTML_VUE.input_text',[ 'label'=>'Descripción de la actividad económica', 'placeholder'=>'Descripción de la actividad económica', 'model'=>'informacion_de_la_empresa.actividad_secundaria.descripcion', 'name'=>"'informacion_de_la_empresa[actividad_secundaria][descripcion]'", 'validate'=>"''"])</div>
                    </div>
                </div>
            </div>

            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Clase de proveedor</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-2">@include('HTML_VUE.input_boolean',[ 'label'=>'Fabricante', 'model'=>'clase_de_proveedor.fabricante', 'name'=>"'clase_de_proveedor[fabricante]'"])</div>
                        <div class="col-md-2">@include('HTML_VUE.input_boolean',[ 'label'=>'Distribuidor', 'model'=>'clase_de_proveedor.distribuidor', 'name'=>"'clase_de_proveedor[distribuidor]'"])</div>
                        <div class="col-md-2">@include('HTML_VUE.input_boolean',[ 'label'=>'Importador', 'model'=>'clase_de_proveedor.importador', 'name'=>"'clase_de_proveedor[importador]'"])</div>
                        <div class="col-md-2">@include('HTML_VUE.input_boolean',[ 'label'=>'Comercializador', 'model'=>'clase_de_proveedor.comercializador', 'name'=>"'clase_de_proveedor[comercializador]'"])</div>
                        <div class="col-md-2">@include('HTML_VUE.input_boolean',[ 'label'=>'Contratista', 'model'=>'clase_de_proveedor.contratista', 'name'=>"'clase_de_proveedor[contratista]'"])</div>
                        <div class="col-md-2">@include('HTML_VUE.input_boolean',[ 'label'=>'Prestador de servicios', 'model'=>'clase_de_proveedor.prestador_de_servicios', 'name'=>"'clase_de_proveedor[prestador_de_servicios]'"])</div>
                    </div>
                    <div class="row">
                        <div class="col-md-2">@include('HTML_VUE.input_boolean',[ 'label'=>'Otro', 'model'=>'clase_de_proveedor.otro.check', 'name'=>"'clase_de_proveedor[otro][check]'"])</div>
                        <div v-if="clase_de_proveedor.otro.check == 'si' " class="col-md-10">@include('HTML_VUE.input_text',[ 'label'=>'Cuál?', 'placeholder'=>'Cuál?', 'model'=>'clase_de_proveedor.otro.cual', 'name'=>"'clase_de_proveedor[otro][cual]'", 'validate'=>"'required'"])</div>
                    </div>
                </div>
            </div>

            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Sector Económico</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-2">@include('HTML_VUE.input_boolean',[ 'label'=>'Metalmecánica', 'model'=>'sector_economico.metalmecanica', 'name'=>"'sector_economico[metalmecanica]'"])</div>
                        <div class="col-md-2">@include('HTML_VUE.input_boolean',[ 'label'=>'Financiero', 'model'=>'sector_economico.financiero', 'name'=>"'sector_economico[financiero]'"])</div>
                        <div class="col-md-2">@include('HTML_VUE.input_boolean',[ 'label'=>'Software/Hardware', 'model'=>'sector_economico.software_hardware', 'name'=>"'sector_economico[software_hardware]'"])</div>
                        <div class="col-md-2">@include('HTML_VUE.input_boolean',[ 'label'=>'Ferreteria', 'model'=>'sector_economico.ferreteria', 'name'=>"'sector_economico[ferreteria]'"])</div>
                        <div class="col-md-2">@include('HTML_VUE.input_boolean',[ 'label'=>'Construccion', 'model'=>'sector_economico.construccion', 'name'=>"'sector_economico[construccion]'"])</div>
                        <div class="col-md-2">@include('HTML_VUE.input_boolean',[ 'label'=>'Salud', 'model'=>'sector_economico.salud', 'name'=>"'sector_economico[salud]'"])</div>
                    </div>
                    <div class="row">
                        <div class="col-md-2">@include('HTML_VUE.input_boolean',[ 'label'=>'Transporte', 'model'=>'sector_economico.transporte', 'name'=>"'sector_economico[transporte]'"])</div>
                        <div class="col-md-2">@include('HTML_VUE.input_boolean',[ 'label'=>'Alimentos', 'model'=>'sector_economico.alimentos', 'name'=>"'sector_economico[alimentos]'"])</div>
                        <div class="col-md-2">@include('HTML_VUE.input_boolean',[ 'label'=>'Consultoría', 'model'=>'sector_economico.consultoria', 'name'=>"'sector_economico[consultoria]'"])</div>
                        <div class="col-md-2">@include('HTML_VUE.input_boolean',[ 'label'=>'Otro', 'model'=>'sector_economico.otro.check', 'name'=>"'sector_economico[otro][check]'"])</div>
                        <div v-if="sector_economico.otro.check == 'si' " class="col-md-4">@include('HTML_VUE.input_text',[ 'label'=>'Cuál?', 'placeholder'=>'Cuál?', 'model'=>'sector_economico.otro.cual', 'name'=>"'sector_economico[otro][cual]'", 'validate'=>"'required'"])</div>
                    </div>
                </div>
            </div>

            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Información Tributaria</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-3">@include('HTML_VUE.input_boolean',[ 'label'=>'Declarante de renta', 'model'=>'informacion_tributaria.declarante', 'name'=>"'informacion_tributaria[declarante]'"])</div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">@include('HTML_VUE.input_boolean',[ 'label'=>'Responsable de IVA', 'model'=>'informacion_tributaria.regimen_comun', 'name'=>"'informacion_tributaria[regimen_comun]'"])</div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">@include('HTML_VUE.input_boolean',[ 'label'=>'No Responsable de IVA', 'model'=>'informacion_tributaria.regimen_simplificado', 'name'=>"'informacion_tributaria[regimen_simplificado]'"])</div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">@include('HTML_VUE.input_boolean',[ 'label'=>'Autorretenedor', 'model'=>'informacion_tributaria.autorretenedor', 'name'=>"'informacion_tributaria[autorretenedor]'"])</div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">@include('HTML_VUE.input_boolean',[ 'label'=>'Gran Contribuyente', 'model'=>'informacion_tributaria.gran_contribuyente', 'name'=>"'informacion_tributaria[gran_contribuyente]'"])</div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">@include('HTML_VUE.input_boolean',[ 'label'=>'Entidad sin ánimo de lucro', 'model'=>'informacion_tributaria.entidad_sin_animo_de_lucro', 'name'=>"'informacion_tributaria[entidad_sin_animo_de_lucro]'"])</div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <h5>Industria y Comercio:</h5>
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-4">@include('HTML_VUE.input_text',[ 'label'=>'Actividad Principal', 'placeholder'=>'Actividad Principal', 'model'=>'informacion_tributaria.industria_y_comercio.actividad_principal', 'name'=>"'informacion_tributaria[industria_y_comercio][actividad_principal]'", 'validate'=>"'required'"])</div>
                                    <div class="col-md-4">@include('HTML_VUE.input_text',[ 'label'=>'Tarifa', 'placeholder'=>'Tarifa', 'model'=>'informacion_tributaria.industria_y_comercio.tarifa', 'name'=>"'informacion_tributaria[industria_y_comercio][tarifa]'", 'validate'=>"'required|decimal:3'"])</div>
                                    <div class="col-md-4">@include('HTML_VUE.input_text',[ 'label'=>'Ciudad', 'placeholder'=>'Ciudad', 'model'=>'informacion_tributaria.industria_y_comercio.ciudad', 'name'=>"'informacion_tributaria[industria_y_comercio][ciudad]'", 'validate'=>"'required'"])</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>



            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Información Financiera</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-4">@include('HTML_VUE.input_date',[ 'label'=>'Última Fecha De Corte', 'placeholder'=>'Última Fecha De Corte', 'model'=>'informacion_financiera.fecha_de_corte', 'name'=>"'informacion_financiera[fecha_de_corte]'", 'validate'=>"'date_format:YYYY-MM-DD'"])</div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">@include('HTML_VUE.input_text',[ 'label'=>'Total Activos', 'placeholder'=>'Total Activos', 'model'=>'informacion_financiera.total_activos', 'name'=>"'informacion_financiera[total_activos]'", 'validate'=>"'decimal:3'"])</div>
                        <div class="col-md-4">@include('HTML_VUE.input_text',[ 'label'=>'Total Pasivos', 'placeholder'=>'Total Pasivos', 'model'=>'informacion_financiera.total_pasivos', 'name'=>"'informacion_financiera[total_pasivos]'", 'validate'=>"'decimal:3'"])</div>
                        <div class="col-md-4">@include('HTML_VUE.input_text',[ 'label'=>'Total Patrimonio', 'placeholder'=>'Total Patrimonio', 'model'=>'informacion_financiera.total_patrimonio', 'name'=>"'informacion_financiera[total_patrimonio]'", 'validate'=>"'decimal:3'"])</div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">@include('HTML_VUE.input_text',[ 'label'=>'Total Ingresos', 'placeholder'=>'Total Ingresos', 'model'=>'informacion_financiera.total_ingresos', 'name'=>"'informacion_financiera[total_ingresos]'", 'validate'=>"'decimal:3'"])</div>
                        <div class="col-md-4">@include('HTML_VUE.input_text',[ 'label'=>'Total Egresos', 'placeholder'=>'Total Egresos', 'model'=>'informacion_financiera.total_egresos', 'name'=>"'informacion_financiera[total_egresos]'", 'validate'=>"'decimal:3'"])</div>
                        <div class="col-md-4">@include('HTML_VUE.input_text',[ 'label'=>'Utilidad', 'placeholder'=>'Utilidad', 'model'=>'informacion_financiera.total_utilidad', 'name'=>"'informacion_financiera[total_utilidad]'", 'validate'=>"'decimal:3'"])</div>
                    </div>
                </div>
            </div>

            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Documentos Requeridos</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">

                    <div class="row">
                        <div class="col-md-6">@include('HTML_VUE.input_file', [ 'label'=>'RUT', 'model'=>'documentos_requeridos.rut', 'name'=>"'documentos_requeridos[rut]'", 'validate'=>"''"])</div>
                        <div class="col-md-6">@include('HTML_VUE.input_file', [ 'label'=>'Certificado de camara de comercio', 'model'=>'documentos_requeridos.camara_de_comercio', 'name'=>"'documentos_requeridos[camara_de_comercio]'", 'validate'=>"''"])</div>
                        <div class="col-md-6">@include('HTML_VUE.input_file', [ 'label'=>'Referencia Comercial(1)', 'model'=>'documentos_requeridos.referencias_comerciales_1', 'name'=>"'documentos_requeridos[referencias_comerciales_1]'", 'validate'=>"''"])</div>
                        <div class="col-md-6">@include('HTML_VUE.input_file', [ 'label'=>'Referencia Comercial(2)', 'model'=>'documentos_requeridos.referencias_comerciales_2', 'name'=>"'documentos_requeridos[referencias_comerciales_2]'", 'validate'=>"''"])</div>
                        <div class="col-md-6">@include('HTML_VUE.input_file', [ 'label'=>'Certificación bancaria', 'model'=>'documentos_requeridos.certificacion_bancaria', 'name'=>"'documentos_requeridos[certificacion_bancaria]'", 'validate'=>"''"])</div>
                        <div class="col-md-6">@include('HTML_VUE.input_file', [ 'label'=>'Otros documentos', 'model'=>'documentos_requeridos.certificacion_de_calidad', 'name'=>"'documentos_requeridos[certificacion_de_calidad]'", 'validate'=>"''"])</div>
                    </div>

                    <!-- <div class="row">
                                <div class="col-md-2">@include('HTML_VUE.input_boolean',[ 'label'=>'RUT', 'model'=>'documentos_requeridos.rut', 'name'=>"'documentos_requeridos[rut]'"])</div>
                                <div class="col-md-2">@include('HTML_VUE.input_boolean',[ 'label'=>'Certificado de camara de comercio', 'model'=>'documentos_requeridos.camara_de_comercio', 'name'=>"'documentos_requeridos[camara_de_comercio]'"])</div>
                                <div class="col-md-2">@include('HTML_VUE.input_boolean',[ 'label'=>'Referencias Comerciales(2)', 'model'=>'documentos_requeridos.referencias_comerciales', 'name'=>"'documentos_requeridos[referencias_comerciales]'"])</div>
                                <div class="col-md-2">@include('HTML_VUE.input_boolean',[ 'label'=>'Certificación bancaria', 'model'=>'documentos_requeridos.certificacion_bancaria', 'name'=>"'documentos_requeridos[certificacion_bancaria]'"])</div>
                                <div class="col-md-2">@include('HTML_VUE.input_boolean',[ 'label'=>'Certificación de calidad', 'model'=>'documentos_requeridos.certificacion_de_calidad', 'name'=>"'documentos_requeridos[certificacion_de_calidad]'"])</div>
                            </div> -->
                </div>
            </div>
            @include('web.includes.politica_de_privacidad_y_declaracion_de_fondos')
            <div class="row">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-primary">{{trans('message.edit')}}</button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection

@section('local-css')
<style type="text/css">
    [v-cloak] {
        display: none;
    }

    .grid-images-item {
        width: 100%;
        padding-top: 100%;
        border: solid 1px;
        background-size: contain;
        background-position: 50% 50%;
        background-repeat: no-repeat;
    }

    .container-sortable {
        background: transparent;
        border: 1px solid #3c8dbc;
        border-top: 3px solid #3c8dbc;
    }
</style>
@endsection

@section('local-script')

<!--<script src="http://liceo_compras.test/js/dist/locale/es.js" type="text/javascript"></script>
    <script src="http://liceo_compras.test/js/moment.min.js"></script>-->
    <script type="text/javascript">
        
        //Vue.use(VeeValidate, {locale:'es'});

        var section = new Vue({
            el: '#sectionApp',
            data: {
                options_si_no:{
                    si:'Si',
                    no:'No'
                },

            	informacion_general: {
                    razon_social: `{!!old('informacion_general.razon_social')!!}`,
                    nit: `{!!old('informacion_general.nit')!!}`,
                    ciudad: `{!!old('informacion_general.ciudad')!!}`,
                    direccion: `{!!old('informacion_general.direccion')!!}`,
                    telefono: `{!!old('informacion_general.telefono')!!}`,
                    email: `{!!old('informacion_general.email')!!}`,
                    persona_de_contacto: {
                        nombre: `{!!old('informacion_general.persona_de_contacto.nombre')!!}`,
                        cargo: `{!!old('informacion_general.persona_de_contacto.cargo')!!}`,
                        telefono: `{!!old('informacion_general.persona_de_contacto.telefono')!!}`,
                        email: `{!!old('informacion_general.persona_de_contacto.email')!!}`
                    },
                    certificacion_de_calidad: {
                        check: `{!!old('informacion_general.certificacion_de_calidad.check')!!}`,
                        cual: `{!!old('informacion_general.certificacion_de_calidad.cual')!!}`
                    }
                },


                informacion_de_la_empresa: {
                    actividad_principal: {
                        codigo_ciiu: `{!!old('informacion_de_la_empresa.actividad_principal.codigo_ciiu')!!}`,
                        descripcion: `{!!old('informacion_de_la_empresa.actividad_principal.descripcion')!!}` 
                    },
                    actividad_secundaria: {
                        codigo_ciiu: `{!!old('informacion_de_la_empresa.actividad_secundaria.codigo_ciiu')!!}`,
                        descripcion: `{!!old('informacion_de_la_empresa.actividad_secundaria.descripcion')!!}` 
                    }
                },

                clase_de_proveedor: {
                    fabricante: `{!!old('clase_de_proveedor.fabricante')!!}`,
                    distribuidor: `{!!old('clase_de_proveedor.distribuidor')!!}`,
                    importador: `{!!old('clase_de_proveedor.importador')!!}`,
                    comercializador: `{!!old('clase_de_proveedor.comercializador')!!}`,
                    contratista: `{!!old('clase_de_proveedor.contratista')!!}`,
                    prestador_de_servicios: `{!!old('clase_de_proveedor.prestador_de_servicios')!!}`,
                    otro: {
                        check: `{!!old('clase_de_proveedor.otro.check')!!}`,
                        cual: `{!!old('clase_de_proveedor.otro.cual')!!}`
                    }
                },

                sector_economico:{
                    metalmecanica: `{!!old('sector_economico.metalmecanica')!!}`,
                    financiero: `{!!old('sector_economico.financiero')!!}`,
                    software_hardware: `{!!old('sector_economico.software_hardware')!!}`,
                    ferreteria: `{!!old('sector_economico.ferreteria')!!}`,
                    construccion: `{!!old('sector_economico.construccion')!!}`,
                    salud: `{!!old('sector_economico.salud')!!}`,
                    transporte: `{!!old('sector_economico.transporte')!!}`,
                    alimentos: `{!!old('sector_economico.alimentos')!!}`,
                    consultoria: `{!!old('sector_economico.consultoria')!!}`,
                    otro: {
                        check: `{!!old('sector_economico.otro.check')!!}`,
                        cual: `{!!old('sector_economico.otro.cual')!!}`
                    }
                },


                informacion_tributaria: {
                    declarante: `{!!old('informacion_tributaria.declarante')!!}`,
                    regimen_comun: `{!!old('informacion_tributaria.regimen_comun')!!}`,
                    regimen_simplificado: `{!!old('informacion_tributaria.regimen_simplificado')!!}`,
                    autorretenedor: `{!!old('informacion_tributaria.autorretenedor')!!}`,
                    gran_contribuyente: `{!!old('informacion_tributaria.gran_contribuyente')!!}`,
                    entidad_sin_animo_de_lucro: `{!!old('informacion_tributaria.entidad_sin_animo_de_lucro')!!}`,
                    industria_y_comercio:{
                        actividad_principal: `{!!old('informacion_tributaria.industria_y_comercio.actividad_principal')!!}`,
                        tarifa: `{!!old('informacion_tributaria.industria_y_comercio.tarifa')!!}`,
                        ciudad: `{!!old('informacion_tributaria.industria_y_comercio.ciudad')!!}`
                    }
                },

                informacion_financiera: {
                    fecha_de_corte: `{!!old('informacion_financiera.fecha_de_corte')!!}`,
                    total_activos: `{!!old('informacion_financiera.total_activos')!!}`,
                    total_pasivos: `{!!old('informacion_financiera.total_pasivos')!!}`,
                    total_patrimonio: `{!!old('informacion_financiera.total_patrimonio')!!}`,
                    total_ingresos: `{!!old('informacion_financiera.total_ingresos')!!}`,
                    total_egresos: `{!!old('informacion_financiera.total_egresos')!!}`,
                    total_utilidad: `{!!old('informacion_financiera.total_utilidad')!!}`
                },
                
                documentos_requeridos: {
                    rut: `{!!old('documentos_requeridos.rut')!!}`,
                    camara_de_comercio: `{!!old('documentos_requeridos.camara_de_comercio')!!}`,
                    referencias_comerciales_1: `{!!old('documentos_requeridos.referencias_comerciales_1')!!}`,
                    referencias_comerciales_2: `{!!old('documentos_requeridos.referencias_comerciales_2')!!}`,
                    certificacion_bancaria: `{!!old('documentos_requeridos.certificacion_bancaria')!!}`,
                    certificacion_de_calidad: `{!!old('documentos_requeridos.certificacion_de_calidad')!!}`
                }
                
            },
            methods: {
	            
                validateBeforeSubmit() {
                    this.$validator.validateAll().then((result) => {
				        if (result) {
				          document.querySelector('#form-section').submit();
				          
				        }else{
				        	alert('Existen errores en el formulario!!!');
				        }
				    });
                },
            },
            mounted(){
                /*$(document).on('change', '.input-image', this.addGridImage);
                $(document).on('click', '.box-header .box-tools button.sortable-button', function(){
                    $('i', this).toggleClass('fa-window-minimize');
                    $('i', this).toggleClass('fa-window-maximize');
                });*/
            }
        });

        //section.$validator.installDateTimeValidators(moment);
        
        @foreach($errors->toArray()?:[] as $key=>$value)
            section.errors.add('{{dot_to_html($key)}}', '{{$errors->first($key)}}');
        @endforeach
        
    </script>
{{ session()->forget('_old_input') }}
@endsection