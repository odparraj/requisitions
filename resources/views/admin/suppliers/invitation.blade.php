@extends('adminlte::layouts.app')

@section('htmlheader_title')
	Proveedores
@endsection

@section('contentheader_title')
	Proveedores
@endsection

@section('contentheader_description')
	Enviar Invitación
@endsection

@section('main-content')
	<div class="row" id="sectionApp" v-cloak>
		<div class="col-md-12">
			@include('includes.session-alert')
			<form id="form-section" action="{{ route('suppliers.send_invitation') }}" method="POST" enctype="multipart/form-data" @submit.prevent="validateBeforeSubmit">
				<input type="hidden" name="_method" value="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                
                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-primary container-sortable">
                            <div class="box-header with-border">
                                <h3 class="box-title">Lista de emails</h3>
                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-primary btn-xs" v-on:click="addEmail()">Añadir Email</button>                                    
                                </div>
                            </div>
                            <div class="box-body">                                
                                <div v-for="(item, index) in emails" class="box">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">
                                            Email                                           
                                        </h3>
                                        <div class="box-tools pull-right">
                                            <button type="button" class="btn btn-box-tool" title="Remove" v-on:click="removeEmail(index)"><i class="fa fa-times"></i></button>
                                        </div>
                                    </div>
                                    <div class="box-body" :id="'emails_'+index">
                                        <div class="row">
                                            <div class="col-md-12">@include('HTML_VUE.input_text',['model'=>"emails[index]", 'name'=>"'emails['+index+']'", 'placeholder'=>'Email', 'validate'=>"'required|email'"])</div>
                                        </div>
                                    </div>
                                </div>                                
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
    				<div class="col-md-12">
    					<button type="submit" class="btn btn-primary">Enviar Invitación</button>
    				</div>
                </div>
			</form>
		</div>
	</div>
@endsection

@section('local-css')
	<style type="text/css">
		[v-cloak] {
			display: none;
		}
		.grid-images-item{
            width: 100%;
            padding-top: 100%;
            border:solid 1px;
            background-size: contain;
            background-position: 50% 50%;
            background-repeat: no-repeat;
        }
        .container-sortable{
            background: transparent;
            border: 1px solid #3c8dbc;
            border-top: 3px solid #3c8dbc;
        }
	</style>
@endsection

@section('local-script')

    <!--<script src="http://liceo_compras.test/js/dist/locale/es.js" type="text/javascript"></script>
    <script src="http://liceo_compras.test/js/moment.min.js"></script>-->

    <script type="text/javascript">
        
        //Vue.use(VeeValidate, {locale:'es'});

        var section = new Vue({
            el: '#sectionApp',
            data: {
                emails: ['']                
            },
            methods: {
                addEmail(){
                    this.emails.push('');
                },
                removeEmail(index){
                    this.emails.splice(index,1);
                },
                validateBeforeSubmit() {
                    this.$validator.validateAll().then((result) => {
				        if (result) {
				          document.querySelector('#form-section').submit();
				          
				        }else{
				        	alert('Existen errores en el formulario!!!');
				        }
				    });
                }
            },
            mounted(){
            }
        });

        //section.$validator.installDateTimeValidators(moment);
        
        @foreach($errors->toArray()?:[] as $key=>$value)
            section.errors.add('{{dot_to_html($key)}}', '{{$errors->first($key)}}');
        @endforeach
        
    </script>
    {{ session()->forget('_old_input') }}
@endsection