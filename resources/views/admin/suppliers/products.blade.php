@extends('adminlte::layouts.app')

@section('htmlheader_title')
	Proveedores 
@endsection

@section('contentheader_title')
	Proveedores 
@endsection

@section('contentheader_description')
	Productos
@endsection

@section('main-content')
	<div class="row" id="sectionApp" v-cloak>
		<div class="col-md-12">
			@include('includes.session-alert')
			<form id="form-section" action="{{ route('supplier.update_products', $supplier->id) }}" method="POST" enctype="multipart/form-data" @submit.prevent="validateBeforeSubmit">
				<input type="hidden" name="_method" value="POST">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="row">
                	<div class="col-md-12">
                        <div class="form-group" :class="{'has-error': errors.has('products') }">
                            <label class="control-label">Productos</label>
                            <v-select multiple v-validate="'required'" v-model="products" :options="products_options" label="nombre" placeholder="Seleccionar Productos"></v-select>
                            <input v-for="product in products" type="hidden" name="products[]" v-bind:value="product.id">
                            <span class="help-block" v-if="errors.has('products')">@{{errors.first('products')}}</span>                            
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group" :class="{'has-error': errors.has('areas') }">
                            <label class="control-label">Areas</label>
                            <v-select multiple v-validate="'required'" v-model="areas" :options="areas_options" label="nombre" placeholder="Seleccionar Areas"></v-select>
                            <input v-for="area in areas" type="hidden" name="areas[]" v-bind:value="area.id">
                            <span class="help-block" v-if="errors.has('areas')">@{{errors.first('areas')}}</span>                            
                        </div>
                    </div>
                </div>
                <div class="row">
    				<div class="col-md-12">
    					<button type="submit" class="btn btn-primary">Guardar</button>
    				</div>
                </div>
			</form>
		</div>
	</div>
@endsection

@section('local-css')
	<style type="text/css">
		[v-cloak] {
			display: none;
		}
		.grid-images-item{
            width: 100%;
            padding-top: 100%;
            border:solid 1px;
            background-size: contain;
            background-position: 50% 50%;
            background-repeat: no-repeat;
        }
        .container-sortable{
            background: transparent;
            border: 1px solid #3c8dbc;
            border-top: 3px solid #3c8dbc;
        }

        .v-select .dropdown-toggle {
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;
            display: flex;
            padding: 0 0 4px;
            background: none;
            border: 1px solid rgba(60,60,60,.26);
            border-radius: 0 !important;
            white-space: normal;
        }
	</style>
@endsection

@section('local-script')

    <!--<script src="http://liceo_compras.test/js/dist/locale/es.js" type="text/javascript"></script>
    <script src="http://liceo_compras.test/js/moment.min.js"></script>-->
	<script src="https://unpkg.com/vue-select@2.5.1/dist/vue-select.js"></script>
    <script type="text/javascript">
        
        //Vue.use(VeeValidate, {locale:'es'});
        Vue.component('v-select', VueSelect.VueSelect);

        var section = new Vue({
            el: '#sectionApp',
            data: {
                products: {!!old('products')!!} ,
                products_options: {!!$products_options!!},
                areas: {!!old('areas')!!},
                areas_options: {!!$areas_options!!}                
            },
            methods: {	            
                validateBeforeSubmit() {
                    this.$validator.validateAll().then((result) => {
				        if (result) {
				          document.querySelector('#form-section').submit();
				          
				        }else{
				        	alert('Existen errores en el formulario!!!');
				        }
				    });
                }
            },
            mounted(){
            }
        });

        //section.$validator.installDateTimeValidators(moment);
        
        @foreach($errors->toArray()?:[] as $key=>$value)
            section.errors.add('{{dot_to_html($key)}}', '{{$errors->first($key)}}');
        @endforeach
        
    </script>
    {{ session()->forget('_old_input') }}
@endsection