@extends('adminlte::layouts.app')

@section('htmlheader_title')
	Test
@endsection

@section('contentheader_title')
	Test
@endsection

@section('contentheader_description')
	@lang('message.create')
@endsection

@section('main-content')
	<div class="row" id="sectionApp" v-cloak>
		<div class="col-md-12">
			@include('includes.session-alert')
			<form id="form-section" action="{{ route('tests.store') }}" method="POST" enctype="multipart/form-data" @submit.prevent="validateBeforeSubmit">
				<input type="hidden" name="_method" value="POST">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="row">
        			<div class="col-md-12">
                        <div class="box box-primary container-sortable">
                            <div class="box-header with-border">
                                <h3 class="box-title">Documentos_Anexos</h3>
                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-primary btn-xs" v-on:click="addDocumentosAnexos('Archivo')">Archivo</button>                              
                                    
                                </div>
                            </div>
                            <div class="box-body">
                                <draggable v-model="documentos_anexos" :options="{handle:'.handle'}">
                                    <div v-for="(item, index) in documentos_anexos" class="box">
                                        <div class="box-header with-border">
                                            <h3 class="box-title">
                                                <span class="handle" style="cursor: pointer;">
                                                    <i class="fa fa-ellipsis-v"></i>
                                                    <i class="fa fa-ellipsis-v"></i>
                                                </span>
                                                @{{item['type']}}
                                                
                                            </h3>
                                            <div class="box-tools pull-right">
                                                <button type="button" class="btn btn-box-tool sortable-button" :data-target="'#documentos_anexos_'+index" data-toggle="collapse" title="Collapse"><i class="fa fa-window-maximize"></i></button>
                                                <button type="button" class="btn btn-box-tool" title="Remove" v-on:click="removeDocumentosAnexos(index)"><i class="fa fa-times"></i></button>
                                            </div>
                                        </div>
                                        <div class="box-body collapse" :id="'documentos_anexos_'+index">
                                            <div class="row">
                                                <div v-if="item['type'] =='Archivo'">
                                                    <input type="hidden" :name="'documentos_anexos['+index+'][type]'" value="Archivo">
                                                    
                                                    <div class="col-md-12">@include('HTML_VUE.input_text',['model'=>"documentos_anexos[index]['data']['nombre']", 'name'=>"'documentos_anexos['+index+'][data][nombre]'", 'placeholder'=>'Nombre', 'label'=>'Nombre', 'validate'=>"''"])</div>
                                                    <div class="col-md-12">@include('HTML_VUE.input_file',['model'=>"documentos_anexos[index]['data']['file']", 'name'=>"'documentos_anexos['+index+'][data][file]'", 'placeholder'=>'Documento pdf | excel', 'label'=>'Documento pdf | excel', 'validate'=>"''"])</div>
                                                                                                    
                                                </div>
                                                                                            
                                            </div>
                                        </div>
                                    </div>
                                </draggable>
                            </div>
                        </div>
                    </div>    				                    
                </div>
                <div class="row">
    				<div class="col-md-12">
    					<button type="submit" class="btn btn-primary">{{trans('message.create')}}</button>
    				</div>
                </div>
			</form>
		</div>
	</div>
@endsection

@section('local-css')
	<style type="text/css">
		[v-cloak] {
			display: none;
		}
		.grid-images-item{
            width: 100%;
            padding-top: 100%;
            border:solid 1px;
            background-size: contain;
            background-position: 50% 50%;
            background-repeat: no-repeat;
        }
        .container-sortable{
            background: transparent;
            border: 1px solid #3c8dbc;
            border-top: 3px solid #3c8dbc;
        }
	</style>
@endsection

@section('local-script')

    <!--<script src="http://liceo_compras.test/js/dist/locale/es.js" type="text/javascript"></script>
    <script src="http://liceo_compras.test/js/moment.min.js"></script>-->

    <script type="text/javascript">
        
        //Vue.use(VeeValidate, {locale:'es'});

        var section = new Vue({
            el: '#sectionApp',
            data: {

            	documentos_anexos: {!!json_encode(old('documentos_anexos')?:[])!!}  
                
            },
            methods: {
            	addDocumentosAnexos(type){
                    elements= {
                        'Archivo': {
                            type:'Archivo',
                            data: { 
                                  nombre:'',   file:''
                            }
                        },                            
                        
                    };
                    this.documentos_anexos.push(elements[type]);
                },
                removeDocumentosAnexos(index){
                    this.documentos_anexos.splice(index,1);
                },
	            
                validateBeforeSubmit() {
                    this.$validator.validateAll().then((result) => {
				        if (result) {
				          document.querySelector('#form-section').submit();
				          
				        }else{
				        	alert('Existen errores en el formulario!!!');
				        }
				    });
                },
                addGridImage(e){

                    var model= $(e.currentTarget).data('model');
                    var idx= $(e.currentTarget).data('idx');
                    var info= $(e.currentTarget).data('info');
                    if(idx!=undefined){
                        model= model.replace('index',idx);
                    }
                    console.log('model '+model);
                    console.log('idx '+idx);
                    var files = e.target.files || e.dataTransfer.files;
                    var vm = this;

                    for (var i = 0; i < files.length; i++) {
                        var reader = new FileReader();
                        reader.onload = function(e) {
                            var is_array;
                            eval('is_array= Array.isArray(vm.'+model+');');

                            if(is_array){
                                if(info!=undefined){
                                    console.log('info '+info);
                                    let data={
                                        image: e.target.result,
                                        text1:'',
                                        text2:'',
                                        text3:'',
                                    }
                                    eval('vm.'+model+".push(data);");
                                }else{
                                    eval('vm.'+model+".push(e.target.result);");
                                }
                            }else{
                                eval('vm.'+model+"= e.target.result;");
                            }
                        }
                        reader.readAsDataURL(files[i]);
                    }
                },
                removeGridImage(key,model,index=undefined){
                    console.log('key: '+key+' model: '+model+' index:'+index);
                    if(index != undefined){
                        model= model.replace('index',index);
                    }
                    eval('this.'+model+".splice(key,1);");
                    //this[model].splice(key,1);
                }
            },
            mounted(){
                $(document).on('change', '.input-image', this.addGridImage);
                $(document).on('click', '.box-header .box-tools button.sortable-button', function(){
                    $('i', this).toggleClass('fa-window-minimize');
                    $('i', this).toggleClass('fa-window-maximize');
                });
            }
        });

        //section.$validator.installDateTimeValidators(moment);
        
        @foreach($errors->toArray()?:[] as $key=>$value)
            section.errors.add('{{dot_to_html($key)}}', '{{$errors->first($key)}}');
        @endforeach
        
    </script>
    {{ session()->forget('_old_input') }}
@endsection