<table class="table table-bordered">
    <thead>
        <tr>
            <th>Proveedor</th>
            <th>Estado</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($cotizaciones?:[] as $key=> $cotizacion)
        <tr>
            <td>
                <a href="{{route('suppliers.show',['id'=>1])}}"
                    target="_blank">
                    {!!array_get($cotizacion->supplier,'informacion_general.razon_social','No Registrado')!!}
                </a>
            </td>
            <td>{!!$cotizacion->contenido?'Tramitada por el proveedor':'Enviada al proveedor'!!}</td>
        </tr>            
        @endforeach
    </tbody>
</table>