@component('mail::message')
# Invitación de regístro

El Liceo de Colombia Bilingüe te envia esta invitación para unirte a nuestro grupo de proveedores.

@component('mail::button', ['url' => route('supplier_external.register')])
Regristrarme
@endcomponent

Gracias,<br>
{{ config('app.name') }}
@endcomponent
