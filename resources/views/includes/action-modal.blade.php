<div class="modal fade" id="{!!$action!!}Modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span></button>
                <h4 class="modal-title"><i class="icon fa fa-warning fa-lg text-yellow"></i> Confirmación</h4>
            </div>
            <div class="modal-body">
                <h5>{!!$message!!} <b><span>@{{to_action}}</span></b> ?</h5>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn pull-left" data-dismiss="modal">Cerrar</button>
                <form :action="'{{$url}}'+'/'+to_action" method="POST">
                    <input type="hidden" name="_method" value="{!!$method!!}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <button type="submit" class="btn btn-warning">Continuar</button>
                </form>
            </div>
        </div>
    </div>
</div>