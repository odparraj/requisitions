@if(Auth::user()->can($permission))
<a class="btn btn-primary" href="{!!$url!!}" role="button">{!!$text!!}</a>
@endif