<div class="btn-group">
	@foreach($items as $item)
		<a href="{!!url($item['file'])!!}" title="{!!$item['nombre']!!}" target="_blank" role="button" class="btn btn-flat btn-xs {!!config('mixedmedia.buttons')[$loop->index%19]!!}"><i class="fa fa-file"></i></a>
	@endforeach
</div>