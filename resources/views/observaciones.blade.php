<table class="table table-bordered">
    <thead>
        <tr>
            <th>Estado</th>
            <th>Comentario</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($supplier_order->observaciones?:[] as $key=> $observacion)
        <tr>
            <td>{!!$map[$key]!!}</td>
            <td>{!!nl2br($observacion)!!}</td>
        </tr>            
        @endforeach
    </tbody>
</table>