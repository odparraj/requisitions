<html>

<head>
    <meta content="text/html; charset=UTF-8" http-equiv="content-type">
    <style type="text/css">
        ol {
            margin: 0;
            padding: 0
        }
        
        table td,
        table th {
            padding: 0
        }
        
        .c2 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 0pt;
            border-right-width: 0pt;
            border-left-color: #000000;
            vertical-align: top;
            border-right-color: #000000;
            border-left-width: 0pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 0pt;
            width: 60%;
            border-top-color: #000000;
            border-bottom-style: solid
        }
        
        .c3 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 1pt;
            border-right-width: 1pt;
            border-left-color: #000000;
            vertical-align: top;
            border-right-color: #000000;
            border-left-width: 1pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 1pt;
            width: 10%;
            border-top-color: #000000;
            border-bottom-style: solid
        }
        
        .c8 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 0pt;
            border-right-width: 0pt;
            border-left-color: #000000;
            vertical-align: top;
            border-right-color: #000000;
            border-left-width: 0pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 0pt;
            width: 40%;
            border-top-color: #000000;
            border-bottom-style: solid
        }
        
        .c18 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 0pt;
            border-right-width: 0pt;
            border-left-color: #000000;
            vertical-align: top;
            border-right-color: #000000;
            border-left-width: 0pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 0pt;
            width: 40%;
            border-top-color: #000000;
            border-bottom-style: solid
        }
        
        .c34 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 1pt;
            border-right-width: 1pt;
            border-left-color: #000000;
            vertical-align: top;
            border-right-color: #000000;
            border-left-width: 1pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 1pt;
            width: 60%;
            border-top-color: #000000;
            border-bottom-style: solid
        }
        
        .c27 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 0pt;
            border-right-width: 0pt;
            border-left-color: #000000;
            vertical-align: top;
            border-right-color: #000000;
            border-left-width: 0pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 1pt;
            width: 100%;
            border-top-color: #000000;
            border-bottom-style: solid
        }
        
        .c6 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 1pt;
            border-right-width: 1pt;
            border-left-color: #000000;
            vertical-align: top;
            border-right-color: #000000;
            border-left-width: 1pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 1pt;
            width: 40%;
            border-top-color: #000000;
            border-bottom-style: solid
        }
        
        .c15 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 1pt;
            border-right-width: 1pt;
            border-left-color: #000000;
            vertical-align: top;
            border-right-color: #000000;
            border-left-width: 1pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 1pt;
            width: 10%;
            border-top-color: #000000;
            border-bottom-style: solid
        }
        
        .c19 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 1pt;
            border-right-width: 0pt;
            border-left-color: #000000;
            vertical-align: top;
            border-right-color: #000000;
            border-left-width: 0pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 0pt;
            width: 100%;
            border-top-color: #000000;
            border-bottom-style: solid
        }
        
        .c17 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 1pt;
            border-right-width: 1pt;
            border-left-color: #000000;
            vertical-align: top;
            border-right-color: #000000;
            border-left-width: 1pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 1pt;
            width: 10%;
            border-top-color: #000000;
            border-bottom-style: solid
        }
        
        .c29 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 0pt;
            border-right-width: 0pt;
            border-left-color: #000000;
            vertical-align: top;
            border-right-color: #000000;
            border-left-width: 0pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 0pt;
            width: 20%;
            border-top-color: #000000;
            border-bottom-style: solid
        }
        
        .c22 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 0pt;
            border-right-width: 0pt;
            border-left-color: #000000;
            vertical-align: top;
            border-right-color: #000000;
            border-left-width: 0pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 0pt;
            width: 100%;
            border-top-color: #000000;
            border-bottom-style: solid
        }
        
        .c4 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 0pt;
            border-right-width: 0pt;
            border-left-color: #000000;
            vertical-align: top;
            border-right-color: #000000;
            border-left-width: 0pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 0pt;
            width: 10%;
            border-top-color: #000000;
            border-bottom-style: solid
        }
        
        .c14 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 0pt;
            border-right-width: 0pt;
            border-left-color: #000000;
            vertical-align: top;
            border-right-color: #000000;
            border-left-width: 0pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 0pt;
            width: 20%;
            border-top-color: #000000;
            border-bottom-style: solid
        }
        
        .c7 {
            color: #000000;
            font-weight: 700;
            text-decoration: none;
            vertical-align: baseline;
            font-size: 9pt;
            font-family: "Arial";
            font-style: normal
        }
        
        .c11 {
            padding-top: 0pt;
            padding-bottom: 0pt;
            line-height: 1.15;
            orphans: 2;
            widows: 2;
            text-align: left;
            height: 11pt
        }
        
        .c33 {
            color: #000000;
            font-weight: 400;
            text-decoration: none;
            vertical-align: baseline;
            font-size: 12pt;
            font-family: "Arial";
            font-style: normal
        }
        
        .c0 {
            color: #000000;
            font-weight: 400;
            text-decoration: none;
            vertical-align: baseline;
            font-size: 9pt;
            font-family: "Arial";
            font-style: normal
        }
        
        .c12 {
            padding-top: 0pt;
            padding-bottom: 0pt;
            line-height: 1.0;
            text-align: center;
            height: auto;
        }
        
        .c13 {
            padding-top: 0pt;
            padding-bottom: 0pt;
            line-height: 1.0;
            text-align: right;
            height: 11pt
        }
        
        .c1 {
            padding-top: 0pt;
            padding-bottom: 0pt;
            line-height: 1.0;
            text-align: left;
            height: 11pt
        }
        
        .c21 {
            border-spacing: 0;
            border-collapse: collapse;
            margin-right: auto
        }
        
        .c30 {
            padding-top: 0pt;
            padding-bottom: 0pt;
            line-height: 1.0;
            text-align: right
        }
        
        .c10 {
            padding-top: 0pt;
            padding-bottom: 0pt;
            line-height: 1.0;
            text-align: left
        }
        
        .c24 {
            padding-top: 0pt;
            padding-bottom: 0pt;
            line-height: 1.0;
            text-align: center
        }
        
        .c32 {
            background-color: #ffffff;
            width: 100%;
            padding: 0pt 0pt 0pt 0pt
        }
        
        .c31 {
            font-size: 9pt;
            font-weight: 700
        }
        
        .c20 {
            height: 19pt
        }
        
        .c25 {
            height: 21pt
        }
        
        .title {
            padding-top: 0pt;
            color: #000000;
            font-size: 26pt;
            padding-bottom: 3pt;
            font-family: "Arial";
            line-height: 1.15;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }
        
        .subtitle {
            padding-top: 0pt;
            color: #666666;
            font-size: 15pt;
            padding-bottom: 16pt;
            font-family: "Arial";
            line-height: 1.15;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }
        
        li {
            color: #000000;
            font-size: 11pt;
            font-family: "Arial"
        }
        
        p {
            margin: 0;
            color: #000000;
            font-size: 11pt;
            font-family: "Arial"
        }
        
        h1 {
            padding-top: 20pt;
            color: #000000;
            font-size: 20pt;
            padding-bottom: 6pt;
            font-family: "Arial";
            line-height: 1.15;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }
        
        h2 {
            padding-top: 18pt;
            color: #000000;
            font-size: 16pt;
            padding-bottom: 6pt;
            font-family: "Arial";
            line-height: 1.15;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }
        
        h3 {
            padding-top: 16pt;
            color: #434343;
            font-size: 14pt;
            padding-bottom: 4pt;
            font-family: "Arial";
            line-height: 1.15;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }
        
        h4 {
            padding-top: 14pt;
            color: #666666;
            font-size: 12pt;
            padding-bottom: 4pt;
            font-family: "Arial";
            line-height: 1.15;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }
        
        h5 {
            padding-top: 12pt;
            color: #666666;
            font-size: 11pt;
            padding-bottom: 4pt;
            font-family: "Arial";
            line-height: 1.15;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }
        
        h6 {
            padding-top: 12pt;
            color: #666666;
            font-size: 11pt;
            padding-bottom: 4pt;
            font-family: "Arial";
            line-height: 1.15;
            page-break-after: avoid;
            font-style: italic;
            orphans: 2;
            widows: 2;
            text-align: left
        }
    </style>
</head>

<body class="c32">
    <table class="c21" width="100%">
        <tbody>
            <tr class="c25">
                <td class="c29" colspan="2" rowspan="3">
                    <p class="c24"><span style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 74px; height: 82.00px;"><img alt="" src="https://compras.liceodecolombia.edu.co/images/image1.png" style="height: 82.00px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);" title=""></span></p>
                </td>
                <td class="c2" colspan="6" rowspan="2">
                    <p class="c1"><span class="c0"></span></p>
                </td>
                <td class="c14" colspan="2" rowspan="1">
                    <p class="c10"><span class="c0">Orden No. {{$purchase_order->id}}</span></p>
                </td>
            </tr>
            <tr class="c25">
                <td class="c14" colspan="2" rowspan="1">
                    <p class="c10"><span class="c0">RF {{$purchase_order->quotation->supplierOrder->id}}-{{$purchase_order->id}}</span></p>
                </td>
            </tr>
            <tr class="c25">
                <td class="c2" colspan="6" rowspan="1">
                    <p class="c24"><span class="c33">ORDEN DE COMPRA O DE SERVICIO</span></p>
                </td>
                <td class="c14" colspan="2" rowspan="1">
                    <p class="c1"><span class="c0"></span></p>
                </td>
            </tr>
            <tr class="c25">
                <td class="c22" colspan="10" rowspan="1">
                    <p class="c1"><span class="c0"></span></p>
                    <p class="c1"><span class="c0"></span></p>
                    <p class="c10"><span class="c0">FECHA ORDEN DE COMPRA: <b>{!!$purchase_order->fecha_orden!!}</b></span></p>
                </td>
            </tr>
            <tr class="c20">
                <td class="c2" colspan="6" rowspan="1">
                    <p class="c10"><span class="c0">&Aacute;REA QUE SOLICITA: <b>{!!array_get($purchase_order->quotation->supplierOrder->areaOperativa,'nombre','')!!}</b></span></p>
                </td>
                <td class="c18" colspan="4" rowspan="1">
                    <p class="c10"><span class="c0">SEG&Uacute;N COTIZACI&Oacute;N No. {{$purchase_order->quotation->id}}</span></p>
                </td>
            </tr>
            <tr class="c20">
                <td class="c2" colspan="6" rowspan="1">
                    <p class="c10"><span class="c0">FECHA DE AUTORIZACI&Oacute;N: <b>{!!array_get($purchase_order->quotation->supplierOrder,'fecha_de_autorizacion','')!!}</b></span></p>
                </td>
                <td class="c18" colspan="4" rowspan="1">
                    <p class="c10"><span class="c0">FECHA DE ENTREGA: <b>{!!$purchase_order->fecha_entrega!!}</b></span></p>
                </td>
            </tr>
            <tr class="c20">
                <td class="c2" colspan="6" rowspan="1">
                    <p class="c10"><span class="c0">PROVEEDOR: <b>{!!array_get($purchase_order->quotation->supplier,'informacion_general.razon_social','')!!}</b></span></p>
                </td>
                <td class="c18" colspan="4" rowspan="1">
                    <p class="c10"><span class="c0">NIT: <b>{!!array_get($purchase_order->quotation->supplier,'informacion_general.nit','')!!}</b></span></p>
                </td>
            </tr>
            <tr class="c20">
                <td class="c2" colspan="6" rowspan="1">
                    <p class="c10"><span class="c0">DIRECCI&Oacute;N PROVEEDOR: <b>{!!array_get($purchase_order->quotation->supplier,'informacion_general.direccion','')!!}</b></span></p>
                </td>
                <td class="c18" colspan="4" rowspan="1">
                    <p class="c10"><span class="c0">TEL: <b>{!!array_get($purchase_order->quotation->supplier,'informacion_general.telefono','')!!}</b></span></p>
                </td>
            </tr>
            <tr class="c20">
                <td class="c2" colspan="6" rowspan="1">
                    <p class="c10"><span class="c0">CONTACTO PROVEEDOR: <b>{!!array_get($purchase_order->quotation->supplier,'informacion_general.persona_de_contacto.nombre','')!!}</b></span></p>
                </td>
                <td class="c18" colspan="4" rowspan="1">
                    <p class="c10"><span class="c0">E-MAIL: <b>{!!array_get($purchase_order->quotation->supplier,'informacion_general.persona_de_contacto.email','')!!}</b></span></p>
                </td>
            </tr>
            <tr class="c20">
                <td class="c27" colspan="10" rowspan="1">
                    <p class="c10"><span class="c0">DESCRIPCI&Oacute;N:</span></p>
                </td>
            </tr>
            <tr class="c20">
                <td class="c3" colspan="1" rowspan="1">
                    <p class="c24"><span class="c7">Cantidad</span></p>
                </td>
                <td class="c6" colspan="4" rowspan="1">
                    <p class="c24"><span class="c7">Detalle</span></p>
                </td>
                <td class="c3" colspan="1" rowspan="1">
                    <p class="c24"><span class="c7">V/r Unitario</span></p>
                </td>
                <td class="c15" colspan="1" rowspan="1">
                    <p class="c24"><span class="c7">% Dto</span></p>
                </td>
                <td class="c15" colspan="1" rowspan="1">
                    <p class="c24"><span class="c7">% Iva</span></p>
                </td>
                <td class="c17" colspan="1" rowspan="1">
                    <p class="c24"><span class="c7">Valor Neto</span></p>
                </td>
                <td class="c17" colspan="1" rowspan="1">
                    <p class="c24"><span class="c7">Valor Total</span></p>
                </td>
            </tr>
            @foreach($purchase_order->items as $item)
            @php
            // dump($item);
            // die();
            @endphp
            <tr class="c20">
                <td class="c3" colspan="1" rowspan="1">
                    <p class="c12"><span class="c0"><b>{!!$item['cantidad']!!}</b></span></p>
                </td>
                <td class="c6" colspan="4" rowspan="1">
                    <p class="c12"><span class="c0"><b>{!!$item['descripcion_provedor']!!}</b></span></p>
                </td>
                <td class="c3" colspan="1" rowspan="1">
                    <p class="c1"><span class="c0"><b>{!!number_format($item['valor_unitario'],2)!!}</b></span></p>
                </td>
                <td class="c15" colspan="1" rowspan="1">
                    <p class="c13"><span class="c0"><b>{!!$item['descuento_tarifa']!!}</b></span></p>
                </td>
                <td class="c15" colspan="1" rowspan="1">
                    <p class="c13"><span class="c0"><b>{!!$item['iva_tarifa']!!}</b></span></p>
                </td>
                <td class="c17" colspan="1" rowspan="1">
                    <p class="c13"><span class="c0"><b>{!!number_format($purchase_order->calcularNeto($item),2)!!}</b></span></p>
                </td>
                <td class="c17" colspan="1" rowspan="1">
                    <p class="c13"><span class="c0"><b>{!!number_format($purchase_order->calcularTotalItem($item),2)!!}</b></span></p>
                </td>
            </tr>
            @endforeach
            
            <tr class="c20">
                <td class="c34" colspan="6" rowspan="3">
                    <p class="c1"><span class="c0"></span></p>
                </td>
                <td class="c15" colspan="2" rowspan="1">
                    <p class="c10"><span class="c7">Subtotal</span></p>
                </td>
                <td class="c17" colspan="2" rowspan="1">
                    <p class="c1"><span class="c0"><b>{{number_format($purchase_order->calcularSubtotal(),2)}}</b></span></p>
                </td>
            </tr>
            <tr class="c20">
                <td class="c15" colspan="2" rowspan="1">
                    <p class="c10"><span class="c7">Iva</span></p>
                </td>
                <td class="c17" colspan="2" rowspan="1">
                    <p class="c1"><span class="c0"><b>{{number_format($purchase_order->calcularIva(),2)}}</b></span></p>
                </td>
            </tr>
            <tr class="c20">
                <td class="c15" colspan="2" rowspan="1">
                    <p class="c10"><span class="c7">Total</span></p>
                </td>
                <td class="c17" colspan="2" rowspan="1">
                    <p class="c1"><span class="c0"><b>{{number_format($purchase_order->calcularTotal(),2)}}</b></span></p>
                </td>
            </tr>
            <tr class="c20">
                <td class="c19" colspan="10" rowspan="2">
                    <p class="c1"><span class="c0"></span></p>
                    <p class="c1"><span class="c0"></span></p>
                    <p class="c10"><span class="c0">VALOR TOTAL (n&uacute;meros y letras):</span></p>
                    @php
                    $letras= NumeroALetras::convertir($purchase_order->calcularTotal(), 'pesos', 'centavos M/cte');
                    @endphp
                    <p class="c1" style="margin-top:15px;"><span class="c0">{{$letras}}</span></p>
                    <hr>
                    <p class="c1"><span class="c0"></span></p>
                    <p class="c1"><span class="c0"></span></p>
                    <p class="c10"><span class="c31">Observaciones:</span><span class="c0">&nbsp;{!!$purchase_order->observaciones!!}</span></p>
                </td>
            </tr>
            <tr class="c20"></tr>
            <tr class="c20">
                <td class="c8" colspan="4" rowspan="1">
                    <p class="c1"><span class="c0"></span></p>
                    <p class="c1"><span class="c0"></span></p>
                    <p class="c1"><span class="c0"></span></p>
                    <p class="c1"><span class="c0"></span></p>
                    <p class="c1"><span class="c0"></span></p>
                    <p class="c1"><span class="c0"></span></p>
                    <hr>
                    <p class="c1"><span class="c0"></span></p>
                    <p class="c24"><span class="c0">SOLICITADO POR</span></p>
                </td>
                <td class="c4" colspan="1" rowspan="1">
                    <p class="c1"><span class="c0"></span></p>
                </td>
                <td class="c4" colspan="1" rowspan="1">
                    <p class="c1"><span class="c0"></span></p>
                </td>
                <td class="c18" colspan="4" rowspan="1">
                    <p class="c1"><span class="c0"></span></p>
                    <p class="c1"><span class="c0"></span></p>
                    <p class="c1"><span class="c0"></span></p>
                    <p class="c1"><span class="c0"></span></p>
                    <p class="c1"><span class="c0"></span></p>
                    <p class="c1"><span class="c0"></span></p>
                    <hr>
                    <p class="c1"><span class="c0"></span></p>
                    <p class="c24"><span class="c0">AUTORIZADO POR</span></p>
                </td>
            </tr>
            <tr class="c20">
                <td class="c8" colspan="4" rowspan="1">
                    <p class="c10"><span class="c0">Nombre: {{ title_case( array_get($purchase_order->quotation->supplierOrder->tramitante, 'name') )}}</span></p>
                    <p class="c10"><span class="c0">Cargo: {{ title_case( array_get($purchase_order->quotation->supplierOrder->tramitante, 'cargo') )}} </span></p>
                    <p class="c10"><span class="c0">Tel&eacute;fono: 6760585</span></p>
                    <p class="c10"><span class="c0">Correo electr&oacute;nico: {{ array_get($purchase_order->quotation->supplierOrder->tramitante, 'email') }}</span></p>
                </td>
                <td class="c4" colspan="1" rowspan="1">
                    <p class="c1"><span class="c0"></span></p>
                </td>
                <td class="c4" colspan="1" rowspan="1">
                    <p class="c1"><span class="c0"></span></p>
                </td>
                <td class="c18" colspan="4" rowspan="1">
                    @php
                        $autorizante = $purchase_order->quotation->supplierOrder->autorizante();
                    @endphp

                    <p class="c10"><span class="c0">Nombre: {{ title_case($autorizante->name) }}</span></p>
                    <p class="c10"><span class="c0">Cargo: {{ title_case($autorizante->cargo) }}</span></p>
                </td>
            </tr>
            <tr class="c20">
                <td class="c22" colspan="10" rowspan="1">
                    <p class="c1"><span class="c7"></span></p>
                    <p class="c1"><span class="c7"></span></p>
                    <p class="c10"><span class="c31">Nota:</span><span class="c0">&nbsp;Elaborar Factura de Venta y/o cuenta de cobro a  nombre del LICEO DE COLOMBIA, Nit.830081422-2 y entregar el producto o servicio en la Calle 219 50-30.</span></p>
                </td>
            </tr>
            <tr class="c20">
                <td class="c4" colspan="1" rowspan="1">
                    <p class="c1"><span class="c0"></span></p>
                </td>
                <td class="c4" colspan="1" rowspan="1">
                    <p class="c1"><span class="c0"></span></p>
                </td>
                <td class="c4" colspan="1" rowspan="1">
                    <p class="c1"><span class="c0"></span></p>
                </td>
                <td class="c4" colspan="1" rowspan="1">
                    <p class="c1"><span class="c0"></span></p>
                </td>
                <td class="c4" colspan="1" rowspan="1">
                    <p class="c1"><span class="c0"></span></p>
                </td>
                <td class="c4" colspan="1" rowspan="1">
                    <p class="c1"><span class="c0"></span></p>
                </td>
                <td class="c4" colspan="1" rowspan="1">
                    <p class="c1"><span class="c0"></span></p>
                </td>
                <td class="c4" colspan="1" rowspan="1">
                    <p class="c1"><span class="c0"></span></p>
                </td>
                <td class="c14" colspan="2" rowspan="1">
                    <p class="c30"><span class="c0">FT-FIN-025-V3</span></p>
                </td>
            </tr>
        </tbody>
    </table>
</body>

</html>