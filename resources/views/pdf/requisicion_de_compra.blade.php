<html>

<head>
    <meta content="text/html; charset=UTF-8" http-equiv="content-type">
    <style type="text/css">
        ol {
            margin: 0;
            padding: 0
        }
        
        table td,
        table th {
            padding: 0
        }
        
        .c26 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 1pt;
            border-right-width: 0pt;
            border-left-color: #000000;
            vertical-align: top;
            border-right-color: #000000;
            border-left-width: 0pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 1pt;
            width: 60%;
            border-top-color: #000000;
            border-bottom-style: solid
        }
        
        .c15 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 0pt;
            border-right-width: 0pt;
            border-left-color: #000000;
            vertical-align: top;
            border-right-color: #000000;
            border-left-width: 0pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 1pt;
            /*width: 50%;*/
            border-top-color: #000000;
            border-bottom-style: solid
        }
        
        .c23 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 0pt;
            border-right-width: 0pt;
            border-left-color: #000000;
            vertical-align: top;
            border-right-color: #000000;
            border-left-width: 0pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 0pt;
            width: 100%;
            border-top-color: #000000;
            border-bottom-style: solid
        }
        
        .c9 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 1pt;
            border-right-width: 0pt;
            border-left-color: #000000;
            vertical-align: top;
            border-right-color: #000000;
            border-left-width: 0pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 0pt;
            width: 100%;
            border-top-color: #000000;
            border-bottom-style: solid
        }
        
        .c13 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 1pt;
            border-right-width: 1pt;
            border-left-color: #000000;
            vertical-align: top;
            border-right-color: #000000;
            border-left-width: 1pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 1pt;
            width: 20%;
            border-top-color: #000000;
            border-bottom-style: solid
        }
        
        .c19 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 1pt;
            border-right-width: 1pt;
            border-left-color: #000000;
            vertical-align: top;
            border-right-color: #000000;
            border-left-width: 1pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 1pt;
            width: 100%;
            border-top-color: #000000;
            border-bottom-style: solid
        }
        
        .c24 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 1pt;
            border-right-width: 0pt;
            border-left-color: #000000;
            vertical-align: top;
            border-right-color: #000000;
            border-left-width: 0pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 1pt;
            width: 20%;
            border-top-color: #000000;
            border-bottom-style: solid
        }
        
        .c0 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 1pt;
            border-right-width: 1pt;
            border-left-color: #000000;
            vertical-align: top;
            border-right-color: #000000;
            border-left-width: 1pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 1pt;
            width: 10%;
            border-top-color: #000000;
            border-bottom-style: solid
        }
        
        .c11 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 1pt;
            border-right-width: 1pt;
            border-left-color: #000000;
            vertical-align: top;
            border-right-color: #000000;
            border-left-width: 1pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 1pt;
            width: 60%;
            border-top-color: #000000;
            border-bottom-style: solid
        }
        
        .c17 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 1pt;
            border-right-width: 0pt;
            border-left-color: #000000;
            vertical-align: top;
            border-right-color: #000000;
            border-left-width: 0pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 1pt;
            width: 10%;
            border-top-color: #000000;
            border-bottom-style: solid
        }
        
        .c1 {
            color: #000000;
            font-weight: 400;
            text-decoration: none;
            vertical-align: baseline;
            font-size: 10pt;
            font-family: "Arial";
            font-style: normal
        }
        
        .c21 {
            color: #000000;
            font-weight: 400;
            text-decoration: none;
            vertical-align: baseline;
            font-size: 18pt;
            font-family: "Arial";
            font-style: normal
        }
        
        .c16 {
            color: #ffffff;
            font-weight: 700;
            text-decoration: none;
            vertical-align: baseline;
            font-size: 10pt;
            font-family: "Arial";
            font-style: normal
        }
        
        .c20 {
            padding-top: 0pt;
            padding-bottom: 0pt;
            line-height: 1.15;
            orphans: 2;
            widows: 2;
            text-align: left;
            height: 11pt
        }
        
        .c2 {
            padding-top: 0pt;
            padding-bottom: 0pt;
            line-height: 1.0;
            text-align: left;
            height: 11pt
        }
        
        .c4 {
            padding-top: 0pt;
            padding-bottom: 0pt;
            line-height: 1.0;
            text-align: center;
            height: auto;
        }
        
        .c7 {
            padding-top: 0pt;
            padding-bottom: 0pt;
            line-height: 1.0;
            text-align: left
        }
        
        .c25 {
            padding-top: 0pt;
            padding-bottom: 0pt;
            line-height: 1.0;
            text-align: right
        }
        
        .c8 {
            border-spacing: 0;
            border-collapse: collapse;
            margin-right: auto
            width: 100%;
        }
        
        .c14 {
            padding-top: 0pt;
            padding-bottom: 0pt;
            line-height: 1.0;
            text-align: center
        }
        
        .c22 {
            width: 100%;
            padding: 0;
            margin: 0;
        }
        
        .c27 {
            font-size: 10pt;
            font-weight: 700
        }
        
        .c5 {
            height: 21pt
        }
        
        .c12 {
            background-color: #f79646
        }
        
        .c18 {
            background-color: #ffffff
        }
        
        .title {
            padding-top: 0pt;
            color: #000000;
            font-size: 26pt;
            padding-bottom: 3pt;
            font-family: "Arial";
            line-height: 1.15;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }
        
        .subtitle {
            padding-top: 0pt;
            color: #666666;
            font-size: 15pt;
            padding-bottom: 16pt;
            font-family: "Arial";
            line-height: 1.15;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }
        
        li {
            color: #000000;
            font-size: 11pt;
            font-family: "Arial"
        }
        
        p {
            margin: 0;
            color: #000000;
            font-size: 11pt;
            font-family: "Arial"
        }
        
        h1 {
            padding-top: 20pt;
            color: #000000;
            font-size: 20pt;
            padding-bottom: 6pt;
            font-family: "Arial";
            line-height: 1.15;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }
        
        h2 {
            padding-top: 18pt;
            color: #000000;
            font-size: 16pt;
            padding-bottom: 6pt;
            font-family: "Arial";
            line-height: 1.15;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }
        
        h3 {
            padding-top: 16pt;
            color: #434343;
            font-size: 14pt;
            padding-bottom: 4pt;
            font-family: "Arial";
            line-height: 1.15;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }
        
        h4 {
            padding-top: 14pt;
            color: #666666;
            font-size: 12pt;
            padding-bottom: 4pt;
            font-family: "Arial";
            line-height: 1.15;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }
        
        h5 {
            padding-top: 12pt;
            color: #666666;
            font-size: 11pt;
            padding-bottom: 4pt;
            font-family: "Arial";
            line-height: 1.15;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }
        
        h6 {
            padding-top: 12pt;
            color: #666666;
            font-size: 11pt;
            padding-bottom: 4pt;
            font-family: "Arial";
            line-height: 1.15;
            page-break-after: avoid;
            font-style: italic;
            orphans: 2;
            widows: 2;
            text-align: left
        }
    </style>
</head>

<body class="c18 c22">
    <table class="c8" width="100%">
        <tbody>
            <tr class="c5">
                <td class="c23" colspan="10" rowspan="1">
                    <p class="c14"><span class="c21">Formato Requisici&oacute;n Interna</span></p>
                </td>
            </tr>
            <tr class="c5">
                <td class="c15" colspan="5" rowspan="1">
                    <p class="c7"><span class="c1"><b>Fecha de Solicitud:</b> {{$supplier_order->created_at->format('Y-m-d')}}</span></p>
                    <p class="c7"><span class="c1"><b>&Aacute;rea que solicita:</b> {{$supplier_order->areaOperativa->nombre}}</span></p>
                    <p class="c7"><span class="c1"><b>Persona que realiza la requisici&oacute;n:</b> {{$supplier_order->tramitante->name}} </span></p>
                    <p class="c7"><span class="c1"><b>Fecha requerida de entrega:</b> {{$supplier_order->fecha_de_entrega}}</span></p>
                    <p class="c7"><span class="c1"><b>Proveedor recomendado:</b>  {{ array_get($supplier_order->proveedor_recomendado,'nombre','')}}</span></p>
                    <p class="c7"><span class="c1"><b>Persona Contacto:</b> {{ array_get($supplier_order->proveedor_recomendado,'contacto.nombre','')}}</span></p>
                </td>
                <td class="c15" colspan="5" rowspan="1">
                    <p class="c2"><span class="c1"></span></p>
                    <p class="c2"><span class="c1"></span></p>
                    <p class="c7"><span class="c1"><b>Persona que solicita la compra:</b> {{$supplier_order->solicitante->name}}</span></p>
                    <p class="c2"><span class="c1"></span></p>
                    <p class="c2"><span class="c1"></span></p>
                    <p class="c7"><span class="c1 c18"><b>Tel&eacute;fono de contacto:</b> {{ array_get($supplier_order->proveedor_recomendado,'contacto.telefono','')}}</span></p>
                    <p class="c2"><span class="c1 c18"></span></p>
                </td>
            </tr>
            <tr class="c5">
                <td class="c0 c12" colspan="1" rowspan="1">
                    <p class="c14"><span class="c16">Unidad de Medida</span></p>
                </td>
                <td class="c0 c12" colspan="1" rowspan="1">
                    <p class="c14"><span class="c16">Cantidad</span></p>
                </td>
                <td class="c11 c12" colspan="6" rowspan="1">
                    <p class="c14"><span class="c16">Descripci&oacute;n detallada del Producto o Servicio</span></p>
                </td>
                <td class="c13 c12" colspan="2" rowspan="1">
                    <p class="c14"><span class="c16">Especificaciones T&eacute;cnicas</span></p>
                </td>
            </tr>
            @foreach($supplier_order->requisicion_de_compra?:[] as $detalle)
            @php
                $unidad= array_get($detalle,'unidad','');
                $cantidad= array_get($detalle,'cantidad','');
                $descripcion= nl2br(array_get($detalle,'descripcion',''));
                $especificaciones_tecnicas= nl2br(array_get($detalle,'especificaciones_tecnicas',''));
            @endphp
            <tr class="c5">
                <td class="c0" colspan="1" rowspan="1">
                    <p class="c4"><span class="c1"></span>{{$unidad?$unidades[$unidad]:''}}</p>
                </td>
                <td class="c0" colspan="1" rowspan="1">
                    <p class="c4"><span class="c1"></span>{{$cantidad}}</p>
                </td>
                <td class="c11" colspan="6" rowspan="1">
                    <p class="c4"><span class="c1"></span>{!!$descripcion!!}</p>
                </td>
                <td class="c13" colspan="2" rowspan="1">
                    <p class="c4"><span class="c1"></span>{!!$especificaciones_tecnicas!!}</p>
                </td>
            </tr>
            @endforeach
            
            <tr class="c5">
                <td class="c17" colspan="1" rowspan="1">
                    <p class="c4"><span class="c1"></span></p>
                </td>
                <td class="c17" colspan="1" rowspan="1">
                    <p class="c4"><span class="c1"></span></p>
                </td>
                <td class="c26" colspan="6" rowspan="1">
                    <p class="c4"><span class="c1"></span></p>
                </td>
                <td class="c24" colspan="2" rowspan="1">
                    <p class="c4"><span class="c1"></span></p>
                </td>
            </tr>
            <tr class="c5">
                <td class="c19" colspan="10" rowspan="1">
                    <p class="c7"><span class="c27">Observaciones: </span><span class="c1">{{array_get($supplier_order,'observaciones.requisition','')}}</span></p>
                    <p class="c2"><span class="c1"></span></p>
                    <p class="c2"><span class="c1"></span></p>
                    <p class="c2"><span class="c1"></span></p>
                    <p class="c2"><span class="c1"></span></p>
                </td>
            </tr>
            <tr class="c5">
                <td class="c9" colspan="10" rowspan="1">
                    <p class="c25"><span class="c1">FT-FIN-040-V1</span></p>
                </td>
            </tr>
        </tbody>
    </table>
</body>

</html>