@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Matriz Comparativa
@endsection

@section('contentheader_title')
    Matriz Comparativa
@endsection

@section('contentheader_description')
    
@endsection

@section('main-content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary container-sortable">
                <div class="box-header with-border">
                    <h3 class="box-title">Matriz Comparativa Evaluación y Selección de Proveedores</h3>
                </div>
                <div class="box-body">
                    <table class="table table-condensed table-bordered">
                        <thead>
                            <tr>
                                <th>Items a evaluar</th>
                                @foreach($cotizaciones?:[] as $cotizacion)
                                <th width="{!!70/(count($cotizaciones))!!}%">Proveedor {!!$loop->iteration!!}</th>
                                @endforeach
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Razon social del proveedor</td>
                                @foreach($cotizaciones?:[] as $cotizacion)
                                <td>{!!array_get($cotizacion->supplier,'informacion_general.razon_social','Proveedor sin nombre')!!}</td>
                                @endforeach                                            
                            </tr>
                            <tr>
                                <td>Cotización N°</td>
                                @foreach($cotizaciones?:[] as $cotizacion)
                                <td>{!!$cotizacion->id!!}</td>
                                @endforeach                                            
                            </tr>
                            @foreach($supplier_order->requisicion_de_compra?:[] as $key=> $item)
                            <tr>
                                <td><b>{{'Destalle '.$item['id']}} :</b> {{$item['descripcion']}}</td>                                            
                                @foreach($cotizaciones?:[] as $cotizacion)
                                <td class="text-left">{!!array_get($cotizacion->contenido,'contenido.'.$key.'.descripcion','')!!}</td>
                                @endforeach
                            </tr>
                            <tr>
                                <td>Valor unitario</td>
                                @foreach($cotizaciones?:[] as $cotizacion)
                                <td class="text-right">{!!array_get($cotizacion->contenido,'contenido.'.$key.'.status',null)=='true'?number_format(array_get($cotizacion->contenido,'contenido.'.$key.'.valor_unitario','0'),2):'No Cotizado'!!}</td>
                                @endforeach                                            
                            </tr>
                            <tr>
                                <td>% Iva</td>
                                @foreach($cotizaciones?:[] as $cotizacion)
                                <td class="text-right">{!!array_get($cotizacion->contenido,'contenido.'.$key.'.status',null)=='true'?array_get($cotizacion->contenido,'contenido.'.$key.'.iva_tarifa',''):'No Cotizado'!!}</td>
                                @endforeach                                            
                            </tr>
                            <tr>
                                <td>% Dto</td>
                                @foreach($cotizaciones?:[] as $cotizacion)
                                <td class="text-right">{!!array_get($cotizacion->contenido,'contenido.'.$key.'.status',null)=='true'?array_get($cotizacion->contenido,'contenido.'.$key.'.descuento_tarifa',''):'No Cotizado'!!}</td>
                                @endforeach                                            
                            </tr>
                            <tr>
                                <td>Valor Real Unitario</td>
                                @foreach($cotizaciones?:[] as $cotizacion)
                                <td class="text-right">{!!array_get($cotizacion->contenido,'contenido.'.$key.'.status',null)=='true'?number_format($cotizacion->calcularValor($key),2):'No Cotizado'!!}</td>
                                @endforeach                                            
                            </tr>
                            <tr>
                                <td>Cantidad Cotizada</td>
                                @foreach($cotizaciones?:[] as $cotizacion)
                                <td class="text-right">{!!array_get($cotizacion->contenido,'contenido.'.$key.'.status',null)=='true'?array_get($cotizacion->contenido,'contenido.'.$key.'.cantidad',0):'0'!!}/[{!!array_get($item, 'cantidad','')!!}]</td>
                                @endforeach                                            
                            </tr>
                            <tr>
                                <td>Total Valor Real * Cantidad Cotizada</td>
                                @foreach($cotizaciones?:[] as $cotizacion)
                                <td class="text-right">{!!array_get($cotizacion->contenido,'contenido.'.$key.'.status',null)=='true'?number_format($cotizacion->calcularValorCantidad($key),2):'No Cotizado'!!}</td>
                                @endforeach                                            
                            </tr>
                                
                            @endforeach
                            <tr>
                                <td><b>Total General</b></td>
                                @foreach($cotizaciones?:[] as $cotizacion)
                                <td class="text-right"><b>{!!number_format($cotizacion->calcularTotal(),2)!!}</b></td>
                                @endforeach                                            
                            </tr>
                            <tr>
                                <td><b>Dto General</b></td>
                                @foreach($cotizaciones?:[] as $cotizacion)
                                <td class="text-right"><b>{!!array_get($cotizacion->contenido,'descuento_general','0')!!}</b></td>
                                @endforeach                                            
                            </tr>
                            <tr>
                                <td>Descuentos Otorgados</td>
                                @foreach($cotizaciones?:[] as $cotizacion)
                                <td>{!!array_get($cotizacion->contenido,'criterios.descuento','')!!}</td>
                                @endforeach                                            
                            </tr>
                            <tr>
                                <td>Forma de pago</td>
                                @foreach($cotizaciones?:[] as $cotizacion)
                                @php
                                $metodo_de_pago= array_get($cotizacion->contenido,'criterios.forma_de_pago','');
                                @endphp
                                <td>{!!array_get($formas_de_pago,$metodo_de_pago,'')!!}</td>
                                @endforeach                                            
                            </tr>
                            <tr>
                                <td>Garantías</td>
                                @foreach($cotizaciones?:[] as $cotizacion)
                                <td>{!!array_get($cotizacion->contenido,'criterios.garantia','')!!}</td>
                                @endforeach                                            
                            </tr>
                            <tr>
                                <td>Disponibilidad del producto o servicio</td>
                                @foreach($cotizaciones?:[] as $cotizacion)
                                <td>{!!array_get($cotizacion->contenido,'criterios.tiempo_de_entrega','')!!}</td>
                                @endforeach                                            
                            </tr>
                            <tr>
                                <td>Tiempo de Experiencia</td>
                                @foreach($cotizaciones?:[] as $cotizacion)
                                @php
                                $experiencia= array_get($cotizacion->contenido,'criterios.experiencia','');
                                @endphp
                                <td>{!!array_get($tiempos_de_experiencia,$experiencia,'')!!}</td>
                                @endforeach                                            
                            </tr>
                            <tr>
                                <td>Sistema de Gestión de Calidad</td>
                                @foreach($cotizaciones?:[] as $cotizacion)
                                <td>{!!array_get($cotizacion->contenido,'criterios.certificado_de_gestion','')!!}</td>
                                @endforeach                                            
                            </tr>
                            {{-- <tr>
                                <td>Anexó documentos iniciales requeridos</td>
                                @foreach($cotizaciones?:[] as $cotizacion)
                                <td>{!!array_get($cotizacion->contenido,'criterios.descuento','')!!}</td>
                                @endforeach                                            
                            </tr> --}}
                            <tr>
                                <td>Observaciones del proveedor</td>
                                @foreach($cotizaciones?:[] as $cotizacion)
                                <td>{!!array_get($cotizacion->contenido,'observaciones','')!!}</td>
                                @endforeach                                            
                            </tr>
                            <tr>
                                <td>Documento Anexo</td>
                                @foreach($cotizaciones?:[] as $cotizacion)
                                    @php
                                        $file = array_get($cotizacion->contenido,'documento_anexo','')
                                    @endphp
                                    @if($file)
                                    <td><a role="button" class="btn btn-xs btn-primary" target="_blank" href="{!!url($file)!!}">ver</a></td>
                                    @else
                                    <td></td>
                                    @endif
                                @endforeach                                            
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    
        <div class="col-md-12">
            <div class="pull-right">FT-FIN-042-V1</div>
        </div>            
    </div>
@endsection

@section('local-css')
    <style type="text/css">

        .container-sortable{
            background: transparent;
            border: 1px solid #3c8dbc;
            border-top: 3px solid #3c8dbc;
        }

        .table-bordered {
            border: 1px solid #337ab7;
        }

        .table-bordered>tbody>tr>td, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>td, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>thead>tr>th {
            border: 1px solid #337ab7;
        }

        .table-hover>tbody>tr:hover {
            background-color: white;
        }

        @media print {
            table td.highlighted {
                background-color: rgba(247, 202, 24, 0.3) !important;
            }
        }

    </style>
@endsection
