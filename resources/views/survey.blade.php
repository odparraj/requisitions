@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Encuesta de satisfacción
@endsection

@section('contentheader_title')
    Encuesta de satisfacción
@endsection

@section('contentheader_description')
    
@endsection

@section('main-content')
    @forelse($purchaseOrders as $purchaseOrder)
    <div class="row">                    
        <div class="col-md-12">
            <h3>Calificación del proveedor: {{$purchaseOrder->quotation->supplier->informacion_general['razon_social']}}</h3>
            <table class="table table-hover table-bordered">
                <thead>
                    <tr>
                        <th class="text-left" width="80%">Pregunta</th>
                        <th class="text-center" width="20%">Calificación</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($purchaseOrder->respuestas as $pregunta)
                    <tr>
                        <td>
                            <h4>{{$pregunta->enunciado}}</h4>
                        </td>
                        <td class="text-center">
                            {{$pregunta->pivot->score}}
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="col-md-12">                           
            <p><b>Observaciones:</b> {{$purchaseOrder->observaciones_encuesta?:'Ninguna'}}</p>
            <br>
        </div>
    </div>
    @empty
    <h3>La Encuesta de satisfacción aun no ha sido diligenciada.</h3>
    @endforelse
@endsection

@section('local-css')
    <style type="text/css">

        .container-sortable{
            background: transparent;
            border: 1px solid #3c8dbc;
            border-top: 3px solid #3c8dbc;
        }

        .table-bordered {
            border: 1px solid #337ab7;
        }

        .table-bordered>tbody>tr>td, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>td, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>thead>tr>th {
            border: 1px solid #337ab7;
        }

        .table-hover>tbody>tr:hover {
            background-color: white;
        }

        @media print {
            table td.highlighted {
                background-color: rgba(247, 202, 24, 0.3) !important;
            }
        }

    </style>
@endsection