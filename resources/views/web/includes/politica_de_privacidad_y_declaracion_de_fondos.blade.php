<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Política de Privacidad</h3>
    </div>
    <div class="box-body">
        <div class="row">
            <div class="col-md-12">
                <p class="text-justify">
                    Con el envío y diligenciamiento de este formulario autorizo de manera libre, previa, expresa, voluntaria y debidamente informada, a LICEO DE COLOMBIA BILINGÜE a recaudar, almacenar, usar, circular, suprimir, procesar, compilar, intercambiar, dar tratamiento, actualizar y disponer en cualquier forma los datos que han sido suministrados, con la finalidad de: 
                    (i) realizar las actividades pertinentes a la relación comercial entre las empresas y 
                    (ii) las demás finalidades estrechamente asociadas y necesarias de carácter institucional. Esta información será utilizada, de conformidad con las políticas de tratamiento de datos personales de Liceo de Colombia Bilingüe que puede consultar a través de nuestra página web <a href="https://www.liceodecolombia.edu.co/politica-de-tratamiento-de-datos-personales/" target="_blank">www.liceodecolombia.edu.co/politica-de-tratamiento-de-datos-personales/</a>
                </p>
            </div>
        </div>
    </div>
</div>
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Declaración de origen de fondos</h3>
    </div>
    <div class="box-body">
        <div class="row">
            <div class="col-md-12">
                <p class="text-justify">
                    Con el envío y diligenciamiento de este formulario declaro expresamente que:
                </p>
                <p class="text-justify">
                    Mi actividad, profesión u oficio es lícita, se ejerce dentro del marco legal y los recursos que poseo no provienen de actividades ilícitas de las
                    contempladas en el Código Penal Colombiano. La información suministrada en este documento es veraz y verificable y me comprometo a actualizarla anualmente, el incumplimiento
                    de esta obligación faculta al Liceo de Colombia, para revocar y/o rescindir unilateralmente el contrato. Los recursos que se deriven de esta relación contractual no se destinarán a
                    delitos fuente relacionados con Lavado de Activos y la Financiación del Terrorismo - LA/FT, grupos terroristas, corrupcion, opacidad, fraude o actividades terroristas.
                    Manifiesto que no he sido declarado responsable judicialmente por la comisión de delitos contra la Administración Pública cuya pena sea privativa de la libertad o que afecten el
                    patrimonio del Estado o por delitos fuente relacionados con LA/FT, o la pertenencia, promoción o financiación de grupos ilegales, delitos de lesa humanidad, narcotráfico, fraude,
                    opacidad o corrupción.
                </p>

            </div>
        </div>
    </div>
</div>