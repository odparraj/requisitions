@extends('web.layouts.index')

@section('htmlheader_title')
	Proveedor
@endsection

@section('main-content')
    <div class="container my-15">
        <h1 class="text-center mb-5">Registro de proveedor</h1>
        <div class="row">
            <div class="col-md-12">
                @include('includes.session-alert')
            </div>
        </div>
    </div>
@endsection

@section('local-css')
	<style type="text/css">
        body{
            background-color: #ecf0f5;
        }
        .my-15{
            margin-top: 15vh; 
            margin-bottom: 15vh;
        }
        .mb-5{
            margin-bottom: 5vh;
        }
	</style>
@endsection