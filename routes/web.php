<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('register', function(){
	return redirect('/login');
});
Route::post('register', function(){
	return redirect('/login');
});

Route::get('/', function () {
    return redirect('/login');
});

Route::get('files/{file}','WebController@file');

Route::group(['middleware' => 'auth'], function () {
    //    Route::get('/link1', function ()    {
//        // Uses Auth Middleware
//    });

    //Please do not remove this if you want adminlte:route and adminlte:link commands to works correctly.
    #adminlte_routes
});


Route::middleware(['auth'])->prefix('admin')->group(function(){
	
	Route::get('sections/only-create','Admin\SectionController@onlyCreate')->name('sections.only-create');
	Route::post('sections/only-store','Admin\SectionController@onlyStore')->name('sections.only-store');
	Route::get('sections/dt','Admin\SectionController@toDatatable')->name('sections.dt');
	Route::resource('sections', 'Admin\SectionController');

    Route::get('users/dt','Admin\UserController@toDatatable')->name('users.dt');
    Route::resource('users', 'Admin\UserController');
    
    Route::get('permissions/dt','Admin\PermissionController@toDatatable')->name('permissions.dt');
    Route::resource('permissions', 'Admin\PermissionController');
    
    Route::get('roles/dt','Admin\RoleController@toDatatable')->name('roles.dt');
    Route::resource('roles', 'Admin\RoleController');

    Route::get('administrative_areas/dt','Admin\AdministrativeAreaController@toDatatable')->name('administrative_areas.dt');
	Route::resource('administrative_areas', 'Admin\AdministrativeAreaController');
	
	Route::get('operative_areas/dt','Admin\OperativeAreaController@toDatatable')->name('operative_areas.dt');
	Route::resource('operative_areas', 'Admin\OperativeAreaController');
	
	Route::get('payment_methods/dt','Admin\PaymentMethodController@toDatatable')->name('payment_methods.dt');
	Route::resource('payment_methods', 'Admin\PaymentMethodController');

	Route::get('experience_ranges/dt','Admin\ExperienceRangeController@toDatatable')->name('experience_ranges.dt');
	Route::resource('experience_ranges', 'Admin\ExperienceRangeController');
	
	Route::get('suppliers/dt','Admin\SupplierController@toDatatable')->name('suppliers.dt');
	Route::get('suppliers/invitation','Admin\SupplierController@formInvitation')->name('suppliers.form_invitation');
	Route::post('suppliers/invitation','Admin\SupplierController@sendInvitation')->name('suppliers.send_invitation');
	Route::get('suppliers/{supplier}/products','Admin\SupplierController@formProducts')->name('supplier.edit_products');
	Route::post('suppliers/{supplier}/products','Admin\SupplierController@updateProducts')->name('supplier.update_products');
	Route::resource('suppliers', 'Admin\SupplierController');

	/*RUTAS PARA LA ORDENES DE PROVEEDORES*/
	Route::get('supplier_orders/{supplier_order}/ordenes_json','Admin\SupplierOrderController@orderJson')->name('supplier_orders.orders_json');
	Route::get('supplier_orders/{supplier_order}/ordenes','Admin\SupplierOrderController@listOrder')->name('supplier_orders.orders_index');

	Route::get('supplier_orders/create_order/{supplier_order}','Admin\SupplierOrderController@formCreateOrder')->name('supplier_orders.form_order');
	Route::put('supplier_orders/create_order/{supplier_order}','Admin\SupplierOrderController@storeOrder')->name('supplier_orders.store_order');
	
	Route::get('supplier_orders/selection_matrix/{supplier_order}','Admin\SupplierOrderController@selectionMatrix')->name('supplier_orders.selection_matrix');
	Route::get('supplier_orders/observaciones/{supplier_order}','Admin\SupplierOrderController@observaciones')->name('supplier_orders.observaciones');
	Route::get('supplier_orders/cotizaciones/{supplier_order}','Admin\SupplierOrderController@cotizaciones')->name('supplier_orders.cotizaciones');

	Route::get('supplier_orders/requisition/dt','Admin\SupplierOrderController@toDatatableRequisition')->name('supplier_orders.dt_requisition');
	Route::get('supplier_orders/requisition','Admin\SupplierOrderController@indexRequisition')->name('supplier_orders.index_requisition');
	Route::get('supplier_orders/requisition/create','Admin\SupplierOrderController@createRequisition')->name('supplier_orders.create_requisition');
	Route::post('supplier_orders/requisition','Admin\SupplierOrderController@storeRequisition')->name('supplier_orders.store_requisition');

	Route::get('supplier_orders/requisition/pdf/{supplier_order}','Admin\SupplierOrderController@pdfRequisition')->name('supplier_orders.pdf_requisition');

	Route::get('supplier_orders/autorization/dt','Admin\SupplierOrderController@toDatatableAutorization')->name('supplier_orders.dt_autorization');
	Route::get('supplier_orders/autorization','Admin\SupplierOrderController@indexAutorization')->name('supplier_orders.index_autorization');
	Route::get('supplier_orders/autorization/{supplier_order}/edit','Admin\SupplierOrderController@editAutorization')->name('supplier_orders.edit_autorization');
	Route::put('supplier_orders/autorization/{supplier_order}','Admin\SupplierOrderController@updateAutorization')->name('supplier_orders.update_autorization');
	Route::get('supplier_orders/autorization/{supplier_order}/reject','Admin\SupplierOrderController@rejectAutorization')->name('supplier_orders.reject_autorization');
	Route::put('supplier_orders/autorization/{supplier_order}/refuse','Admin\SupplierOrderController@refuseAutorization')->name('supplier_orders.refuse_autorization');
	Route::get('supplier_orders/autorization/{supplier_order}/pause','Admin\SupplierOrderController@pauseAutorization')->name('supplier_orders.pause_autorization');
	Route::put('supplier_orders/autorization/{supplier_order}/stop','Admin\SupplierOrderController@stopAutorization')->name('supplier_orders.stop_autorization');

	Route::get('supplier_orders/quotation/dt','Admin\SupplierOrderController@toDatatableQuotation')->name('supplier_orders.dt_quotation');
	Route::get('supplier_orders/quotation','Admin\SupplierOrderController@indexQuotation')->name('supplier_orders.index_quotation');
	Route::get('supplier_orders/quotation/{supplier_order}/edit','Admin\SupplierOrderController@editQuotation')->name('supplier_orders.edit_quotation');
	Route::put('supplier_orders/quotation/{supplier_order}','Admin\SupplierOrderController@updateQuotation')->name('supplier_orders.update_quotation');

	Route::get('supplier_orders/regenerate/{supplier_order}','Admin\SupplierOrderController@regenerateGet')->name('supplier_orders.regenerate_get');
	Route::put('supplier_orders/regenerate/{supplier_order}','Admin\SupplierOrderController@regeneratePut')->name('supplier_orders.regenerate_put');

	Route::get('supplier_orders/supplier_quote/dt','Admin\SupplierOrderController@toDatatableSupplierQuote')->name('supplier_orders.dt_supplier_quote');
	Route::get('supplier_orders/supplier_quote','Admin\SupplierOrderController@indexSupplierQuote')->name('supplier_orders.index_supplier_quote');
	Route::get('supplier_orders/supplier_quote/{quotation}/edit','Admin\SupplierOrderController@editSupplierQuote')->name('supplier_orders.edit_supplier_quote');
	Route::put('supplier_orders/supplier_quote/{quotation}','Admin\SupplierOrderController@updateSupplierQuote')->name('supplier_orders.update_supplier_quote');

	Route::get('supplier_orders/preselection/dt','Admin\SupplierOrderController@toDatatablePreselection')->name('supplier_orders.dt_preselection');
	Route::get('supplier_orders/preselection','Admin\SupplierOrderController@indexPreselection')->name('supplier_orders.index_preselection');
	Route::get('supplier_orders/preselection/{supplier_order}/edit','Admin\SupplierOrderController@editPreselection')->name('supplier_orders.edit_preselection');
	Route::put('supplier_orders/preselection/{supplier_order}','Admin\SupplierOrderController@updatePreselection')->name('supplier_orders.update_preselection');

	Route::get('supplier_orders/preselection/pdf/{purchase_order}','Admin\SupplierOrderController@pdfPurchaseOrder')->name('supplier_orders.pdf_purchase_order');

	Route::get('supplier_orders/send/dt','Admin\SupplierOrderController@toDatatableSend')->name('supplier_orders.dt_send');
	Route::get('supplier_orders/send','Admin\SupplierOrderController@indexSend')->name('supplier_orders.index_send');
	//Route::get('supplier_orders/send/{supplier_order}/edit','Admin\SupplierOrderController@editSend')->name('supplier_orders.edit_send');
	Route::put('supplier_orders/send/{supplier_order}','Admin\SupplierOrderController@updateSend')->name('supplier_orders.update_send');

	Route::get('supplier_orders/score/dt','Admin\SupplierOrderController@toDatatableScore')->name('supplier_orders.dt_score');
	Route::get('supplier_orders/score','Admin\SupplierOrderController@indexScore')->name('supplier_orders.index_score');
	Route::get('supplier_orders/score/{supplier_order}/edit','Admin\SupplierOrderController@editScore')->name('supplier_orders.edit_score');
	Route::put('supplier_orders/score/{supplier_order}','Admin\SupplierOrderController@updateScore')->name('supplier_orders.update_score');


	Route::get('supplier_orders/dt','Admin\SupplierOrderController@toDatatable')->name('supplier_orders.dt');
	Route::resource('supplier_orders', 'Admin\SupplierOrderController');

	/***************************************/

	Route::get('order_priorities/dt','Admin\OrderPriorityController@toDatatable')->name('order_priorities.dt');
	Route::resource('order_priorities', 'Admin\OrderPriorityController');

	Route::get('measurement_units/dt','Admin\MeasurementUnitController@toDatatable')->name('measurement_units.dt');
	Route::resource('measurement_units', 'Admin\MeasurementUnitController');

	Route::get('quotations/dt','Admin\QuotationController@toDatatable')->name('quotations.dt');
	Route::resource('quotations', 'Admin\QuotationController');

	Route::get('satisfaction_questions/dt','Admin\SatisfactionQuestionController@toDatatable')->name('satisfaction_questions.dt');
	Route::resource('satisfaction_questions', 'Admin\SatisfactionQuestionController');
	
	Route::get('purchase_orders/dt','Admin\PurchaseOrderController@toDatatable')->name('purchase_orders.dt');
	Route::resource('purchase_orders', 'Admin\PurchaseOrderController');

	Route::get('reports/dt','Admin\ReportController@toDatatable')->name('reports.dt');
	Route::resource('reports', 'Admin\ReportController');

	Route::get('products/dt','Admin\ProductController@toDatatable')->name('products.dt');
	Route::resource('products', 'Admin\ProductController');
	
	Route::get('suplier_order/{supplier_order}/survey', 'Admin\SupplierOrderController@showSurvey')->name('supplier_orders.survey');

	Route::get('supplier_orders/to_calification', function(){})->name('supplier_orders.index_send_to_calification');
	Route::put('supplier_orders/to_calification/{supplier_order}','Admin\SupplierOrderController@sendToCalification')->name('supplier_orders.send_to_calification');
});

Route::get('orden/{purchase_order}', 'WebController@getPdf')->name('purchase_orders_pdf');

Route::get('preselection', function(){
	return view('admin.supplier_orders.preselection');
});

Route::get('supplier/create', 'Admin\SupplierController@supplierRegisterView')->name('supplier_external.register');
Route::get('supplier/register', 'Admin\SupplierController@supplierRegisterSuccess')->name('supplier_external.register.success');
Route::post('supplier', 'Admin\SupplierController@supplierRegisterRequest')->name('supplier_external.store');
