<?php

namespace Tests\Feature;

use App\PurchaseOrder;
use App\Quotation;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class PurchaseOrderTest extends TestCase
{
    use RefreshDatabase;

    public function testCreatingPurchaseOrder()
    {
        $cotizacion = factory(Quotation::class)->create();

        $order = PurchaseOrder::create([
            'cotizacion' => $cotizacion->id,
            'fecha_orden' => '2022-05-17',
            'fecha_pedido' => '2022-05-17',
            'fecha_entrega' => '2022-05-17',
            'observaciones_encuesta' => '',
            'observaciones' => '',
            'items' => [],
        ]);

        $this->assertEquals(config('liceo.order_pdf_version'), $order->document_version);
    }
}
